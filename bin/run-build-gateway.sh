#!/bin/bash

if [[ ! -z ${CI} ]]
then
  nx run-many --target=\"build:lib\";
else
  # turbo run turbo:build:gateway --filter=!{./apps/la-source/ape/gateway/build/standalone/apps/la-source/ape/gateway};
  # turbo run turbo:build:gateway;
  concurrently -k -n BUILD,HTTP -s first 'pnpm --filter=next-amplify-gen2 buildGateway' 'pnpm start:client';
fi
