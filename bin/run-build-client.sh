#!/bin/bash

if [[ ! -z ${CI} ]]
then
  nx run-many --target=\"build:lib\";
else
  # nx run-many --target=\"build\" --verbose --no-cloud --exclude=@flexiness/on_board_event_app;
  turbo run build;
  # turbo run build --filter=!next-amplify-gen2{./apps/la-source/ape/gateway/build/standalone/**/*};
  # turbo run build --filter=!next-amplify-gen2;
  # turbo run build --dry-run --filter=!next-amplify-gen2;
fi
