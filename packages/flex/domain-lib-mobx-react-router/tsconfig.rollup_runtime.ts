const path = require('path')
const cwd = process.cwd()

const tsconfig = {
  compilerOptions: {
    module: 'commonjs',
    moduleResolution: 'node',
    target: 'es6',
    lib: ["esnext","dom","dom.iterable"],
    experimentalDecorators: true,
    allowJs: true,
    skipLibCheck: true,
    esModuleInterop: true,
    allowSyntheticDefaultImports: true,
    strict: true,
    forceConsistentCasingInFileNames: true,
    noFallthroughCasesInSwitch: true,
    resolveJsonModule: true,
    isolatedModules: true,
    noEmit: true,
    jsx: 'react',
    sourceMap: true,
    baseUrl: '.',
    typeRoots: ['node_modules/@types'],
    rootDir: `${path.join(cwd, '../domain-app/src')}`
  },
  include: [
    `${path.join(cwd, '../domain-app/src/FlexMobxReactRouter.ts')}`
  ]
}

export default tsconfig
