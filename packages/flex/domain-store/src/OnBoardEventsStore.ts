/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-console */
import {
  type CurrentMobxFormKey,
  type CurrentMobxFormStep,
  type FlexGlobalThis,
  type MobxFormArray,
  type MobxFormObject,
  type OnBoardEventApiTypes,
  type Table,
  type TableData,
} from 'flexiness'
import { action, computed, makeAutoObservable, observable, toJS } from 'mobx'
// import {
//   EEventType
// } from 'on-board-event-api'
// const {
//   EEventType
// } = await import('on-board-event-api')
// import { getEnumValues } from '@flexiness/domain-utils'

// import localForage from 'localforage'
import { makePersistable } from 'mobx-persist-store'
// import { makePersistable, getPersistedStore } from 'mobx-persist-store'
import { isServer } from './utils/index.js'

declare let globalThis: FlexGlobalThis

export class OnBoardEventsMobxStore {
  constructor(
    public displayedOnBoardEventDataMap: Map<string, OnBoardEventApiTypes.OnBoardEvent>,
    public subscribedOnBoardEventDataMap: Map<string, OnBoardEventApiTypes.OnBoardEvent>,
    public currentTablesDataArray: Table[],
    public createOnBoardEvent: OnBoardEventApiTypes.OnBoardEvent,
    public createTime: OnBoardEventApiTypes.TTime,
    public createTimes: Map<string, OnBoardEventApiTypes.TTime>,
    public createAttendees: OnBoardEventApiTypes.Attendees,
    public createTag: OnBoardEventApiTypes.Tag,
    public createUser: OnBoardEventApiTypes.User,
    public createPost: OnBoardEventApiTypes.Post,
    public createComment: OnBoardEventApiTypes.Comment,
    public currentStep: number,
    public invalidateStep: Map<number, boolean>,
    public currentAttendeeIndex: number,
    public currentDay: OnBoardEventApiTypes.EDayOfWeek | null,
    public currentMobxFormStep: CurrentMobxFormStep,
    public currentMobxFormKey: CurrentMobxFormKey,
    public currentKeyStep: Map<number, string>,
  ) {
    makeAutoObservable(this, {
      displayedOnBoardEventDataMap: observable,
      AllDisplayedOnBoardEventDataMap: computed,
      AllTablesDataArray: computed,
      subscribedOnBoardEventDataMap: observable,
      AllSubscribedOnBoardEventDataMap: computed,
      currentTablesDataArray: observable,
      createOnBoardEvent: observable,
      CurrentOnBoardEvent: computed,
      createTime: observable,
      createTimes: observable,
      createAttendees: observable,
      createTag: observable,
      createUser: observable,
      createPost: observable,
      createComment: observable,
      currentStep: observable,
      invalidateStep: observable,
      currentAttendeeIndex: observable,
      currentDay: observable,
      currentMobxFormStep: observable,
      currentKeyStep: observable,
      currentMobxFormKey: observable,
      setDisplayedOnBoardEventDataMap: action,
      addDisplayedOnBoardEventDataMap: action,
      setTables: action,
      connvertToTables: action,
      addToTables: action,
      activateTable: action,
      setSubscribedOnBoardEventDataMap: action,
      setCreateOnBoardEvent: action,
      unsetCreateOnBoardEvent: action,
      setCreateTime: action,
      setCreateTimes: action,
      unsetCreateTimes: action,
      deleteTime: action,
      AllTimes: computed,
      updateAllTimes: action,
      setCreateAttendees: action,
      unsetCreateAttendees: action,
      updateCurrentAttendee: action,
      deleteAttendee: action,
      AllAttendees: computed,
      updateAllAttendees: action,
      setCreateTag: action,
      setCreateUser: action,
      setCreatePost: action,
      setCreateComment: action,
      _setCurrentStep: action,
      setValidatedCurrentStep: action,
      resetCurrentStep: action,
      setInvalidateStep: action,
      setCurrentAttendeeIndex: action,
      CurrentAttendeeIndex: computed,
      setCurrentDay: action,
      setCurrentMobxFormByStep: action,
      setCurrentMobxFormByKey: action,
      setCurrentKeyByStep: action,
    })
    this.displayedOnBoardEventDataMap = displayedOnBoardEventDataMap
    this.subscribedOnBoardEventDataMap = subscribedOnBoardEventDataMap
    this.currentTablesDataArray = currentTablesDataArray
    this.createOnBoardEvent = createOnBoardEvent
    this.createTime = createTime
    this.createTimes = createTimes
    this.createAttendees = createAttendees
    this.createTag = createTag
    this.createUser = createUser
    this.createPost = createPost
    this.createComment = createComment
    this.currentStep = currentStep
    this.invalidateStep = invalidateStep
    this.currentAttendeeIndex = currentAttendeeIndex
    this.currentDay = currentDay
    this.currentMobxFormStep = currentMobxFormStep
    this.currentMobxFormKey = currentMobxFormKey
    this.currentKeyStep = currentKeyStep
    // makePersistable(this, { name: 'CreateOnBoardEvent', properties: ['createOnBoardEvent'], storage: !isServer ? localForage : undefined })
    void makePersistable(this, {
      name: 'FlexOnBoardEventsMobxStore',
      properties: [
        'createOnBoardEvent',
        // 'currentStep' // error on form3.tsx (reading 'rawInput')
      ],
      storage: !isServer ? window.localStorage : undefined,
    })
  }

  // https://github.com/mobxjs/mst-gql

  // https://javascript.plainenglish.io/javascript-convert-array-to-map-12907a8a334a
  // https://www.geeksforgeeks.org/how-to-convert-an-array-of-objects-to-a-map-in-javascript/
  // https://stackoverflow.com/questions/75290595/convert-array-to-map-typescript
  // https://stackoverflow.com/questions/67722467/use-set-in-mobx-with-typescript

  setDisplayedOnBoardEventDataMap = (data: OnBoardEventApiTypes.OnBoardEvent[]) => {
    data.forEach((event) => !this.displayedOnBoardEventDataMap.has(event.id) && this.displayedOnBoardEventDataMap.set(event.id, event))
    this.connvertToTables()
    if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | displayedOnBoardEventDataMap', toJS(this.displayedOnBoardEventDataMap))
  }

  addDisplayedOnBoardEventDataMap = (data: OnBoardEventApiTypes.OnBoardEvent[]) => {
    data.forEach((event) => !this.displayedOnBoardEventDataMap.has(event.id) && this.displayedOnBoardEventDataMap.set(event.id, event))
    this.addToTables(data)
    if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | displayedOnBoardEventDataMap', toJS(this.displayedOnBoardEventDataMap))
  }

  get AllDisplayedOnBoardEventDataMap() {
    return toJS(this.displayedOnBoardEventDataMap)
  }

  setTables(data: Table[]) {
    this.currentTablesDataArray = Array.from(new Set([...data]))
  }

  connvertToTables() {
    this.currentTablesDataArray = Array.from(this.displayedOnBoardEventDataMap.values()).map((onBoardEvent, index) => {
      return {
        ...onBoardEvent,
        tableId: index,
        order: 9999,
        active: false,
      } as Table
    })
  }

  addToTables(data: OnBoardEventApiTypes.OnBoardEvent[]) {
    const currentTableArray: Table[] = this.currentTablesDataArray
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    const newTableArray = data.reduce((arr, onBoardEvent) => {
      arr.push({
        ...onBoardEvent,
        tableId: currentTableArray.length !== 0 ? currentTableArray[currentTableArray.length - 1].tableId + 1 : 0,
        order: 9999,
        active: false,
      } as Table)
      return arr
    }, currentTableArray) as Table[]
    this.currentTablesDataArray = newTableArray
  }

  activateTable(tableData: TableData) {
    this.currentTablesDataArray?.map((table) => {
      table.active = table.tableId === tableData.tableId ?? false
    })
  }

  get AllTablesDataArray() {
    return toJS(this.currentTablesDataArray)
  }

  setSubscribedOnBoardEventDataMap = (data: OnBoardEventApiTypes.OnBoardEvent[]) => {
    data.forEach((event) => !this.subscribedOnBoardEventDataMap.has(event.id) && this.subscribedOnBoardEventDataMap.set(event.id, event))
    if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | subscribedOnBoardEventDataMap', toJS(this.subscribedOnBoardEventDataMap))
  }

  get AllSubscribedOnBoardEventDataMap() {
    return toJS(this.subscribedOnBoardEventDataMap)
  }

  // getCreatedOnBoardEvent = (key: keyof OnBoardEvent) => {
  //   return this.createOnBoardEvent[key]
  // }

  // getCurrentMobxFormByStep = async (currentStep?: number) => {
  //   console.log('getting form')
  //   return computed(() => (currentStep ? this.currentMobxFormStep.get(currentStep) : this.currentMobxFormStep.get(this.currentStep))).get()
  // }

  setCreateOnBoardEvent = (newCreateOnBoardEvent: OnBoardEventApiTypes.OnBoardEvent) => {
    this.createOnBoardEvent = newCreateOnBoardEvent
    // if (this.currentStep === 3) console.log('OnBoardEventsStore | createOnBoardEvent', toJS(this.createOnBoardEvent))
    if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | createOnBoardEvent', toJS(this.createOnBoardEvent))

    // if (!isServer) {
    //   reaction(() => JSON.stringify(this.createOnBoardEvent), json => {
    //     localStorage.setItem('createOnBoardEvent', json)
    //     // typedStorage.setItem(createOnBoardEvent.name as keyof OnBoardEvent, JSON.stringify(createOnBoardEvent))
    //   }, {
    //     delay: 500,
    //   })
    // }
  }

  get CurrentOnBoardEvent() {
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | get AllTimes', toJS(this.createTimes))
    return toJS(this.createOnBoardEvent)
  }

  unsetCreateOnBoardEvent = () => {
    this.createOnBoardEvent = {} as OnBoardEventApiTypes.OnBoardEvent
  }

  setCreateTime = (newTime: OnBoardEventApiTypes.TTime) => {
    // this.createTime = {
    //   ...this.createTime,
    //   ...newTime
    // }
    this.createTime = newTime
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | newTime', toJS(newTime))
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | createTime', toJS(this.createTime))
    this.setCreateTimes(this.createTime)
  }

  setCreateTimes = (newTime: OnBoardEventApiTypes.TTime) => {
    this.createTimes = this.createTimes.set(newTime.day, newTime)
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | createTimes is an Array', toJS(Array.isArray(this.createTimes)))
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | createTimes', toJS(this.createTimes))
  }

  unsetCreateTimes = () => {
    this.createTimes = new Map()
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | createTimes is an Array', toJS(Array.isArray(this.createTimes)))
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | unsetCreateTimes', toJS(this.createTimes))
  }

  deleteTime = (day: OnBoardEventApiTypes.EDayOfWeek) => {
    this.createTimes.delete(day)
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | deleteTime', toJS(this.createTimes))
  }

  get AllTimes() {
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | get AllTimes', toJS(this.createTimes))
    return toJS(this.createTimes)
  }

  updateAllTimes = () => {
    // const updateTimes = [this.AllTimes]
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | updateAllTimes | updateTimes', toJS(updateTimes))
    // const updatedAttendees = [...(this.AllAttendees?.attendees ?? [])]
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | updateAllTimes | updatedAttendees', toJS(updatedAttendees))
    // updateTimes.map((time, index) => {
    //   this.setCreateTimes({
    //     ...(this.AllTimes ?? []),
    //     [time.day as string]: {
    //       day: time.day as unknown as OnBoardEventApiTypes.EDayOfWeek,
    //       attendees: updatedAttendees as unknown as TAttendee[],
    //     },
    //   })
    // })
    console.log('OnBoardEventsStore | updateAllTimes', toJS(this.createTimes))
  }

  setCreateAttendees = (newAttendee: OnBoardEventApiTypes.Attendees) => {
    this.createAttendees = newAttendee
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | createAttendees', toJS(this.createAttendees))
  }

  unsetCreateAttendees = () => {
    this.createAttendees = {} as OnBoardEventApiTypes.Attendees
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | unsetCreateAttendees', toJS(this.createAttendees))
  }

  updateCurrentAttendee = (newAttendeeData: OnBoardEventApiTypes.TAttendee) => {
    const updatedAttendees = this.AllAttendees
    updatedAttendees[this.CurrentAttendeeIndex] = {
      ...updatedAttendees[this.CurrentAttendeeIndex],
      ...newAttendeeData,
    }
    this.setCreateAttendees({
      ...this.createAttendees,
      attendees: updatedAttendees,
    })
  }

  deleteAttendee = (index: number) => {
    if (index === 0) return
    const keepAttendees = this.AllAttendees.filter((_attendee, _index) => index !== _index)
    this.setCreateAttendees({
      ...this.createAttendees,
      attendees: keepAttendees,
    })
  }

  updateAllAttendees = (newAttendeeDataArray: OnBoardEventApiTypes.TAttendee[]) => {
    // if (!this.createAttendees?.attendees?.length) return
    const updatedAttendees = this.AllAttendees

    newAttendeeDataArray.map((newAttendeeData, index) => {
      // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | updateAllAttendees | newAttendeeData', toJS(newAttendeeData))
      // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | updateAllAttendees | updatedAttendees[index]', toJS(updatedAttendees?.[index]))
      updatedAttendees[index] = {
        ...updatedAttendees?.[index],
        ...newAttendeeData,
      }
    })

    this.setCreateAttendees({
      ...this.createAttendees,
      attendees: updatedAttendees,
    })

    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | updateAllAttendees | updatedAttendees', toJS(updatedAttendees))
  }

  get AllAttendees() {
    const allAttendees = [...(this.createAttendees?.attendees ?? [])]
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | get AllAttendees', toJS(allAttendees))
    return allAttendees
  }

  setCreateTag = (newTag: OnBoardEventApiTypes.Tag) => {
    this.createTag = newTag
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | createTag', toJS(this.createTag))
  }

  setCreateUser = (newUser: OnBoardEventApiTypes.User) => {
    this.createUser = newUser
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | createUser', toJS(this.createUser))
  }

  setCreatePost = (newPost: OnBoardEventApiTypes.Post) => {
    this.createPost = newPost
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | createPost', toJS(this.createPost))
  }

  setCreateComment = (newComment: OnBoardEventApiTypes.Comment) => {
    this.createComment = newComment
    // if (process.env.DEBUG === 'true') console.log('OnBoardEventsStore | createComment', toJS(this.createComment))
  }

  _setCurrentStep = (step: number) => {
    this.currentStep = this.currentStep + step
  }

  setValidatedCurrentStep = (step: number) => {
    switch (true) {
      // Next
      case this.currentStep + step > this.currentStep:
        setTimeout(() => {
          if (this.invalidateStep.get(this.currentStep) === true) return
          if (this.invalidateStep.get(this.currentStep) === false) {
            this._setCurrentStep(step)
          }
        }, 100)
        break
      // Previous
      case this.currentStep + step < this.currentStep:
        this.invalidateStep.set(this.currentStep, false)
        this._setCurrentStep(step)
        break
      default:
        this._setCurrentStep(step)
        break
    }
  }

  resetCurrentStep = () => {
    this.currentStep = 0
  }

  setInvalidateStep = (status: boolean) => {
    this.invalidateStep = this.invalidateStep.set(this.currentStep, status)
  }

  setCurrentAttendeeIndex = (index: number) => {
    this.currentAttendeeIndex = index
  }

  get CurrentAttendeeIndex() {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-enum-comparison
    if (toJS(this.createOnBoardEvent.eventType) === 'PROPOSAL') return 0
    return this.currentAttendeeIndex
  }

  setCurrentDay = (maybeDay: OnBoardEventApiTypes.EDayOfWeek | null) => {
    this.currentDay = maybeDay
  }

  setCurrentMobxFormByStep = ({
    form,
    formTypeKey,
    currentStep,
  }: {
    form: unknown
    formTypeKey: 'MobxFormObject' | 'MobxFormArray'
    currentStep?: number
  }) => {
    // if (process.env.DEBUG === 'true') console.log('set form by step')
    const _form = () => {
      if (formTypeKey === 'MobxFormObject') {
        return form as MobxFormObject
      } else {
        return form as MobxFormArray
      }
    }
    this.currentMobxFormStep = currentStep
      ? this.currentMobxFormStep.set(currentStep, _form())
      : this.currentMobxFormStep.set(this.currentStep, _form())
  }

  setCurrentMobxFormByKey = ({ form, formTypeKey, key }: { form: unknown; formTypeKey: 'MobxFormObject' | 'MobxFormArray'; key: string }) => {
    // if (process.env.DEBUG === 'true') console.log('set form by key')
    const _form = () => {
      if (formTypeKey === 'MobxFormObject') {
        return form as MobxFormObject
      } else {
        return form as MobxFormArray
      }
    }
    this.currentMobxFormKey = this.currentMobxFormKey.set(key, _form())
  }

  setCurrentKeyByStep = (key: string, currentStep?: number) => {
    // if (process.env.DEBUG === 'true') console.log('set key by step')
    this.currentKeyStep = currentStep ? this.currentKeyStep.set(currentStep, key) : this.currentKeyStep.set(this.currentStep, key)
  }
}

let OnBoardEventsStore: OnBoardEventsMobxStore | undefined = globalThis.Flexiness?.domainApp?.OnBoardEventsStore
export function getOnBoardEventsStore() {
  if (!OnBoardEventsStore) {
    OnBoardEventsStore = new OnBoardEventsMobxStore(
      // displayedOnBoardEventDataMap
      new Map(),
      // subscribedOnBoardEventDataMap
      new Map(),
      // currentTablesDataArray
      [],
      // createOnBoardEvent
      {} as OnBoardEventApiTypes.OnBoardEvent,
      // createTime
      {} as OnBoardEventApiTypes.TTime,
      // createTimes
      new Map(),
      // createAttendees
      {} as OnBoardEventApiTypes.Attendees,
      // createTag
      {} as OnBoardEventApiTypes.Tag,
      // createUser
      {} as OnBoardEventApiTypes.User,
      // createPost
      {} as OnBoardEventApiTypes.Post,
      // createComment
      {} as OnBoardEventApiTypes.Comment,
      // currentStep
      0,
      // invalidateStep
      new Map(),
      // currentAttendeeIndex
      0,
      // currentDay
      null,
      // currentMobxFormStep
      new Map(),
      // currentMobxFormKey
      new Map(),
      // currentKeyStep
      new Map(),
    )
    globalThis.Flexiness = {
      ...globalThis.Flexiness,
      domainApp: { ...globalThis.Flexiness?.domainApp, OnBoardEventsStore },
    }
  }
  return OnBoardEventsStore
}
