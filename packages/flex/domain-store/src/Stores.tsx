import React from 'react'
import { getUIStore, UserInterfaceStore } from './UIStore.js'
import { getOnBoardEventsStore, OnBoardEventsMobxStore } from './OnBoardEventsStore.js'
import { isServer } from './utils/index.js'

interface Stores {
  UIStore: UserInterfaceStore
  OnBoardEventsStore: OnBoardEventsMobxStore
}

let clientSideStores: Stores

function getStores() {
  if (isServer) {
    return {
      UIStore: getUIStore(),
      OnBoardEventsStore: getOnBoardEventsStore(),
    }
  }
  if (!clientSideStores) {
    clientSideStores = {
      UIStore: getUIStore(),
      OnBoardEventsStore: getOnBoardEventsStore(),
    }
  }

  return clientSideStores
}

interface StoreContextType {
  stores?: Stores
}

interface StoreProviderProps {
  children: React.ReactNode
  value?: StoreContextType
}

const StoreContext = React.createContext<StoreContextType | null>(null)

const StoreProvider: React.FC<StoreProviderProps> = ({ children }) => {
  return <StoreContext.Provider value={{ stores: getStores() }}>{children}</StoreContext.Provider>
}

export { getStores, StoreProvider }
