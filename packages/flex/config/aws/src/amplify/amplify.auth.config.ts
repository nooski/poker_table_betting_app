/* eslint-disable camelcase */
import type { ResourcesConfig } from 'aws-amplify'

// import { isNodeRuntime } from '@flexiness/domain-utils'
// import { type DotenvConfigOptions } from 'dotenv'

// if (isNodeRuntime) {
//   const { default: path } = await import('path')
//   const { fileURLToPath } = await import('url').then((module) => module.default)
//   const __filename = fileURLToPath(import.meta.url)
//   const __dirname = path.dirname(__filename)
//   const dotenv = await import('dotenv').then((module) => module.default)
//   const dotenvExpand = await import('dotenv-expand').then((module) => module.default)
//   let _envProcess = {}
//   dotenv.config({
//     processEnv: _envProcess,
//     path: path.resolve(__dirname, `../../../../env/public/.env.${process.env['NODE_ENV']}`),
//     override: true
//   }) as DotenvConfigOptions
//   dotenvExpand.expand(_envProcess)
//   // console.log(process.env)
// }

const AMPLIFY_AUTH_CONFIG_V1 = {
  aws_project_region: process.env.FLEX_AWS_PROJECT_REGION ?? '',
  aws_cognito_identity_pool_id: process.env.FLEX_AWS_COGNITO_IDENTITY_POOL ?? '',
  aws_cognito_region: process.env.FLEX_AWS_APPSYNC_REGION ?? '',
  aws_user_pools_id: process.env.FLEX_AWS_COGNITO_USER_POOL_ID ?? '',
  aws_user_pools_web_client_id: process.env.FLEX_AWS_COGNITO_USER_POOL_APP_CLIENT_ID ?? '',
  oauth: {
    domain: process.env.FLEX_AWS_COGNITO_OAUTH_DOMAIN ?? '',
    scopes: ['phone', 'email', 'profile', 'openid', 'aws.cognito.signin.user.admin'],
    redirectSignIn: [process.env.FLEX_POKER_CLIENT_HOST ?? ''],
    redirectSignOut: [process.env.FLEX_POKER_CLIENT_HOST ?? ''],
    responseType: 'code', // or 'token', note that REFRESH token will only be generated when the responseType is code
  },
  federationTarget: 'COGNITO_USER_AND_IDENTITY_POOLS',
  aws_cognito_username_attributes: ['EMAIL', 'PHONE_NUMBER'],
  aws_cognito_social_providers: ['GOOGLE'],
  aws_cognito_signup_attributes: ['EMAIL', 'FAMILY_NAME', 'GIVEN_NAME', 'PHONE_NUMBER'],
  aws_cognito_mfa_configuration: 'OFF',
  aws_cognito_mfa_types: ['SMS'],
  aws_cognito_password_protection_settings: {
    passwordPolicyMinLength: 8,
    passwordPolicyCharacters: ['REQUIRES_LOWERCASE', 'REQUIRES_UPPERCASE', 'REQUIRES_NUMBERS', 'REQUIRES_SYMBOLS'],
  },
  aws_cognito_verification_mechanisms: ['EMAIL'],
}

const AMPLIFY_AUTH_CONFIG_V2: ResourcesConfig = {
  // eslint-disable-next-line max-len
  // https://docs.aws.amazon.com/prescriptive-guidance/latest/patterns/authenticate-react-application-users-by-using-amazon-cognito-and-aws-amplify.html
  // Auth: {
  //   region: process.env.FLEX_AWS_PROJECT_REGION,
  //   userPoolId: process.env.USER_POOL_ID,
  //   userPoolWebClientId: process.env.USER_POOL_APP_CLIENT_ID
  // }
  // https://aws.amazon.com/blogs/mobile/secure-aws-appsync-with-iam-permissions-using-the-aws-cdk/
  // https://docs.amplify.aws/react/build-a-backend/auth/set-up-auth/
  Auth: {
    Cognito: {
      //  Amazon Cognito User Pool ID
      userPoolId: process.env.FLEX_AWS_COGNITO_USER_POOL_ID ?? '',
      // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
      userPoolClientId: process.env.FLEX_AWS_COGNITO_USER_POOL_APP_CLIENT_ID ?? '',
      // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
      identityPoolId: process.env.FLEX_AWS_COGNITO_IDENTITY_POOL ?? '',
      // OPTIONAL - This is used when autoSignIn is enabled for Auth.signUp
      // 'code' is used for Auth.confirmSignUp, 'link' is used for email link verification
      signUpVerificationMethod: 'code', // 'code' | 'link'
      loginWith: {
        // OPTIONAL - Hosted UI configuration
        username: false,
        email: true,
        phone: true,
        oauth: {
          domain: process.env.FLEX_AWS_COGNITO_OAUTH_DOMAIN ?? '',
          // https://stackoverflow.com/a/77596876/10159170
          scopes: ['phone', 'email', 'profile', 'openid', 'aws.cognito.signin.user.admin'],
          redirectSignIn: [`${process.env.FLEX_POKER_CLIENT_HOST}/`, `${process.env.FLEX_GATEWAY_HOST}/`, `${process.env.FLEX_PROTOCOL}${process.env.FLEX_DOMAIN_NAME}:${process.env.FLEX_PROXY_PORT}/`],
          redirectSignOut: [`${process.env.FLEX_POKER_CLIENT_HOST}/`, `${process.env.FLEX_GATEWAY_HOST}/`, `${process.env.FLEX_PROTOCOL}${process.env.FLEX_DOMAIN_NAME}:${process.env.FLEX_PROXY_PORT}/`],
          responseType: 'code', // or 'token', note that REFRESH token will only be generated when the responseType is code
          providers: ['Google'],
        },
      },
    },
  },
  API: {
    GraphQL: {
      endpoint: process.env.FLEX_AWS_APPSYNC_GRAPHQL_ENDPOINT ?? '',
      // customEndpoint: process.env.FLEX_AWS_APPSYNC_GRAPHQL_ENDPOINT ?? '',
      region: process.env.FLEX_AWS_PROJECT_REGION ?? '',
      // customEndpointRegion: process.env.FLEX_AWS_PROJECT_REGION ?? '',
      defaultAuthMode: 'userPool',
      // Set the default auth mode to "apiKey" and provide the API key value
      // defaultAuthMode: 'apiKey',
      // apiKey: process.env.FLEX_AWS_APPSYNC_APIKEY ?? '',
    },
  },
}

export { AMPLIFY_AUTH_CONFIG_V1, AMPLIFY_AUTH_CONFIG_V2 }
