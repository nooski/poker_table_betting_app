import { Amplify } from 'aws-amplify'
import * as AmplifyAPIServer from 'aws-amplify/api/server'
import * as AmplifyAuthServer from 'aws-amplify/auth/server'
import * as AmplifyUtils from 'aws-amplify/utils'
import * as xstate from 'xstate'

// https://stackoverflow.com/questions/76872395/looking-for-a-guide-to-access-aws-appsync-graphql-api-from-nodejs-app-via-http-r

import { Sha256 } from '@aws-crypto/sha256-js'
import { HttpRequest } from '@smithy/protocol-http'
import { SignatureV4 } from '@smithy/signature-v4'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const signedFetch = async (graphqlObject: { [key: string]: any } ) => {

  if (!graphqlObject) return

  // const { fetchAuthSession } = AmplifyAuthServer

  const apiUrl = new URL(process.env.FLEX_AWS_APPSYNC_GRAPHQL_ENDPOINT!)

  const signer = new SignatureV4({
    service: 'appsync',
    region: process.env.FLEX_AWS_APPSYNC_REGION!,
    credentials: {
      accessKeyId: process.env.FLEX_AWS_APPSYNC_API_ID!,
      secretAccessKey: process.env.FLEX_AWS_APPSYNC_APIKEY!
    },
    sha256: Sha256,
  })

  // set up the HTTP request
  const request = new HttpRequest({
    hostname: apiUrl.host,
    path: apiUrl.pathname,
    body: JSON.stringify(graphqlObject),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      host: apiUrl.hostname,
      // Authorization: (await fetchAuthSession()).tokens?.idToken?.toString()
    },
  })

  const signedRequest = await signer.sign(request)

  const {
    headers,
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    body,
    method
  } = signedRequest

  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  const awsSignedRequest = await fetch(process.env.FLEX_AWS_APPSYNC_GRAPHQL_ENDPOINT!, {
    headers,
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    body,
    method
  }).then((res) => res.json())

  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  return awsSignedRequest
}

export {
  Amplify,
  AmplifyAPIServer, AmplifyAuthServer,
  AmplifyUtils,
  signedFetch,
  xstate
}
