'use client'

import * as AmplifyUIReact from '@aws-amplify/ui-react'
import { CognitoIdentity } from '@aws-sdk/client-cognito-identity'
import { Amplify } from 'aws-amplify'
import * as AmplifyAPI from 'aws-amplify/api'
import * as AmplifyAuth from 'aws-amplify/auth'
import * as AmplifyUtils from 'aws-amplify/utils'
import * as xstate from 'xstate'

export {
  createAWSCredentialsAndIdentityIdProvider, createKeyValueStorageFromCookieStorageAdapter,
  createUserPoolsTokenProvider, runWithAmplifyServerContext
} from 'aws-amplify/adapter-core'

export {
  Amplify,
  AmplifyUIReact,
  AmplifyAPI, AmplifyAuth,
  AmplifyUtils,
  CognitoIdentity,
  xstate
}
