/* eslint-disable camelcase */

// https://docs.aws.amazon.com/amplify/latest/userguide/environment-variables.html#access-env-vars
// https://docs.aws.amazon.com/amplify/latest/userguide/ssr-environment-variables.html
// https://github.com/aws-amplify/amplify-cli/issues/3643

// import { isNodeRuntime } from '@flexiness/domain-utils'
// import { type DotenvConfigOptions } from 'dotenv'

// if (isNodeRuntime) {
//   const { default: path } = await import('path')
//   const { fileURLToPath } = await import('url').then((module) => module.default)
//   const __filename = fileURLToPath(import.meta.url)
//   const __dirname = path.dirname(__filename)
//   const dotenv = await import('dotenv').then((module) => module.default)
//   const dotenvExpand = await import('dotenv-expand').then((module) => module.default)
//   let _envProcess = {}
//   dotenv.config({
//     processEnv: _envProcess,
//     path: path.resolve(__dirname, `../../../../env/public/.env.${process.env['NODE_ENV']}`),
//     override: true
//   }) as DotenvConfigOptions
//   dotenvExpand.expand(_envProcess)
//   // console.log(process.env)
// }

const AMPLIFY_API_CONFIG = {
  aws_project_region: process.env.FLEX_AWS_PROJECT_REGION ?? '',
  aws_appsync_graphqlEndpoint: process.env.FLEX_AWS_APPSYNC_GRAPHQL_ENDPOINT ?? '',
  aws_appsync_region: process.env.FLEX_AWS_APPSYNC_REGION ?? '',
  aws_appsync_authenticationType: process.env.FLEX_AWS_APPSYNC_AUTHENTICATIONTYPE ?? '',
  aws_appsync_apiKey: process.env.FLEX_AWS_APPSYNC_APIKEY ?? '',
}

export { AMPLIFY_API_CONFIG }
