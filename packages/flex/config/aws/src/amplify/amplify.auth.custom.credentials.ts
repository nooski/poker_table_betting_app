/* eslint-disable no-console */

import { isServer } from '@flexiness/domain-utils'

// https://docs.amplify.aws/javascript/build-a-backend/auth/advanced-workflows/

import { CognitoIdentity } from '@aws-sdk/client-cognito-identity'
import {
  // fetchAuthSession,
  CredentialsAndIdentityIdProvider,
  CredentialsAndIdentityId,
  GetCredentialsOptions,
} from 'aws-amplify/auth'

// You can make use of the sdk to get identityId and credentials
const cognitoidentity = new CognitoIdentity({
  // region: AMPLIFY_AUTH_CONFIG_V1.aws_project_region,
  region: process.env.FLEX_AWS_PROJECT_REGION ?? '',
})

// Note: The custom provider class must implement CredentialsAndIdentityIdProvider
class CustomCredentialsProvider implements CredentialsAndIdentityIdProvider {
  // Example class member that holds the login information
  federatedLogin?: {
    domain: string
    token: string
  }

  // Custom method to load the federated login information
  loadFederatedLogin(login?: typeof this.federatedLogin) {
    // You may also persist this by caching if needed
    this.federatedLogin = login
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async getCredentialsAndIdentityId(getCredentialsOptions: GetCredentialsOptions): Promise<CredentialsAndIdentityId | undefined> {
    try {
      // You can add in some validation to check if the token is available before proceeding
      // You can also refresh the token if it's expired before proceeding

      const getIdResult = await cognitoidentity.getId({
        // Get the identityPoolId from config
        // IdentityPoolId: AMPLIFY_AUTH_CONFIG_V1.aws_cognito_identity_pool_id,
        IdentityPoolId: process.env.FLEX_AWS_COGNITO_IDENTITY_POOL ?? '',
        // @ts-expect-error - Object is possibly 'undefined'.ts(2532)
        Logins: { [this.federatedLogin.domain]: this.federatedLogin.token },
      })

      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call
      const cognitoCredentialsResult = await cognitoidentity.getCredentialsForIdentity({
        IdentityId: getIdResult.IdentityId,
        // @ts-expect-error - Object is possibly 'undefined'.ts(2532)
        Logins: { [this.federatedLogin.domain]: this.federatedLogin.token },
      })

      const credentials: CredentialsAndIdentityId = {
        credentials: {
          // @ts-expect-error - Type 'string | undefined' is not assignable to type 'string'.ts(2322)
          accessKeyId: cognitoCredentialsResult.Credentials?.AccessKeyId,
          // @ts-expect-error - Type 'string | undefined' is not assignable to type 'string'.ts(2322)
          secretAccessKey: cognitoCredentialsResult.Credentials?.SecretKey,
          sessionToken: cognitoCredentialsResult.Credentials?.SessionToken,
          expiration: cognitoCredentialsResult.Credentials?.Expiration,
        },
        identityId: getIdResult.IdentityId,
      }
      return credentials
    } catch (e) {
      console.log('Error getting credentials: ', e)
    }
  }

  // Implement this to clear any cached credentials and identityId. This can be called when signing out of the federation service.
  clearCredentialsAndIdentityId(): void {}
}

const getCustomCredentialsProvider = async () => {
  const dotenv = await import('dotenv').then((module) => module.default)
  dotenv.config()
  if (isServer && process.env.DEBUG === 'true') console.log('process.env', process.env)
  return new CustomCredentialsProvider()
}

export { getCustomCredentialsProvider }
