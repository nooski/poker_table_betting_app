/* eslint-disable no-console */

import type { ResourcesConfig } from 'aws-amplify'
import type { Client } from 'aws-amplify/api'
import { AMPLIFY_AUTH_CONFIG_V2 } from './amplify.auth.config.js'

import * as AMPLIFY_CLIENT from './amplify.client.mjs'

import { isServer } from '@flexiness/domain-utils'

let config: ResourcesConfig
let client: Client | undefined

const initAmplifyConfigClient = async (type?: 'API-apiKey' | 'API-userPool') => {
  const {
    Amplify,
    AmplifyAuth,
    AmplifyAPI,
    // AmplifyUtils
  } = AMPLIFY_CLIENT

  const amplifyConfigure = async (config: ResourcesConfig, authMode: string) => {
    Amplify.configure(config, { ssr: true })
    if (isServer && process.env.DEBUG === 'true') console.log(`Amplify ${authMode} config`, Amplify.getConfig())
    return
  }

  const { generateClient } = AmplifyAPI
  const { fetchAuthSession } = AmplifyAuth
  // const { parseAmplifyConfig } = AmplifyUtils

  // https://github.com/aws-amplify/amplify-js/issues/12986
  const existingConfig = Amplify.getConfig()
  // console.log('initAmplifyConfigClient', existingConfig)

  switch (type) {
    case 'API-userPool':
      config = {
        ...AMPLIFY_AUTH_CONFIG_V2,
        ...existingConfig,
        API: {
          GraphQL: {
            ...AMPLIFY_AUTH_CONFIG_V2?.API?.GraphQL,
            ...existingConfig?.API?.GraphQL,
            defaultAuthMode: 'userPool',
            headers: async () => ({
              Authorization: (await fetchAuthSession()).tokens?.idToken?.toString(),
            }),
          },
        },
      } as ResourcesConfig
      await amplifyConfigure(config, 'userPool').then(async() => {
        client = generateClient({ authMode: 'userPool', authToken: (await fetchAuthSession()).tokens?.accessToken?.toString() })
        if (isServer && process.env.DEBUG === 'true') console.log('Graphql userPool client', client)
      }).catch(error => console.log(error))
      break
    case 'API-apiKey':
    default:
      config = {
        ...AMPLIFY_AUTH_CONFIG_V2,
        ...existingConfig,
        ...{
          API: {
            GraphQL: {
              ...AMPLIFY_AUTH_CONFIG_V2?.API?.GraphQL,
              ...existingConfig?.API?.GraphQL,
              defaultAuthMode: 'apiKey',
              apiKey: process.env.FLEX_AWS_APPSYNC_APIKEY,
            },
          },
        },
      } as ResourcesConfig
      await amplifyConfigure(config, 'apiKey').then(() => {
        client = generateClient({ authMode: 'apiKey' })
        if (isServer && process.env.DEBUG === 'true') console.log('Graphql apiKey client', client)
      }).catch(error => console.log(error))
      break
  }

  return client
}

export {
  initAmplifyConfigClient,
}
