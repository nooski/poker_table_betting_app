enum EActivity {
  AUTRE
  BABYSITTING
  COVOITURAGE
  PEDIBUS
  SOUTIEN_DEVOIRS
}

enum EDayOfWeek {
  FRIDAY
  MONDAY
  SATURDAY
  SUNDAY
  THURSDAY
  TUESDAY
  WEDNESDAY
}

enum EEventType {
  PROPOSAL
  REQUEST
}

enum ELifeCycle {
  ALL_YEAR
  DATE_RANGE
}

enum EPostStatus {
  ERROR
  PENDING
  SUCCESS
}

enum ESchool {
  BEL_AIR
  FERDINAND_BUISSON
  LA_SOURCE
  MARITAIN_RENAN
  NOTRE_DAME
}

enum ESchoolLevel {
  COLLEGE_3EME
  COLLEGE_4EME
  COLLEGE_5EME
  COLLEGE_6EME
  LYCEE_PREMIERE
  LYCEE_SECONDE
  LYCEE_TERMINALE
  MATERNELLE_GS
  PRIMAIRE_CE1
  PRIMAIRE_CE2
  PRIMAIRE_CM1
  PRIMAIRE_CM2
  PRIMAIRE_CP
}

enum EStatus {
  ACCEPTED
  DECLINED
  NEEDSACTION
  TENTATIVE
}

enum EWhichWay {
  AT_HOME
  AT_SCHOOL
  FROM_SCHOOL
  TO_SCHOOL
}

enum EAttendeeRole {
  PARTICIPANT
  RESPONSABLE
}

type TAddress @aws_api_key @aws_cognito_user_pools {
  city: String
  itinerary: TPointList
  location: TPoint
  postcode: String
  street1: String
  street2: String
  additional: String
}

type TCalendarDateTime @aws_api_key @aws_cognito_user_pools {
  dateTime: AWSDateTime
  timeZone: String
}

type TPoint @aws_api_key @aws_cognito_user_pools {
  latitude: Float!
  longitude: Float!
}

type TPointList @aws_api_key @aws_cognito_user_pools {
  points: [TPoint!]!
}

interface IStudent {
  firstName: String
  lastName: String
  school: ESchool
  schoolLevel: ESchoolLevel
}

type TAttendee implements IStudent @aws_api_key @aws_cognito_user_pools {
  role: EAttendeeRole
  address: TAddress
  displayName: String
  firstName: String
  lastName: String
  school: ESchool
  schoolLevel: ESchoolLevel
  organizer: Boolean
  status: EStatus
  self: Boolean
  endParticipation: AWSDateTime
  startParticipation: AWSDateTime
}

type Attendees @aws_api_key @aws_cognito_user_pools {
  id: ID!
  created: AWSDateTime
  updated: AWSDateTime
  attendees: [TAttendee!]!
  event: OnBoardEvent
  user: User!
  _version: Int!
  _deleted: Boolean
  _lastChangedAt: AWSTimestamp!
  timesAttendeesId: ID
  onBoardEventAttendeesId: ID
  attendeesUserId: ID!
  owner: String
}

type TTime @aws_api_key @aws_cognito_user_pools {
  day: EDayOfWeek!
  attendees: [TAttendee!]!
}

type Times @aws_api_key @aws_cognito_user_pools {
  id: ID!
  created: AWSDateTime
  updated: AWSDateTime
  times: [TTime!]!
  attendees(filter: ModelAttendeesFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelAttendeesConnection
  event: OnBoardEvent
  user: User!
  _version: Int!
  _deleted: Boolean
  _lastChangedAt: AWSTimestamp!
  onBoardEventTimesId: ID
  timesUserId: ID!
  owner: String
}

type OnBoardEvent @aws_api_key @aws_cognito_user_pools {
  id: ID!
  created: AWSDateTime
  updated: AWSDateTime
  description: String!
  name: String!
  activity: EActivity!
  eventType: EEventType!
  whichWay: EWhichWay!
  lifeCycle: ELifeCycle!
  reoccur: Boolean
  startDate: AWSDateTime
  endDate: AWSDateTime
  end: TCalendarDateTime
  start: TCalendarDateTime
  recurrence: String
  attendees(filter: ModelAttendeesFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelAttendeesConnection
  times(filter: ModelTimesFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelTimesConnection
  tags(filter: ModelOnBoardEventTagsFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelOnBoardEventTagsConnection
  creator: User!
  organizer: User
  users(filter: ModelOnBoardEventUsersFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelOnBoardEventUsersConnection
  _version: Int!
  _deleted: Boolean
  _lastChangedAt: AWSTimestamp!
  onBoardEventCreatorId: ID!
  onBoardEventOrganizerId: ID
  owner: String
}

type Post @aws_api_key @aws_cognito_user_pools {
  id: ID!
  created: AWSDateTime
  updated: AWSDateTime
  name: String
  poststatus: EPostStatus
  comments(filter: ModelCommentFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelCommentConnection
  user: User!
  _version: Int!
  _deleted: Boolean
  _lastChangedAt: AWSTimestamp!
  userPostsId: ID
  owner: String
}

type Comment @aws_api_key @aws_cognito_user_pools {
  id: ID!
  content: String!
  post: Post
  created: AWSDateTime!
  updated: AWSDateTime!
  _version: Int!
  _deleted: Boolean
  _lastChangedAt: AWSTimestamp!
  postCommentsId: ID
  owner: String
}

type User {
  id: ID!
  created: AWSDateTime
  updated: AWSDateTime
  username: String!
  displayName: String
  firstName: String!
  lastName: String!
  email: AWSEmail!
  phoneNumber: AWSPhone
  posts(filter: ModelPostFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelPostConnection
  events(filter: ModelOnBoardEventUsersFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelOnBoardEventUsersConnection
  _version: Int!
  _deleted: Boolean
  _lastChangedAt: AWSTimestamp!
  owner: String
}

type Tag @aws_api_key @aws_cognito_user_pools {
  id: ID!
  created: AWSDateTime
  updated: AWSDateTime
  name: String!
  events(filter: ModelOnBoardEventTagsFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelOnBoardEventTagsConnection
  _version: Int!
  _deleted: Boolean
  _lastChangedAt: AWSTimestamp!
  owner: String
}

type OnBoardEventTags @aws_api_key @aws_cognito_user_pools {
  id: ID!
  onBoardEventId: ID!
  tagId: ID!
  onBoardEvent: OnBoardEvent!
  tag: Tag!
  createdAt: AWSDateTime!
  updatedAt: AWSDateTime!
  _version: Int!
  _deleted: Boolean
  _lastChangedAt: AWSTimestamp!
  owner: String
}

type OnBoardEventUsers @aws_api_key @aws_cognito_user_pools {
  id: ID!
  onBoardEventId: ID!
  userId: ID!
  onBoardEvent: OnBoardEvent!
  user: User!
  createdAt: AWSDateTime!
  updatedAt: AWSDateTime!
  _version: Int!
  _deleted: Boolean
  _lastChangedAt: AWSTimestamp!
  owner: String
}

input ModelStringInput {
  ne: String
  eq: String
  le: String
  lt: String
  ge: String
  gt: String
  contains: String
  notContains: String
  between: [String]
  beginsWith: String
  attributeExists: Boolean
  attributeType: ModelAttributeTypes
  size: ModelSizeInput
}

input ModelIntInput {
  ne: Int
  eq: Int
  le: Int
  lt: Int
  ge: Int
  gt: Int
  between: [Int]
  attributeExists: Boolean
  attributeType: ModelAttributeTypes
}

input ModelFloatInput {
  ne: Float
  eq: Float
  le: Float
  lt: Float
  ge: Float
  gt: Float
  between: [Float]
  attributeExists: Boolean
  attributeType: ModelAttributeTypes
}

input ModelBooleanInput {
  ne: Boolean
  eq: Boolean
  attributeExists: Boolean
  attributeType: ModelAttributeTypes
}

input ModelIDInput {
  ne: ID
  eq: ID
  le: ID
  lt: ID
  ge: ID
  gt: ID
  contains: ID
  notContains: ID
  between: [ID]
  beginsWith: ID
  attributeExists: Boolean
  attributeType: ModelAttributeTypes
  size: ModelSizeInput
}

input ModelSubscriptionStringInput {
  ne: String
  eq: String
  le: String
  lt: String
  ge: String
  gt: String
  contains: String
  notContains: String
  between: [String]
  beginsWith: String
  in: [String]
  notIn: [String]
}

input ModelSubscriptionIntInput {
  ne: Int
  eq: Int
  le: Int
  lt: Int
  ge: Int
  gt: Int
  between: [Int]
  in: [Int]
  notIn: [Int]
}

input ModelSubscriptionFloatInput {
  ne: Float
  eq: Float
  le: Float
  lt: Float
  ge: Float
  gt: Float
  between: [Float]
  in: [Float]
  notIn: [Float]
}

input ModelSubscriptionBooleanInput {
  ne: Boolean
  eq: Boolean
}

input ModelSubscriptionIDInput {
  ne: ID
  eq: ID
  le: ID
  lt: ID
  ge: ID
  gt: ID
  contains: ID
  notContains: ID
  between: [ID]
  beginsWith: ID
  in: [ID]
  notIn: [ID]
}

enum ModelAttributeTypes {
  binary
  binarySet
  bool
  list
  map
  number
  numberSet
  string
  stringSet
  _null
}

input ModelSizeInput {
  ne: Int
  eq: Int
  le: Int
  lt: Int
  ge: Int
  gt: Int
  between: [Int]
}

enum ModelSortDirection {
  ASC
  DESC
}

input TAttendeeInput {
  role: EAttendeeRole
  address: TAddressInput
  displayName: String
  firstName: String
  lastName: String
  school: ESchool
  schoolLevel: ESchoolLevel
  organizer: Boolean
  status: EStatus
  self: Boolean
  endParticipation: AWSDateTime
  startParticipation: AWSDateTime
}

input TAddressInput {
  city: String
  itinerary: TPointListInput
  location: TPointInput
  postcode: String
  street1: String
  street2: String
  additional: String
}

input TPointListInput {
  points: [TPointInput!]!
}

input TPointInput {
  latitude: Float!
  longitude: Float!
}

type ModelAttendeesConnection @aws_api_key @aws_cognito_user_pools {
  items: [Attendees]!
  nextToken: String
  startedAt: AWSTimestamp
}

input ModelAttendeesFilterInput {
  id: ModelIDInput
  created: ModelStringInput
  updated: ModelStringInput
  and: [ModelAttendeesFilterInput]
  or: [ModelAttendeesFilterInput]
  not: ModelAttendeesFilterInput
  _deleted: ModelBooleanInput
  timesAttendeesId: ModelIDInput
  onBoardEventAttendeesId: ModelIDInput
  attendeesUserId: ModelIDInput
}

type Query {
  getUser(id: ID!): User
  listUsers(filter: ModelUserFilterInput, limit: Int, nextToken: String): ModelUserConnection
  syncUsers(filter: ModelUserFilterInput, limit: Int, nextToken: String, lastSync: AWSTimestamp): ModelUserConnection
  onboardUsersByUsername(username: String!, sortDirection: ModelSortDirection, filter: ModelUserFilterInput, limit: Int, nextToken: String): ModelUserConnection
  onboardUsersByDisplayName(displayName: String!, sortDirection: ModelSortDirection, filter: ModelUserFilterInput, limit: Int, nextToken: String): ModelUserConnection
  onboardUsersByFirstName(firstName: String!, sortDirection: ModelSortDirection, filter: ModelUserFilterInput, limit: Int, nextToken: String): ModelUserConnection
  onboardUsersByLastName(lastName: String!, sortDirection: ModelSortDirection, filter: ModelUserFilterInput, limit: Int, nextToken: String): ModelUserConnection
  onboardUsersByEmail(email: AWSEmail!, sortDirection: ModelSortDirection, filter: ModelUserFilterInput, limit: Int, nextToken: String): ModelUserConnection
  onboardUsersByPhoneNumber(phoneNumber: AWSPhone!, sortDirection: ModelSortDirection, filter: ModelUserFilterInput, limit: Int, nextToken: String): ModelUserConnection
  getAttendees(id: ID!): Attendees @aws_api_key @aws_cognito_user_pools
  listAttendees(filter: ModelAttendeesFilterInput, limit: Int, nextToken: String): ModelAttendeesConnection @aws_api_key @aws_cognito_user_pools
  syncAttendees(filter: ModelAttendeesFilterInput, limit: Int, nextToken: String, lastSync: AWSTimestamp): ModelAttendeesConnection @aws_api_key @aws_cognito_user_pools
  getTimes(id: ID!): Times @aws_api_key @aws_cognito_user_pools
  listTimes(filter: ModelTimesFilterInput, limit: Int, nextToken: String): ModelTimesConnection @aws_api_key @aws_cognito_user_pools
  syncTimes(filter: ModelTimesFilterInput, limit: Int, nextToken: String, lastSync: AWSTimestamp): ModelTimesConnection @aws_api_key @aws_cognito_user_pools
  getOnBoardEvent(id: ID!): OnBoardEvent @aws_api_key @aws_cognito_user_pools
  listOnBoardEvents(filter: ModelOnBoardEventFilterInput, limit: Int, nextToken: String): ModelOnBoardEventConnection @aws_api_key @aws_cognito_user_pools
  syncOnBoardEvents(filter: ModelOnBoardEventFilterInput, limit: Int, nextToken: String, lastSync: AWSTimestamp): ModelOnBoardEventConnection @aws_api_key @aws_cognito_user_pools
  onboardEventByName(name: String!, sortDirection: ModelSortDirection, filter: ModelOnBoardEventFilterInput, limit: Int, nextToken: String): ModelOnBoardEventConnection @aws_api_key @aws_cognito_user_pools
  onboardEventByActivity(activity: EActivity!, sortDirection: ModelSortDirection, filter: ModelOnBoardEventFilterInput, limit: Int, nextToken: String): ModelOnBoardEventConnection @aws_api_key @aws_cognito_user_pools
  onboardEventByEventType(eventType: EEventType!, sortDirection: ModelSortDirection, filter: ModelOnBoardEventFilterInput, limit: Int, nextToken: String): ModelOnBoardEventConnection @aws_api_key @aws_cognito_user_pools
  onboardEventByWhichWay(whichWay: EWhichWay!, sortDirection: ModelSortDirection, filter: ModelOnBoardEventFilterInput, limit: Int, nextToken: String): ModelOnBoardEventConnection @aws_api_key @aws_cognito_user_pools
  getPost(id: ID!): Post @aws_api_key @aws_cognito_user_pools
  listPosts(filter: ModelPostFilterInput, limit: Int, nextToken: String): ModelPostConnection @aws_api_key @aws_cognito_user_pools
  syncPosts(filter: ModelPostFilterInput, limit: Int, nextToken: String, lastSync: AWSTimestamp): ModelPostConnection @aws_api_key @aws_cognito_user_pools
  getComment(id: ID!): Comment @aws_api_key @aws_cognito_user_pools
  listComments(filter: ModelCommentFilterInput, limit: Int, nextToken: String): ModelCommentConnection @aws_api_key @aws_cognito_user_pools
  syncComments(filter: ModelCommentFilterInput, limit: Int, nextToken: String, lastSync: AWSTimestamp): ModelCommentConnection @aws_api_key @aws_cognito_user_pools
  getTag(id: ID!): Tag @aws_api_key @aws_cognito_user_pools
  listTags(filter: ModelTagFilterInput, limit: Int, nextToken: String): ModelTagConnection @aws_api_key @aws_cognito_user_pools
  syncTags(filter: ModelTagFilterInput, limit: Int, nextToken: String, lastSync: AWSTimestamp): ModelTagConnection @aws_api_key @aws_cognito_user_pools
  getOnBoardEventTags(id: ID!): OnBoardEventTags @aws_api_key @aws_cognito_user_pools
  listOnBoardEventTags(filter: ModelOnBoardEventTagsFilterInput, limit: Int, nextToken: String): ModelOnBoardEventTagsConnection @aws_api_key @aws_cognito_user_pools
  syncOnBoardEventTags(filter: ModelOnBoardEventTagsFilterInput, limit: Int, nextToken: String, lastSync: AWSTimestamp): ModelOnBoardEventTagsConnection @aws_api_key @aws_cognito_user_pools
  onBoardEventTagsByOnBoardEventId(onBoardEventId: ID!, sortDirection: ModelSortDirection, filter: ModelOnBoardEventTagsFilterInput, limit: Int, nextToken: String): ModelOnBoardEventTagsConnection @aws_api_key @aws_cognito_user_pools
  onBoardEventTagsByTagId(tagId: ID!, sortDirection: ModelSortDirection, filter: ModelOnBoardEventTagsFilterInput, limit: Int, nextToken: String): ModelOnBoardEventTagsConnection @aws_api_key @aws_cognito_user_pools
  getOnBoardEventUsers(id: ID!): OnBoardEventUsers @aws_api_key @aws_cognito_user_pools
  listOnBoardEventUsers(filter: ModelOnBoardEventUsersFilterInput, limit: Int, nextToken: String): ModelOnBoardEventUsersConnection @aws_api_key @aws_cognito_user_pools
  syncOnBoardEventUsers(filter: ModelOnBoardEventUsersFilterInput, limit: Int, nextToken: String, lastSync: AWSTimestamp): ModelOnBoardEventUsersConnection @aws_api_key @aws_cognito_user_pools
  onBoardEventUsersByOnBoardEventId(onBoardEventId: ID!, sortDirection: ModelSortDirection, filter: ModelOnBoardEventUsersFilterInput, limit: Int, nextToken: String): ModelOnBoardEventUsersConnection @aws_api_key @aws_cognito_user_pools
  onBoardEventUsersByUserId(userId: ID!, sortDirection: ModelSortDirection, filter: ModelOnBoardEventUsersFilterInput, limit: Int, nextToken: String): ModelOnBoardEventUsersConnection @aws_api_key @aws_cognito_user_pools
}

input ModelAttendeesConditionInput {
  created: ModelStringInput
  updated: ModelStringInput
  and: [ModelAttendeesConditionInput]
  or: [ModelAttendeesConditionInput]
  not: ModelAttendeesConditionInput
  _deleted: ModelBooleanInput
  timesAttendeesId: ModelIDInput
  onBoardEventAttendeesId: ModelIDInput
  attendeesUserId: ModelIDInput
}

input CreateAttendeesInput {
  id: ID
  created: AWSDateTime
  updated: AWSDateTime
  attendees: [TAttendeeInput!]!
  _version: Int
  timesAttendeesId: ID
  onBoardEventAttendeesId: ID
  attendeesUserId: ID!
}

input UpdateAttendeesInput {
  id: ID!
  created: AWSDateTime
  updated: AWSDateTime
  attendees: [TAttendeeInput!]
  _version: Int
  timesAttendeesId: ID
  onBoardEventAttendeesId: ID
  attendeesUserId: ID
}

input DeleteAttendeesInput {
  id: ID!
  _version: Int
}

type Mutation {
  createAttendees(input: CreateAttendeesInput!, condition: ModelAttendeesConditionInput): Attendees
  updateAttendees(input: UpdateAttendeesInput!, condition: ModelAttendeesConditionInput): Attendees
  deleteAttendees(input: DeleteAttendeesInput!, condition: ModelAttendeesConditionInput): Attendees
  createTimes(input: CreateTimesInput!, condition: ModelTimesConditionInput): Times
  updateTimes(input: UpdateTimesInput!, condition: ModelTimesConditionInput): Times
  deleteTimes(input: DeleteTimesInput!, condition: ModelTimesConditionInput): Times
  createOnBoardEvent(input: CreateOnBoardEventInput!, condition: ModelOnBoardEventConditionInput): OnBoardEvent
  updateOnBoardEvent(input: UpdateOnBoardEventInput!, condition: ModelOnBoardEventConditionInput): OnBoardEvent
  deleteOnBoardEvent(input: DeleteOnBoardEventInput!, condition: ModelOnBoardEventConditionInput): OnBoardEvent
  createPost(input: CreatePostInput!, condition: ModelPostConditionInput): Post
  updatePost(input: UpdatePostInput!, condition: ModelPostConditionInput): Post
  deletePost(input: DeletePostInput!, condition: ModelPostConditionInput): Post
  createComment(input: CreateCommentInput!, condition: ModelCommentConditionInput): Comment
  updateComment(input: UpdateCommentInput!, condition: ModelCommentConditionInput): Comment
  deleteComment(input: DeleteCommentInput!, condition: ModelCommentConditionInput): Comment
  createUser(input: CreateUserInput!, condition: ModelUserConditionInput): User
  updateUser(input: UpdateUserInput!, condition: ModelUserConditionInput): User
  deleteUser(input: DeleteUserInput!, condition: ModelUserConditionInput): User
  createTag(input: CreateTagInput!, condition: ModelTagConditionInput): Tag
  updateTag(input: UpdateTagInput!, condition: ModelTagConditionInput): Tag
  deleteTag(input: DeleteTagInput!, condition: ModelTagConditionInput): Tag
  createOnBoardEventTags(input: CreateOnBoardEventTagsInput!, condition: ModelOnBoardEventTagsConditionInput): OnBoardEventTags
  updateOnBoardEventTags(input: UpdateOnBoardEventTagsInput!, condition: ModelOnBoardEventTagsConditionInput): OnBoardEventTags
  deleteOnBoardEventTags(input: DeleteOnBoardEventTagsInput!, condition: ModelOnBoardEventTagsConditionInput): OnBoardEventTags
  createOnBoardEventUsers(input: CreateOnBoardEventUsersInput!, condition: ModelOnBoardEventUsersConditionInput): OnBoardEventUsers
  updateOnBoardEventUsers(input: UpdateOnBoardEventUsersInput!, condition: ModelOnBoardEventUsersConditionInput): OnBoardEventUsers
  deleteOnBoardEventUsers(input: DeleteOnBoardEventUsersInput!, condition: ModelOnBoardEventUsersConditionInput): OnBoardEventUsers
}

input ModelSubscriptionAttendeesFilterInput {
  id: ModelSubscriptionIDInput
  created: ModelSubscriptionStringInput
  updated: ModelSubscriptionStringInput
  and: [ModelSubscriptionAttendeesFilterInput]
  or: [ModelSubscriptionAttendeesFilterInput]
  _deleted: ModelBooleanInput
}

type Subscription {
  onCreateUser(filter: ModelSubscriptionUserFilterInput, owner: String): User @aws_subscribe(mutations: ["createUser"])
  onUpdateUser(filter: ModelSubscriptionUserFilterInput, owner: String): User @aws_subscribe(mutations: ["updateUser"])
  onDeleteUser(filter: ModelSubscriptionUserFilterInput, owner: String): User @aws_subscribe(mutations: ["deleteUser"])
  onCreateAttendees(filter: ModelSubscriptionAttendeesFilterInput, owner: String): Attendees @aws_subscribe(mutations: ["createAttendees"]) @aws_api_key @aws_cognito_user_pools
  onUpdateAttendees(filter: ModelSubscriptionAttendeesFilterInput, owner: String): Attendees @aws_subscribe(mutations: ["updateAttendees"]) @aws_api_key @aws_cognito_user_pools
  onDeleteAttendees(filter: ModelSubscriptionAttendeesFilterInput, owner: String): Attendees @aws_subscribe(mutations: ["deleteAttendees"]) @aws_api_key @aws_cognito_user_pools
  onCreateTimes(filter: ModelSubscriptionTimesFilterInput, owner: String): Times @aws_subscribe(mutations: ["createTimes"]) @aws_api_key @aws_cognito_user_pools
  onUpdateTimes(filter: ModelSubscriptionTimesFilterInput, owner: String): Times @aws_subscribe(mutations: ["updateTimes"]) @aws_api_key @aws_cognito_user_pools
  onDeleteTimes(filter: ModelSubscriptionTimesFilterInput, owner: String): Times @aws_subscribe(mutations: ["deleteTimes"]) @aws_api_key @aws_cognito_user_pools
  onCreateOnBoardEvent(filter: ModelSubscriptionOnBoardEventFilterInput, owner: String): OnBoardEvent @aws_subscribe(mutations: ["createOnBoardEvent"]) @aws_api_key @aws_cognito_user_pools
  onUpdateOnBoardEvent(filter: ModelSubscriptionOnBoardEventFilterInput, owner: String): OnBoardEvent @aws_subscribe(mutations: ["updateOnBoardEvent"]) @aws_api_key @aws_cognito_user_pools
  onDeleteOnBoardEvent(filter: ModelSubscriptionOnBoardEventFilterInput, owner: String): OnBoardEvent @aws_subscribe(mutations: ["deleteOnBoardEvent"]) @aws_api_key @aws_cognito_user_pools
  onCreatePost(filter: ModelSubscriptionPostFilterInput, owner: String): Post @aws_subscribe(mutations: ["createPost"]) @aws_api_key @aws_cognito_user_pools
  onUpdatePost(filter: ModelSubscriptionPostFilterInput, owner: String): Post @aws_subscribe(mutations: ["updatePost"]) @aws_api_key @aws_cognito_user_pools
  onDeletePost(filter: ModelSubscriptionPostFilterInput, owner: String): Post @aws_subscribe(mutations: ["deletePost"]) @aws_api_key @aws_cognito_user_pools
  onCreateComment(filter: ModelSubscriptionCommentFilterInput, owner: String): Comment @aws_subscribe(mutations: ["createComment"]) @aws_api_key @aws_cognito_user_pools
  onUpdateComment(filter: ModelSubscriptionCommentFilterInput, owner: String): Comment @aws_subscribe(mutations: ["updateComment"]) @aws_api_key @aws_cognito_user_pools
  onDeleteComment(filter: ModelSubscriptionCommentFilterInput, owner: String): Comment @aws_subscribe(mutations: ["deleteComment"]) @aws_api_key @aws_cognito_user_pools
  onCreateTag(filter: ModelSubscriptionTagFilterInput, owner: String): Tag @aws_subscribe(mutations: ["createTag"]) @aws_api_key @aws_cognito_user_pools
  onUpdateTag(filter: ModelSubscriptionTagFilterInput, owner: String): Tag @aws_subscribe(mutations: ["updateTag"]) @aws_api_key @aws_cognito_user_pools
  onDeleteTag(filter: ModelSubscriptionTagFilterInput, owner: String): Tag @aws_subscribe(mutations: ["deleteTag"]) @aws_api_key @aws_cognito_user_pools
  onCreateOnBoardEventTags(filter: ModelSubscriptionOnBoardEventTagsFilterInput, owner: String): OnBoardEventTags @aws_subscribe(mutations: ["createOnBoardEventTags"]) @aws_api_key @aws_cognito_user_pools
  onUpdateOnBoardEventTags(filter: ModelSubscriptionOnBoardEventTagsFilterInput, owner: String): OnBoardEventTags @aws_subscribe(mutations: ["updateOnBoardEventTags"]) @aws_api_key @aws_cognito_user_pools
  onDeleteOnBoardEventTags(filter: ModelSubscriptionOnBoardEventTagsFilterInput, owner: String): OnBoardEventTags @aws_subscribe(mutations: ["deleteOnBoardEventTags"]) @aws_api_key @aws_cognito_user_pools
  onCreateOnBoardEventUsers(filter: ModelSubscriptionOnBoardEventUsersFilterInput, owner: String): OnBoardEventUsers @aws_subscribe(mutations: ["createOnBoardEventUsers"]) @aws_api_key @aws_cognito_user_pools
  onUpdateOnBoardEventUsers(filter: ModelSubscriptionOnBoardEventUsersFilterInput, owner: String): OnBoardEventUsers @aws_subscribe(mutations: ["updateOnBoardEventUsers"]) @aws_api_key @aws_cognito_user_pools
  onDeleteOnBoardEventUsers(filter: ModelSubscriptionOnBoardEventUsersFilterInput, owner: String): OnBoardEventUsers @aws_subscribe(mutations: ["deleteOnBoardEventUsers"]) @aws_api_key @aws_cognito_user_pools
}

input TTimeInput {
  day: EDayOfWeek!
  attendees: [TAttendeeInput!]!
}

type ModelTimesConnection @aws_api_key @aws_cognito_user_pools {
  items: [Times]!
  nextToken: String
  startedAt: AWSTimestamp
}

input ModelTimesFilterInput {
  id: ModelIDInput
  created: ModelStringInput
  updated: ModelStringInput
  and: [ModelTimesFilterInput]
  or: [ModelTimesFilterInput]
  not: ModelTimesFilterInput
  _deleted: ModelBooleanInput
  onBoardEventTimesId: ModelIDInput
  timesUserId: ModelIDInput
}

input ModelTimesConditionInput {
  created: ModelStringInput
  updated: ModelStringInput
  and: [ModelTimesConditionInput]
  or: [ModelTimesConditionInput]
  not: ModelTimesConditionInput
  _deleted: ModelBooleanInput
  onBoardEventTimesId: ModelIDInput
  timesUserId: ModelIDInput
}

input CreateTimesInput {
  id: ID
  created: AWSDateTime
  updated: AWSDateTime
  times: [TTimeInput!]!
  _version: Int
  onBoardEventTimesId: ID
  timesUserId: ID!
}

input UpdateTimesInput {
  id: ID!
  created: AWSDateTime
  updated: AWSDateTime
  times: [TTimeInput!]
  _version: Int
  onBoardEventTimesId: ID
  timesUserId: ID
}

input DeleteTimesInput {
  id: ID!
  _version: Int
}

input ModelSubscriptionTimesFilterInput {
  id: ModelSubscriptionIDInput
  created: ModelSubscriptionStringInput
  updated: ModelSubscriptionStringInput
  and: [ModelSubscriptionTimesFilterInput]
  or: [ModelSubscriptionTimesFilterInput]
  _deleted: ModelBooleanInput
}

input TCalendarDateTimeInput {
  dateTime: AWSDateTime
  timeZone: String
}

type ModelOnBoardEventConnection @aws_api_key @aws_cognito_user_pools {
  items: [OnBoardEvent]!
  nextToken: String
  startedAt: AWSTimestamp
}

input ModelEActivityInput {
  eq: EActivity
  ne: EActivity
}

input ModelEEventTypeInput {
  eq: EEventType
  ne: EEventType
}

input ModelEWhichWayInput {
  eq: EWhichWay
  ne: EWhichWay
}

input ModelELifeCycleInput {
  eq: ELifeCycle
  ne: ELifeCycle
}

input ModelOnBoardEventFilterInput {
  id: ModelIDInput
  created: ModelStringInput
  updated: ModelStringInput
  description: ModelStringInput
  name: ModelStringInput
  activity: ModelEActivityInput
  eventType: ModelEEventTypeInput
  whichWay: ModelEWhichWayInput
  lifeCycle: ModelELifeCycleInput
  reoccur: ModelBooleanInput
  startDate: ModelStringInput
  endDate: ModelStringInput
  recurrence: ModelStringInput
  and: [ModelOnBoardEventFilterInput]
  or: [ModelOnBoardEventFilterInput]
  not: ModelOnBoardEventFilterInput
  _deleted: ModelBooleanInput
  onBoardEventCreatorId: ModelIDInput
  onBoardEventOrganizerId: ModelIDInput
}

input ModelOnBoardEventConditionInput {
  created: ModelStringInput
  updated: ModelStringInput
  description: ModelStringInput
  name: ModelStringInput
  activity: ModelEActivityInput
  eventType: ModelEEventTypeInput
  whichWay: ModelEWhichWayInput
  lifeCycle: ModelELifeCycleInput
  reoccur: ModelBooleanInput
  startDate: ModelStringInput
  endDate: ModelStringInput
  recurrence: ModelStringInput
  and: [ModelOnBoardEventConditionInput]
  or: [ModelOnBoardEventConditionInput]
  not: ModelOnBoardEventConditionInput
  _deleted: ModelBooleanInput
  onBoardEventCreatorId: ModelIDInput
  onBoardEventOrganizerId: ModelIDInput
}

input CreateOnBoardEventInput {
  id: ID
  created: AWSDateTime
  updated: AWSDateTime
  description: String!
  name: String!
  activity: EActivity!
  eventType: EEventType!
  whichWay: EWhichWay!
  lifeCycle: ELifeCycle!
  reoccur: Boolean
  startDate: AWSDateTime
  endDate: AWSDateTime
  end: TCalendarDateTimeInput
  start: TCalendarDateTimeInput
  recurrence: String
  _version: Int
  onBoardEventCreatorId: ID!
  onBoardEventOrganizerId: ID
}

input UpdateOnBoardEventInput {
  id: ID!
  created: AWSDateTime
  updated: AWSDateTime
  description: String
  name: String
  activity: EActivity
  eventType: EEventType
  whichWay: EWhichWay
  lifeCycle: ELifeCycle
  reoccur: Boolean
  startDate: AWSDateTime
  endDate: AWSDateTime
  end: TCalendarDateTimeInput
  start: TCalendarDateTimeInput
  recurrence: String
  _version: Int
  onBoardEventCreatorId: ID
  onBoardEventOrganizerId: ID
}

input DeleteOnBoardEventInput {
  id: ID!
  _version: Int
}

input ModelSubscriptionOnBoardEventFilterInput {
  id: ModelSubscriptionIDInput
  created: ModelSubscriptionStringInput
  updated: ModelSubscriptionStringInput
  description: ModelSubscriptionStringInput
  name: ModelSubscriptionStringInput
  activity: ModelSubscriptionStringInput
  eventType: ModelSubscriptionStringInput
  whichWay: ModelSubscriptionStringInput
  lifeCycle: ModelSubscriptionStringInput
  reoccur: ModelSubscriptionBooleanInput
  startDate: ModelSubscriptionStringInput
  endDate: ModelSubscriptionStringInput
  recurrence: ModelSubscriptionStringInput
  and: [ModelSubscriptionOnBoardEventFilterInput]
  or: [ModelSubscriptionOnBoardEventFilterInput]
  _deleted: ModelBooleanInput
}

type ModelPostConnection @aws_api_key @aws_cognito_user_pools {
  items: [Post]!
  nextToken: String
  startedAt: AWSTimestamp
}

input ModelEPostStatusInput {
  eq: EPostStatus
  ne: EPostStatus
}

input ModelPostFilterInput {
  id: ModelIDInput
  created: ModelStringInput
  updated: ModelStringInput
  name: ModelStringInput
  poststatus: ModelEPostStatusInput
  and: [ModelPostFilterInput]
  or: [ModelPostFilterInput]
  not: ModelPostFilterInput
  _deleted: ModelBooleanInput
  userPostsId: ModelIDInput
}

input ModelPostConditionInput {
  created: ModelStringInput
  updated: ModelStringInput
  name: ModelStringInput
  poststatus: ModelEPostStatusInput
  and: [ModelPostConditionInput]
  or: [ModelPostConditionInput]
  not: ModelPostConditionInput
  _deleted: ModelBooleanInput
  userPostsId: ModelIDInput
}

input CreatePostInput {
  id: ID
  created: AWSDateTime
  updated: AWSDateTime
  name: String
  poststatus: EPostStatus
  _version: Int
  userPostsId: ID
}

input UpdatePostInput {
  id: ID!
  created: AWSDateTime
  updated: AWSDateTime
  name: String
  poststatus: EPostStatus
  _version: Int
  userPostsId: ID
}

input DeletePostInput {
  id: ID!
  _version: Int
}

input ModelSubscriptionPostFilterInput {
  id: ModelSubscriptionIDInput
  created: ModelSubscriptionStringInput
  updated: ModelSubscriptionStringInput
  name: ModelSubscriptionStringInput
  poststatus: ModelSubscriptionStringInput
  and: [ModelSubscriptionPostFilterInput]
  or: [ModelSubscriptionPostFilterInput]
  _deleted: ModelBooleanInput
}

type ModelCommentConnection @aws_api_key @aws_cognito_user_pools {
  items: [Comment]!
  nextToken: String
  startedAt: AWSTimestamp
}

input ModelCommentFilterInput {
  id: ModelIDInput
  content: ModelStringInput
  and: [ModelCommentFilterInput]
  or: [ModelCommentFilterInput]
  not: ModelCommentFilterInput
  _deleted: ModelBooleanInput
  postCommentsId: ModelIDInput
}

input ModelCommentConditionInput {
  content: ModelStringInput
  and: [ModelCommentConditionInput]
  or: [ModelCommentConditionInput]
  not: ModelCommentConditionInput
  _deleted: ModelBooleanInput
  postCommentsId: ModelIDInput
}

input CreateCommentInput {
  id: ID
  content: String!
  _version: Int
  postCommentsId: ID
}

input UpdateCommentInput {
  id: ID!
  content: String
  _version: Int
  postCommentsId: ID
}

input DeleteCommentInput {
  id: ID!
  _version: Int
}

input ModelSubscriptionCommentFilterInput {
  id: ModelSubscriptionIDInput
  content: ModelSubscriptionStringInput
  and: [ModelSubscriptionCommentFilterInput]
  or: [ModelSubscriptionCommentFilterInput]
  _deleted: ModelBooleanInput
}

type ModelUserConnection {
  items: [User]!
  nextToken: String
  startedAt: AWSTimestamp
}

input ModelUserFilterInput {
  id: ModelIDInput
  created: ModelStringInput
  updated: ModelStringInput
  username: ModelStringInput
  displayName: ModelStringInput
  firstName: ModelStringInput
  lastName: ModelStringInput
  email: ModelStringInput
  phoneNumber: ModelStringInput
  and: [ModelUserFilterInput]
  or: [ModelUserFilterInput]
  not: ModelUserFilterInput
  _deleted: ModelBooleanInput
}

input ModelUserConditionInput {
  created: ModelStringInput
  updated: ModelStringInput
  username: ModelStringInput
  displayName: ModelStringInput
  firstName: ModelStringInput
  lastName: ModelStringInput
  email: ModelStringInput
  phoneNumber: ModelStringInput
  and: [ModelUserConditionInput]
  or: [ModelUserConditionInput]
  not: ModelUserConditionInput
  _deleted: ModelBooleanInput
}

input CreateUserInput {
  id: ID
  created: AWSDateTime
  updated: AWSDateTime
  username: String!
  displayName: String
  firstName: String!
  lastName: String!
  email: AWSEmail!
  phoneNumber: AWSPhone
  _version: Int
}

input UpdateUserInput {
  id: ID!
  created: AWSDateTime
  updated: AWSDateTime
  username: String
  displayName: String
  firstName: String
  lastName: String
  email: AWSEmail
  phoneNumber: AWSPhone
  _version: Int
}

input DeleteUserInput {
  id: ID!
  _version: Int
}

input ModelSubscriptionUserFilterInput {
  id: ModelSubscriptionIDInput
  created: ModelSubscriptionStringInput
  updated: ModelSubscriptionStringInput
  username: ModelSubscriptionStringInput
  displayName: ModelSubscriptionStringInput
  firstName: ModelSubscriptionStringInput
  lastName: ModelSubscriptionStringInput
  email: ModelSubscriptionStringInput
  phoneNumber: ModelSubscriptionStringInput
  and: [ModelSubscriptionUserFilterInput]
  or: [ModelSubscriptionUserFilterInput]
  _deleted: ModelBooleanInput
}

type ModelTagConnection @aws_api_key @aws_cognito_user_pools {
  items: [Tag]!
  nextToken: String
  startedAt: AWSTimestamp
}

input ModelTagFilterInput {
  id: ModelIDInput
  created: ModelStringInput
  updated: ModelStringInput
  name: ModelStringInput
  and: [ModelTagFilterInput]
  or: [ModelTagFilterInput]
  not: ModelTagFilterInput
  _deleted: ModelBooleanInput
}

input ModelTagConditionInput {
  created: ModelStringInput
  updated: ModelStringInput
  name: ModelStringInput
  and: [ModelTagConditionInput]
  or: [ModelTagConditionInput]
  not: ModelTagConditionInput
  _deleted: ModelBooleanInput
}

input CreateTagInput {
  id: ID
  created: AWSDateTime
  updated: AWSDateTime
  name: String!
  _version: Int
}

input UpdateTagInput {
  id: ID!
  created: AWSDateTime
  updated: AWSDateTime
  name: String
  _version: Int
}

input DeleteTagInput {
  id: ID!
  _version: Int
}

input ModelSubscriptionTagFilterInput {
  id: ModelSubscriptionIDInput
  created: ModelSubscriptionStringInput
  updated: ModelSubscriptionStringInput
  name: ModelSubscriptionStringInput
  and: [ModelSubscriptionTagFilterInput]
  or: [ModelSubscriptionTagFilterInput]
  _deleted: ModelBooleanInput
}

type ModelOnBoardEventTagsConnection @aws_api_key @aws_cognito_user_pools {
  items: [OnBoardEventTags]!
  nextToken: String
  startedAt: AWSTimestamp
}

input ModelOnBoardEventTagsFilterInput {
  id: ModelIDInput
  onBoardEventId: ModelIDInput
  tagId: ModelIDInput
  and: [ModelOnBoardEventTagsFilterInput]
  or: [ModelOnBoardEventTagsFilterInput]
  not: ModelOnBoardEventTagsFilterInput
  _deleted: ModelBooleanInput
}

input ModelOnBoardEventTagsConditionInput {
  onBoardEventId: ModelIDInput
  tagId: ModelIDInput
  and: [ModelOnBoardEventTagsConditionInput]
  or: [ModelOnBoardEventTagsConditionInput]
  not: ModelOnBoardEventTagsConditionInput
  _deleted: ModelBooleanInput
}

input CreateOnBoardEventTagsInput {
  id: ID
  onBoardEventId: ID!
  tagId: ID!
  _version: Int
}

input UpdateOnBoardEventTagsInput {
  id: ID!
  onBoardEventId: ID
  tagId: ID
  _version: Int
}

input DeleteOnBoardEventTagsInput {
  id: ID!
  _version: Int
}

input ModelSubscriptionOnBoardEventTagsFilterInput {
  id: ModelSubscriptionIDInput
  onBoardEventId: ModelSubscriptionIDInput
  tagId: ModelSubscriptionIDInput
  and: [ModelSubscriptionOnBoardEventTagsFilterInput]
  or: [ModelSubscriptionOnBoardEventTagsFilterInput]
  _deleted: ModelBooleanInput
}

type ModelOnBoardEventUsersConnection @aws_api_key @aws_cognito_user_pools {
  items: [OnBoardEventUsers]!
  nextToken: String
  startedAt: AWSTimestamp
}

input ModelOnBoardEventUsersFilterInput {
  id: ModelIDInput
  onBoardEventId: ModelIDInput
  userId: ModelIDInput
  and: [ModelOnBoardEventUsersFilterInput]
  or: [ModelOnBoardEventUsersFilterInput]
  not: ModelOnBoardEventUsersFilterInput
  _deleted: ModelBooleanInput
}

input ModelOnBoardEventUsersConditionInput {
  onBoardEventId: ModelIDInput
  userId: ModelIDInput
  and: [ModelOnBoardEventUsersConditionInput]
  or: [ModelOnBoardEventUsersConditionInput]
  not: ModelOnBoardEventUsersConditionInput
  _deleted: ModelBooleanInput
}

input CreateOnBoardEventUsersInput {
  id: ID
  onBoardEventId: ID!
  userId: ID!
  _version: Int
}

input UpdateOnBoardEventUsersInput {
  id: ID!
  onBoardEventId: ID
  userId: ID
  _version: Int
}

input DeleteOnBoardEventUsersInput {
  id: ID!
  _version: Int
}

input ModelSubscriptionOnBoardEventUsersFilterInput {
  id: ModelSubscriptionIDInput
  onBoardEventId: ModelSubscriptionIDInput
  userId: ModelSubscriptionIDInput
  and: [ModelSubscriptionOnBoardEventUsersFilterInput]
  or: [ModelSubscriptionOnBoardEventUsersFilterInput]
  _deleted: ModelBooleanInput
}
