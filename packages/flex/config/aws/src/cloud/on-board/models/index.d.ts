import { ModelInit, MutableModel, __modelMeta__, ManagedIdentifier } from '@aws-amplify/datastore'
// @ts-ignore
import { LazyLoading, LazyLoadingDisabled, AsyncItem, AsyncCollection } from '@aws-amplify/datastore'

export enum EActivity {
  AUTRE = 'AUTRE',
  BABYSITTING = 'BABYSITTING',
  COVOITURAGE = 'COVOITURAGE',
  PEDIBUS = 'PEDIBUS',
  SOUTIEN_DEVOIRS = 'SOUTIEN_DEVOIRS',
}

export enum EDayOfWeek {
  FRIDAY = 'FRIDAY',
  MONDAY = 'MONDAY',
  SATURDAY = 'SATURDAY',
  SUNDAY = 'SUNDAY',
  THURSDAY = 'THURSDAY',
  TUESDAY = 'TUESDAY',
  WEDNESDAY = 'WEDNESDAY',
}

export enum EEventType {
  PROPOSAL = 'PROPOSAL',
  REQUEST = 'REQUEST',
}

export enum ELifeCycle {
  ALL_YEAR = 'ALL_YEAR',
  DATE_RANGE = 'DATE_RANGE',
}

export enum EPostStatus {
  ERROR = 'ERROR',
  PENDING = 'PENDING',
  SUCCESS = 'SUCCESS',
}

export enum ESchool {
  BEL_AIR = 'BEL_AIR',
  FERDINAND_BUISSON = 'FERDINAND_BUISSON',
  LA_SOURCE = 'LA_SOURCE',
  MARITAIN_RENAN = 'MARITAIN_RENAN',
  NOTRE_DAME = 'NOTRE_DAME',
}

export enum ESchoolLevel {
  COLLEGE_3_EME = 'COLLEGE_3EME',
  COLLEGE_4_EME = 'COLLEGE_4EME',
  COLLEGE_5_EME = 'COLLEGE_5EME',
  COLLEGE_6_EME = 'COLLEGE_6EME',
  LYCEE_PREMIERE = 'LYCEE_PREMIERE',
  LYCEE_SECONDE = 'LYCEE_SECONDE',
  LYCEE_TERMINALE = 'LYCEE_TERMINALE',
  MATERNELLE_GS = 'MATERNELLE_GS',
  PRIMAIRE_CE1 = 'PRIMAIRE_CE1',
  PRIMAIRE_CE2 = 'PRIMAIRE_CE2',
  PRIMAIRE_CM1 = 'PRIMAIRE_CM1',
  PRIMAIRE_CM2 = 'PRIMAIRE_CM2',
  PRIMAIRE_CP = 'PRIMAIRE_CP',
}

export enum EStatus {
  ACCEPTED = 'ACCEPTED',
  DECLINED = 'DECLINED',
  NEEDSACTION = 'NEEDSACTION',
  TENTATIVE = 'TENTATIVE',
}

export enum EWhichWay {
  AT_HOME = 'AT_HOME',
  AT_SCHOOL = 'AT_SCHOOL',
  FROM_SCHOOL = 'FROM_SCHOOL',
  TO_SCHOOL = 'TO_SCHOOL',
}

export enum EAttendeeRole {
  PARTICIPANT = 'PARTICIPANT',
  RESPONSABLE = 'RESPONSABLE',
}

type EagerTAddress = {
  readonly city?: string | null
  readonly itinerary?: TPointList | null
  readonly location?: TPoint | null
  readonly postcode?: string | null
  readonly street1?: string | null
  readonly street2?: string | null
  readonly additional?: string | null
}

type LazyTAddress = {
  readonly city?: string | null
  readonly itinerary?: TPointList | null
  readonly location?: TPoint | null
  readonly postcode?: string | null
  readonly street1?: string | null
  readonly street2?: string | null
  readonly additional?: string | null
}

export declare type TAddress = LazyLoading extends LazyLoadingDisabled ? EagerTAddress : LazyTAddress

export declare const TAddress: new (init: ModelInit<TAddress>) => TAddress

type EagerTCalendarDateTime = {
  readonly dateTime?: string | null
  readonly timeZone?: string | null
}

type LazyTCalendarDateTime = {
  readonly dateTime?: string | null
  readonly timeZone?: string | null
}

export declare type TCalendarDateTime = LazyLoading extends LazyLoadingDisabled ? EagerTCalendarDateTime : LazyTCalendarDateTime

export declare const TCalendarDateTime: new (init: ModelInit<TCalendarDateTime>) => TCalendarDateTime

type EagerTPoint = {
  readonly latitude: number
  readonly longitude: number
}

type LazyTPoint = {
  readonly latitude: number
  readonly longitude: number
}

export declare type TPoint = LazyLoading extends LazyLoadingDisabled ? EagerTPoint : LazyTPoint

export declare const TPoint: new (init: ModelInit<TPoint>) => TPoint

type EagerTPointList = {
  readonly points: TPoint[]
}

type LazyTPointList = {
  readonly points: TPoint[]
}

export declare type TPointList = LazyLoading extends LazyLoadingDisabled ? EagerTPointList : LazyTPointList

export declare const TPointList: new (init: ModelInit<TPointList>) => TPointList

type EagerTAttendee = {
  readonly role?: EAttendeeRole | keyof typeof EAttendeeRole | null
  readonly address?: TAddress | null
  readonly displayName?: string | null
  readonly firstName?: string | null
  readonly lastName?: string | null
  readonly school?: ESchool | keyof typeof ESchool | null
  readonly schoolLevel?: ESchoolLevel | keyof typeof ESchoolLevel | null
  readonly organizer?: boolean | null
  readonly status?: EStatus | keyof typeof EStatus | null
  readonly self?: boolean | null
  readonly endParticipation?: string | null
  readonly startParticipation?: string | null
}

type LazyTAttendee = {
  readonly role?: EAttendeeRole | keyof typeof EAttendeeRole | null
  readonly address?: TAddress | null
  readonly displayName?: string | null
  readonly firstName?: string | null
  readonly lastName?: string | null
  readonly school?: ESchool | keyof typeof ESchool | null
  readonly schoolLevel?: ESchoolLevel | keyof typeof ESchoolLevel | null
  readonly organizer?: boolean | null
  readonly status?: EStatus | keyof typeof EStatus | null
  readonly self?: boolean | null
  readonly endParticipation?: string | null
  readonly startParticipation?: string | null
}

export declare type TAttendee = LazyLoading extends LazyLoadingDisabled ? EagerTAttendee : LazyTAttendee

export declare const TAttendee: new (init: ModelInit<TAttendee>) => TAttendee

type EagerTTime = {
  readonly day: EDayOfWeek | keyof typeof EDayOfWeek
  readonly attendees: TAttendee[]
}

type LazyTTime = {
  readonly day: EDayOfWeek | keyof typeof EDayOfWeek
  readonly attendees: TAttendee[]
}

export declare type TTime = LazyLoading extends LazyLoadingDisabled ? EagerTTime : LazyTTime

export declare const TTime: new (init: ModelInit<TTime>) => TTime

type EagerAttendees = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Attendees, 'id'>
  }
  readonly id: string
  readonly created?: string | null
  readonly updated?: string | null
  readonly attendees: TAttendee[]
  readonly event?: OnBoardEvent | null
  readonly user: User
  readonly onBoardEventAttendeesId?: string | null
  readonly attendeesUserId: string
  readonly timesAttendeesId?: string | null
}

type LazyAttendees = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Attendees, 'id'>
  }
  readonly id: string
  readonly created?: string | null
  readonly updated?: string | null
  readonly attendees: TAttendee[]
  readonly event: AsyncItem<OnBoardEvent | undefined>
  readonly user: AsyncItem<User>
  readonly onBoardEventAttendeesId?: string | null
  readonly attendeesUserId: string
  readonly timesAttendeesId?: string | null
}

export declare type Attendees = LazyLoading extends LazyLoadingDisabled ? EagerAttendees : LazyAttendees

export declare const Attendees: (new (init: ModelInit<Attendees>) => Attendees) & {
  copyOf(source: Attendees, mutator: (draft: MutableModel<Attendees>) => MutableModel<Attendees> | void): Attendees
}

type EagerTimes = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Times, 'id'>
  }
  readonly id: string
  readonly created?: string | null
  readonly updated?: string | null
  readonly times: TTime[]
  readonly attendees?: (Attendees | null)[] | null
  readonly event?: OnBoardEvent | null
  readonly user: User
  readonly onBoardEventTimesId?: string | null
  readonly timesUserId: string
}

type LazyTimes = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Times, 'id'>
  }
  readonly id: string
  readonly created?: string | null
  readonly updated?: string | null
  readonly times: TTime[]
  readonly attendees: AsyncCollection<Attendees>
  readonly event: AsyncItem<OnBoardEvent | undefined>
  readonly user: AsyncItem<User>
  readonly onBoardEventTimesId?: string | null
  readonly timesUserId: string
}

export declare type Times = LazyLoading extends LazyLoadingDisabled ? EagerTimes : LazyTimes

export declare const Times: (new (init: ModelInit<Times>) => Times) & {
  copyOf(source: Times, mutator: (draft: MutableModel<Times>) => MutableModel<Times> | void): Times
}

type EagerOnBoardEvent = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<OnBoardEvent, 'id'>
  }
  readonly id: string
  readonly created?: string | null
  readonly updated?: string | null
  readonly description: string
  readonly name: string
  readonly activity: EActivity | keyof typeof EActivity
  readonly eventType: EEventType | keyof typeof EEventType
  readonly whichWay: EWhichWay | keyof typeof EWhichWay
  readonly lifeCycle: ELifeCycle | keyof typeof ELifeCycle
  readonly reoccur?: boolean | null
  readonly startDate?: string | null
  readonly endDate?: string | null
  readonly end?: TCalendarDateTime | null
  readonly start?: TCalendarDateTime | null
  readonly recurrence?: string | null
  readonly attendees?: (Attendees | null)[] | null
  readonly times?: (Times | null)[] | null
  readonly tags?: (OnBoardEventTags | null)[] | null
  readonly creator: User
  readonly organizer?: User | null
  readonly users?: (OnBoardEventUsers | null)[] | null
  readonly onBoardEventCreatorId: string
  readonly onBoardEventOrganizerId?: string | null
}

type LazyOnBoardEvent = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<OnBoardEvent, 'id'>
  }
  readonly id: string
  readonly created?: string | null
  readonly updated?: string | null
  readonly description: string
  readonly name: string
  readonly activity: EActivity | keyof typeof EActivity
  readonly eventType: EEventType | keyof typeof EEventType
  readonly whichWay: EWhichWay | keyof typeof EWhichWay
  readonly lifeCycle: ELifeCycle | keyof typeof ELifeCycle
  readonly reoccur?: boolean | null
  readonly startDate?: string | null
  readonly endDate?: string | null
  readonly end?: TCalendarDateTime | null
  readonly start?: TCalendarDateTime | null
  readonly recurrence?: string | null
  readonly attendees: AsyncCollection<Attendees>
  readonly times: AsyncCollection<Times>
  readonly tags: AsyncCollection<OnBoardEventTags>
  readonly creator: AsyncItem<User>
  readonly organizer: AsyncItem<User | undefined>
  readonly users: AsyncCollection<OnBoardEventUsers>
  readonly onBoardEventCreatorId: string
  readonly onBoardEventOrganizerId?: string | null
}

export declare type OnBoardEvent = LazyLoading extends LazyLoadingDisabled ? EagerOnBoardEvent : LazyOnBoardEvent

export declare const OnBoardEvent: (new (init: ModelInit<OnBoardEvent>) => OnBoardEvent) & {
  copyOf(source: OnBoardEvent, mutator: (draft: MutableModel<OnBoardEvent>) => MutableModel<OnBoardEvent> | void): OnBoardEvent
}

type EagerPost = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Post, 'id'>
  }
  readonly id: string
  readonly created?: string | null
  readonly updated?: string | null
  readonly name?: string | null
  readonly poststatus?: EPostStatus | keyof typeof EPostStatus | null
  readonly comments?: (Comment | null)[] | null
  readonly user: User
  readonly userPostsId: string
}

type LazyPost = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Post, 'id'>
  }
  readonly id: string
  readonly created?: string | null
  readonly updated?: string | null
  readonly name?: string | null
  readonly poststatus?: EPostStatus | keyof typeof EPostStatus | null
  readonly comments: AsyncCollection<Comment>
  readonly user: AsyncItem<User>
  readonly userPostsId: string
}

export declare type Post = LazyLoading extends LazyLoadingDisabled ? EagerPost : LazyPost

export declare const Post: (new (init: ModelInit<Post>) => Post) & {
  copyOf(source: Post, mutator: (draft: MutableModel<Post>) => MutableModel<Post> | void): Post
}

type EagerComment = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Comment, 'id'>
    readOnlyFields: 'created' | 'updated'
  }
  readonly id: string
  readonly content: string
  readonly post?: Post | null
  readonly created?: string | null
  readonly updated?: string | null
  readonly postCommentsId?: string | null
}

type LazyComment = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Comment, 'id'>
    readOnlyFields: 'created' | 'updated'
  }
  readonly id: string
  readonly content: string
  readonly post: AsyncItem<Post | undefined>
  readonly created?: string | null
  readonly updated?: string | null
  readonly postCommentsId?: string | null
}

export declare type Comment = LazyLoading extends LazyLoadingDisabled ? EagerComment : LazyComment

export declare const Comment: (new (init: ModelInit<Comment>) => Comment) & {
  copyOf(source: Comment, mutator: (draft: MutableModel<Comment>) => MutableModel<Comment> | void): Comment
}

type EagerUser = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<User, 'id'>
  }
  readonly id: string
  readonly created?: string | null
  readonly updated?: string | null
  readonly username: string
  readonly displayName?: string | null
  readonly firstName: string
  readonly lastName: string
  readonly email: string
  readonly phoneNumber?: string | null
  readonly posts?: (Post | null)[] | null
  readonly events?: (OnBoardEventUsers | null)[] | null
}

type LazyUser = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<User, 'id'>
  }
  readonly id: string
  readonly created?: string | null
  readonly updated?: string | null
  readonly username: string
  readonly displayName?: string | null
  readonly firstName: string
  readonly lastName: string
  readonly email: string
  readonly phoneNumber?: string | null
  readonly posts: AsyncCollection<Post>
  readonly events: AsyncCollection<OnBoardEventUsers>
}

export declare type User = LazyLoading extends LazyLoadingDisabled ? EagerUser : LazyUser

export declare const User: (new (init: ModelInit<User>) => User) & {
  copyOf(source: User, mutator: (draft: MutableModel<User>) => MutableModel<User> | void): User
}

type EagerTag = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Tag, 'id'>
  }
  readonly id: string
  readonly created?: string | null
  readonly updated?: string | null
  readonly name: string
  readonly events?: (OnBoardEventTags | null)[] | null
}

type LazyTag = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Tag, 'id'>
  }
  readonly id: string
  readonly created?: string | null
  readonly updated?: string | null
  readonly name: string
  readonly events: AsyncCollection<OnBoardEventTags>
}

export declare type Tag = LazyLoading extends LazyLoadingDisabled ? EagerTag : LazyTag

export declare const Tag: (new (init: ModelInit<Tag>) => Tag) & {
  copyOf(source: Tag, mutator: (draft: MutableModel<Tag>) => MutableModel<Tag> | void): Tag
}

type EagerOnBoardEventTags = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<OnBoardEventTags, 'id'>
    readOnlyFields: 'createdAt' | 'updatedAt'
  }
  readonly id: string
  readonly onBoardEventId?: string | null
  readonly tagId?: string | null
  readonly onBoardEvent: OnBoardEvent
  readonly tag: Tag
  readonly createdAt?: string | null
  readonly updatedAt?: string | null
}

type LazyOnBoardEventTags = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<OnBoardEventTags, 'id'>
    readOnlyFields: 'createdAt' | 'updatedAt'
  }
  readonly id: string
  readonly onBoardEventId?: string | null
  readonly tagId?: string | null
  readonly onBoardEvent: AsyncItem<OnBoardEvent>
  readonly tag: AsyncItem<Tag>
  readonly createdAt?: string | null
  readonly updatedAt?: string | null
}

export declare type OnBoardEventTags = LazyLoading extends LazyLoadingDisabled ? EagerOnBoardEventTags : LazyOnBoardEventTags

export declare const OnBoardEventTags: (new (init: ModelInit<OnBoardEventTags>) => OnBoardEventTags) & {
  copyOf(source: OnBoardEventTags, mutator: (draft: MutableModel<OnBoardEventTags>) => MutableModel<OnBoardEventTags> | void): OnBoardEventTags
}

type EagerOnBoardEventUsers = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<OnBoardEventUsers, 'id'>
    readOnlyFields: 'createdAt' | 'updatedAt'
  }
  readonly id: string
  readonly onBoardEventId?: string | null
  readonly userId?: string | null
  readonly onBoardEvent: OnBoardEvent
  readonly user: User
  readonly createdAt?: string | null
  readonly updatedAt?: string | null
}

type LazyOnBoardEventUsers = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<OnBoardEventUsers, 'id'>
    readOnlyFields: 'createdAt' | 'updatedAt'
  }
  readonly id: string
  readonly onBoardEventId?: string | null
  readonly userId?: string | null
  readonly onBoardEvent: AsyncItem<OnBoardEvent>
  readonly user: AsyncItem<User>
  readonly createdAt?: string | null
  readonly updatedAt?: string | null
}

export declare type OnBoardEventUsers = LazyLoading extends LazyLoadingDisabled ? EagerOnBoardEventUsers : LazyOnBoardEventUsers

export declare const OnBoardEventUsers: (new (init: ModelInit<OnBoardEventUsers>) => OnBoardEventUsers) & {
  copyOf(source: OnBoardEventUsers, mutator: (draft: MutableModel<OnBoardEventUsers>) => MutableModel<OnBoardEventUsers> | void): OnBoardEventUsers
}
