// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema.js';

const EActivity = {
  "AUTRE": "AUTRE",
  "BABYSITTING": "BABYSITTING",
  "COVOITURAGE": "COVOITURAGE",
  "PEDIBUS": "PEDIBUS",
  "SOUTIEN_DEVOIRS": "SOUTIEN_DEVOIRS"
};

const EDayOfWeek = {
  "FRIDAY": "FRIDAY",
  "MONDAY": "MONDAY",
  "SATURDAY": "SATURDAY",
  "SUNDAY": "SUNDAY",
  "THURSDAY": "THURSDAY",
  "TUESDAY": "TUESDAY",
  "WEDNESDAY": "WEDNESDAY"
};

const EEventType = {
  "PROPOSAL": "PROPOSAL",
  "REQUEST": "REQUEST"
};

const ELifeCycle = {
  "ALL_YEAR": "ALL_YEAR",
  "DATE_RANGE": "DATE_RANGE"
};

const EPostStatus = {
  "ERROR": "ERROR",
  "PENDING": "PENDING",
  "SUCCESS": "SUCCESS"
};

const ESchool = {
  "BEL_AIR": "BEL_AIR",
  "FERDINAND_BUISSON": "FERDINAND_BUISSON",
  "LA_SOURCE": "LA_SOURCE",
  "MARITAIN_RENAN": "MARITAIN_RENAN",
  "NOTRE_DAME": "NOTRE_DAME"
};

const ESchoolLevel = {
  "COLLEGE_3_EME": "COLLEGE_3EME",
  "COLLEGE_4_EME": "COLLEGE_4EME",
  "COLLEGE_5_EME": "COLLEGE_5EME",
  "COLLEGE_6_EME": "COLLEGE_6EME",
  "LYCEE_PREMIERE": "LYCEE_PREMIERE",
  "LYCEE_SECONDE": "LYCEE_SECONDE",
  "LYCEE_TERMINALE": "LYCEE_TERMINALE",
  "MATERNELLE_GS": "MATERNELLE_GS",
  "PRIMAIRE_CE1": "PRIMAIRE_CE1",
  "PRIMAIRE_CE2": "PRIMAIRE_CE2",
  "PRIMAIRE_CM1": "PRIMAIRE_CM1",
  "PRIMAIRE_CM2": "PRIMAIRE_CM2",
  "PRIMAIRE_CP": "PRIMAIRE_CP"
};

const EStatus = {
  "ACCEPTED": "ACCEPTED",
  "DECLINED": "DECLINED",
  "NEEDSACTION": "NEEDSACTION",
  "TENTATIVE": "TENTATIVE"
};

const EWhichWay = {
  "AT_HOME": "AT_HOME",
  "AT_SCHOOL": "AT_SCHOOL",
  "FROM_SCHOOL": "FROM_SCHOOL",
  "TO_SCHOOL": "TO_SCHOOL"
};

const EAttendeeRole = {
  "PARTICIPANT": "PARTICIPANT",
  "RESPONSABLE": "RESPONSABLE"
};

const { Attendees, Times, OnBoardEvent, Post, Comment, User, Tag, OnBoardEventTags, OnBoardEventUsers, TAddress, TCalendarDateTime, TPoint, TPointList, TAttendee, TTime } = initSchema(schema);

export {
  Attendees,
  Times,
  OnBoardEvent,
  Post,
  Comment,
  User,
  Tag,
  OnBoardEventTags,
  OnBoardEventUsers,
  EActivity,
  EDayOfWeek,
  EEventType,
  ELifeCycle,
  EPostStatus,
  ESchool,
  ESchoolLevel,
  EStatus,
  EWhichWay,
  EAttendeeRole,
  TAddress,
  TCalendarDateTime,
  TPoint,
  TPointList,
  TAttendee,
  TTime
};
