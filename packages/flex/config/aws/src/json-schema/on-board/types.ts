import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql'
export type Maybe<T> = T | null
export type InputMaybe<T> = Maybe<T>
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] }
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> }
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> }
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never }
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never }
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string }
  String: { input: string; output: string }
  Boolean: { input: boolean; output: boolean }
  Int: { input: number; output: number }
  Float: { input: number; output: number }
  AWSDate: { input: any; output: any }
  AWSDateTime: { input: Date; output: Date }
  AWSEmail: { input: string; output: string }
  AWSIPAddress: { input: any; output: any }
  AWSJSON: { input: string; output: string }
  AWSPhone: { input: any; output: any }
  AWSTime: { input: any; output: any }
  AWSTimestamp: { input: string; output: string }
  AWSURL: { input: string; output: string }
  BigInt: { input: any; output: any }
  Double: { input: any; output: any }
}

export type Attendees = {
  __typename?: 'Attendees'
  attendees: Array<TAttendee>
  created?: Maybe<Scalars['AWSDateTime']['output']>
  event?: Maybe<OnBoardEvent>
  id: Scalars['ID']['output']
  updated?: Maybe<Scalars['AWSDateTime']['output']>
  user: User
}

export type Comment = {
  __typename?: 'Comment'
  content: Scalars['String']['output']
  id: Scalars['ID']['output']
  post?: Maybe<Post>
}

export enum EActivity {
  Autre = 'AUTRE',
  Babysitting = 'BABYSITTING',
  Covoiturage = 'COVOITURAGE',
  Pedibus = 'PEDIBUS',
  SoutienDevoirs = 'SOUTIEN_DEVOIRS',
}

export enum EAttendeeRole {
  Participant = 'PARTICIPANT',
  Responsable = 'RESPONSABLE',
}

export enum EDayOfWeek {
  Friday = 'FRIDAY',
  Monday = 'MONDAY',
  Saturday = 'SATURDAY',
  Sunday = 'SUNDAY',
  Thursday = 'THURSDAY',
  Tuesday = 'TUESDAY',
  Wednesday = 'WEDNESDAY',
}

export enum EEventType {
  Proposal = 'PROPOSAL',
  Request = 'REQUEST',
}

export enum ELifeCycle {
  AllYear = 'ALL_YEAR',
  DateRange = 'DATE_RANGE',
}

export enum EPostStatus {
  Error = 'ERROR',
  Pending = 'PENDING',
  Success = 'SUCCESS',
}

export enum ESchool {
  BelAir = 'BEL_AIR',
  FerdinandBuisson = 'FERDINAND_BUISSON',
  LaSource = 'LA_SOURCE',
  MaritainRenan = 'MARITAIN_RENAN',
  NotreDame = 'NOTRE_DAME',
}

export enum ESchoolLevel {
  College_3Eme = 'COLLEGE_3EME',
  College_4Eme = 'COLLEGE_4EME',
  College_5Eme = 'COLLEGE_5EME',
  College_6Eme = 'COLLEGE_6EME',
  LyceePremiere = 'LYCEE_PREMIERE',
  LyceeSeconde = 'LYCEE_SECONDE',
  LyceeTerminale = 'LYCEE_TERMINALE',
  MaternelleGs = 'MATERNELLE_GS',
  PrimaireCe1 = 'PRIMAIRE_CE1',
  PrimaireCe2 = 'PRIMAIRE_CE2',
  PrimaireCm1 = 'PRIMAIRE_CM1',
  PrimaireCm2 = 'PRIMAIRE_CM2',
  PrimaireCp = 'PRIMAIRE_CP',
}

export enum EStatus {
  Accepted = 'ACCEPTED',
  Declined = 'DECLINED',
  Needsaction = 'NEEDSACTION',
  Tentative = 'TENTATIVE',
}

export enum EWhichWay {
  AtHome = 'AT_HOME',
  AtSchool = 'AT_SCHOOL',
  FromSchool = 'FROM_SCHOOL',
  ToSchool = 'TO_SCHOOL',
}

export type IStudent = {
  firstName?: Maybe<Scalars['String']['output']>
  lastName?: Maybe<Scalars['String']['output']>
  school?: Maybe<ESchool>
  schoolLevel?: Maybe<ESchoolLevel>
}

export type OnBoardEvent = {
  __typename?: 'OnBoardEvent'
  activity: EActivity
  attendees?: Maybe<Array<Maybe<Attendees>>>
  created?: Maybe<Scalars['AWSDateTime']['output']>
  creator: User
  description: Scalars['String']['output']
  end?: Maybe<TCalendarDateTime>
  endDate?: Maybe<Scalars['AWSDateTime']['output']>
  eventType: EEventType
  id: Scalars['ID']['output']
  lifeCycle: ELifeCycle
  name: Scalars['String']['output']
  organizer?: Maybe<User>
  recurrence?: Maybe<Scalars['String']['output']>
  reoccur?: Maybe<Scalars['Boolean']['output']>
  start?: Maybe<TCalendarDateTime>
  startDate?: Maybe<Scalars['AWSDateTime']['output']>
  tags?: Maybe<Array<Maybe<Tag>>>
  times?: Maybe<Array<Maybe<Times>>>
  updated?: Maybe<Scalars['AWSDateTime']['output']>
  users?: Maybe<Array<Maybe<User>>>
  whichWay: EWhichWay
}

export type Post = {
  __typename?: 'Post'
  comments?: Maybe<Array<Maybe<Comment>>>
  created?: Maybe<Scalars['AWSDateTime']['output']>
  id: Scalars['ID']['output']
  name?: Maybe<Scalars['String']['output']>
  poststatus?: Maybe<EPostStatus>
  updated?: Maybe<Scalars['AWSDateTime']['output']>
  user: User
}

export type TAddress = {
  __typename?: 'TAddress'
  additional?: Maybe<Scalars['String']['output']>
  city?: Maybe<Scalars['String']['output']>
  itinerary?: Maybe<TPointList>
  location?: Maybe<TPoint>
  postcode?: Maybe<Scalars['String']['output']>
  street1?: Maybe<Scalars['String']['output']>
  street2?: Maybe<Scalars['String']['output']>
}

export type TAttendee = IStudent & {
  __typename?: 'TAttendee'
  address?: Maybe<TAddress>
  displayName?: Maybe<Scalars['String']['output']>
  endParticipation?: Maybe<Scalars['AWSDateTime']['output']>
  firstName?: Maybe<Scalars['String']['output']>
  lastName?: Maybe<Scalars['String']['output']>
  organizer?: Maybe<Scalars['Boolean']['output']>
  role?: Maybe<EAttendeeRole>
  school?: Maybe<ESchool>
  schoolLevel?: Maybe<ESchoolLevel>
  self?: Maybe<Scalars['Boolean']['output']>
  startParticipation?: Maybe<Scalars['AWSDateTime']['output']>
  status?: Maybe<EStatus>
}

export type TCalendarDateTime = {
  __typename?: 'TCalendarDateTime'
  dateTime?: Maybe<Scalars['AWSDateTime']['output']>
  timeZone?: Maybe<Scalars['String']['output']>
}

export type TPoint = {
  __typename?: 'TPoint'
  latitude: Scalars['Float']['output']
  longitude: Scalars['Float']['output']
}

export type TPointList = {
  __typename?: 'TPointList'
  points: Array<TPoint>
}

export type TTime = {
  __typename?: 'TTime'
  attendees: Array<TAttendee>
  day: EDayOfWeek
}

export type Tag = {
  __typename?: 'Tag'
  created?: Maybe<Scalars['AWSDateTime']['output']>
  events?: Maybe<Array<Maybe<OnBoardEvent>>>
  id: Scalars['ID']['output']
  name: Scalars['String']['output']
  updated?: Maybe<Scalars['AWSDateTime']['output']>
}

export type Times = {
  __typename?: 'Times'
  attendees?: Maybe<Array<Maybe<Attendees>>>
  created?: Maybe<Scalars['AWSDateTime']['output']>
  event?: Maybe<OnBoardEvent>
  id: Scalars['ID']['output']
  times: Array<TTime>
  updated?: Maybe<Scalars['AWSDateTime']['output']>
  user: User
}

export type User = {
  __typename?: 'User'
  created?: Maybe<Scalars['AWSDateTime']['output']>
  displayName?: Maybe<Scalars['String']['output']>
  email: Scalars['AWSEmail']['output']
  events?: Maybe<Array<Maybe<OnBoardEvent>>>
  firstName: Scalars['String']['output']
  id: Scalars['ID']['output']
  lastName: Scalars['String']['output']
  phoneNumber?: Maybe<Scalars['AWSPhone']['output']>
  posts?: Maybe<Array<Maybe<Post>>>
  updated?: Maybe<Scalars['AWSDateTime']['output']>
  username: Scalars['String']['output']
}

export type ResolverTypeWrapper<T> = Promise<T> | T

export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>
}
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | ResolverWithResolve<TResult, TParent, TContext, TArgs>

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo,
) => Promise<TResult> | TResult

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo,
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo,
) => TResult | Promise<TResult>

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo,
) => Maybe<TTypes> | Promise<Maybe<TTypes>>

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>

export type NextResolverFn<T> = () => Promise<T>

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo,
) => TResult | Promise<TResult>

/** Mapping of interface types */
export type ResolversInterfaceTypes<RefType extends Record<string, unknown>> = {
  IStudent: TAttendee
}

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  AWSDate: ResolverTypeWrapper<Scalars['AWSDate']['output']>
  AWSDateTime: ResolverTypeWrapper<Scalars['AWSDateTime']['output']>
  AWSEmail: ResolverTypeWrapper<Scalars['AWSEmail']['output']>
  AWSIPAddress: ResolverTypeWrapper<Scalars['AWSIPAddress']['output']>
  AWSJSON: ResolverTypeWrapper<Scalars['AWSJSON']['output']>
  AWSPhone: ResolverTypeWrapper<Scalars['AWSPhone']['output']>
  AWSTime: ResolverTypeWrapper<Scalars['AWSTime']['output']>
  AWSTimestamp: ResolverTypeWrapper<Scalars['AWSTimestamp']['output']>
  AWSURL: ResolverTypeWrapper<Scalars['AWSURL']['output']>
  Attendees: ResolverTypeWrapper<Attendees>
  BigInt: ResolverTypeWrapper<Scalars['BigInt']['output']>
  Boolean: ResolverTypeWrapper<Scalars['Boolean']['output']>
  Comment: ResolverTypeWrapper<Comment>
  Double: ResolverTypeWrapper<Scalars['Double']['output']>
  EActivity: EActivity
  EAttendeeRole: EAttendeeRole
  EDayOfWeek: EDayOfWeek
  EEventType: EEventType
  ELifeCycle: ELifeCycle
  EPostStatus: EPostStatus
  ESchool: ESchool
  ESchoolLevel: ESchoolLevel
  EStatus: EStatus
  EWhichWay: EWhichWay
  Float: ResolverTypeWrapper<Scalars['Float']['output']>
  ID: ResolverTypeWrapper<Scalars['ID']['output']>
  IStudent: ResolverTypeWrapper<ResolversInterfaceTypes<ResolversTypes>['IStudent']>
  OnBoardEvent: ResolverTypeWrapper<OnBoardEvent>
  Post: ResolverTypeWrapper<Post>
  String: ResolverTypeWrapper<Scalars['String']['output']>
  TAddress: ResolverTypeWrapper<TAddress>
  TAttendee: ResolverTypeWrapper<TAttendee>
  TCalendarDateTime: ResolverTypeWrapper<TCalendarDateTime>
  TPoint: ResolverTypeWrapper<TPoint>
  TPointList: ResolverTypeWrapper<TPointList>
  TTime: ResolverTypeWrapper<TTime>
  Tag: ResolverTypeWrapper<Tag>
  Times: ResolverTypeWrapper<Times>
  User: ResolverTypeWrapper<User>
}

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  AWSDate: Scalars['AWSDate']['output']
  AWSDateTime: Scalars['AWSDateTime']['output']
  AWSEmail: Scalars['AWSEmail']['output']
  AWSIPAddress: Scalars['AWSIPAddress']['output']
  AWSJSON: Scalars['AWSJSON']['output']
  AWSPhone: Scalars['AWSPhone']['output']
  AWSTime: Scalars['AWSTime']['output']
  AWSTimestamp: Scalars['AWSTimestamp']['output']
  AWSURL: Scalars['AWSURL']['output']
  Attendees: Attendees
  BigInt: Scalars['BigInt']['output']
  Boolean: Scalars['Boolean']['output']
  Comment: Comment
  Double: Scalars['Double']['output']
  Float: Scalars['Float']['output']
  ID: Scalars['ID']['output']
  IStudent: ResolversInterfaceTypes<ResolversParentTypes>['IStudent']
  OnBoardEvent: OnBoardEvent
  Post: Post
  String: Scalars['String']['output']
  TAddress: TAddress
  TAttendee: TAttendee
  TCalendarDateTime: TCalendarDateTime
  TPoint: TPoint
  TPointList: TPointList
  TTime: TTime
  Tag: Tag
  Times: Times
  User: User
}

export type Aws_Api_KeyDirectiveArgs = {}

export type Aws_Api_KeyDirectiveResolver<Result, Parent, ContextType = any, Args = Aws_Api_KeyDirectiveArgs> = DirectiveResolverFn<
  Result,
  Parent,
  ContextType,
  Args
>

export type Aws_AuthDirectiveArgs = {
  cognito_groups: Array<Scalars['String']['input']>
}

export type Aws_AuthDirectiveResolver<Result, Parent, ContextType = any, Args = Aws_AuthDirectiveArgs> = DirectiveResolverFn<
  Result,
  Parent,
  ContextType,
  Args
>

export type Aws_Cognito_User_PoolsDirectiveArgs = {
  cognito_groups?: Maybe<Array<Scalars['String']['input']>>
}

export type Aws_Cognito_User_PoolsDirectiveResolver<
  Result,
  Parent,
  ContextType = any,
  Args = Aws_Cognito_User_PoolsDirectiveArgs,
> = DirectiveResolverFn<Result, Parent, ContextType, Args>

export type Aws_IamDirectiveArgs = {}

export type Aws_IamDirectiveResolver<Result, Parent, ContextType = any, Args = Aws_IamDirectiveArgs> = DirectiveResolverFn<
  Result,
  Parent,
  ContextType,
  Args
>

export type Aws_OidcDirectiveArgs = {}

export type Aws_OidcDirectiveResolver<Result, Parent, ContextType = any, Args = Aws_OidcDirectiveArgs> = DirectiveResolverFn<
  Result,
  Parent,
  ContextType,
  Args
>

export type Aws_SubscribeDirectiveArgs = {
  mutations: Array<Scalars['String']['input']>
}

export type Aws_SubscribeDirectiveResolver<Result, Parent, ContextType = any, Args = Aws_SubscribeDirectiveArgs> = DirectiveResolverFn<
  Result,
  Parent,
  ContextType,
  Args
>

export interface AwsDateScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['AWSDate'], any> {
  name: 'AWSDate'
}

export interface AwsDateTimeScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['AWSDateTime'], any> {
  name: 'AWSDateTime'
}

export interface AwsEmailScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['AWSEmail'], any> {
  name: 'AWSEmail'
}

export interface AwsipAddressScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['AWSIPAddress'], any> {
  name: 'AWSIPAddress'
}

export interface AwsjsonScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['AWSJSON'], any> {
  name: 'AWSJSON'
}

export interface AwsPhoneScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['AWSPhone'], any> {
  name: 'AWSPhone'
}

export interface AwsTimeScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['AWSTime'], any> {
  name: 'AWSTime'
}

export interface AwsTimestampScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['AWSTimestamp'], any> {
  name: 'AWSTimestamp'
}

export interface AwsurlScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['AWSURL'], any> {
  name: 'AWSURL'
}

export type AttendeesResolvers<ContextType = any, ParentType extends ResolversParentTypes['Attendees'] = ResolversParentTypes['Attendees']> = {
  attendees?: Resolver<Array<ResolversTypes['TAttendee']>, ParentType, ContextType>
  created?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  event?: Resolver<Maybe<ResolversTypes['OnBoardEvent']>, ParentType, ContextType>
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>
  updated?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  user?: Resolver<ResolversTypes['User'], ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export interface BigIntScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['BigInt'], any> {
  name: 'BigInt'
}

export type CommentResolvers<ContextType = any, ParentType extends ResolversParentTypes['Comment'] = ResolversParentTypes['Comment']> = {
  content?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>
  post?: Resolver<Maybe<ResolversTypes['Post']>, ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export interface DoubleScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Double'], any> {
  name: 'Double'
}

export type IStudentResolvers<ContextType = any, ParentType extends ResolversParentTypes['IStudent'] = ResolversParentTypes['IStudent']> = {
  __resolveType: TypeResolveFn<'TAttendee', ParentType, ContextType>
  firstName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  lastName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  school?: Resolver<Maybe<ResolversTypes['ESchool']>, ParentType, ContextType>
  schoolLevel?: Resolver<Maybe<ResolversTypes['ESchoolLevel']>, ParentType, ContextType>
}

export type OnBoardEventResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['OnBoardEvent'] = ResolversParentTypes['OnBoardEvent'],
> = {
  activity?: Resolver<ResolversTypes['EActivity'], ParentType, ContextType>
  attendees?: Resolver<Maybe<Array<Maybe<ResolversTypes['Attendees']>>>, ParentType, ContextType>
  created?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  creator?: Resolver<ResolversTypes['User'], ParentType, ContextType>
  description?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  end?: Resolver<Maybe<ResolversTypes['TCalendarDateTime']>, ParentType, ContextType>
  endDate?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  eventType?: Resolver<ResolversTypes['EEventType'], ParentType, ContextType>
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>
  lifeCycle?: Resolver<ResolversTypes['ELifeCycle'], ParentType, ContextType>
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  organizer?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType>
  recurrence?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  reoccur?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>
  start?: Resolver<Maybe<ResolversTypes['TCalendarDateTime']>, ParentType, ContextType>
  startDate?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  tags?: Resolver<Maybe<Array<Maybe<ResolversTypes['Tag']>>>, ParentType, ContextType>
  times?: Resolver<Maybe<Array<Maybe<ResolversTypes['Times']>>>, ParentType, ContextType>
  updated?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  users?: Resolver<Maybe<Array<Maybe<ResolversTypes['User']>>>, ParentType, ContextType>
  whichWay?: Resolver<ResolversTypes['EWhichWay'], ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export type PostResolvers<ContextType = any, ParentType extends ResolversParentTypes['Post'] = ResolversParentTypes['Post']> = {
  comments?: Resolver<Maybe<Array<Maybe<ResolversTypes['Comment']>>>, ParentType, ContextType>
  created?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  poststatus?: Resolver<Maybe<ResolversTypes['EPostStatus']>, ParentType, ContextType>
  updated?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  user?: Resolver<ResolversTypes['User'], ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export type TAddressResolvers<ContextType = any, ParentType extends ResolversParentTypes['TAddress'] = ResolversParentTypes['TAddress']> = {
  additional?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  city?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  itinerary?: Resolver<Maybe<ResolversTypes['TPointList']>, ParentType, ContextType>
  location?: Resolver<Maybe<ResolversTypes['TPoint']>, ParentType, ContextType>
  postcode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  street1?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  street2?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export type TAttendeeResolvers<ContextType = any, ParentType extends ResolversParentTypes['TAttendee'] = ResolversParentTypes['TAttendee']> = {
  address?: Resolver<Maybe<ResolversTypes['TAddress']>, ParentType, ContextType>
  displayName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  endParticipation?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  firstName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  lastName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  organizer?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>
  role?: Resolver<Maybe<ResolversTypes['EAttendeeRole']>, ParentType, ContextType>
  school?: Resolver<Maybe<ResolversTypes['ESchool']>, ParentType, ContextType>
  schoolLevel?: Resolver<Maybe<ResolversTypes['ESchoolLevel']>, ParentType, ContextType>
  self?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>
  startParticipation?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  status?: Resolver<Maybe<ResolversTypes['EStatus']>, ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export type TCalendarDateTimeResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['TCalendarDateTime'] = ResolversParentTypes['TCalendarDateTime'],
> = {
  dateTime?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  timeZone?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export type TPointResolvers<ContextType = any, ParentType extends ResolversParentTypes['TPoint'] = ResolversParentTypes['TPoint']> = {
  latitude?: Resolver<ResolversTypes['Float'], ParentType, ContextType>
  longitude?: Resolver<ResolversTypes['Float'], ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export type TPointListResolvers<ContextType = any, ParentType extends ResolversParentTypes['TPointList'] = ResolversParentTypes['TPointList']> = {
  points?: Resolver<Array<ResolversTypes['TPoint']>, ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export type TTimeResolvers<ContextType = any, ParentType extends ResolversParentTypes['TTime'] = ResolversParentTypes['TTime']> = {
  attendees?: Resolver<Array<ResolversTypes['TAttendee']>, ParentType, ContextType>
  day?: Resolver<ResolversTypes['EDayOfWeek'], ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export type TagResolvers<ContextType = any, ParentType extends ResolversParentTypes['Tag'] = ResolversParentTypes['Tag']> = {
  created?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  events?: Resolver<Maybe<Array<Maybe<ResolversTypes['OnBoardEvent']>>>, ParentType, ContextType>
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  updated?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export type TimesResolvers<ContextType = any, ParentType extends ResolversParentTypes['Times'] = ResolversParentTypes['Times']> = {
  attendees?: Resolver<Maybe<Array<Maybe<ResolversTypes['Attendees']>>>, ParentType, ContextType>
  created?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  event?: Resolver<Maybe<ResolversTypes['OnBoardEvent']>, ParentType, ContextType>
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>
  times?: Resolver<Array<ResolversTypes['TTime']>, ParentType, ContextType>
  updated?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  user?: Resolver<ResolversTypes['User'], ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export type UserResolvers<ContextType = any, ParentType extends ResolversParentTypes['User'] = ResolversParentTypes['User']> = {
  created?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  displayName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
  email?: Resolver<ResolversTypes['AWSEmail'], ParentType, ContextType>
  events?: Resolver<Maybe<Array<Maybe<ResolversTypes['OnBoardEvent']>>>, ParentType, ContextType>
  firstName?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>
  lastName?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  phoneNumber?: Resolver<Maybe<ResolversTypes['AWSPhone']>, ParentType, ContextType>
  posts?: Resolver<Maybe<Array<Maybe<ResolversTypes['Post']>>>, ParentType, ContextType>
  updated?: Resolver<Maybe<ResolversTypes['AWSDateTime']>, ParentType, ContextType>
  username?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>
}

export type Resolvers<ContextType = any> = {
  AWSDate?: GraphQLScalarType
  AWSDateTime?: GraphQLScalarType
  AWSEmail?: GraphQLScalarType
  AWSIPAddress?: GraphQLScalarType
  AWSJSON?: GraphQLScalarType
  AWSPhone?: GraphQLScalarType
  AWSTime?: GraphQLScalarType
  AWSTimestamp?: GraphQLScalarType
  AWSURL?: GraphQLScalarType
  Attendees?: AttendeesResolvers<ContextType>
  BigInt?: GraphQLScalarType
  Comment?: CommentResolvers<ContextType>
  Double?: GraphQLScalarType
  IStudent?: IStudentResolvers<ContextType>
  OnBoardEvent?: OnBoardEventResolvers<ContextType>
  Post?: PostResolvers<ContextType>
  TAddress?: TAddressResolvers<ContextType>
  TAttendee?: TAttendeeResolvers<ContextType>
  TCalendarDateTime?: TCalendarDateTimeResolvers<ContextType>
  TPoint?: TPointResolvers<ContextType>
  TPointList?: TPointListResolvers<ContextType>
  TTime?: TTimeResolvers<ContextType>
  Tag?: TagResolvers<ContextType>
  Times?: TimesResolvers<ContextType>
  User?: UserResolvers<ContextType>
}

export type DirectiveResolvers<ContextType = any> = {
  aws_api_key?: Aws_Api_KeyDirectiveResolver<any, any, ContextType>
  aws_auth?: Aws_AuthDirectiveResolver<any, any, ContextType>
  aws_cognito_user_pools?: Aws_Cognito_User_PoolsDirectiveResolver<any, any, ContextType>
  aws_iam?: Aws_IamDirectiveResolver<any, any, ContextType>
  aws_oidc?: Aws_OidcDirectiveResolver<any, any, ContextType>
  aws_subscribe?: Aws_SubscribeDirectiveResolver<any, any, ContextType>
}
