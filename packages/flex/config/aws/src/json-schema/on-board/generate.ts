// __dirname is not defined in ES module scope
import * as path from 'path'
import { fileURLToPath, parse } from 'url'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

// require.resolve for ES modules
import { createRequire } from 'module'
const require = createRequire(import.meta.url)

import { __Schema, graphqlSync, getIntrospectionQuery, IntrospectionQuery } from 'graphql'

import { fromIntrospectionQuery } from 'graphql-2-json-schema'

// const schema = await import(path.resolve(
//   __dirname,
//   '../../../../../../../',
//   'amplify/backend/api/v3onBoardEvent/schema.graphql',
// )) as typeof __Schema
// const schema = await import(`${process.env.FLEX_PROJ_ROOT}/amplify/backend/api/v3onBoardEvent/schema.graphql`)
// const schema = require(`${process.env.FLEX_PROJ_ROOT}/amplify/backend/api/v3onBoardEvent/schema.graphql`
// import schema from './schema.graphql'
// const schema = await import('./schema.graphql') as unknown as typeof __Schema
// const schema = require('./schema.graphql') as typeof __Schema
const schema = await import('./schema.graphql')
// console.log('schema', schema.toString())

const options = {
  // Whether or not to ignore GraphQL internals that are probably not relevant
  // to documentation generation.
  // Defaults to `true`
  // ignoreInternals: true,
  ignoreInternals: false,
  // Whether or not to properly represent GraphQL Lists with Nullable elements
  // as type "array" with items being an "anyOf" that includes the possible
  // type and a "null" type.
  // Defaults to `false` for backwards compatibility, but in future versions
  // the effect of `true` is likely going to be the default and only way. It is
  // highly recommended that new implementations set this value to `true`.
  // nullableArrayItems: true,
  nullableArrayItems: false,
  // Indicates how to define the `ID` scalar as part of a JSON Schema. Valid options
  // are `string`, `number`, or `both`. Defaults to `string`
  // idTypeMapping: 'string'
}

// schema is your GraphQL schema.
// @ts-expect-error
const introspection = graphqlSync(schema, getIntrospectionQuery()).data as unknown as IntrospectionQuery

const jsonSchema = fromIntrospectionQuery(introspection, options)

console.log(jsonSchema)
