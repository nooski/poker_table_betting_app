// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Immutable, castDraft, Draft } from 'immer'
import { PokerBoard, PokerTables, PokerTimer } from 'on-board-event-common'

// https://freshman.tech/snippets/typescript/fix-value-not-exist-eventtarget/
type HTMLElementEvent<T extends HTMLElement> = (Event | React.ChangeEvent) & {
  target: T
}

interface GraphQLResult {
  data?: Record<string, unknown>
  errors?: [object]
  extensions?: {
    [key: string]: unknown
  }
}

interface Table {
  id?: string
  name?: string
  desc?: string | null
  tableId: number
  order: number
  active: boolean
  createdAt?: string
  updatedAt?: string
  __typename?: string
}

interface PokerTableTimer {
  id: number
  time: Date
}

interface PokerTableMapValue {
  id: string
  tableId: number
  nextPlayerAction: Date
}

type PokerTableMap = Map<number, PokerTableMapValue>

type PokerPlayer = Immutable<{
  id: string
  joining: boolean
  name: string | null
  vote: number | null
  observer: boolean
  tables?: Table[]
  timers?: PokerTimer
  joinedTables: number[]
  joinedTableTimers?: PokerTableTimer[]
  joinedTablesMap: PokerTableMap
}>

type ActionPokerBoard = (state?: PokerBoard) => PokerBoard
type ActionPokerTables = (state?: PokerTables) => PokerTables
type ActionPokerTimer = (state?: PokerTimer) => PokerTimer

interface IPokerMsg {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [x: string]: any
  source?: string
  action: (ActionPokerBoard | ActionPokerTables | ActionPokerTimer) | string
  // action: string
  currentlyVoting?: string
  name?: string | null
  vote?: number | null
  observer?: boolean
  tableID?: number
  nextPlayerAction?: Date
  hour?: number
  minute?: number
  second?: number
  date?: Date
  timeReadable?: string
  timeUTC?: Date
}

interface IPokerAction extends IPokerMsg {
  id: string | number
  seq?: number
}

type PokerAction = Immutable<IPokerAction>

interface PokerBoardComponentProps {
  board: PokerBoard
  myPlayer: PokerPlayer
  vote: number
  activeTable: Table
  joinedTables: number[]
  changeCurrentlyVoting: (event: HTMLElementEvent<HTMLTextAreaElement>) => void
  clearVotes: () => void
  showVotes: () => void
  makeVote: (event: React.MouseEvent<HTMLButtonElement>) => void
}

type VoteResults = {
  average: number
  voteCounts: {
    vote: number
    count: number
  }[]
} | null

type PointOption = number | string

interface AppProps {
  isStandalone: boolean
}

interface PokerTablesComponentProps {
  myPlayer?: PokerPlayer
  tables: PokerTables
  allTables: Table[]
  createTable: (tableName: string, tableDesc: string) => void
  deleteTable: (id: string, tableId: number) => void
  activateTable: (id: string, tableId: number) => void
}

interface PokerClickEvent {
  id: string
  tableId: number
}

interface PokerTablesModalCreateProps {
  active: boolean
  setModalShow: (arg0: boolean) => void
  createTable: (tableName: string, tableDesc: string) => void
}
