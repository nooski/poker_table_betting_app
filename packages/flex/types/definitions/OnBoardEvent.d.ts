/* eslint-disable @typescript-eslint/no-unused-vars */
import { Immutable } from 'immer'
import { PokerBoard, PokerTables, PokerTimer } from 'on-board-event-common'
// import {
//   OnBoardEvent,
//   EActivity,
//   EEventType,
//   EWhichWay,
//   ELifeCycle,
//   User,
//   ModelAttendeesConnection,
//   ModelTimesConnection,
//   ModelOnBoardEventTagsConnection,
//   ModelOnBoardEventUsersConnection,
// } from 'on-board-event-api'
import type * as OnBoardEventApiTypes from 'on-board-event-api'
// import { UserInterfaceStore } from '@flexiness/domain-store'

import { MobxZodField } from '@monoid-dev/mobx-zod-form'
import { ReactForm } from '@monoid-dev/mobx-zod-form-react'
import { z } from 'zod'

// https://freshman.tech/snippets/typescript/fix-value-not-exist-eventtarget/
type HTMLElementEvent<T extends HTMLElement> = (Event | React.ChangeEvent) & {
  target: T
}

interface GraphQLResult {
  data?: Record<string, unknown>
  errors?: [object]
  extensions?: {
    [key: string]: unknown
  }
}

interface Table extends OnBoardEventApiTypes.OnBoardEvent {
  id: string
  tableId: number
  order: number
  active: boolean
}

interface TableData extends OnBoardEventApiTypes.OnBoardEvent {
  tableName?: string
  tableDesc?: string
  id: string
  tableId?: number
}

// type ConvertToTable = {
//   new (events: OnBoardEventApiTypes.OnBoardEvent[]): Table[]
// }

type ConvertToTable = (events: OnBoardEventApiTypes.OnBoardEvent[]) => Table[]

interface PokerTableTimer {
  id: number
  time: Date
}

interface PokerTableMapValue {
  id: string
  tableId: number
  nextPlayerAction: Date
}

type PokerTableMap = Map<number, PokerTableMapValue>

type PokerPlayer = Immutable<{
  id: string
  joining: boolean
  name: string | null
  vote: number | null
  observer: boolean
  tables?: Table[]
  timers?: PokerTimer
  joinedTables: number[]
  joinedTableTimers?: PokerTableTimer[]
  joinedTablesMap: PokerTableMap
}>

type ActionPokerBoard = (state?: PokerBoard) => PokerBoard
type ActionPokerTables = (state?: PokerTables) => PokerTables
type ActionPokerTimer = (state?: PokerTimer) => PokerTimer

interface IPokerMsg {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [x: string]: any
  source?: string
  action: (ActionPokerBoard | ActionPokerTables | ActionPokerTimer) | string
  // action: string
  tableData?: TableData
  currentlyVoting?: string
  name?: string | null
  vote?: number | null
  observer?: boolean
  tableID?: number
  nextPlayerAction?: Date
  hour?: number
  minute?: number
  second?: number
  date?: Date
  timeReadable?: string
  timeUTC?: Date
}

interface IPokerAction extends IPokerMsg {
  id: string | number
  seq?: number
}

type PokerAction = Immutable<IPokerAction>

interface PokerBoardComponentProps {
  board: PokerBoard
  myPlayer: PokerPlayer
  vote: number
  activeTable: Table
  joinedTables: number[]
  // changeCurrentlyVoting: (event: HTMLElementEvent<HTMLTextAreaElement>) => void
  changeCurrentlyVoting: (event: string) => void
  clearVotes: () => void
  showVotes: () => void
  // makeVote: (event: React.MouseEvent<HTMLButtonElement>) => void
  makeVote: (event: number) => void
}

type VoteResults = {
  average: number
  voteCounts: {
    vote: number
    count: number
  }[]
} | null

type PointOption = number | string

interface AppProps {
  isStandalone: boolean
}

interface PokerTablesComponentProps {
  myPlayer?: PokerPlayer
  tables: PokerTables
  allTables: Table[]
  createTable: (tableName: string, tableDesc: string) => Promise<OnBoardEventApiTypes.OnBoardEvent | undefined>
  deleteTable: (id: string, tableId: number) => void
  activateTable: (id: string, tableId: number) => void
  updateTables: () => void
}

interface PokerClickEvent {
  id: string
  tableId: number
}

interface PokerTablesModalCreateProps {
  active?: boolean
  setModalShow?: (arg0: boolean) => void
  createTable: (tableName: string, tableDesc: string) => Promise<OnBoardEventApiTypes.OnBoardEvent | undefined>
  updateTables: () => void
  formStep?: number
  tableName?: string
  tableDesc?: string
}

interface Form3Props {
  createTable: (tableName: string, tableDesc: string) => Promise<OnBoardEventApiTypes.OnBoardEvent | undefined>
  updateTables: () => void
}

type MobxFormObject = ReactForm<z.AnyZodObject>
type MobxFormArray = ReactForm<z.ZodArray<z.AnyZodObject, 'many'>>
// type MobxFormArray = ReactForm<z.ZodArray<z.ZodTypeAny, 'many'>>

type MobxForm<A extends 'MobxFormObject' | 'MobxFormArray'> = A extends 'MobxFormObject'
  ? MobxFormObject
  : A extends 'MobxFormArray'
    ? MobxFormArray
    : never

type MobxFormFieldInput = MobxZodField<z.ZodString | z.ZodNumber | z.ZodOptional<z.ZodTypeAny>>
type MobxFormFieldEnum = MobxZodField<z.ZodEnum<[string, ...string[]]>>
// type MobxFormFieldEnum = MobxZodField<z.ZodEnum<[string, ...string[]]> | z.ZodOptional<z.ZodTypeAny>>

type CurrentMobxFormStep = Map<number, MobxForm<'MobxFormObject' | 'MobxFormArray'>>
type CurrentMobxFormKey = Map<string, MobxForm<'MobxFormObject' | 'MobxFormArray'>>

interface AtttendeesInitialOutput extends Partial<OnBoardEventApiTypes.TAttendee> {
  __typename: 'TAttendee'
  displayName: string
  firstName: string
  lastName: string
  startParticipation: string
  endParticipation: string
  role: OnBoardEventApiTypes.EAttendeeRole
}
