
import { AmplifyAPITypes } from '@flexiness/aws';
import type * as OnBoardEventApiTypes from 'on-board-event-api';

type AmplifyAPIClient = AmplifyAPITypes.Client | undefined

export { OnBoardEventApiTypes, type AmplifyAPIClient };
