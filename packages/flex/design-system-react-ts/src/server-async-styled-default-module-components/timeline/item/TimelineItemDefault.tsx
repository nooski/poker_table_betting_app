// @ts-nocheck

'use server'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { TimelineItemWebProps } from './TimelineItemProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType default
import styles from 'flex-design-system-framework/main/all.module.scss'
// import { type Styles } from '@flex-design-system/framework'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Timeline Item Component
 * @param className {string} Additionnal CSS Classes
 * @param active {boolean} Active Timeline Item
 */
const TimelineItem = async ({ className, classList, active, ...others }: TimelineItemWebProps): Promise<React.JSX.Element> => {
  const classes = classNames(styles.timelineItem, active && styles.active, className, validate(classList))

  return <div className={classes} {...others} />
}

export default TimelineItem
