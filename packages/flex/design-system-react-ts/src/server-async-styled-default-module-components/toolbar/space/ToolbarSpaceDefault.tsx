// @ts-nocheck

'use server'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { ToolbarSpaceWebProps } from './ToolbarSpaceProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType default
import styles from 'flex-design-system-framework/main/all.module.scss'
// import { type Styles } from '@flex-design-system/framework'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Toolbar Space Component
 * @param className {string} Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 */
const ToolbarSpace = async ({ className, classList, ...others }: ToolbarSpaceWebProps): Promise<React.JSX.Element> => (
  <div className={classNames(styles.toolbarSpace, className, validate(classList))} {...others} />
)

export default ToolbarSpace
