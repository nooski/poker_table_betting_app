// @ts-nocheck

'use server'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { NavbarEndWebProps } from './NavbarEndProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType default
import styles from 'flex-design-system-framework/main/all.module.scss'
// import { type Styles } from '@flex-design-system/framework'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Navbar Navbar End Component
 * @param children {ReactNode} Children
 * @param className {string} Additionnal css classes
 */
const NavbarEnd = async ({ children, className, classList, ...others }: NavbarEndWebProps): Promise<React.JSX.Element> => {
  const classes = classNames(styles.navbarEnd, className, validate(classList))

  return (
    <div className={classes} {...others}>
      <div className={styles.navbarIcons}>{children}</div>
    </div>
  )
}

export default NavbarEnd
