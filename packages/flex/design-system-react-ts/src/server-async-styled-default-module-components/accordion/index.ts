// import Accordion from './Accordion'
import Accordion from './AccordionDefault.js'
import AccordionItem from './item/index.js'
import AccordionHeader from './header/index.js'
import AccordionBody from './body/index.js'

export { Accordion, AccordionItem, AccordionHeader, AccordionBody }
