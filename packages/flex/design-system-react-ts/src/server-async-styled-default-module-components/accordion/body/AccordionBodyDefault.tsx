// @ts-nocheck

'use server'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { AccordionBodyProps } from './AccordionBodyProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType default
import styles from 'flex-design-system-framework/main/all.module.scss'
// import { type Styles } from '@flex-design-system/framework'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Accordion Body Component
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param children {ReactNode} Children for Accordion body
 */
const AccordionBody = async ({ children, className, classList, ...others }: AccordionBodyProps): Promise<React.JSX.Element> => (
  <div className={classNames(styles.accordionBody, styles.isClipped, className, validate(classList))} {...others}>
    <div className={styles.accordionContent}>{children}</div>
  </div>
)

export default AccordionBody
