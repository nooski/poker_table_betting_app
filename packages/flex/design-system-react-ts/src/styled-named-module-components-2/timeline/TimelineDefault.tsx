'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
import { TimelineWebProps } from './TimelineProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// // ///////////////////////////////////////////////////////////////////////////

/**
 * Timeline Component
 * @param className {string} Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 * @param notifications {boolean} Timeline notification rendering
 */
const Timeline = ({ className, classList, notifications, ...others }: TimelineWebProps): React.JSX.Element => {
  const classes = classNames(styles.timeline, notifications && notifications, className, validate(classList))

  return <div className={classes} {...others} />
}

export default Timeline
