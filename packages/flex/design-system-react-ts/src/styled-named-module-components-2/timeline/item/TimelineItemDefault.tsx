'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { TimelineItemWebProps } from './TimelineItemProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// // ///////////////////////////////////////////////////////////////////////////

/**
 * Timeline Item Component
 * @param className {string} Additionnal CSS Classes
 * @param active {boolean} Active Timeline Item
 */
const TimelineItem = ({ className, classList, active, ...others }: TimelineItemWebProps): React.JSX.Element => {
  const classes = classNames(styles.timelineItem, active && styles.active, className, validate(classList))

  return <div className={classes} {...others} />
}

export default TimelineItem
