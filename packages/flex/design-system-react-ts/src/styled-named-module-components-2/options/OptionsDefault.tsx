'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
import { OptionsProps } from './OptionsProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// // ///////////////////////////////////////////////////////////////////////////

/**
 * Options Component
 * @param className {string} Additionnal CSS Classes
 * @param inverted {boolean} Inverted options
 */
const Options = ({ className, classList, inverted, ...others }: OptionsProps): React.JSX.Element => {
  const classes = classNames(styles.options, inverted && styles.isInverted, className, validate(classList))

  return <div className={classes} {...others} />
}

export default Options
