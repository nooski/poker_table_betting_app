'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { PricingPlanExtraWebProps } from './PricingPlanExtraProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// // ///////////////////////////////////////////////////////////////////////////

/**
 * Pricing Plan Extra Component
 * @param children {ReactNode} Children
 * @param className {string} Additionnal css classes
 */
const PricingPlanExtra = ({ className, classList, ...others }: PricingPlanExtraWebProps): React.JSX.Element => {
  const classes = classNames(styles.pricingPlanExtra, className, validate(classList))

  return <div className={classes} {...others} />
}

export default PricingPlanExtra
