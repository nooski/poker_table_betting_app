'use client'

import React from 'react'
import classNames from 'classnames'
import { has, validate } from '../../../services/index.js'
import { camelCase } from 'lodash'
import { PricingPlanHeaderWebProps } from './PricingPlanHeaderProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Pricing Plan Header Component
 * @param children {ReactNode} Children
 * @param className {string} Additionnal css classes
 * @param background {BackgroundStyle} Custom background color
 */
const PricingPlanHeader = ({ className, classList, background, ...others }: PricingPlanHeaderWebProps): React.JSX.Element => {
  const classes = classNames(styles.planHeader, background && camelCase(has(background.getClassName())), className, validate(classList))

  return <div className={classes} {...others} />
}

export default PricingPlanHeader
