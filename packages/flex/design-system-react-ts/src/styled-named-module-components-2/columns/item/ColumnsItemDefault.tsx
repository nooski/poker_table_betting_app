'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../../services/index.js'
import { camelCase } from 'lodash'
import { ColumnsItemProps } from './ColumnsItemProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Columns Item Component - Columns Child
 * @param size {ColumnsSize} Size 1-12
 * - -------------------------- WEB PROPERTIES -------------------------------
 * @param narrow {boolean} Narrow column item
 * @param className {string} Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 * @param mobileSize {ColumnsSize} Apply => is-size-mobile
 * @param tabletSize {ColumnsSize} Apply => is-size-tablet
 * @param desktopSize {ColumnsSize} Apply => is-size-desktop
 */
const ColumnsItem = ({ className, classList, size, mobileSize, tabletSize, desktopSize, narrow, ...others }: ColumnsItemProps): React.JSX.Element => {
  const classes = classNames(
    styles.column,
    size && camelCase(is(`${size}`)),
    mobileSize && camelCase(is(`${mobileSize}-mobile`)),
    tabletSize && camelCase(is(`${tabletSize}-tablet`)),
    desktopSize && camelCase(is(`${desktopSize}-desktop`)),
    narrow && styles.isNarrow,
    className,
    validate(classList),
  )

  return <div className={classes} {...others} />
}
export default ColumnsItem
