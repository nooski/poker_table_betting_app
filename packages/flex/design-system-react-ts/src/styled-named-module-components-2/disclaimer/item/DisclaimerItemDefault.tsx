'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
// import { camelCase } from 'lodash'
import { DisclaimerItemWebProps } from './DisclaimerItemProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// // ///////////////////////////////////////////////////////////////////////////

/**
 * Disclaimer Item component
 * @param children {ReactNode} Diclaimer Item Children
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 */
const DisclaimerItem = ({ className, classList, ...others }: DisclaimerItemWebProps): React.JSX.Element => {
  const classes = classNames(styles.disclaimerItem, className, validate(classList))

  return <div className={classes} {...others} />
}

export default DisclaimerItem
