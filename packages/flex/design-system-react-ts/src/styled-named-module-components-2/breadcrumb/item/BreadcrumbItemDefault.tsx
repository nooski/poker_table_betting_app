'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { Text, TextMarkup } from '../../text/index.js'
import { BreadcrumbItemWebProps } from './BreadcrumbItemProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Breadcrumb Item Component
 * @param children {string} Breadcrumb Item Text
 * @param active {boolean} Active link
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param href {string} Url
 */
const BreadcrumbItem = ({ children, active, className, classList, ...others }: BreadcrumbItemWebProps): React.JSX.Element => {
  const classes = classNames(active && styles.isActive, className, validate(classList))

  return (
    <li className={classes}>
      <Text markup={TextMarkup.A} {...others}>
        {children}
      </Text>
    </li>
  )
}

export default BreadcrumbItem
