'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../services/index.js'
import { camelCase } from 'lodash'
import { TextProps } from './TextProps.js'
import { TextMarkup } from './TextEnum.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Text component
 * @param children {string} Text child
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param href {string} If Text Markup is A
 * @param title {string} title attribute
 * @param onClick {Function} onClick Event
 * @param inverted {Boolean} Text white color
 */
const Text = ({ level, markup, children, className, classList, href, title, onClick, typo, inverted, ...others }: TextProps): React.JSX.Element => {
  const classes = classNames(
    {
      [styles.text]: true,
    },
    level && camelCase(is(`${level}`)),
    inverted && styles.isInverted,
    typo && camelCase(`${typo}`),
    className,
    validate(classList),
  )

  /**
   * If no markup return p with default level 1
   */
  const Tag = markup && (markup in TextMarkup || Object.values(TextMarkup).includes(markup)) ? markup : 'p'

  return (
    <Tag onClick={onClick} title={title} className={classes} {...(Tag === TextMarkup.A && { href: href })} {...others}>
      {children}
    </Tag>
  )
}

export default Text
