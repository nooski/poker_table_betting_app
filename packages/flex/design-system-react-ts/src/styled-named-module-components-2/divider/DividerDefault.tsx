'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
import { DividerProps } from './DividerProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Divider Component
 * @param content {string} Add text content for Divider
 * @param unboxed {boolean} Full-width separator in another component
 * @param marginless {boolean} Marginless divider
 * - -------------------------- WEB PROPERTIES -------------------------------
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes (ONLY FOR WEB)

 */
const Divider = ({ className, classList, unboxed, content, marginless, ...others }: DividerProps): React.JSX.Element => {
  const classes = classNames(styles.isDivider, unboxed && styles.isUnboxed, marginless && styles.isMarginless, className, validate(classList))

  if (content) {
    return <hr className={classes} {...others} data-content={content} />
  }

  return <hr className={classes} {...others} />
}

export default Divider
