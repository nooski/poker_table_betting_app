'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
import { RowsProps } from './RowsProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// // ///////////////////////////////////////////////////////////////////////////

/**
 * Rows Component
 * @param children {ReactNode} Rows children
 * - ------------------- WEB PROPERTIES -------------------------
 * @param className {string} additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 */
const Rows = ({ className, classList, ...others }: RowsProps): JSX.Element => (
  <div className={classNames(styles.rows, className, validate(classList))} {...others} />
)

export default Rows
