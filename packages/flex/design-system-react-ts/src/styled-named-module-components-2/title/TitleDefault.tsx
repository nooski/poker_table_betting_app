'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../services/index.js'
import { camelCase } from 'lodash'
import { TitleProps } from './TitleProps.js'
import { TitleLevel, TitleMarkup } from './TitleEnum.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Title component
 * @param children {ReactNode} Title child
 * @param level {TitleLevel|number} Title size : 1-7
 * @param inverted {Boolean} Title white color
 * - --------------- WEB PROPERTIES ----------------------------------
 * @param markup {string} h1 | h2 | h3 | h4 | h5 | h6 | p | span | div
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param typo
 * @param skeleton
 * @param others
 */
const Title = ({
  level = TitleLevel.LEVEL1, // defaultProps
  markup,
  children,
  className,
  classList,
  typo,
  skeleton,
  inverted,
  ...others
}: TitleProps): React.JSX.Element => {
  const [isLoading, setIsLoading] = React.useState<boolean>(skeleton || false)
  const classes = classNames(
    {
      [styles.title]: true,
      [camelCase(is(`${level}`))]: level,
    },
    typo,
    isLoading ? camelCase(is('loading')) : camelCase(is('loaded')),
    inverted && camelCase(is('inverted')),
    className,
    validate(classList),
  )

  React.useEffect(() => {
    setIsLoading(skeleton || false)
  }, [skeleton])

  /**
   * If no markup return div with default level 1
   * key in Enum works only in TS or with number enum for JS
   * for string enum (as in this case) we need to use Object.values.includes for JS usage
   * string enum aren't reverse mapped so the first solution doesn't work
   */
  const Tag = markup && (markup in TitleMarkup || Object.values(TitleMarkup).includes(markup)) ? markup : 'div'

  return (
    <Tag className={classes} {...others}>
      {children}
    </Tag>
  )
}

export default Title
