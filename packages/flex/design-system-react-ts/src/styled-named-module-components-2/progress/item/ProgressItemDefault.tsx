'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../../services/index.js'
import { camelCase } from 'lodash'
import { ProgressItemProps } from './ProgressItemProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Progress Item component - Only if stacked
 * @param percent {number} Progress percent
 * @param minPercent {number} Default min percent is 100
 * @param maxPercent {number} Default max percent is 100
 * @param alert {AlertState} Progress alert variant (SUCCESS|INFO|WARNING|DANGER)
 * -------------------------- WEB PROPERTIES -------------------------------
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 */
const ProgressItem = ({
  className,
  classList,
  percent,
  maxPercent = 100,
  minPercent = 0,
  alert,
  ...others
}: ProgressItemProps): React.JSX.Element => {
  const classes = classNames(
    styles.progressBar,
    alert && camelCase(is(alert.getClassName())),
    !alert && camelCase(is('primary')),
    className,
    validate(classList),
  )

  return (
    <div
      {...(percent && { style: { width: `${percent}%` } })}
      className={classes}
      role='progressbar'
      aria-valuenow={percent}
      aria-valuemin={minPercent}
      aria-valuemax={maxPercent}
      {...others}
    />
  )
}

export default ProgressItem
