'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { NavbarStartWebProps } from './NavbarStartProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// // ///////////////////////////////////////////////////////////////////////////

/**
 * Navbar Start Component
 * @param children {ReactNode} Navbar child
 * @param className {string} Additionnal css classes
 */
const NavbarStart = ({ className, classList, ...others }: NavbarStartWebProps): React.JSX.Element => {
  const classes = classNames(styles.navbarStart, className, validate(classList))

  return <div className={classes} {...others} />
}

export default NavbarStart
