'use client'

import React from 'react'
import classNames from 'classnames'
import { nanoid } from 'nanoid'
import { validate, is, has } from '../../services/index.js'
import { camelCase } from 'lodash'
import { RadioProps } from './RadioProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Radio Component
 * @param checked {boolean} Checked Radio
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param disabled {boolean} Disabled
 * @param readOnly {boolean} readonly Radio
 * @param id {string} Id for button, by default id is generate
 * @param label {string} Label for Radio
 * @param labelClassName {string} addtionnial CSS Classes for label
 * @param onClick {ClickEvent}
 * @param onChange {ChangeEvent}
 * @param name {string} Name for radio
 * @param value {string} Value for radio
 */
const Radio = ({
  checked,
  className,
  classList,
  disabled,
  readonly,
  id,
  label,
  labelClassName,
  onChange,
  onClick,
  name,
  value,
  inverted,
  ...others
}: RadioProps): React.JSX.Element => {
  const [_checked, setChecked] = React.useState<boolean>(checked || false)

  const classes = classNames(
    styles.input,
    camelCase(is('checkradio')),
    camelCase(is('info')),
    camelCase(is('hidden')),
    inverted && camelCase(is('inverted')),
    className && !className.includes(camelCase(is('inverted'))) && camelCase(has('background-color')),
    className,
    validate(classList),
  )
  const labelClasses = classNames(checked && camelCase(has('text-info')), labelClassName)

  React.useEffect(() => {
    if (!readonly) {
      setChecked(checked || false)
    }
  }, [checked, readonly])

  const idGenerated = nanoid()

  return (
    <div className={styles.field}>
      <div className={classNames(styles.control, disabled && camelCase(is('disabled')))}>
        <input
          className={classes}
          type='radio'
          readOnly={readonly}
          id={id || idGenerated}
          disabled={disabled}
          name={name}
          value={value}
          checked={readonly ? checked : _checked}
          onChange={(e: React.ChangeEvent) => {
            return e
          }}
          onClick={(e: React.MouseEvent) => {
            const target = e.target as HTMLInputElement
            // if (!readonly && target.checked !== undefined) {
            //   setChecked(target.checked)
            // }
            target.value = value || ''
            if (onChange) {
              onChange({
                radioId: target.id,
                radioValue: target.value,
                radioName: target.name,
                radioChecked: target.checked,
              })
            }
            if (onClick) {
              onClick({
                radioId: target.id,
                radioValue: target.value,
                radioName: target.name,
                radioChecked: target.checked,
              })
            }
          }}
          {...others}
        />
        <label htmlFor={id || idGenerated} className={labelClasses}>
          {label}
        </label>
      </div>
    </div>
  )
}

export default Radio
