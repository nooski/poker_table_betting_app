'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { AccordionBodyProps } from './AccordionBodyProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// // ///////////////////////////////////////////////////////////////////////////

/**
 * Accordion Body Component
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param children {ReactNode} Children for Accordion body
 */
const AccordionBody = ({ children, className, classList, ...others }: AccordionBodyProps): JSX.Element => (
  <div className={classNames(styles.accordionBody, styles.isClipped, className, validate(classList))} {...others}>
    <div className={styles.accordionContent}>{children}</div>
  </div>
)

export default AccordionBody
