'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { BoxTableContainerProps } from './BoxTableContainerProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// // ///////////////////////////////////////////////////////////////////////////

/**
 * Box Table Component
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param children {ReactNode} Children
 */
const BoxTableContainer = ({ className, classList, ...others }: BoxTableContainerProps): JSX.Element => (
  <div className={classNames(styles.box, styles.tableContainer, className, validate(classList))} {...others} />
)

export default BoxTableContainer
