'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { BoxFooterProps } from './BoxFooterProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// // ///////////////////////////////////////////////////////////////////////////

/**
 * Box Footer Component
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param children {ReactNode} Children
 */
const BoxFooter = ({ className, classList, children, ...others }: BoxFooterProps): JSX.Element => (
  <div className={classNames(styles.boxFooter, className, validate(classList))} {...others}>
    {children}
  </div>
)

export default BoxFooter
