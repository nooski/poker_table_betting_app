'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../../services/index.js'
import { camelCase } from 'lodash'
import { IconProps } from '../IconProps.js'
import { IconName } from '../IconNameEnum.js'
import { IconStatus } from '../IconEnum.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import * as styles from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

const StatusIcon = ({ className, classList, name, status, statusPosition, size, ...others }: IconProps): React.JSX.Element => {
  // }: IconProps): React.JSX.Element & React.DetailedHTMLProps<React.HTMLAttributes<HTMLSpanElement>, HTMLSpanElement> => {
  const ancestorClasses = classNames(
    styles.icon,
    size && camelCase(is(`${size}`)),
    styles.isAncestor,
    styles.hasStatus,
    className,
    validate(classList),
  )

  const descendantClasses = classNames(
    styles.icon,
    styles.isCircled,
    styles.isDescendant,
    status && camelCase(is(`${status}`)),
    statusPosition && camelCase(is(`${statusPosition}`)),
  )

  const iconName = camelCase(name)
  const descendantIcon = status === IconStatus.SUCCESS ? IconName.UI_CHECK_CIRCLE_S : IconName.UI_TIMES_CIRCLE_S
  const descendantIconName = camelCase(descendantIcon)

  return (
    <span className={ancestorClasses} aria-hidden='true' {...others}>
      <span className={iconName}>
        <span className={descendantClasses}>
          <span className={descendantIconName} />
        </span>
      </span>
    </span>
  )
}

export default StatusIcon
