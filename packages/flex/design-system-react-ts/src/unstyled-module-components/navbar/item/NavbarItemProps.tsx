import { HiddenMobile, HiddenTablet, HiddenDesktop, Alternate } from '../../../objects/facets/index.js'
import { NavbarItemMarkup } from './NavbarItemEnum'

/**
 * Navbar Item Interface
 */
export interface NavbarItemProps extends HiddenMobile, HiddenTablet, HiddenDesktop, Alternate {
  children?: React.ReactNode
  megaDropdown?: boolean
  hoverable?: boolean
  active?: boolean
}

/**
 * Navbar Item Web Interface
 */
export interface NavbarItemWebProps extends NavbarItemProps {
  className?: string
  id?: string
  markup?: NavbarItemMarkup
}
