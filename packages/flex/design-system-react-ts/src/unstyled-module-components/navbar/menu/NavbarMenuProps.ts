import { HiddenTouch } from '../../../objects/facets/index.js'

/**
 * Navbar Menu Interface
 */
export interface NavbarMenuProps extends HiddenTouch {
  children?: React.ReactNode | string
  newNavbar?: boolean
}

/**
 * Navbar Menu Web Interface
 */
export interface NavbarMenuWebProps extends NavbarMenuProps {
  className?: string
}
