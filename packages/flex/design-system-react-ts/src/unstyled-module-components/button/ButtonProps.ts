import { ButtonMarkup } from './ButtonEnum'
import { ClickEvent } from '../../events/index.js'
import { Fullwidth, AlertProps, VariantProps, Loadable, Invertable } from '../../objects/facets/index.js'

/**
 * Button Interface
 */
export interface ButtonProps extends Loadable, Invertable, VariantProps, AlertProps, Fullwidth {
  onClick?: ClickEvent
  children?: React.ReactNode | string
  disabled?: boolean
  small?: boolean
  markup?: ButtonMarkup
  href?: string
  className?: string
  to?: string
  id?: string
  compact?: boolean
}
