import { Invertable } from '../../objects/facets/index.js'

/**
 * Options Interface
 */
export interface OptionsProps extends Invertable {
  children?: React.ReactNode | string
  className?: string
}
