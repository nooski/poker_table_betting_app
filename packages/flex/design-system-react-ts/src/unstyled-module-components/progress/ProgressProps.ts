import { AlertProps, Stacked } from '../../objects/facets/index.js'

/**
 * Progress Interface
 */
export interface ProgressProps extends AlertProps, Stacked {
  children?: React.ReactNode | string
  percent?: number
  maxPercent?: number
  small?: boolean
  uniqueLegend?: string
  firstExtremLegend?: string
  secondExtremLegend?: string
  className?: string
}
