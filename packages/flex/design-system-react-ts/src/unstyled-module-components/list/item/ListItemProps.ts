import { GenericChildren } from '../../../generics/index.js'

/**
 * ListItem Interface
 */
import { IconName } from '../../icon'

export enum ListIconStatus {
  SUCCESS = 'success',
  DANGER = 'danger',
}

export interface ListItemProps {
  children?: GenericChildren
  className?: string
  customIcon?: IconName
  status?: ListIconStatus
  title?: string
  description?: string
}
