import { ClickEvent } from '../../events/index.js'
import { Centerable } from '../../objects/facets/index.js'

/**
 * Tabs Interface
 */
export interface TabsProps extends Centerable {
  children?: React.ReactNode | string
  onClick?: ClickEvent
  disabled?: boolean
  activeIndex?: number
  rightAlign?: boolean
  clipped?: boolean
  fullwidth?: boolean
  className?: string
}
