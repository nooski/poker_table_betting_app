import { Small, AlertProps, VariantProps, Hat } from '../../objects/facets/index.js'

export interface SliderProps extends Small, VariantProps, AlertProps, Hat {
  children?: React.ReactNode | string
  stretched?: boolean
  className?: string
  iconClassName?: string
  motionLess?: boolean
}
