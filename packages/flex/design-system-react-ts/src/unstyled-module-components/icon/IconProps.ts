import { IconSize, IconStatus, IconPosition, TextIconMarkup, IconStatusPosition, IconColor } from './IconEnum'
import { IconName } from './IconNameEnum'
import { Stacked } from '../../objects/facets/index.js'
import { ClickEvent } from '../../events/index.js'

type Styles = { [key: string]: unknown }

/**
 * Icon Interface
 */
export interface IconProps extends Stacked {
  name: IconName
  status?: IconStatus
  badgeContent?: string
  size?: IconSize
  circled?: boolean
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  content?: any
  position?: IconPosition
  markup?: TextIconMarkup
  statusPosition?: IconStatusPosition
  stretched?: boolean
  color?: IconColor
  onClick?: ClickEvent
  className?: string
  textClassName?: string
  style?: Styles
}
