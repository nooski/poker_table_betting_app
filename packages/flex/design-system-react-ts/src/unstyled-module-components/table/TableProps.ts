import { Fullwidth } from '../../objects/facets/index.js'

export interface TableProps extends Fullwidth {
  children?: React.ReactNode | string
  bordered?: boolean
  comparative?: boolean
  className?: string
}
