export interface TableTrProps {
  children?: React.ReactNode | string
  expandable?: boolean
  // eslint-disable-next-line @typescript-eslint/no-redundant-type-constituents
  expanded?: boolean | React.ReactNode | string
  className?: string
}
