import { TagVariant } from './TagEnum'

// eslint-disable-next-line @typescript-eslint/no-redundant-type-constituents
type TagClickEventHandler = React.MouseEvent<Element> | unknown

export interface TagClickEvent {
  (e: TagClickEventHandler): void
}

/**
 * Tag Interface
 */
export interface TagProps {
  children?: React.ReactNode | string
  variant?: TagVariant
  deletable?: boolean
  onClick?: TagClickEvent
  inverted?: boolean
  className?: string
}
