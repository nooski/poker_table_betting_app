import { Invertable, Small, AlertProps, VariantProps, Hat } from '../../objects/facets/index.js'
import { StickerMarkup } from './StickerEnum'

export interface StickerProps extends Small, VariantProps, AlertProps, Hat, Invertable {
  children?: React.ReactNode | string
  stretched?: boolean
  className?: string
  markup?: StickerMarkup
}
