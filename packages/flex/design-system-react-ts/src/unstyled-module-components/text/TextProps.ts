import { TextLevel, TextMarkup } from './TextEnum'
import { TypographyColor, TypographyTransform, TypographyBold, TypographyAlign } from '../../objects/typography/index.js'
import { ClickEvent } from '../../events/index.js'
import { Invertable } from '../../objects/facets/index.js'

type Styles = { [key: string]: unknown }

/**
 * Text Interface
 */
export interface TextProps extends Invertable {
  level?: TextLevel
  children?: React.ReactNode | string
  typo?: TypographyColor | TypographyTransform | TypographyBold | TypographyAlign
  onClick?: ClickEvent
  markup?: TextMarkup
  className?: string
  href?: string
  title?: string
  style?: Styles
}
