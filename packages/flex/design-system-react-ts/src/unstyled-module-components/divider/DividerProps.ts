import { Marginless } from '../../objects/facets/index.js'

/**
 * Divider Interface
 */
export interface DividerProps extends Marginless {
  content?: string
  unboxed?: boolean
  className?: string
}
