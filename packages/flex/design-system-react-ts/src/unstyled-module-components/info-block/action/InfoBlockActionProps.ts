import { ClickEvent } from '../../../events/index.js'

export interface InfoBlockActionProps {
  children?: React.ReactNode | string
  className?: string
  onClick?: ClickEvent
}
