'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { RowsItemProps } from './RowItemProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { row, isNarrow } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Rows Item Component
 * @param narrow {boolean} Align same elements horizontaly
 * - -------------------------- WEB PROPERTIES -------------------
 *  @param className {string} additionnal CSS Classes
 */
const RowItem = ({ className, classList, narrow, ...others }: RowsItemProps): React.JSX.Element => {
  const classes = classNames(row, narrow && isNarrow, className, validate(classList))

  return <div className={classes} {...others} />
}

export default RowItem
