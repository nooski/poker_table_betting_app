import React from 'react'
import classNames from 'classnames'
import { ColumnsItem } from '../../columns/index.js'
import { SliderItemProps } from './SliderItemProps.js'
import { is } from '../../../services/index.js'

/**
 * Slider Item component
 * @param className {string} Additionnal css classes
 * @param children {ReactNode} Slider Item child
 * @param activeBool {boolean} Default active item
 */
const SliderItem = ({ children, activeBool, className, ...others }: SliderItemProps): React.JSX.Element => {
  const classes = classNames(activeBool && is('active'), className)

  return (
    <ColumnsItem size={12} className={classes} {...others} data-slider-page>
      {children}
    </ColumnsItem>
  )
}

export default SliderItem
