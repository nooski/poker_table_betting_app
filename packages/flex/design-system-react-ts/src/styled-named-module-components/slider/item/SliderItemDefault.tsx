'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { ColumnsItem } from '../../columns/index.js'
import { SliderItemProps } from './SliderItemProps.js'
// import { Fade } from 'react-awesome-reveal'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { isActive } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Slider Item component
 * @param className {string} Additionnal css classes
 * @param children {ReactNode} Slider Item child
 * @param activeBool {boolean} Default active item
 */
const SliderItem = ({ children, activeBool, className, classList, ...others }: SliderItemProps): React.JSX.Element => {
  const classes = classNames(activeBool && isActive, className, validate(classList))

  return (
    // <Slide direction='right'>
    //   <ColumnsItem size={12} className={classes} {...others} data-slider-page>
    //     {children}
    //   </ColumnsItem>
    // </Slide>
    // <Fade cascade={true} damping={0.25} triggerOnce={true} direction='right' duration={1000}>
    //   <ColumnsItem size={12} className={classes} {...others} data-slider-page>
    //     {children}
    //   </ColumnsItem>
    // </Fade>
    <ColumnsItem size={12} className={classes} {...others} data-slider-page>
      {children}
    </ColumnsItem>
  )
}

export default SliderItem
