import { GenericChildren } from '../../../generics/index.js'

/**
 * Slider Step Interface
 */

export interface SliderItemProps {
  children?: GenericChildren | string
  activeBool?: boolean
  className?: string
  classList?: string[]
}
