'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../services/index.js'
import { camelCase } from 'lodash'
import { TableProps } from './TableProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { table } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Table Component
 * @param bordered {boolean} bordered table
 * - -------------------------- WEB PROPERTIES -------------------------------
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param fullwidth {boolean} Fullwidth table
 * @param comparative {boolean} If specific design add this
 */
const Table = ({ className, classList, fullwidth, bordered, comparative, ...others }: TableProps): React.JSX.Element => {
  const classes = classNames(
    table,
    fullwidth && camelCase(is('fullwidth')),
    bordered && camelCase(is('bordered')),
    comparative && camelCase(is('comparative')),
    className,
    validate(classList),
  )

  return <table className={classes} {...others} />
}

export default Table
