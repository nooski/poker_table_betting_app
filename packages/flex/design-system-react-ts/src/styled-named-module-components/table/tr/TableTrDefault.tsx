'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { TableTrProps } from './TableTrProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { isExpandable, isExpanded } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Table TR Component
 * @param expandable {boolean} Lines can display additional information
 * @param expanded {boolean} An unfolded line will also receive class
 * - -------------------------- WEB PROPERTIES -------------------------------
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 */
const TableTr = ({ className, classList, expandable, expanded, ...others }: TableTrProps): React.JSX.Element => {
  const classes = classNames(expandable && isExpandable, expanded ? isExpanded : null, className, validate(classList))

  return <tr className={classes} {...others} />
}

export default TableTr
