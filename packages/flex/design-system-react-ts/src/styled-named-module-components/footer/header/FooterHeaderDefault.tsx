'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { FooterHeaderWebProps } from './FooterHeaderProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { accordionHeader } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Footer Sub Component - Subfooter
 * @param children {ReactNode} Children for Subfooter
 * @param className {string} Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 */
const FooterHeader = ({ className, classList, ...others }: FooterHeaderWebProps): React.JSX.Element => {
  const classes = classNames(accordionHeader, className, validate(classList))

  return <div className={classes} {...others} />
}

export default FooterHeader
