'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { FooterDesktopWebProps } from './FooterDesktopProps.js'
import { Accordion } from '../../accordion/index.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { isHiddenTouch, isFooterDesktop, isFullwidth } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Footer Desktop Component
 * @param children {ReactNode} Desktop Footer Children
 * @param className {string} Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 * @param fullwidth {boolean} Footer fullwidth
 */
const FooterDesktop = ({ children, className, classList, fullwidth, ...others }: FooterDesktopWebProps): React.JSX.Element => {
  const classes = classNames(isHiddenTouch, isFooterDesktop, fullwidth && isFullwidth, className, validate(classList))

  // Desktop Footer
  return (
    <Accordion className={classes} {...others}>
      {children}
    </Accordion>
  )
}

export default FooterDesktop
