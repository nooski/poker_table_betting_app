'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { FooterWrapperWebProps } from './FooterWrapperProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { accordion, isActive } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Footer Wrapper Component - Article for Header & Body
 * @param children {ReactNode} Children for Subfooter
 * @param className {string} Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 */
const FooterWrapper = ({ className, classList, ...others }: FooterWrapperWebProps): React.JSX.Element => {
  const classes = classNames(accordion, isActive, className, validate(classList))

  return <article className={classes} {...others} />
}

export default FooterWrapper
