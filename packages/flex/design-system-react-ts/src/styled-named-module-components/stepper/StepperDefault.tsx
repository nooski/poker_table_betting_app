'use client'

import React from 'react'
import classNames from 'classnames'
import { has, validate } from '../../services/index.js'
import { camelCase } from 'lodash'
import { StepperProps } from './StepperProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { stepper, section } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Stepper Component
 * @param className Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 * @param centered Center the stepper
 */
const Stepper = ({ className, classList, centered, ...others }: StepperProps): React.JSX.Element => {
  const classes = classNames(stepper, className, validate(classList))

  const centerClasses = classNames(section, camelCase(has('text-centered')), className)

  if (centered) {
    return (
      <section className={centerClasses}>
        <div className={classes} {...others} />
      </section>
    )
  }

  return <div className={classes} {...others} />
}

export default Stepper
