import { StepperStepMarkup } from './StepperStepEnum.js'

import { GenericChildren } from '../../../generics/index.js'

/**
 * Stepper Step Interface
 */
export interface StepperStepProps {
  children?: GenericChildren | string
  activeBool?: boolean
  current?: boolean
  done?: boolean
  label?: string
  step?: number | string
  className?: string
  classList?: string[]
  markup?: StepperStepMarkup
}
