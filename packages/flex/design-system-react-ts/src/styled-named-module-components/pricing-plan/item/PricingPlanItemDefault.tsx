'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../../services/index.js'
import { camelCase } from 'lodash'
import { PricingPlanItemWebProps } from './PricingPlanItemProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { planItem, isNarrow } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Pricing Plan Item Component
 * @param children {ReactNode} Children
 * @param className {string} Additionnal css classes
 * @param spacing {SpacingLevel} 1 to 12
 * @param narrow {boolean} Apply narrow
 */
const PricingPlanItems = ({ className, classList, spacing, narrow, ...others }: PricingPlanItemWebProps): React.JSX.Element => {
  const classes = classNames(planItem, spacing && camelCase(is(`${spacing}`)), narrow && isNarrow, className, validate(classList))

  return <div className={classes} {...others} />
}

export default PricingPlanItems
