'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { PricingPlanProductWebProps } from './PricingPlanProductProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { pricingPlanProduct, hasHat } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Pricing Plan Product Component
 * @param children {ReactNode} Children
 * @param className {string} Additionnal css classes
 * @param hat {boolean} Has hat
 */
const PricingPlanProduct = ({ className, classList, hat, ...others }: PricingPlanProductWebProps): React.JSX.Element => {
  const classes = classNames(pricingPlanProduct, hat && hasHat, className, validate(classList))

  return <div className={classes} {...others} />
}

export default PricingPlanProduct
