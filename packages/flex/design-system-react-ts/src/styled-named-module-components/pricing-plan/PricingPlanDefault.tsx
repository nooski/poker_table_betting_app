'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
import { PricingPlanWebProps } from './PricingPlanProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { pricingPlan } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Pricing Plan Component
 * @param children {ReactNode} Children
 * @param className {string} Additionnal css classes
 */
const PricingPlan = ({ className, classList, ...others }: PricingPlanWebProps): React.JSX.Element => {
  const classes = classNames(pricingPlan, className, validate(classList))

  return <div className={classes} {...others} />
}

export default PricingPlan
