'use client'

import React from 'react'
import { ViewProps } from './ViewProps.js'
import classNames from 'classnames'
import { is, validate } from '../../services/index.js'
import { camelCase } from 'lodash'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
// import { isLoading, isLoaded } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * View Component (DIV EQUIVALENT)
 * @param children {string} View child
 * @param style {CSSProperties} View custom style
 * - ------------------ WEB PROPERTIES ---------------
 * @param className {string} Additionnal css classes
 * @param loading {Loading} Loading View
 * @param theme {Theme} Themed View
 */
const View = ({ children, style, className, classList, loading, theme, ...others }: ViewProps): React.JSX.Element => {
  const classes = classNames(loading && camelCase(is(loading.getClassName())), className, validate(classList))

  if (!children) {
    return <div style={style} {...(theme ? { ['data-flex-theme']: theme } : {})} {...others} />
  }

  return (
    <div style={style} {...(theme ? { ['data-flex-theme']: theme } : {})} className={classes} {...others}>
      {children}
    </div>
  )
}

export default View
