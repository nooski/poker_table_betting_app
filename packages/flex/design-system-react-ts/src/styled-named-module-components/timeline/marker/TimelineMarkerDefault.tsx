'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { TimelineMarkerWebProps } from './TimelineMarkerProps.js'
import { Icon, IconSize } from '../../icon/index.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { timelineMarker, isIcon, cardHeaderIcon, hasTextGrey } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Timeline Item Component
 * @param className {string} Additionnal CSS Classes
 * @param iconClassname {string} Additionnal CSS Classes for icon
 * @param iconName {IconName} Icon Name - sample : IconName.ENVELOPE
 */
const TimelineItem = ({ className, classList, iconClassname, iconName, ...others }: TimelineMarkerWebProps): React.JSX.Element => {
  const classes = classNames(timelineMarker, isIcon, className, validate(classList))

  return (
    <div className={classes} {...others}>
      <div className={cardHeaderIcon}>
        <Icon className={classNames(iconClassname, hasTextGrey)} name={iconName} size={IconSize.SMALL} />
      </div>
    </div>
  )
}

export default TimelineItem
