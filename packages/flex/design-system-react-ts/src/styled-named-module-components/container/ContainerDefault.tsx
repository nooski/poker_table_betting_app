'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
// import { camelCase } from 'lodash'
import { ContainerProps } from './ContainerProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { container, isFluid } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Container Component
 * @param fluid {boolean} Make the container usable across the width of your section
 * - -------------------------- WEB PROPERTIES -------------------------------
 * @param className {string} Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 */
const Container = ({ className, classList, fluid, ...others }: ContainerProps): React.JSX.Element => {
  const classes = classNames(container, fluid && isFluid, className, validate(classList))

  return <div className={classes} {...others} />
}

export default Container
