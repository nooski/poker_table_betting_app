'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../services/index.js'
import { camelCase } from 'lodash'
import { TagProps } from './TagProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { tag, isHidden, isInverted, tags, hasAddons } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Tag Component
 * @param children {ReactNode} Add childrens for tag
 * @param variant {TagVariant} Available tag variants
 * @param deletable {boolean} Adding delete icon
 * @param inverted {boolean} Inverted tag
 * @param onClick {Function} OnClick Event
 * @param className {string} Additionnal CSS Classes
 * @param hovered {boolean} Hover mode
 **/
const Tag = ({
  children,
  className,
  classList,
  variant,
  hovered,
  deletable,
  onClick,
  onMouseEnter,
  onMouseLeave,
  inverted,
  ...others
}: TagProps): React.JSX.Element => {
  const [display, setDisplay] = React.useState<boolean>(deletable || false)
  const [_hoveredItem, setHoveredItem] = React.useState<boolean>(hovered ?? false)
  const classes = classNames(
    tag,
    deletable && isHidden,
    variant && camelCase(is(`${variant}`)),
    inverted && isInverted,
    _hoveredItem && camelCase(is('hovered')),
    className,
    validate(classList),
  )

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onClickHandle = (e: any) => {
    setDisplay(!display)
    if (onClick) {
      onClick(e)
    }
  }
  const onnMouseEnterHandle = (e: React.SyntheticEvent) => {
    setHoveredItem(true)
    if (onMouseEnter) {
      onMouseEnter(e)
    }
  }

  const onMouseLeaveHandle = (e: React.SyntheticEvent) => {
    setHoveredItem(false)
    if (onMouseLeave) {
      onMouseLeave(e)
    }
  }

  const deleteClasses = classNames(
    tags,
    variant && camelCase(is(`${variant}`)),
    deletable && [hasAddons, is('delete')],
    inverted && isInverted,
    _hoveredItem && camelCase(is('hovered')),
    className,
    validate(classList),
  )

  React.useEffect(() => {
    setDisplay(deletable || false)
  }, [deletable])

  React.useEffect(() => {
    setHoveredItem(hovered ?? false)
  }, [hovered])

  // Deletable tag
  if (deletable && display) {
    return (
      <div className={deleteClasses} onMouseEnter={onnMouseEnterHandle} onMouseLeave={onMouseLeaveHandle}>
        <span className={classNames(tag)}>{children}</span>
        <button onClick={onClickHandle} className={classNames(tag)} />
      </div>
    )
  }

  // Default tag
  return (
    <span className={classes} onClick={onClick && onClick} onMouseEnter={onnMouseEnterHandle} onMouseLeave={onMouseLeaveHandle} {...others}>
      {children}
    </span>
  )
}

export default Tag
