'use client'

import React from 'react'
import classNames from 'classnames'
import { is, has, validate } from '../../services/index.js'
import { camelCase } from 'lodash'
import { ProductTourWebProps } from './ProductTourProps.js'
import { Icon, IconName, IconSize } from '../icon/index.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { productTour, isActive, arrow, icon, isMedium, isRounded, close } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Product Tour Component
 * @param children {ReactNode} Title child
 * @param className {string} Additionnal css classes
 * @param activeBool {boolean} _activeItem component
 * @param arrowDirection {ArrowDirection} UP|BOTTOM||LEFT|RIGHT - _activeItem arrow
 * @param arrowAlign {ArrowAlign} ONE_FIFTH|ONE_QUARTER|ONE_THRID|TWO_FIFTHS|THREE_FIFTHS|
 * TWO_THIRDS|THREE_QUARTERS|FOUR_FIFTHS
 * @param closeable {boolean} _activeItem close icon
 * @param avatarSrc {string} _activeItem avatar if source
 * @param avatarDirection {AvatarDirection} LEFT|RIGHT
 */
const ProductTour = ({
  children,
  className,
  classList,
  activeBool,
  arrowDirection,
  arrowAlign,
  closeable,
  avatarSrc,
  avatarDirection,
  ...others
}: ProductTourWebProps): React.JSX.Element => {
  const [_activeItem, setActiveItem] = React.useState<boolean>(activeBool || false)

  React.useEffect(() => {
    setActiveItem(activeBool || false)
  }, [activeBool])

  const classes = classNames(
    productTour,
    _activeItem && isActive,
    // avatarDirection && has(`icon-${avatarDirection}`),
    avatarDirection && camelCase(has(`icon-${avatarDirection}`)),
    className,
    validate(classList),
  )

  return (
    <div className={classes} {...others}>
      {arrowDirection && <div className={classNames(arrow, camelCase(is(arrowDirection)), arrowAlign && camelCase(is(arrowAlign)))} />}
      {avatarSrc && (
        <span className={classNames(icon, isMedium)}>
          <img className={isRounded} src={avatarSrc} alt={'avatar'} />
        </span>
      )}
      {closeable && (
        <div style={{ cursor: 'pointer' }} onClick={() => setActiveItem(!_activeItem)}>
          <Icon size={IconSize.SMALL} name={IconName.UI_TIMES} className={close} />
        </div>
      )}
      <div className={'product-tour-content'}>{children}</div>
    </div>
  )
}

export default ProductTour
