'use client'

import React from 'react'
import classNames from 'classnames'
import { is, has } from '../../services/index.js'
import { ProductTourWebProps } from './ProductTourProps.js'
import { Icon, IconName, IconSize } from '../icon/index.js'

/**
 * Product Tour Component
 * @param children {ReactNode} Title child
 * @param className {string} Additionnal css classes
 * @param activeBool {boolean} _activeItem component
 * @param arrowDirection {ArrowDirection} UP|BOTTOM||LEFT|RIGHT - _activeItem arrow
 * @param arrowAlign {ArrowAlign} ONE_FIFTH|ONE_QUARTER|ONE_THRID|TWO_FIFTHS|THREE_FIFTHS|
 * TWO_THIRDS|THREE_QUARTERS|FOUR_FIFTHS
 * @param closeable {boolean} _activeItem close icon
 * @param avatarSrc {string} _activeItem avatar if source
 * @param avatarDirection {AvatarDirection} LEFT|RIGHT
 */
const ProductTour = ({
  children,
  className,
  activeBool,
  arrowDirection,
  arrowAlign,
  closeable,
  avatarSrc,
  avatarDirection,
  ...others
}: ProductTourWebProps): React.JSX.Element => {
  const [_activeItem, setActiveItem] = React.useState<boolean>(activeBool || false)

  React.useEffect(() => {
    setActiveItem(activeBool || false)
  }, [activeBool])

  const classes = classNames('product-tour', _activeItem && is('active'), avatarDirection && has(`icon-${avatarDirection}`), className)

  return (
    <div className={classes} {...others}>
      {arrowDirection && <div className={classNames('arrow', is(arrowDirection), arrowAlign && is(arrowAlign))} />}
      {avatarSrc && (
        <span className={classNames('icon', is('medium'))}>
          <img className={is('rounded')} src={avatarSrc} alt={'avatar'} />
        </span>
      )}
      {closeable && (
        <div style={{ cursor: 'pointer' }} onClick={() => setActiveItem(!_activeItem)}>
          <Icon size={IconSize.SMALL} name={IconName.UI_TIMES} className='close' />
        </div>
      )}
      <div className='product-tour-content'>{children}</div>
    </div>
  )
}

export default ProductTour
