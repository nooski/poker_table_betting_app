'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { NavbarDropdownWebProps } from './NavbarDropdownProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { navbarDropdown } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Navbar Dropdown Component
 * @param children {ReactNode} Children
 * @param className {string} Additionnal css classes
 */
const NavbarDropdown = ({ className, classList, ...others }: NavbarDropdownWebProps): React.JSX.Element => {
  const classes = classNames(navbarDropdown, className, validate(classList))

  return <div className={classes} {...others} />
}

export default NavbarDropdown
