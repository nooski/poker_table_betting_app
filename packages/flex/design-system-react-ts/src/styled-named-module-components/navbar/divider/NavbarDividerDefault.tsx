'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { NavbarDividerWebProps } from './NavbarDividerProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { navbarBrand } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Navbar Divider
 * @param children {ReactNode} Navbar Divider Children
 * @param className {string} Additionnal css classes
 */
const NavbarDivider = ({ className, classList, ...others }: NavbarDividerWebProps): React.JSX.Element => {
  const classes = classNames(navbarBrand, className, validate(classList))

  return <div className={classes} {...others} />
}

export default NavbarDivider
