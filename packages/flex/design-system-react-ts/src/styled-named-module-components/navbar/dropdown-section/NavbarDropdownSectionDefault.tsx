'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../../services/index.js'
import { NavbarDropdownSectionWebProps } from './NavbarDropdownSectionProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { navbarDropdownSection } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Navbar Dropdown Section Component
 * @param children {ReactNode} Children
 * @param className {string} Additionnal css classes
 * @param extras {boolean} Adding extras content
 */
const NavbarDropdownSection = ({ className, classList, extras, ...others }: NavbarDropdownSectionWebProps): React.JSX.Element => {
  const classes = classNames(navbarDropdownSection, extras && is('extras'), className, validate(classList))

  return <div className={classes} {...others} />
}

export default NavbarDropdownSection
