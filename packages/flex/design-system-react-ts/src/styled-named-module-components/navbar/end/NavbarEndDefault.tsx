'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { NavbarEndWebProps } from './NavbarEndProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { navbarEnd, navbarIcons } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Navbar Navbar End Component
 * @param children {ReactNode} Children
 * @param className {string} Additionnal css classes
 */
const NavbarEnd = ({ children, className, classList, ...others }: NavbarEndWebProps): React.JSX.Element => {
  const classes = classNames(navbarEnd, className, validate(classList))

  return (
    <div className={classes} {...others}>
      <div className={navbarIcons}>{children}</div>
    </div>
  )
}

export default NavbarEnd
