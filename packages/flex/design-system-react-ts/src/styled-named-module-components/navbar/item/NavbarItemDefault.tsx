'use client'

import React from 'react'
import classNames from 'classnames'
import { nanoid } from 'nanoid'
import { validate } from '../../../services/index.js'
import { NavbarItemWebProps } from './NavbarItemProps.js'
import { NavbarItemMarkup } from './NavbarItemEnum.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import {
  navbarItem,
  isMegadropdownParent,
  isHoverable,
  isHiddenMobile,
  isHiddenTablet,
  isHiddenDesktop,
  isAlternate,
  isActive,
} from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Navbar Item Component
 * @param children {ReactNode} Children
 * @param className {string} Additionnal css classes
 * @param id {string} Custom id
 * @param markup {NavbarItemMarkup} Available : (DIV|A|SPAN|P) : Default : DIV
 * @param hoverable {boolean} Hoverable item
 * @param hiddenMobile {boolean} Hide on mobile
 * @param hiddenTablet {boolean} Hide on tablet
 * @param hiddenDesktop {boolean} Hide on desktop
 * @param alternate {boolean} Use it for connect button
 */
const NavbarItem = ({
  children,
  id,
  className,
  classList,
  megaDropdown,
  hoverable,
  markup,
  hiddenMobile,
  hiddenTablet,
  hiddenDesktop,
  alternate,
  activeBool,
  ...others
}: NavbarItemWebProps): React.JSX.Element => {
  const classes = classNames(
    navbarItem,
    megaDropdown && isMegadropdownParent,
    hoverable && isHoverable,
    hiddenMobile && isHiddenMobile,
    hiddenTablet && isHiddenTablet,
    hiddenDesktop && isHiddenDesktop,
    alternate && isAlternate,
    activeBool && isActive,
    className,
    validate(classList),
  )

  const Tag = markup && (markup in NavbarItemMarkup || Object.values(NavbarItemMarkup).includes(markup)) ? markup : 'div'

  const idGenerated = nanoid()

  return (
    <Tag id={id || idGenerated} className={classes} {...others}>
      {children}
    </Tag>
  )
}

export default NavbarItem
