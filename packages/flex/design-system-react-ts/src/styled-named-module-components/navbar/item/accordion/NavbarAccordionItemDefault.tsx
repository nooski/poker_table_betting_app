'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../../services/index.js'
import { NavbarAccordionItemWebProps } from './NavbarAccordionItemProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { navbarItemAccordion, navbarItemAccordionHeader, navbarItemAccordionContent } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Navbar Accordion Item Component
 * @param children {ReactNode} Children
 * @param className {string} Additionnal css classes
 * @param headerContent {string} Content text for navbar-item-accordion-header
 */
const NavbarAccordionItem = ({ children, className, classList, headerContent, ...others }: NavbarAccordionItemWebProps): React.JSX.Element => {
  const classes = classNames(navbarItemAccordion, className, validate(classList))
  const headerClasses = classNames(navbarItemAccordionHeader)
  const contentClasses = classNames(navbarItemAccordionContent)

  return (
    <div className={classes} {...others}>
      <div className={headerClasses}>{headerContent}</div>
      <div className={contentClasses}>{children}</div>
    </div>
  )
}

export default NavbarAccordionItem
