'use client'

import React from 'react'
import classNames from 'classnames'
// import { camelCase } from 'lodash'
import { validate } from '../../../services/index.js'
import { InfoBlockHeaderProps } from './InfoBlockHeaderProps.js'
import { InfoBlockStatus } from '../InfoBlockEnum.js'
import { Icon, IconName } from '../../icon/index.js'
import { Title, TitleLevel } from '../../title/index.js'
// import { is } from '../../../services/index.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { hasTextCentered, isLarge, isWarning, isSuccess } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Info Block Header
 * @param children {string} Header title content
 * @param status {InfoBlockStatus} Icon status for header => SUCCESS|WARNING|DANGER
 * @param customIcon {IconName} Custom Icon for Info Block Header
 * - -------------------------- WEB PROPERTIES -------------------------------
 * @param className {string} Additionnal CSS Classes
 */
const InfoBlockHeader = ({ className, classList, status, children, customIcon, ...others }: InfoBlockHeaderProps): React.JSX.Element => {
  const classes = classNames('info-block-header', hasTextCentered, className, validate(classList))

  return (
    <header className={classes} {...others}>
      {status && (
        <Icon
          name={(customIcon && customIcon) || (status === InfoBlockStatus.WARNING && IconName.EXCLAMATION_CIRCLE) || IconName.CHECK_CIRCLE}
          className={classNames(
            isLarge,
            // camelCase(is(`${status}`)),
            {
              [isWarning]: status === InfoBlockStatus.WARNING,
              [isSuccess]: status === InfoBlockStatus.SUCCESS,
            },
          )}
        />
      )}
      <span>{children && typeof children.valueOf() === 'string' ? <Title level={TitleLevel.LEVEL3}>{children}</Title> : children}</span>
    </header>
  )
}

export default InfoBlockHeader
