'use client'

import React from 'react'
import classNames from 'classnames'
import { has, validate } from '../../../services/index.js'
import { camelCase } from 'lodash'
import { ProgressRadialProps } from './ProgressRadialProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { radialProgressBar, label as labelClass, description as descriptionClass, pie } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Progress Radial component
 * @param percent {number} Progress percent
 * @param label {string} Custom label
 * @param description {string} Custom description
 * -------------------------- WEB PROPERTIES -------------------------------
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * -------------------------- NATIVE PROPERTIES -------------------------------
 * @param alert {AlertState} Progress alert variant (SUCCESS|INFO|WARNING|DANGER|TERTIARY)
 * @param full {boolean} Full progressRadial
 * @param disk {boolean} Disk ProgressRadial
 */
const ProgressRadial = ({ className, classList, percent, label, description, ...others }: ProgressRadialProps): React.JSX.Element => {
  const classes = classNames(radialProgressBar, percent && camelCase(`progress-${percent}`), className, validate(classList))

  return (
    <div className={classes} {...others}>
      {percent && (!label || !description) && <span className={labelClass}>{percent}%</span>}
      {(label || description) && (
        <>
          {label && <span className={classNames(labelClass, camelCase(has('description')))}>{label}</span>}
          {description && <span className={descriptionClass}>{description}</span>}
        </>
      )}
      <div className={pie} />
    </div>
  )
}

export default ProgressRadial
