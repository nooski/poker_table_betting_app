'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../services/index.js'
import { camelCase } from 'lodash'
import { PopoverWebProps } from './PopoverProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { popover, isPopoverActive, popoverContent } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Popover Component
 * @param children {ReactNode} Popover children
 * @param className {string} Additionnal CSS Classes
 * @param direction {PopoverDirection} Popover direction (DOWN|LEFT|RIGHT)
 * @param children {ReactNode} Content React Node Element
 * @param content {ReactNode} Content of the popover (hidden popover if null|undefined)
 * @param activeBool {boolean} Is the popover active
 * @param arrowPosition {PopoverArrowPosition} Position of the popover arrow
 */
const Popover = ({
  className,
  classList,
  direction,
  children,
  activeBool,
  arrowPosition,
  content,
  ...others
}: PopoverWebProps): React.JSX.Element => {
  const classes = classNames(
    popover,
    direction != null && camelCase(is(`popover-${direction}`)),
    activeBool && isPopoverActive,
    arrowPosition != null && camelCase(is(`arrow-${arrowPosition}`)),
    className,
    validate(classList),
  )

  return (
    <div className={classes} {...others}>
      {children}
      {content != null && <div className={popoverContent}>{content}</div>}
    </div>
  )
}

export default Popover
