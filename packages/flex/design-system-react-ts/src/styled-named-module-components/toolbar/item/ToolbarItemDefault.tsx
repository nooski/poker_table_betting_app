'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { ToolbarItemWebProps } from './ToolbarItemProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { toolbarItem, isClippedToBottom } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Toolbar Item Component
 * @param className {string} Additionnal CSS Classes
 * @param clippedToBottom {boolean} Is clipped to bottom
 */
const ToolbarItem = ({ className, classList, clippedToBottom, ...others }: ToolbarItemWebProps): React.JSX.Element => {
  const classes = classNames(toolbarItem, clippedToBottom && isClippedToBottom, className, validate(classList))

  return <div className={classes} {...others} />
}

export default ToolbarItem
