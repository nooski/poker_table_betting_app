'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { ToolbarSpaceWebProps } from './ToolbarSpaceProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { toolbarSpace } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Toolbar Space Component
 * @param className {string} Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 */
const ToolbarSpace = ({ className, classList, ...others }: ToolbarSpaceWebProps): JSX.Element => (
  <div className={classNames(toolbarSpace, className, validate(classList))} {...others} />
)

export default ToolbarSpace
