'use client'

import React from 'react'
import classNames from 'classnames'
import { has, validate } from '../../services/index.js'
import { camelCase } from 'lodash'
import { ToolbarWebProps } from './ToolbarProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { toolbar } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Toolbar Component
 * @param className {string} Additionnal CSS Classes
 * @param background {BackgroundStyle} Custom background color
 */
const Toolbar = ({ className, classList, background, ...others }: ToolbarWebProps): React.JSX.Element => {
  const classes = classNames(toolbar, background && camelCase(has(background.getClassName())), className, validate(classList))

  return <div className={classes} {...others} />
}

export default Toolbar
