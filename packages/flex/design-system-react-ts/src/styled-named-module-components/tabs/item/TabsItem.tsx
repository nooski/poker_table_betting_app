/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */

import React from 'react'
import classNames from 'classnames'
import { Text, TextMarkup } from '../../text/index.js'
import { TabsItemProps } from './TabsItemProps.js'
import { TargetElement } from '../../../generics/index.js'

/**
 * Tabs Item Component
 * @param activeBool {boolean} active tab item
 * @param children {ReactChild} React Child Element
 * @param onClick onClick Event
 * - -------------------------- WEB PROPERTIES -------------------------------
 * @param className {string} Additionnal CSS Classes
 */
const TabsItem = ({ activeBool, children, className, onClick, ...others }: TabsItemProps): React.JSX.Element => {
  const [_activeItem, setActiveItem] = React.useState<boolean>(activeBool || false)

  // accessibility
  const a11y = {
    li: {
      role: 'presentation',
    },
    a: {
      role: 'tab',
      'aria-selected': _activeItem,
    },
  }

  React.useEffect(() => {
    setActiveItem(activeBool || false)
  }, [activeBool])

  return (
    <li
      className={classNames(className, { 'is-active': _activeItem })}
      {...a11y.li}
      {...others}
      onClick={(e: React.MouseEvent) => {
        const target = e.target as TargetElement
        setActiveItem(activeBool || false)
        target.active = activeBool
        if (onClick) {
          onClick(e)
        }
      }}
    >
      {children && typeof children.valueOf() === 'string' && (
        <Text markup={TextMarkup.A} {...a11y.a} {...others}>
          {String(children)}
        </Text>
      )}
    </li>
  )
}

export default TabsItem
