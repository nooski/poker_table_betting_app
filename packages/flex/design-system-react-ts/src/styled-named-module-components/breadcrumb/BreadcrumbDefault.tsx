'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
// import { camelCase } from 'lodash'
import { BreadcrumbWebProps } from './BreadcrumbProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { breadcrumb } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Breadcrumb Component
 * @param children {ReactNode} Breadcrumb Children
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 */
const Breadcrumb = ({ children, className, classList, ...others }: BreadcrumbWebProps): React.JSX.Element => {
  return (
    <nav className={classNames(breadcrumb, className, validate(classList))} aria-label='breadcrumbs' {...others}>
      <ul>{children}</ul>
    </nav>
  )
}

export default Breadcrumb
