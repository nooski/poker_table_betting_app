import React from 'react'
import classNames from 'classnames'
import { Text, TextMarkup } from '../../text/index.js'
import { BreadcrumbItemWebProps } from './BreadcrumbItemProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { isActive } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Breadcrumb Item Component
 * @param children {string} Breadcrumb Item Text
 * @param activeBool {boolean} Active link
 * @param className {string} Additionnal CSS Classes
 * @param href {string} Url
 */
const BreadcrumbItem = ({ children, activeBool, className, ...others }: BreadcrumbItemWebProps): React.JSX.Element => {
  const classes = classNames(activeBool && isActive, className)

  return (
    <li className={classes}>
      <Text markup={TextMarkup.A} {...others}>
        {children}
      </Text>
    </li>
  )
}

export default BreadcrumbItem
