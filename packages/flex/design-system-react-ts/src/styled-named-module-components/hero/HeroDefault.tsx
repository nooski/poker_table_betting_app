'use client'

import React from 'react'
import classNames from 'classnames'
import { has, validate } from '../../services/index.js'
import { camelCase } from 'lodash'
import { HeroProps } from './HeroProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { hero, isPrimary, hasBackground, heroBody } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Hero Component
 * @param children {ReactNode} Hero Children
 * @param backgroundSrc {string} If source, it will display background option
 * @param variant {VariantState} Hero background color : primary|secondary|tertiary
 * - -------------------------- WEB PROPERTIES -------------------------------
 * @param className {string} Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 */
const Hero = ({ children, className, classList, backgroundSrc, variant, ...others }: HeroProps): React.JSX.Element => {
  const classes = classNames(
    hero,
    variant && camelCase(has(`background-${variant.getClassName()}`)),
    backgroundSrc && [isPrimary, hasBackground],
    className,
    validate(classList),
  )

  if (variant) {
    return (
      <section className={classes} {...others}>
        <div className={classNames(heroBody)}>{children}</div>
      </section>
    )
  }

  return (
    <section {...(backgroundSrc && { style: { backgroundImage: `url(${backgroundSrc})` } })} className={classes} {...others}>
      <div className={classNames(heroBody)}>{children}</div>
    </section>
  )
}

export default Hero
