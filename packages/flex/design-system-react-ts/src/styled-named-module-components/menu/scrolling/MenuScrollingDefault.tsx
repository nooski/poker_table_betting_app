'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { camelCase } from 'lodash'
import { MenuScrollingProps } from './MenuScrollingProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { menu, hasBackgroundWhite as hasBackgroundWhiteClass } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

const a11y = { role: 'scrolling-menu' }

/**
 * Menu Component
 *  @param className {string} Additionnal CSS Classes
 *  @param children {number} ReactNode} Dropdown Children
 */

const MenuScrolling = ({ className, classList, hasBackgroundWhite, pulled, ...others }: MenuScrollingProps): React.JSX.Element => {
  /**
   * If no markup return p with default level 1
   */
  const classes = classNames(
    menu,
    camelCase(`is-pulled-${pulled ?? 'left'}`),
    hasBackgroundWhite && hasBackgroundWhiteClass,
    className,
    validate(classList),
  )

  return <aside className={classes} {...a11y} {...others} />
}

export default MenuScrolling
