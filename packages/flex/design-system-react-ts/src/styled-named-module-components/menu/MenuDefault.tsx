'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
import { MenuWebProps } from './MenuProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { menuList, asideMenuList } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

const a11y = { role: 'menu' }

/**
 * Menu Component
 *  @param className {string} Additionnal CSS Classes
 *  @param children {ReactNode} Dropdown Children
 *  @param notASide {ReactNode} Menu is in MenuScrolling
 */

const Menu = ({ className, classList, notASide, ...others }: MenuWebProps): JSX.Element => (
  <ul className={classNames(menuList, !notASide && asideMenuList, className, validate(classList))} {...a11y} {...others} />
)

export default Menu
