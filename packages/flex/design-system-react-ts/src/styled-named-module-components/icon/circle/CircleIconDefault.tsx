'use client'

import React from 'react'
import classNames from 'classnames'
import { getStatusBackground, is, validate } from '../../../services/index.js'
import { camelCase } from 'lodash'
import { IconProps } from '../IconProps.js'
import { IconStatus } from '../IconEnum.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { icon, hasTextWhite, isCircled } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

const CircleIcon = ({ className, classList, name, status, size, ...others }: IconProps): React.JSX.Element => {
  // }: IconProps): React.JSX.Element | React.DetailedHTMLProps<React.HTMLAttributes<HTMLSpanElement>, HTMLSpanElement> => {
  const background = getStatusBackground(status || '', IconStatus.TERTIARY)
  const classes = classNames(icon, hasTextWhite, camelCase(is(`${size}`)), isCircled, camelCase(background), className, validate(classList))

  const iconName = camelCase(name)

  return (
    <span className={classes} {...others}>
      <i className={iconName} />
    </span>
  )
}

export default CircleIcon
