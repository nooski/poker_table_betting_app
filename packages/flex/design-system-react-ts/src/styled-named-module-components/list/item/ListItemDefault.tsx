'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { ListItemProps, ListIconStatus } from './ListItemProps.js'
import { Icon, IconSize } from '../../icon/index.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { isDanger, isSuccess } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * ListItem Component
 * @param className {string} Additionnal CSS Classes
 */

const ListItem = ({ className, classList, children, customIcon, status, title, description }: ListItemProps): React.JSX.Element => {
  const classes = classNames(className, validate(classList))

  if (customIcon) {
    return (
      <li className={classes}>
        <Icon
          className={classNames({
            [isDanger]: status === ListIconStatus.DANGER,
            [isSuccess]: status === ListIconStatus.SUCCESS,
          })}
          name={customIcon}
          size={IconSize.SMALL}
        />
        <span>{children}</span>
      </li>
    )
  }

  if (title || description) {
    return (
      <li className={classes}>
        <b>{title}</b>
        <p>{children || description}</p>
        <br />
      </li>
    )
  }

  return <li className={classes}>{children}</li>
}

export default ListItem
