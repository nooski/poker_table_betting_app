'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
import { ListProps } from './ListProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { iconList } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * ListItem Component
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 */

const List = ({ className, classList, hasIcon, children, ...others }: ListProps): React.JSX.Element => {
  const classes = classNames(hasIcon && iconList, className, validate(classList))

  return (
    <ul className={classes} {...others}>
      {children}
    </ul>
  )
}

export default List
