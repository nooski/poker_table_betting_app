'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { ButtonListWebProps } from './ButtonListProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { buttons } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Button List Component
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 */
const ButtonList = ({ className, classList, ...others }: ButtonListWebProps): JSX.Element => (
  <div className={classNames(buttons, className, validate(classList))} {...others} />
)

export default ButtonList
