'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { AccordionHeaderProps } from './AccordionHeaderProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { toggle, button, isBordered, isShadowless, accordionHeader } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

// accessibility
const a11y = {
  'aria-label': 'toggle',
}

/**
 * Accordion Header
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param toggleBool {boolean} Toggle Header
 * @param toggleBox {string} toggle direction
 * @param toggleBoxClass {string} Additionnal Classes for toggle box
 */
const AccordionHeader = ({
  children,
  className,
  classList,
  toggleBool,
  toggleBox,
  toggleBoxClass,
  ...others
}: AccordionHeaderProps): React.JSX.Element => {
  const toggleBtnClasses = classNames(toggle, button, isBordered, isShadowless, toggleBoxClass)

  return (
    <div className={classNames(accordionHeader, className, validate(classList))} {...others}>
      {toggleBox === 'left' && <button className={toggleBtnClasses} {...a11y} />}
      {children}
      {toggleBool && <button className={toggle} {...a11y} />}
      {toggleBox === 'right' && <button className={toggleBtnClasses} {...a11y} />}
    </div>
  )
}

export default AccordionHeader
