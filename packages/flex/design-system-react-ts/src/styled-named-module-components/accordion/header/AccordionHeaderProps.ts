import { GenericChildren } from '../../../generics/index.js'

/**
 * AccordionHeader Interface
 */
export interface AccordionHeaderProps {
  // children?: React.ReactNode | undefined
  children?: GenericChildren | string
  toggleBool?: boolean
  toggleBox?: string
  toggleBoxClass?: string
  className?: string
  classList?: string[]
}
