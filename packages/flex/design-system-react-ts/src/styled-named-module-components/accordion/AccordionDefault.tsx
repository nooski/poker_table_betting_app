'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../services/index.js'
import { camelCase } from 'lodash'
import { AccordionProps } from './AccordionProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { accordions } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Accordion Component
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param boxed {boolean} Boxed Accordion
 */
const Accordion = ({ className, classList, boxed, ...others }: AccordionProps): React.JSX.Element => {
  const classes = classNames(accordions, boxed && camelCase(is('boxed')), className, validate(classList))
  return <section className={classes} {...others} />
}

export default Accordion
