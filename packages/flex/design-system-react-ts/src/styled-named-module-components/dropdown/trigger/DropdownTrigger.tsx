import React from 'react'
import classNames from 'classnames'
import { DropdownTriggerWebProps } from './DropdownTriggerProps.js'
import { has, is } from '../../../services/index.js'

/**
 * Dropdown Trigger Component
 * @param className {string} Additionnal CSS Classes
 * @param activeBool {boolean} Active trigger
 * @param onClick {onClick} onClick event
 * @param label {string} Trigger label
 * @param placeholder {string} Trigger placeholder
 */
const DropdownTrigger = ({ className, activeBool, onClick, label, name, ...others }: DropdownTriggerWebProps): React.JSX.Element => {
  const [_activeItem, setActiveItem] = React.useState<boolean>(activeBool || false)

  const classes = classNames('dropdown-trigger', _activeItem && is('_activeItem'), className)

  React.useEffect(() => {
    setActiveItem(activeBool || false)
  }, [activeBool])

  return (
    <div
      onClick={(e: React.MouseEvent) => {
        const target = e.target as HTMLFormElement
        setActiveItem(!_activeItem)
        target.active = !_activeItem
        if (onClick) {
          onClick({
            active: target.active,
          })
        }
      }}
      className={classes}
      {...others}
    >
      <div className='field'>
        <div className={`control ${has('dynamic-placeholder')}`}>
          <div className='select'>
            <select name={name} />
            <label className='input-dynamic-placeholder'>{label}</label>
          </div>
        </div>
      </div>
    </div>
  )
}

export default DropdownTrigger
