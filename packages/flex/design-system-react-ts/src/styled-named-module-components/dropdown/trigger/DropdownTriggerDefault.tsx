'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../../services/index.js'
// import { camelCase } from 'lodash'
import { DropdownTriggerWebProps } from './DropdownTriggerProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { dropdownTrigger, field, control, hasDynamicPlaceholder, select, inputDynamicPlaceholder } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Dropdown Trigger Component
 * @param className {string} Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 * @param activeBool {boolean} Active trigger
 * @param onClick {onClick} onClick event
 * @param label {string} Trigger label
 * @param placeholder {string} Trigger placeholder
 */
const DropdownTrigger = ({ className, classList, activeBool, onClick, label, name, ...others }: DropdownTriggerWebProps): React.JSX.Element => {
  const [_activeItem, setActiveItem] = React.useState<boolean>(activeBool || false)

  const classes = classNames(dropdownTrigger, _activeItem && is('_activeItem'), className, validate(classList))

  React.useEffect(() => {
    setActiveItem(activeBool || false)
  }, [activeBool])

  return (
    <div
      onClick={(e: React.MouseEvent) => {
        const target = e.target as HTMLFormElement
        setActiveItem(!_activeItem)
        target.active = !_activeItem
        if (onClick) {
          onClick({
            active: target.active,
          })
        }
      }}
      className={classes}
      {...others}
    >
      <div className={field}>
        <div className={classNames(control, hasDynamicPlaceholder)}>
          <div className={select}>
            <select name={name} />
            <label className={inputDynamicPlaceholder}>{label}</label>
          </div>
        </div>
      </div>
    </div>
  )
}

export default DropdownTrigger
