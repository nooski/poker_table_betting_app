'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
// import { camelCase } from 'lodash'
import { DropdownWebProps } from './DropdownProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { dropdown, tile, isActive, field, control } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Dropdown Component
 * @param className {string} Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 * @param children {ReactNode} Dropdown Children
 * @param activeBool {boolean} Activated Dropdown
 */
const Dropdown = ({ className, classList, children, activeBool, ...others }: DropdownWebProps): React.JSX.Element => {
  const [_activeItem, setActiveItem] = React.useState<boolean>(activeBool || false)

  const classes = classNames(dropdown, tile, _activeItem && isActive, className, validate(classList))

  React.useEffect(() => {
    setActiveItem(activeBool || false)
  }, [activeBool])

  return (
    <div className={field}>
      <div className={control}>
        <div
          onClick={() => {
            setActiveItem(!_activeItem)
          }}
          className={classes}
          {...others}
        >
          {children}
        </div>
      </div>
    </div>
  )
}

export default Dropdown
