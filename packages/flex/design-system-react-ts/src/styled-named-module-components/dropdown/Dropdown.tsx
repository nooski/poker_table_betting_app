import React from 'react'
import classNames from 'classnames'
import { DropdownWebProps } from './DropdownProps.js'
import { is } from '../../services/index.js'

/**
 * Dropdown Component
 * @param className {string} Additionnal CSS Classes
 * @param children {ReactNode} Dropdown Children
 * @param activeBool {boolean} Activated Dropdown
 */
const Dropdown = ({ className, children, activeBool, ...others }: DropdownWebProps): React.JSX.Element => {
  const [_activeItem, setActiveItem] = React.useState<boolean>(activeBool || false)

  const classes = classNames('dropdown tile', _activeItem && is('active'), className)

  React.useEffect(() => {
    setActiveItem(activeBool || false)
  }, [activeBool])

  return (
    <div className='field'>
      <div className='control'>
        <div
          onClick={() => {
            setActiveItem(!_activeItem)
          }}
          className={classes}
          {...others}
        >
          {children}
        </div>
      </div>
    </div>
  )
}

export default Dropdown
