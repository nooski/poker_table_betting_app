'use client'

import React from 'react'
import classNames from 'classnames'
import { nanoid } from 'nanoid'
import { validate, is, has } from '../../services/index.js'
import { camelCase } from 'lodash'
import { CheckboxProps } from './CheckboxProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { input, field, control } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Checkbox Component
 * @param checked {boolean} Checked Checkbox
 * @param disabled {boolean} Disabled
 * @param readOnly {boolean} readonly Checkbox
 * @param id {string} Id for button, by default id is generate
 * @param label {string} Label for Checkbox
 * @param onClick {ClickEvent}
 * @param onChange {ChangeEvent}
 * @param name {string} Name for checkbox
 * @param inverted {boolean} Inveted Checkbox color
 * - -------------------------- WEB PROPERTIES -------------------------------
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param labelClassName {string} addtionnial CSS Classes for label
 * @param value {string} Value for checkbox
 * @param removeControl {boolean} Remove control class
 * @param removeField {boolean} Remove field class
 */
const Checkbox = ({
  checked,
  className,
  classList,
  disabled,
  readonly,
  id,
  label,
  labelClassName,
  onChange,
  onClick,
  name,
  value,
  inverted,
  removeControl,
  removeField,
  ...others
}: CheckboxProps): React.JSX.Element => {
  const [_checkedItem, setCheckedItem] = React.useState<boolean>(checked || false)

  const classes = classNames(
    input,
    camelCase(is('checkradio')),
    camelCase(is('info')),
    camelCase(is('hidden')),
    inverted && camelCase(is('inverted')),
    className && !className.includes(camelCase(is('inverted'))) && camelCase(has('background-color')),
    className,
    validate(classList),
  )

  const labelClasses = classNames(checked && camelCase(has('text-info')), labelClassName)

  React.useEffect(() => {
    if (!readonly) {
      setCheckedItem(checked || false)
    }
  }, [checked, readonly])

  const idGenerated = nanoid()

  return (
    <div className={(!removeField && field) || ''}>
      <div className={classNames(!removeControl && control, disabled && camelCase(is('disabled')))}>
        <input
          className={classes}
          type='checkbox'
          readOnly={readonly}
          id={id || idGenerated}
          disabled={disabled}
          name={name}
          value={value}
          checked={readonly ? checked : _checkedItem}
          onChange={(e: React.ChangeEvent) => {
            return e
          }}
          onClick={(e: React.MouseEvent) => {
            const target = e.target as HTMLInputElement
            if (!readonly && target.checked !== undefined) {
              setCheckedItem(target.checked)
            }
            target.value = value || ''
            if (onChange) {
              onChange({
                checkboxId: target.id,
                checkboxValue: target.value,
                checkboxName: target.name,
                checkboxChecked: target.checked,
              })
            }
            if (onClick) {
              onClick({
                checkboxId: target.id,
                checkboxValue: target.value,
                checkboxName: target.name,
                checkboxChecked: target.checked,
              })
            }
          }}
          {...others}
        />
        <label htmlFor={id || idGenerated} className={labelClasses}>
          {label}
        </label>
      </div>
    </div>
  )
}

export default Checkbox
