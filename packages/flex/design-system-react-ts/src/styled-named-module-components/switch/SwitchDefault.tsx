/* eslint-disable */
/* eslint-disable @typescript-eslint/no-unused-vars */

'use client'

import React from 'react'
import classNames from 'classnames'
import { nanoid } from 'nanoid'
import { is, validate } from '../../services/index.js'
import { camelCase } from 'lodash'
import { SwitchProps } from './SwitchProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { isSwitch, isCheckradio, isInverted, field, control, label as labelClass } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Switch Component
 * @param id {string} Is auto generate by default
 * @param label {string} Switch label
 * @param value {string} Switch value
 * @param checked {boolean} Checked switch
 * @param onChange {Function} onChange event
 * @param alert {AlertState} Alert Variant (INFO|SUCCESS|WARNING|DANGER)
 * @param disabled {boolean} Switch disabled
 * @param readonly {boolean} Switch readonly
 * @param name {string} Switch name
 * @param inverted {boolean} Invert switch color
 * -------------------------- WEB PROPERTIES -------------------------------
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 */

const Switch = ({
  className,
  classList,
  id,
  label,
  value,
  checked,
  onChange,
  alert,
  disabled,
  readonly,
  name,
  inverted,
  ...others
}: SwitchProps): React.JSX.Element => {
  const [_checkedItem, setCheckedItem] = React.useState<boolean>(checked || false)

  React.useEffect(() => {
    setCheckedItem(checked || false)
  }, [checked])

  const classes = classNames(
    isSwitch,
    isCheckradio,
    alert && camelCase(is(alert.getClassName())),
    disabled && camelCase(is(`${disabled}`)),
    inverted && isInverted,
    className,
    validate(classList),
  )

  React.useEffect(() => {
    if (!readonly) {
      setCheckedItem(checked || false)
    }
  }, [checked, readonly])

  const idGenerated = nanoid()

  return (
    <div className={field}>
      <div className={control}>
        <input
          onChange={(e) => {
            if (!readonly) {
              setCheckedItem(!_checkedItem)
            }
            if (onChange) {
              onChange({ switchState: e.target.checked, switchName: e.target.name })
            }
          }}
          name={name}
          value={value}
          checked={readonly ? checked : _checkedItem}
          readOnly={readonly}
          className={classes}
          id={`switch-${id || idGenerated}`}
          type='checkbox'
          {...others}
        />
        <label className={labelClass} htmlFor={`switch-${id || idGenerated}`}>{label}</label>
      </div>
    </div>
  )
}

export { Switch }
