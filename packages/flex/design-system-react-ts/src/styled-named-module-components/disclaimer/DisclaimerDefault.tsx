'use client'

import React from 'react'
import classNames from 'classnames'
import { is, validate } from '../../services/index.js'
// import { camelCase } from 'lodash'
import { DisclaimerWebProps } from './DisclaimerProps.js'
import { Accordion, AccordionItem, AccordionHeader, AccordionBody } from '../accordion/index.js'
import { Title, TitleLevel } from '../title/index.js'
import { Text } from '../text/index.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import {
  disclaimer,
  disclaimerHeader,
  isGrouped,
  accordionBody,
  isClipped,
  disclaimerContent,
  isActive,
  subtitle,
} from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Disclaimer component
 * @param children {React.ReactNode|string} Disclaimer Item Children
 * @param className {string} Additionnal css classes
 * @param classList {array} Additionnal css classes
 * @param title {string} Disclaimer Title
 * @param activeBool {boolean} Active Disclaimer Bar
 */
const Disclaimer = ({ children, className, classList, title, activeBool, ...others }: DisclaimerWebProps): React.JSX.Element => {
  const classes = classNames(disclaimer, is('tri'), className, validate(classList))

  const wrapperClasses = classNames(disclaimerHeader, isGrouped, is('tri'))

  const classesBody = classNames(accordionBody, isClipped, is('tri'))

  const classesContent = classNames(disclaimerContent, activeBool && isActive, subtitle, is('tri'))

  return (
    <Accordion className={classes} {...others}>
      <AccordionItem>
        <AccordionHeader className={wrapperClasses} toggleBox='left' toggleBoxClass={is('bordered')}>
          <Title className={is('tri')} level={TitleLevel.LEVEL6}>
            {title}
          </Title>
        </AccordionHeader>
        <AccordionBody className={classesBody}>
          <div className={classesContent}>{children && typeof children.valueOf() === 'string' ? <Text>{String(children)}</Text> : children}</div>
        </AccordionBody>
      </AccordionItem>
    </Accordion>
  )
}

export default Disclaimer
