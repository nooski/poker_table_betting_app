'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
import { SliceProps } from './SliceProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { slice, isDisabled, hasLongCta, field, control } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Slice Component
 * @param className {string} Additionnal CSS Classes
 * @param children {ReactNode} Children for Slice
 * @param onClick {Function} onClick Event Slice
 * @param disabled {boolean} Disabled Slice
 * @param longCta {boolean} Change to the CTA line
 * @param selectable {boolean} Apply Field, Control classes wrapped
 */
const Slice = ({ children, className, classList, onClick, disabled, longCta, selectable, ...others }: SliceProps): React.JSX.Element => {
  const classes = classNames(slice, disabled && isDisabled, longCta && hasLongCta, className, validate(classList))

  if (selectable) {
    return (
      <div onClick={onClick && onClick} className={classes} {...others}>
        <div className={field}>
          <div className={control}>{children}</div>
        </div>
      </div>
    )
  }

  return (
    <div onClick={onClick && onClick} className={classes} {...others}>
      {children}
    </div>
  )
}

export default Slice
