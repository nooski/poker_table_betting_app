'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { SliceIconProps } from './SliceIconProps.js'
import { Icon } from '../../icon/index.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { sliceIcon } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Slice Icon Component
 * @param className {string} Additionnal CSS Classes
 * @param iconSize {IconSize} Size for icon
 * @param iconName {IconName} Name for icon
 * @param iconColor {IconColor} Custom color for icon
 */
const SliceIcon = ({ className, classList, iconSize, iconName, iconColor, ...others }: SliceIconProps): React.JSX.Element => {
  const classes = classNames(sliceIcon, className, validate(classList))

  return (
    <div className={classes} {...others}>
      <Icon name={iconName} {...(iconColor && { color: iconColor })} {...(iconSize && { size: iconSize })} />
    </div>
  )
}

export default SliceIcon
