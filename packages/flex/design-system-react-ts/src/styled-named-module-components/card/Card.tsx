import React from 'react'
import classNames from 'classnames'
import { CardProps } from './CardProps.js'
import { is } from '../../services/index.js'

/**
 * Card Component
 * @param flat Adding border for Card content
 * @param horizontal Horizontal Card orientation
 * @param floating Floating card
 * - ------------------ WEB PROPERTIES -----------------------
 * @param className Additionnal CSS Classes
 * @param skeleton Loading card
 */
const Card = ({ className, flat, horizontal, floating, skeleton, ...others }: CardProps): React.JSX.Element => {
  const [_loadingItem, setLoadingItem] = React.useState<boolean>(skeleton || false)

  React.useEffect(() => {
    setLoadingItem(skeleton || false)
  }, [skeleton])

  const classes = classNames(
    'card',
    flat && is('flat'),
    horizontal && [is('horizontal'), is('vcentered')],
    floating && is('floating'),
    _loadingItem ? is('loading') : is('loaded'),
    className,
  )

  return <div className={classes} {...others} />
}

export default Card
