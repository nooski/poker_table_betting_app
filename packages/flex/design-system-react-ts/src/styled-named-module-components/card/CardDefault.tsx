'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
import { CardProps } from './CardProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { card, isFlat, isHorizontal, isVcentered, isFloating, isLoading, isLoaded } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Card Component
 * @param flat Adding border for Card content
 * @param horizontal Horizontal Card orientation
 * @param floating Floating card
 * - ------------------ WEB PROPERTIES -----------------------
 * @param className Additionnal CSS Classes
 * @param classList {array} Additionnal css classes
 * @param skeleton Loading card
 */
const Card = ({ className, classList, flat, horizontal, floating, skeleton, ...others }: CardProps): React.JSX.Element => {
  const [_loadingItem, setLoadingItem] = React.useState<boolean>(skeleton || false)

  React.useEffect(() => {
    setLoadingItem(skeleton || false)
  }, [skeleton])

  const classes = classNames(
    card,
    flat && isFlat,
    horizontal && [isHorizontal, isVcentered],
    floating && isFloating,
    _loadingItem ? isLoading : isLoaded,
    className,
    validate(classList),
  )

  return <div className={classes} {...others} />
}

export default Card
