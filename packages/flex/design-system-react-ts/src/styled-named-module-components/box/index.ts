// import Box from './Box'
import { Box } from './BoxDefault.js'
import BoxContent from './content/index.js'
import BoxFooter from './footer/index.js'
import BoxHeader from './header/index.js'
import BoxTableContainer from './table-container/index.js'

export * from './BoxProps.js'
export { Box, BoxHeader, BoxContent, BoxFooter, BoxTableContainer }
