'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../services/index.js'
import { PricingTableWebProps } from './PricingTableProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { pricingTable, isSpecial } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Pricing Table Component
 * @param children {ReactNode} Title child
 * @param className {string} Additionnal css classes
 * @param special {boolean} New offers Pricing Table
 */
const PricingTable = ({ className, classList, special, ...others }: PricingTableWebProps): React.JSX.Element => {
  const classes = classNames(pricingTable, special && isSpecial, className, validate(classList))

  return <div className={classes} {...others} />
}

export default PricingTable
