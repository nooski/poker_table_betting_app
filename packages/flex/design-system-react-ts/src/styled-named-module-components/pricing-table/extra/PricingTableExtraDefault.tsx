'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { PricingTableExtraWebProps } from './PricingTableExtraProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType named
import { pricingTableExtra } from '@flex-design-system/framework/named'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Pricing Table Extra Component
 * @param children {ReactNode} Title child
 * @param className {string} Additionnal css classes
 */
const PricingTableExtra = ({ className, classList, ...others }: PricingTableExtraWebProps): React.JSX.Element => {
  const classes = classNames(pricingTableExtra, className, validate(classList))

  return <div className={classes} {...others} />
}

export default PricingTableExtra
