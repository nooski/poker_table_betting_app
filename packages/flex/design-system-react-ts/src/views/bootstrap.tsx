import React, { lazy, Suspense } from 'react'
import { createRoot } from 'react-dom/client'
const container = document.getElementById('root')
const root = createRoot(container!) // createRoot(container) if not using TypeScript

const { FlexBrowserRouter } = await import('@flexiness/domain-lib-mobx-react-router')
// eslint-disable-next-line @typescript-eslint/no-unsafe-return
const Module = lazy(() => import(`${process.env.FLEX_GATEWAY_MODULE_CSS === 'default' ? './ModulesDefault' : './ModulesNamed'}`))

root.render(
  <React.StrictMode>
    <FlexBrowserRouter>
      <Suspense fallback={<div>Loading...</div>}>
        <Module />
      </Suspense>
    </FlexBrowserRouter>
  </React.StrictMode>,
)
