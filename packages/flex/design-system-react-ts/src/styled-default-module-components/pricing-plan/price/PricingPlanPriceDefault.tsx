'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { PricingPlanPriceWebProps } from './PricingPlanPriceProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType default
import styles from 'flex-design-system-framework/main/all.module.scss'
// import { type Styles } from '@flex-design-system/framework'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Pricing Plan Price Component
 * @param children {ReactNode} Children
 * @param className {string} Additionnal css classes
 */
const PricingPlanPrice = ({ className, classList, ...others }: PricingPlanPriceWebProps): React.JSX.Element => {
  const classes = classNames(styles.planPrice, className, validate(classList))

  return <div className={classes} {...others} />
}

export default PricingPlanPrice
