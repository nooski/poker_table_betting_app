'use client'

import React from 'react'
import classNames from 'classnames'
import { validate } from '../../../services/index.js'
import { ToolbarGroupWebProps } from './ToolbarGroupProps.js'

// ///////////////////////////////////////////////////////////////////////////
// /!\ When typed-scss-modules --exportType default
import styles from 'flex-design-system-framework/main/all.module.scss'
// import { type Styles } from '@flex-design-system/framework'
// ///////////////////////////////////////////////////////////////////////////

/**
 * Toolbar Group
 * @param className {string} Additionnal CSS Classes
 * @param elastic {boolean} Is elastic
 */
const ToolbarGroup = ({ className, classList, elastic, ...others }: ToolbarGroupWebProps): React.JSX.Element => {
  const classes = classNames(styles.toolbarGroup, elastic && styles.isElastic, className, validate(classList))

  return <div className={classes} {...others} />
}

export default ToolbarGroup
