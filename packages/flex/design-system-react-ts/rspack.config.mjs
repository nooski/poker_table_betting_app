// https://github.com/web-infra-dev/rspack-repro/blob/main/config.mjs
// https://dev.to/woovi/bundling-many-frontends-with-a-single-rspack-config-5394

// System dependencies
// __dirname is not defined in ES module scope
import * as path from 'path'
import { fileURLToPath } from 'url'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

// require.resolve for ES modules
import { createRequire } from 'module'
const require = createRequire(import.meta.url)

import subprocess from 'node:child_process'
import { promisify } from 'node:util'
const execPromise = promisify(subprocess.exec)

import rspack from '@rspack/core'
const { ModuleFederationPlugin } = require('@module-federation/enhanced-rspack')
const { DotenvPlugin } = require('rspack-plugin-dotenv')
const { CssExtractRspackPlugin } = require('@rspack/core')
const ReactRefreshPlugin = require('@rspack/plugin-react-refresh')

// import AssetsPlugin from 'assets-webpack-plugin'
import LoadableWebpackPlugin from '@loadable/webpack-plugin'
// import CopyWebpackPlugin from 'copy-webpack-plugin'
// import Dotenv from 'dotenv-webpack'
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
// import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import NodePolyfillPlugin from 'node-polyfill-webpack-plugin'
// import { TsconfigPathsPlugin } from 'tsconfig-paths-webpack-plugin'
// import { WebpackManifestPlugin } from 'webpack-manifest-plugin'
// import WebpackRequireFrom from 'webpack-require-from'

const { WebpackManifestPlugin: RspackManifestPlugin } = await import('rspack-manifest-plugin')

const mode = process.env.FLEX_MODE || 'development'
const prod = mode === 'production'

// const findWorkspaceRoot = require('find-yarn-workspace-root')
// const workspacePath = findWorkspaceRoot(__dirname)
// const rootLocation = path.relative(__dirname, workspacePath)
const rootLocation = process.env.FLEX_PROJ_ROOT
console.log('rootLocation : ', rootLocation)
console.log('host : ', process.env.FLEX_DESIGN_SYS_REACT_TS_HOST)
console.log('port : ', process.env.FLEX_DESIGN_SYS_REACT_TS_PORT)
console.log(`env  : FLEX_GATEWAY_MODULE_CSS=${process.env.FLEX_GATEWAY_MODULE_CSS}`)
console.log(`env  : BUILD_RUNNING=${process.env.BUILD_RUNNING}`)

// eslint-disable-next-line @typescript-eslint/no-var-requires
const depsMonorepo = require(`${rootLocation}/package.json`).dependencies
// eslint-disable-next-line @typescript-eslint/no-var-requires
const deps = require('./package.json').dependencies

let _gitCommitSHA = ''
async function getGitCommitSHA() {
  if ('GIT_COMMIT_SHA' in process.env) {
    return process.env.GIT_COMMIT_SHA
  } else {
    const result = await execPromise(`git rev-parse --short HEAD`)
    const { stdout } = result
    if (!stdout) return result
    _gitCommitSHA = stdout.trim()
    // console.log(`Git Commit SHA :`, _gitCommitSHA)
    return _gitCommitSHA
  }
}
_gitCommitSHA = await getGitCommitSHA()

// // https://stackoverflow.com/questions/76700259/how-do-i-replace-a-deprecated-null-loader-regex-test-with-webpack-5s-resolve-al
// // import { globSync } from 'glob'
// const glob = require('glob')
// const ignoreScssAssets = glob.globSync('**/*.scss', { absolute: true });
// const ignoreScssModules = ignoreScssAssets.reduce((acc, cur) => {
//     acc[cur] = false;
//     return acc;
// }, {});
// console.log('ignoreModules: ', ignoreScssModules);

const getConfig = async (env, argv) => {
  const { target } = argv
  const _prod = env === 'production'
  /**
   * @type {import('webpack').Configuration | import('@rspack/cli').Configuration}
   */
  const config = {
    mode: env,

    experiments: {
      css: true,
      topLevelAwait: true,
      // outputModule: true,
      // layers: true, // not recognized by rspack
      rspackFuture: {
        // disableTransformByDefault: true, // not recognized by rspack WTF ??
        // newResolver: true,
        // newTreeshaking: true
      }
    },

    entry: {
      [`mainEntry_${process.env.FLEX_DESIGN_SYS_REACT_TS_NAME}`]: [
        // require.resolve('regenerator-runtime/runtime.js'),
        path.resolve(__dirname, 'src/views/index')
      ],
    },

    context: __dirname,

    // devServer: {
    //   static: {
    //     directory: path.join(__dirname, 'dist'),
    //   },
    //   port: 4009,
    // },

    target: target,

    output: {
      path: path.resolve(__dirname, 'build', 'rspack', target),
      publicPath: `${process.env.FLEX_DESIGN_SYS_REACT_TS_HOST}/${target}/`,
      crossOriginLoading: 'anonymous',
      clean: true,
      filename: '[name].[contenthash].js',
      chunkFilename: '[name].[contenthash].js',
    },

    resolve: {
      extensions: ['.js', '.mjs', '.cjs', '.jsx', '.ts', '.mts', '.cts', '.tsx', '.d.ts', '.ttf', '.scss'],
      // Add support for TypeScripts fully qualified ESM imports.
      extensionAlias: {
        '.js': ['.js', '.jsx', '.ts', '.tsx'],
        '.mjs': ['.mjs', '.mts'],
        '.cjs': ['.cjs', '.cts']
      },
      tsConfigPath: path.resolve(__dirname, 'tsconfig.build.json'),

      // alias: {
      //   ...ignoreScssModules,
      // },

      // fullySpecified: true, // OMG WTF !!

      // fallback: {
      //   ...(target === 'web' &&
      //   {
      //     // See packages/flex/domain-utils/src/utils/git-commit-sha.ts
      //     // 'child_process': false,
      //     // 'node:util': false,
      //     // 'node:path': false,
      //     // 'node:url': false,

      //     // 'url': require.resolve('url'),
      //     // 'fs': require.resolve('browserify-fs'),
      //     // 'buffer': require.resolve('buffer'),
      //     // 'crypto': require.resolve('crypto-browserify'),
      //     // 'stream': require.resolve('stream-browserify'),
      //     crypto: false,
      //   }),
      // },
    },

    devtool: !_prod ? 'cheap-module-source-map': false,
    watch: process.env.BUILD_RUNNING === 'undefined',
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000,
      ignored: /node_modules/,
      stdin: true,
    },

    plugins: [
      ...(target === 'web' || target === 'node'
        ? [
          new ModuleFederationPlugin({
            // name: 'flex_design_system_react_ts_modfed',
            name: `${process.env.FLEX_DESIGN_SYS_REACT_TS_NAME}`,
            filename: 'remoteEntry.js',
            // remotes: {
            //   [`${process.env.ACO_MF_NAME}`]: `${process.env.ACO_MF_NAME}@${process.env.ACO_PROTOCOL}${process.env.ACO_HOSTNAME}:${process.env.ACO_PORT}/remoteEntry.js`
            // },
            remotes: {},
            exposes: {
              // './UnStyled': './src/unstyled',
              // './Styled': './src/styled',
              ...(process.env.FLEX_GATEWAY_MODULE_CSS === 'default' && {
                './ModulesDefault': './src/views/ModulesDefault',
              }),
              ...(process.env.FLEX_GATEWAY_MODULE_CSS === 'named' && {
                './ModulesNamed': './src/views/ModulesNamed'
              }),
            },
            shared: {
              ...deps,
              ...depsMonorepo,
              react: {
                singleton: true,
                requiredVersion: depsMonorepo.react,
                // eager: true,
              },
              // 'react-dom': {
              //   singleton: true,
              //   requiredVersion: depsMonorepo['react-dom'],
              // },
              // 'react-router-dom': {
              //   singleton: true,
              //   requiredVersion: depsMonorepo['react-router-dom'],
              // },
              history: {
                singleton: true,
                requiredVersion: depsMonorepo['history'],
                // eager: true,
              },
              '@emotion/react': {
                singleton: true,
                requiredVersion: depsMonorepo['@emotion/react'],
                eager: true,
              },
              'react-awesome-reveal': {
                singleton: true,
                requiredVersion: depsMonorepo['react-awesome-reveal'],
                eager: true,
              },
              // 'prop-types': {
              //   // singleton: true,
              //   // requiredVersion: depsMonorepo['prop-types'],
              //   eager: true,
              // },
              // lodash: {
              //   singleton: true,
              //   requiredVersion: depsMonorepo['lodash'],
              //   eager: true,
              // },
              // mobx: {
              //   singleton: true,
              //   requiredVersion: depsMonorepo['mobx'],
              // },
              // 'mobx-react-lite': {
              //   singleton: true,
              //   requiredVersion: depsMonorepo['mobx-react-lite'],
              // },
              // '@loadable/component': {
              //   singleton: true,
              //   requiredVersion: depsMonorepo['@loadable/component'],
              // },
              // '@flexiness/domain-lib-mobx-react-router': {
              //   import: '@flexiness/domain-lib-mobx-react-router',
              //   requiredVersion: deps['@flexiness/domain-lib-mobx-react-router'],
              //   shareKey: '@flexiness/domain-lib-mobx-react-router', // under this name the shared module will be placed in the share scope
              //   shareScope: 'default', // share scope with this name will be used
              //   singleton: true, // only a single version of the shared module is allowed
              // },
              '@flex-design-system/framework': {
                import: '@flex-design-system/framework',
                // requiredVersion: deps['@flex-design-system/framework'],
                requiredVersion: require('@flex-design-system/framework/package.json').version,
                shareKey: '@flex-design-system/framework', // under this name the shared module will be placed in the share scope
                shareScope: 'default', // share scope with this name will be used
                singleton: true, // only a single version of the shared module is allowed
              },
            }
          }),

          new HtmlWebpackPlugin({
            template: `./public/index.html`,
          }),

          // new rspack.CopyRspackPlugin({
          //   patterns: [
          //     { from: 'public/favicon.ico', to: './' },
          //     { from: 'public/logo192.png', to: './' },
          //     { from: 'public/logo512.png', to: './' },
          //   ],
          // }),

          // // https://github.com/web-infra-dev/rspack-compat/blob/main/packages/rspack-manifest-plugin%405/rspack.config.js
          new RspackManifestPlugin({
            fileName: 'manifest.json',
            generate: (seed, files, entries) => {
              const manifestFiles = files.reduce((manifest, file) => {
                manifest[file.name] = file.path;
                return manifest;
              }, seed);
              const entrypointFiles = Object.keys(entries).reduce(
                (previous, name) =>
                previous.concat(
                  entries[name].filter(fileName => !fileName.endsWith('.map'))
                ),
                []
              );
              return {
                files: manifestFiles,
                entrypoints: entrypointFiles
              };
            }
          }),

          // !_prod && new ReactRefreshPlugin(),
        ] : []
      ),

      ...(target === 'web'
        ? [
          // // https://github.com/mrsteele/dotenv-webpack/issues/70#issuecomment-942441074
          // new Dotenv({
          //   path: `${process.env.FLEX_PROJ_ROOT}/env/public/.env.${process.env.FLEX_MODE}`,
          //   systemvars: true
          // }),

          // new NodePolyfillPlugin(), // Seriously, pnpm ?
          // new NodePolyfillPlugin({
          //   includeAliases: ['crypto']
          // }),

          new rspack.ProvidePlugin({
            // 'React': [require.resolve('react')],
            Buffer: ['buffer', 'Buffer'],
            process: [require.resolve('process/browser')],
          }),

        ] : []
      ),

      ...(target === 'node'
        ? [
          new rspack.node.NodeTargetPlugin()
        ] : []
      ),

      ...(target === 'extractCss'
        ? [
          new CssExtractRspackPlugin(),
        ] : []
      ),

      new LoadableWebpackPlugin(),

      ...(!process.env.BUILD_RUNNING
        ? [
          new ForkTsCheckerWebpackPlugin({
            typescript: {
              memoryLimit: 2048,
              mode: 'readonly',
              configFile: path.resolve(__dirname, 'tsconfig.json'),
            }
          })
        ]
        : []
      ),

      new DotenvPlugin({
        path: `${process.env.FLEX_PROJ_ROOT}/env/public/.env.${process.env.FLEX_MODE}`,
        systemvars: true
      }),
    ].filter(Boolean),

    module: {
      // parser: {
      //   'css/module': {
      //     namedExports: false,
      //   }
      // },

      // generator: {
      //   'css/module': {
      //     exportsConvention: 'camel-case-only',
      //     exportsOnly: false,
      //     localIdentName: `[name]_[local]__${_gitCommitSHA}`,
      //     esModule: true,
      //   },
      // },

      rules: [
        {
          test: /\.ts(x)?$/,
          exclude: [/node_modules/],
          use: [
            {
              loader: 'builtin:swc-loader',
              /**
               * @type {import('@rspack/core').SwcLoaderOptions}
               */
              options: {
                sourceMap: !_prod,
                jsc: {
                  parser: {
                    syntax: 'typescript',
                    tsx: true,
                  },
                  transform: {
                    react: {
                      runtime: 'automatic',
                      // development: !_prod,
                      // refresh: !_prod,

                      // pragma: 'React.createElement',
                      // pragmaFrag: 'React.Fragment',
                      // throwIfNamespace: true,
                      // development: false,
                      // useBuiltins: false,
                    },
                  },
                },
                env: {
                  targets: 'Chrome >= 48', // browser compatibility
                },
              },
            },
          ],
        },
        {
          test: /\.js(x)?$/,
          exclude: [/node_modules/],
          use: [
            {
              loader: 'builtin:swc-loader',
              options: {
                sourceMap: !_prod,
                jsc: {
                  parser: {
                    syntax: 'ecmascript',
                    jsx: true,
                  },
                  transform: {
                    react: {
                      runtime: 'automatic',
                      // development: !_prod,
                      // refresh: !_prod,

                      // pragma: 'React.createElement',
                      // pragmaFrag: 'React.Fragment',
                      // throwIfNamespace: true,
                      // development: false,
                      // useBuiltins: false,
                    },
                  },
                },
                env: {
                  targets: 'Chrome >= 48', // browser compatibility
                },
              },
            },
          ],
        },

        {
          // type: 'css/module',
          type: 'javascript/auto',
          test: /\.module\.s(a|c)ss$/i,
          use: [
            {
              loader: require.resolve('style-loader'),
              // options: {
              //   insert: require.resolve('@flexiness/domain-utils/style-loader-esm'),
              // },
            },
            {
              loader: require.resolve('css-loader'),
              options: {
                esModule: true,
                sourceMap: !_prod,
                importLoaders: 1,
                modules: {
                  namedExport: false,
                  exportLocalsConvention: 'camel-case-only',
                  mode: 'local',
                  localIdentName: `[name]_[local]__${_gitCommitSHA}`,
                  exportOnlyLocals: false,
                }
              },
            },
            {
              loader: require.resolve('sass-loader'),
              options: {
                implementation: require.resolve('sass'),
                sourceMap: !_prod
              },
            },
          ],
        },

        {
          test: /\.(png|jpe?g|gif)$/i,
          type: 'asset/resource',
        },

        {
          test: /\.svg$/i,
          type: 'asset/inline',
        },

        {
          test: /\.svg$/i,
          issuer: /\.[jt]sx?$/,
          use: ['@svgr/webpack'],
        },

        {
          test: /\.(woff|woff2|eot|ttf|otf|)$/,
          type: 'asset/resource',
        },

        {
          test: /^BUILD_ID$/,
          type: 'asset/source',
        },
      ],
    },

    ...(target === 'extractCss' &&
      {
        optimization: {
          splitChunks: {
            cacheGroups: {
              styles: {
                name: 'flex-framework-styles',
                type: 'css/rspack-css-extract',
                chunks: 'all',
                enforce: true,
              },
            },
          },
        },
      }
    ),
  }

  const rulesExtractCss = [
    {
      test: /\.css$/,
      // /!\ Do nothing here, this is handled by experiments css
    },
    {
      type: 'javascript/auto',
      test: /\.module\.s(a|c)ss$/i,
      use: [
        CssExtractRspackPlugin.loader,
        {
          loader: 'css-loader',
          options: {
            esModule: true,
            sourceMap: !_prod,
            importLoaders: 1,
            ...(process.env.FLEX_GATEWAY_MODULE_CSS === 'default'
              ? {
                modules: {
                  namedExport: false,
                  exportLocalsConvention: 'camel-case-only',
                  mode: 'local',
                  localIdentName: `[name]_[local]__${_gitCommitSHA}`,
                  exportOnlyLocals: false,
                },
              } : null),
            ...(process.env.FLEX_GATEWAY_MODULE_CSS === 'named'
              ? {
                modules: {
                  namedExport: true,
                  exportLocalsConvention: 'camel-case-only',
                  mode: 'local',
                  localIdentName: `[name]_[local]__${_gitCommitSHA}`,
                  exportOnlyLocals: true,
                }
              } : null),
          },
        },
        {
          loader: 'sass-loader',
          options: {
            implementation: 'sass',
            sourceMap: !_prod
          },
        },
      ],
    },
    {
      type: 'javascript/auto',
      test: /\.s[ac]ss$/i,
      exclude: /\.module.(s(a|c)ss)$/,
      use: [
        CssExtractRspackPlugin.loader,
        {
          loader: 'css-loader',
          options: {
            esModule: true,
            sourceMap: !_prod,
            importLoaders: 1,
          }
        },
        {
          loader: 'sass-loader',
          options: {
            implementation: 'sass',
            sourceMap: !_prod
          },
        },
      ],
    },
  ]
  if (target === 'extractCss') config.module.rules.push(...rulesExtractCss)
  return config
}

export default [
  getConfig(mode, { target: 'web'}),
  // getConfig(mode, { target: 'node' }),
  // getConfig(mode, { target: 'extractCss' })
]
