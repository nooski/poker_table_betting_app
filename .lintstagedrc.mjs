import * as path from 'path'

const nextJSBuildEslintCommand = (filenames) =>
  `pnpm --filter next-amplify-gen2 lint:next --fix --file ${filenames
    .map((f) => path.relative(process.cwd(), f))
    .join(' --file ')}`

export default {
  // 'apps/**/gateway/src/**/*.{js,jsx,ts,tsx}': [nextJSBuildEslintCommand],

  // 'apps/**/gateway/src/**/*.{ts,tsx}': (filenames) => [
  //   `eslint ${filenames.join(' ')} --config ./eslint.config.mjs --debug`,
  //   `prettier --write ${filenames.join(' ')}`,
  // ],

  // 'apps/**/on-board/**/src/**/*.{tsx,ts,cts,mts,jsx,js}': (filenames) => [
  //   `eslint ${filenames.join(' ')} --config ./eslint.config.mjs --verbose`,
  //   `prettier --write ${filenames.join(' ')}`,
  // ],

  // 'packages/**/src/**/*.{js,jsx,ts,tsx}': (filenames) => [
  //   `eslint ${filenames.join(' ')} --config ./eslint.config.mjs --verbose`,
  //   `prettier --write ${filenames.join(' ')}`,
  // ],

  'apps/**/gateway/src/**/*.{ts,tsx}': (filenames) => [
    `eslint ${filenames.join(' ')} --config ./.eslintrc.cjs --resolve-plugins-relative-to ${process.env.FLEX_PROJ_ROOT}/packages/flex/config/eslint/legacy`,
    `prettier --write ${filenames.join(' ')}`,
  ],

  'apps/**/on-board/**/src/**/*.{tsx,ts,cts,mts,jsx,js}': (filenames) => [
    `eslint ${filenames.join(' ')} --config ./.eslintrc.cjs --resolve-plugins-relative-to ${process.env.FLEX_PROJ_ROOT}/packages/flex/config/eslint/legacy`,
    `prettier --write ${filenames.join(' ')}`,
  ],

  'packages/**/src/**/*.{js,jsx,ts,tsx}': (filenames) => [
    `eslint ${filenames.join(' ')} --config ./.eslintrc.cjs --resolve-plugins-relative-to ${process.env.FLEX_PROJ_ROOT}/packages/flex/config/eslint/legacy`,
    `prettier --write ${filenames.join(' ')}`,
  ],

  // Prettify only Markdown and JSON files
  // '**/*.{md,json}': (filenames) => `prettier --write ${filenames.join(' ')}`
  // '**/*.{md,json}': (filenames) => `prettier --check ${filenames.join(' ')}`
}
