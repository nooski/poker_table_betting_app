/* eslint-disable @typescript-eslint/no-var-requires */

// System dependencies
// __dirname is not defined in ES module scope
import * as path from 'path'
import { fileURLToPath } from 'url'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

// require.resolve for ES modules
import { createRequire } from 'module'
const require = createRequire(import.meta.url)

import subprocess from 'node:child_process'
import { promisify } from 'node:util'
const execPromise = promisify(subprocess.exec)

import webpack from 'webpack'
const { container } = webpack
// const { ModuleFederationPlugin } = container
const { ModuleFederationPlugin } = require('@module-federation/enhanced')
// import { NodeAsyncHttpRuntime } from '@telenko/node-mf'
import AssetsPlugin from 'assets-webpack-plugin'
import CopyWebpackPlugin from 'copy-webpack-plugin'
import Dotenv from 'dotenv-webpack'
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import NodePolyfillPlugin from 'node-polyfill-webpack-plugin'
import { TsconfigPathsPlugin } from 'tsconfig-paths-webpack-plugin'
import LoadableWebpackPlugin from '@loadable/webpack-plugin'
import { WebpackManifestPlugin } from 'webpack-manifest-plugin'
import WebpackRequireFrom from 'webpack-require-from'
// import nodeExternals from 'webpack-node-externals'

const mode = process.env.FLEX_MODE || 'development'
const prod = mode === 'production'

// const findWorkspaceRoot = require('find-yarn-workspace-root')
// const workspacePath = findWorkspaceRoot(process.env.FLEX_PROJ_ROOT)
// const rootLocation = path.relative(__dirname, workspacePath)
const rootLocation = process.env.FLEX_PROJ_ROOT
console.log('rootLocation : ', rootLocation)
console.log('host : ', process.env.FLEX_POKER_CLIENT_HOST)
console.log('port : ', process.env.FLEX_POKER_CLIENT_PORT)
console.log('mode : ', process.env.FLEX_MODE)
console.log(`env  : FLEX_GATEWAY_MODULE_CSS=${process.env.FLEX_GATEWAY_MODULE_CSS}`)
console.log(`env  : BUILD_RUNNING=${process.env.BUILD_RUNNING}`)

// eslint-disable-next-line @typescript-eslint/no-var-requires
const depsMonorepo = require(`${rootLocation}/package.json`).dependencies
// eslint-disable-next-line @typescript-eslint/no-var-requires
const deps = require('./package.json').dependencies

const getMainConfig = async ({ target, _gitCommitSHA } = { target: 'web', _gitCommitSHA: '' }) => {
  return (
    {
      experiments: {
        // asyncWebAssembly: true,
        // buildHttp: true,
        // lazyCompilation: true,
        // syncWebAssembly: true,

        css: true,
        topLevelAwait: true,
        // outputModule: true,
        layers: true,
        // https://modernjs.dev/en/configure/app/experiments/lazy-compilation.html
        // lazyCompilation: {
        //   imports: true,
        //   entries: true,
        // },
        cacheUnaffected: true,
      },

      entry: {
        [`mainEntry_${process.env.FLEX_POKER_CLIENT_NAME}_${_gitCommitSHA}`]: [
          path.resolve(__dirname, 'src/index')
        ],
      },

      // watchOptions: {
      //   aggregateTimeout: 600,
      //   ignored: [
      //     path.posix.resolve(__dirname, './dist')
      //   ],
      // },

      context: __dirname, // to automatically find tsconfig.json

      // ...(target === 'web'
      //   ? {
      //     devServer: {
      //       static: {
      //         directory: path.join(__dirname, 'public'),
      //         publicPath: `${process.env.FLEX_POKER_CLIENT_HOST}`,
      //       },
      //       headers: {
      //         'Access-Control-Allow-Origin': '*',
      //         'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      //         'Access-Control-Allow-Headers':
      //         'X-Requested-With, content-type, Authorization',
      //       },
      //       open: false,
      //       compress: true,
      //       port: process.env.FLEX_POKER_CLIENT_PORT,
      //     },
      //   } : null
      // ),

      // mode: 'development',
      mode: mode,

      target: 'browserslist:last 1 chrome version',

      output: {
        path: path.resolve(__dirname, 'build', 'webpack', target),
        // publicPath: 'auto',
        // publicPath: `${process.env.FLEX_POKER_CLIENT_HOST}/${target}/`,
        publicPath: `${process.env.FLEX_POKER_CLIENT_HOST}/`,
        // publicPath: `/${process.env.FLEX_POKER_CLIENT_PROXY_PATHNAME}/`,
        crossOriginLoading: 'anonymous',
        clean: true,
        filename: '[name].[contenthash].js',
        chunkFilename: '[name].[contenthash].js',
        pathinfo: false, // https://dev.to/woovi/web-dev-efficiency-achieving-faster-builds-and-lower-memory-load-with-webpack-4cm8

        // chunkFormat: 'module',
        // chunkLoading: 'import',
      },

      optimization: {
        runtimeChunk: false,
      },

      // https://webpack.js.org/configuration/cache/#gitlab-cicd
      // cache: {
      //   type: 'filesystem',
      // },

      // https://github.com/webpack/webpack/issues/16163
      // cache: prod ? {
      //   type: 'filesystem',
      //   maxMemoryGenerations: 5,
      //   cacheDirectory: path.resolve(__dirname, '.webpack-cache', target),
      //   buildDependencies: {
      //     config: [
      //       path.join(__dirname, 'package.json')
      //     ]
      //   }
      // } : {
      //   type: 'memory',
      //   maxGenerations: 5
      // },

      cache: {
        type: 'filesystem',
        // defaults to 10 in development mode and to Infinity in production mode.
        // maxMemoryGenerations: 5,
        cacheDirectory: path.resolve(__dirname, '.webpack-cache', target),
        buildDependencies: {
          config: [
            path.join(__dirname, 'package.json')
          ]
        }
      },

      resolve: {
        extensions: ['.tsx', '.ts', '.mts', '.jsx', '.js', '.mjs', '.d.ts', '.ttf', '.scss'],
        // Add support for TypeScripts fully qualified ESM imports.
        extensionAlias: {
          '.js': ['.js', '.ts', '.tsx'],
          '.cjs': ['.cjs', '.cts'],
          '.mjs': ['.mjs', '.mts']
        },
        plugins: [
          new TsconfigPathsPlugin({
            configFile: path.resolve(__dirname, 'tsconfig.build.json'),
          })
        ],

        fallback: {
          ...(target === 'web' &&
          {
            // See packages/flex/domain-utils/src/utils/git-commit-sha.ts
            // 'child_process': false,
            // 'node:util': false,
            // 'node:path': false,
            // 'node:url': false,

            // 'url': require.resolve('url'),
            // 'fs': require.resolve('browserify-fs'),
            'buffer': require.resolve('buffer'),
            'crypto': require.resolve('crypto-browserify'),
            'stream': require.resolve('stream-browserify'),
          }),
        },

        alias: {
          'flex-design-system-framework/main/all.module.scss': '@flex-design-system/framework',
          'flex-design-system-framework/standalone/flexslider.module.scss': '@flex-design-system/framework/flexslider.scss'
          // process: 'process/browser'
          // './schema': require.resolve('@flexiness/aws/schema'),
          // './schema': './schema.js',
        }
      },

      module: {
        rules: [

          {
            test: /\.(tsx|ts|jsx|js)?$/,
            loader: require.resolve('ts-loader'),
            exclude: /node_modules/,
            options: {
              configFile: path.resolve(__dirname, 'tsconfig.build.json'),
              projectReferences: true,
              transpileOnly: true
            },
          },

          // ///////////////////////////////////////////////////////

          // Load font files and images
          {
            test: /\.(woff|woff2|ttf|eot|svg|jpg|jpeg|png|gif)(\?[\s\S]+)?$/,
            use: [
              {
                loader: require.resolve('file-loader'),
                options: {
                  esModule: true
                }
              }
            ]
          },
        ],
      },

      plugins: [
        new ModuleFederationPlugin({
          name: `${process.env.FLEX_POKER_CLIENT_NAME}`,
          // library: { type: 'var', name: `${process.env.FLEX_POKER_CLIENT_NAME}` },
          filename: `remoteEntry_${process.env.FLEX_POKER_CLIENT_NAME}_${_gitCommitSHA}.js`,
          remotes: {},
          exposes: {
            './App': './src/App',
          },
          shared: [
            {
              ...deps,
              ...depsMonorepo,
              '@flexiness/aws': {
                import: '@flexiness/aws',
                requiredVersion: require('@flexiness/aws/package.json').version,
                shareKey: '@flexiness/aws', // under this name the shared module will be placed in the share scope
                shareScope: 'default', // share scope with this name will be used
                singleton: true, // only a single version of the shared module is allowed
              },
              'on-board-event-api': {
                import: 'on-board-event-api',
                requiredVersion: '0.1.0',
                shareKey: 'on-board-event-api', // under this name the shared module will be placed in the share scope
                shareScope: 'default', // share scope with this name will be used
                singleton: true, // only a single version of the shared module is allowed
              },
              'on-board-event-common': {
                import: 'on-board-event-common',
                requiredVersion: '0.1.0',
                shareKey: 'on-board-event-common', // under this name the shared module will be placed in the share scope
                shareScope: 'default', // share scope with this name will be used
                singleton: true, // only a single version of the shared module is allowed
              },
              '@flexiness/domain-store': {
                import: '@flexiness/domain-store',
                requiredVersion: require('@flexiness/domain-store/package.json').version,
                shareKey: '@flexiness/domain-store', // under this name the shared module will be placed in the share scope
                shareScope: 'default', // share scope with this name will be used
                singleton: true, // only a single version of the shared module is allowed
              },
              '@flexiness/domain-utils': {
                import: '@flexiness/domain-utils',
                requiredVersion: require('@flexiness/domain-utils/package.json').version,
                shareKey: '@flexiness/domain-utils', // under this name the shared module will be placed in the share scope
                shareScope: 'default', // share scope with this name will be used
                singleton: true, // only a single version of the shared module is allowed
              },
              '@flex-design-system/framework': {
                import: '@flex-design-system/framework',
                requiredVersion: require('@flex-design-system/framework/package.json').version,
                shareKey: '@flex-design-system/framework', // under this name the shared module will be placed in the share scope
                shareScope: 'default', // share scope with this name will be used
                singleton: true, // only a single version of the shared module is allowed
              },
              // '@flex-design-system/react-ts': {
              //   import: '@flex-design-system/react-ts',
              //   requiredVersion: require('@flex-design-system/react-ts/package.json').version,
              //   shareKey: '@flex-design-system/react-ts', // under this name the shared module will be placed in the share scope
              //   shareScope: 'default', // share scope with this name will be used
              //   singleton: true, // only a single version of the shared module is allowed
              //   eager: true,
              // },
            }
          ],
        }),

        ...(!process.env.BUILD_RUNNING
          ? [
            new ForkTsCheckerWebpackPlugin({
              typescript: {
                memoryLimit: 2048,
                mode: 'readonly',
                configFile: path.resolve(__dirname, 'tsconfig.json'),
              }
            })
          ]
          : []
        ),

        ...(target === 'web'
          ? [
            // https://github.com/mrsteele/dotenv-webpack/issues/70#issuecomment-942441074
            new Dotenv({
              path: `${process.env.FLEX_PROJ_ROOT}/env/public/.env.${process.env.FLEX_MODE}`,
              systemvars: true
            }),
            new HtmlWebpackPlugin({
              inject: false,
              template: `./public/index.html`,
              // https://github.com/module-federation/module-federation-examples/issues/102
              publicPath: '/',
              filename: `index.ejs`,
            }),
            new CopyWebpackPlugin({
              patterns: [
                { from: 'public/favicon.ico', to: './' },
                { from: 'public/logo192.png', to: './' },
                { from: 'public/logo512.png', to: './' },
              ],
            }),
            new webpack.HotModuleReplacementPlugin(),
          ]
          : []
        ),

        new LoadableWebpackPlugin(),
        new NodePolyfillPlugin(),
        new WebpackManifestPlugin({}),
        new AssetsPlugin({
          filename: 'assets.json',
          entrypoints: true,
          includeFilesWithoutChunk: true
        }),
        // Work around for Buffer is undefined:
        // https://github.com/webpack/changelog-v5/issues/10
        new webpack.ProvidePlugin({
          Buffer: ['buffer', 'Buffer'],
        }),
        new WebpackRequireFrom({
          variableName: `${process.env.FLEX_POKER_CLIENT_NAME}_url`,
          suppressErrors: true
        }),
      ],
    }
  )
}

export { getMainConfig }
