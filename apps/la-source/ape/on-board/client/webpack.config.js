/* eslint-disable @typescript-eslint/no-var-requires */

// // require.resolve for ES modules
import { createRequire } from 'module'
const require = createRequire(import.meta.url)

import subprocess from 'node:child_process'
import { promisify } from 'node:util'
const execPromise = promisify(subprocess.exec)

import { merge as webpackMerge } from 'webpack-merge'

let _gitCommitSHA = ''
async function getGitCommitSHA() {
  if ('GIT_COMMIT_SHA' in process.env) {
    return process.env.GIT_COMMIT_SHA
  } else {
    const result = await execPromise(`git rev-parse --short HEAD`)
    const { stdout } = result
    if (!stdout) return result
    _gitCommitSHA = stdout.trim()
    // console.log(`Git Commit SHA :`, _gitCommitSHA)
    return _gitCommitSHA
  }
}
_gitCommitSHA = await getGitCommitSHA()

const mode = process.env.FLEX_MODE || 'development'
const prod = mode === 'production'

console.log('FLEX_POKER_CLIENT_TARGET : ', process.env.FLEX_POKER_CLIENT_TARGET)
const cssLoader = target => require(`@flexiness/webpack/cssLoaderOnlyMiniExtract.cjs`)(target)
const modularSass = (target, _gitCommitSHA) => require(`@flexiness/webpack/modularSassOnlyMiniExtract.cjs`)(target, _gitCommitSHA)

const getConfig = async (target) => {
  const { getMainConfig } = await import(`./webpack.${target}.mjs`)
  const mainConfig = await getMainConfig({ target, _gitCommitSHA })
  // console.log(mainConfig)

  return webpackMerge(

    mainConfig,

    {
      devtool: !prod ? 'cheap-module-source-map': false,
      watch: process.env.BUILD_RUNNING === 'undefined',
      watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: /node_modules/,
        stdin: true,
      },
    },

    ...(target === 'web' || 'node'
      ? [
        // Css Loaders
        // cssLoader({ target }),

        // Modular Sass loaders
        modularSass({ target, _gitCommitSHA }),
      ]: []
    ),
  )
}

export default [
  getConfig('web'),
  // getConfig('node'),
  // getConfig(`${process.env.FLEX_POKER_CLIENT_TARGET}`)
]
