/* eslint-disable max-len */
/* eslint-disable prefer-destructuring */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-console */

// https://stackoverflow.com/questions/68947813/express-js-send-files-with-hashed-file-names-compiled-in-webpack
// https://www.npmjs.com/package/node-sass-middleware
// https://medium.com/@andy_dover/injecting-critical-css-into-your-header-using-nodejs-express-sass-and-jade-53042c631ae9

// __dirname is not defined in ES module scope
import * as path from 'path'
import { fileURLToPath, parse } from 'url'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

// require.resolve for ES modules
import { createRequire } from 'module'
const require = createRequire(import.meta.url)

import subprocess from 'node:child_process'
import { promisify } from 'node:util'
const execPromise = promisify(subprocess.exec)

import { PathLike, promises as fsPromise } from 'fs'

import { CognitoJwtVerifier } from 'aws-jwt-verify'
// Create the verifier outside your route handlers,
// so the cache is persisted and can be shared amongst them.
const jwtVerifier = CognitoJwtVerifier.create({
  userPoolId: process.env.FLEX_AWS_COGNITO_USER_POOL_ID!,
  tokenUse: 'access',
  clientId: process.env.FLEX_AWS_COGNITO_USER_POOL_APP_CLIENT_ID!,
  scope: 'read',
})

let _gitCommitSHA = ''
async function getGitCommitSHA() {
  if ('GIT_COMMIT_SHA' in process.env) {
    return process.env.GIT_COMMIT_SHA
  } else {
    const result = await execPromise(`git rev-parse --short HEAD`)
    const { stdout } = result
    if (!stdout) return result
    _gitCommitSHA = stdout.trim()
    console.log(`Git Commit SHA :`, _gitCommitSHA)
    return _gitCommitSHA
  }
}
_gitCommitSHA = await getGitCommitSHA() as string

const accessFile = async (path: PathLike) => {
  try {
    await fsPromise.access(path)
    // console.log(path)
    // console.log('*.html path good')
    return path
  } catch {
    return false
  }
}

import Express from 'express'
import { ChunkExtractor } from '@loadable/server'

const webpackPath = path.join(__dirname, 'build', 'webpack', 'web')
const servePath = path.join(__dirname, 'build', `${process.env.FLEX_POKER_CLIENT_BUILD_SYS}`, `${process.env.FLEX_POKER_CLIENT_TARGET}`)

const webpackStats = await accessFile(path.join(webpackPath, 'loadable-stats.json'))
const clientStats = await accessFile(path.join(servePath, 'loadable-stats.json'))

let webpackExtractor
let clientExtractor
let flexFrameworkStylesAsset
if (webpackStats) {
  webpackExtractor = new ChunkExtractor({ statsFile: webpackStats, })
  console.log(webpackExtractor['stats']['assetsByChunkName'])
  flexFrameworkStylesAsset = webpackExtractor['stats']['assetsByChunkName']['flex-framework-styles']
  console.log('flex-framework-styles : ', flexFrameworkStylesAsset)
}
if (clientStats) {
  clientExtractor = new ChunkExtractor({ statsFile: clientStats, entrypoints: [`mainEntry_${process.env.FLEX_POKER_CLIENT_NAME}_${_gitCommitSHA}`], })
  console.log(clientExtractor['stats']['assetsByChunkName'])
}

// https://plainenglish.io/blog/typed-express-request-and-response-with-typescript
import { Send, Query } from 'express-serve-static-core'
export interface TypedRequestBody<T> extends Express.Request {
    body: T
}
export interface TypedRequestQuery<T extends Query> extends Express.Request {
    query: T
}
export interface TypedRequest<T extends Query, U> extends Express.Request {
    body: U,
    query: T
}
export interface TypedResponse<ResBody> extends Express.Response {
    json: Send<ResBody, this>;
}

import http from 'http'
import https from 'https'
import Cors from 'cors'
// import mime from 'mime'
// import accepts from 'accepts'
// import escapeStringRegexp from 'escape-string-regexp'

const { optionsHTTPS } = require('@flexiness/certs')
const { checkIsRoute, getContentSecurityPolicy, generateFlexCSPNonce, getFlexCSPNonce, setFlexCSPNonce } = require('@flexiness/domain-utils')

// import * as nodeParser from 'node-html-parser'
// import serveStatic from 'serve-static'
// import * as serveIndex from 'serve-index'
import regexEscape from 'regex-escape'
import detect from 'detect-port'

// import { default as contentSecurityPolicy } from 'helmet-csp'
import ejs from 'ejs'
ejs.delimiter = '?'; // Means instead use __webpack_nonce__ = '<?=nonce?>'
// import * as psl from 'psl'
// const parsedDomain = psl.parse(`${process.env.FLEX_DOMAIN_NAME}`)

// import findWorkspaceRoot from 'find-yarn-workspace-root'
// const workspacePath = findWorkspaceRoot(__dirname)
// const rootLocation = path.relative(__dirname, workspacePath!)
const rootLocation = process.env.FLEX_PROJ_ROOT

const FLEX_SERVER_RUNNING = process.env.FLEX_SERVER_RUNNING!
const PORT = process.env.FLEX_POKER_CLIENT_PORT!
const HOST = `${process.env.FLEX_PROTOCOL}${process.env.FLEX_POKER_CLIENT_HOSTNAME}:${PORT}`
// const whiteListFlexDomain = `${process.env.FLEX_MODE === 'production' ? `*.${parsedDomain}:*` : 'localhost:*'}`

// function setCustomCacheControl ( res: Express.Response, path: string) {
//   if (serveStatic.mime.lookup(path) === 'text/html') {
//     // Custom Cache-Control for HTML files
//     res.setHeader('Cache-Control', 'public, max-age=0')
//   }
// }

// https://blog.logrocket.com/caching-headers-a-practical-guide-for-frontend-developers/
function setNoCache(res: Express.Response) {
  const date = new Date();
  date.setFullYear(date.getFullYear() - 1);
  res.setHeader('Expires', date.toUTCString());
  res.setHeader('Pragma', 'no-cache');
  res.setHeader('Cache-Control', 'public, no-cache');
}
function setLongTermCache(res: Express.Response) {
  const date = new Date();
  date.setFullYear(date.getFullYear() + 1);
  res.setHeader('Expires', date.toUTCString());
  res.setHeader('Cache-Control', 'public, max-age=31536000, immutable');
}

const corsOptions = Cors({
  ...(process.env.FLEX_MODE === 'development'
    ? { origin: '*' }
    : { origin: [
      new RegExp(`${regexEscape(process.env.FLEX_DOMAIN_NAME!)}`),
      new RegExp(`${regexEscape(`.${process.env.FLEX_BASE_DOMAIN!}`)}$`),
      new RegExp(`${regexEscape(process.env.FLEX_HOST_IP!)}$`)
    ] }
  ),
  methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'],
  allowedHeaders: ['Content-Type', 'X-Requested-With', 'Authorization'],
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
})

// const parseHTML = async (_nonce) => {
//   await fsPromise.readFile(path.join(servePath, 'index.html'), 'utf8', async (err, html)=> {
//     if(err){
//       throw err;
//     }

//     const root = nodeParser(html);

//     const head = root.querySelector('head');
//     if (head !== null) {
//       head.innerHTML = `<script nonce='${_nonce}'>alert(1 + 1);</script>` + head.innerHTML;
//       await fsPromise.writeFile(path.join(servePath, 'index.html'), root.toString(), function (err) {
//         if (err) return console.log(err);
//       });
//     }
//   });
// }

let _nonce = ''
const readNonce = async () => {
  _nonce = JSON.parse(`${await fsPromise.readFile(`${rootLocation}/apps/la-source/ape/gateway/nonce.json`)}`)['nonce']
  setFlexCSPNonce(_nonce)
  // await parseHTML(_nonce)
  // console.log('_nonce', `${getFlexCSPNonce()}`)
}
const generateNonce = async ()  => {
  _nonce = await generateFlexCSPNonce()
  const save = await execPromise(`
    jq -n --arg base64 ${_nonce} '{"nonce":$base64}' > ${path.resolve(`${__dirname}`, 'nonce.json')} && \
    echo ${_nonce} > ${path.resolve(`${__dirname}`, 'nonce.txt')} && \
    echo export const __webpack_nonce__ = \\'${_nonce}\\' > ${path.resolve(`${__dirname}`, 'nonce_webpack.js')} && \
    export FLEX_CSP_NONCE=${_nonce} && \
    echo ${_nonce}
  `)
  const { stdout, stderr } = save
  if (stdout) {
    setFlexCSPNonce(stdout)
    if (process.env.DEBUG === 'true') console.log(`Gateway Express Custom Server csp nonce : ${getFlexCSPNonce()}`)
  }
  if (stderr) {
    console.log(stderr)
    Promise.reject(new Error('Cannot write nonce to json file'))
  }
}

const sedNonceStaticHTML = async (path: string)  => {
  let route = path
  let _nonce = getFlexCSPNonce()
  // console.log('_nonce', _nonce)
  // if (path === '/') route = 'index'
  // console.log(`${__dirname}/build/server/pages`)
  // const replace = await execPromise(`
  //   find ${__dirname}/build/server/pages/ -name '*.html' -print
  // `)

  // const replace = await execPromise(`
  //   sed -i -E 's~(property=\\"csp-nonce\\"\\\scontent=|__webpack_nonce__=|nonce=)(\\")(---CSP_NONCE---|.{43}=)(\\")~\\1"${_nonce}"~g' \
  //   ${__dirname}/build/server/pages/404.html
  // `)

  // https://unix.stackexchange.com/questions/195939/what-is-meaning-of-in-finds-exec-command
  // SED on all HTML pages - synchronous blocking operation - maybe too expensive in ressources overtime
  // const replace = await execPromise(`
  //   find ${__dirname}/build/server/pages/ -name '*.html' -exec \
  //   sed -i -E 's~(property=\\"csp-nonce\\"\\\scontent=|__webpack_nonce__=|nonce=)(\\")(---CSP_NONCE---|.{43}=)(\\")~\\1"${_nonce}"~g' {} ';'
  // `)

  const htmlFileExists = await accessFile(`${__dirname}/build/${process.env.FLEX_POKER_CLIENT_BUILD_SYS}/${process.env.FLEX_POKER_CLIENT_TARGET}/index.html`)
  if (!htmlFileExists) return

  // SED only on HTML landing page called by initial url route
  const replace = await execPromise(`
    sed -i -E "s~(nonce:\\\s')(---CSP_NONCE---|.{43}=)(')~\\1${_nonce}'~g" \
    ${__dirname}/build/${process.env.FLEX_POKER_CLIENT_BUILD_SYS}/${process.env.FLEX_POKER_CLIENT_TARGET}/index.html && \
    sed -i -E 's~(data-nonce=)(\\")(---CSP_NONCE---|.{43}=)(\\")~\\1"${_nonce}"~g' \
    ${__dirname}/build/${process.env.FLEX_POKER_CLIENT_BUILD_SYS}/${process.env.FLEX_POKER_CLIENT_TARGET}/index.html
  `)

  const { stdout, stderr } = replace
  if (stdout) {
    console.log(`Gateway Express sed html successful`)
  }
  if (stderr) {
    console.log(stderr)
    Promise.reject(new Error('Cannot write replace nonce value in html files'))
  }
  return
}

// /////////////////////////////////////////////////////////////////////////////////////////////////////

detect(Number(PORT))
  .then(_port => {
    if (Number(PORT) === Number(_port)) {
      let _currentRoute = ''
      const app = Express()

      // ejs.renderFile({filename: path.join(servePath, 'index.ejs')}, data, options, function(err, str){
      //     // str => Rendered HTML string
      //     console.log(str)
      // });

      app.engine('ejs', ejs.renderFile)
      app.set('views', servePath)

      app.use(corsOptions)
      app.use(async(req, res, next) => {
        // console.log(`req port : ${req.socket.localPort}`)
        if (checkIsRoute(req.path)) {
          // console.log(`req path : ${req.path}`)
          await readNonce()
          // await generateNonce()
          // await sedNonceStaticHTML(req.path)
          // await sedNonceJS()
          _currentRoute = req.path
        }
        res.locals.cspNonce = _nonce
        next()
      })

      // app.use(connectDatadog)

      // // if (process.env.FLEX_MODE === 'production') {
      //   app.use((req, res, next) => {
      //     return getContentSecurityPolicy(req, res, next, _nonce)
      //   });
      // // }

      // app.use((req, res) => {
      //   res.end(`<script nonce='${res.locals.cspNonce}'>alert(1 + 1);</script>`);
      // });

      // app.use('/node', express.static(path.join(__dirname, 'build/node'), {
      //   // maxAge: '1d',
      //   // setHeaders: setCustomCacheControl
      // }))

      // app.use('/web', express.static(servePath), {
      //   index: ['index.html'],
      //   // index: process.env.FLEX_MODE === 'development' ? ['index.html'] : [],
      //   // maxAge: '1d',
      //   // setHeaders: setCustomCacheControl
      // }))

      // // shows you the directory/file list at app root
      // app.use('/', serveIndex(path.join(__dirname, 'build'), {
      //   icons: true
      // }))

      // app.use('/', serveStatic(servePath), {
      //   index: ['index.html'],
      //   // setHeaders(res: Express.Response, path: string) {
      //   //   if (path.match(/(\.html|\/sw\.js)$/)) {
      //   //     setNoCache(res)
      //   //     return
      //   //   }

      //   //   if (path.match(/\.(js|css|png|jpg|jpeg|gif|ico|json)$/)) {
      //   //     setLongTermCache(res)
      //   //   }
      //   // },
      // }))

      // app.use('/', Express.static(servePath), {
      //   index: ['index.ejs'],
      // }))

      app.use('/', Express.static(servePath))
      // app.use('/styles', Express.static(servePath))
      app.use('/styles', Express.static(webpackPath))
      app.use(`/${process.env.FLEX_POKER_CLIENT_PROXY_PATHNAME}`, Express.static(servePath))
      // app.use('/styles', Express.static(`${process.env.FLEX_PROJ_ROOT}/apps/la-source/ape/gateway/build`))
      // app.use('/css', Express.static(path.join(`${rootLocation}/packages/flex/design-system-framework/dist/modules`)))

      app.route('/').get(async (req: Express.Request, res: Express.Response) => {
        console.log("what's up client app !!")

        res.render(path.join(servePath, 'index.ejs'), {
          nonce: res.locals.cspNonce,
          ...(flexFrameworkStylesAsset !== 'undefined' &&
            {
              flexFrameworkStyles: `/styles/${flexFrameworkStylesAsset}`
            }
          )
        })
      })

      // app.use(nocache());

      const server = `${process.env.FLEX_PROTOCOL}` === 'http://'
        ? http.createServer(app)
        : https.createServer(optionsHTTPS(), app)

      server.listen(Number(PORT), `${process.env.FLEX_POKER_CLIENT_HOSTNAME!}`, 34, () => {
        console.log(`[${process.env.FLEX_PROTOCOL!.slice(0, -3).toUpperCase()}] : ${HOST} :`, server.address())
        console.log(`${FLEX_SERVER_RUNNING} ${HOST}`)
      }).on('error', (err) => {
        throw err
      })
    } else {
      console.log(`ALREADY RUNNING ${HOST}`)
      console.log(`${FLEX_SERVER_RUNNING} ${HOST}`)
      // process.exit(0);
      process.kill(process.pid, 0)
    }
  })
  .catch(err => {
    console.log(err)
  })
