import React from 'react'
import { PokerPlayer, VoteResults, PointOption, PokerBoardComponentProps } from 'flexiness'

import classNames from 'classnames'

// import {
//   Button,
//   ButtonMarkup,
//   Title,
//   TitleLevel,
//   Textarea,
//   VariantState,
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import { Button, ButtonMarkup, Title, TitleLevel, Textarea, VariantState } from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

// const { ClientSyncStyledDefault } = await import('flex_design_system_react_ts_styled_default')
// const {
//   Button,
//   ButtonMarkup,
//   Title,
//   TitleLevel,
//   Textarea,
//   VariantState,
//   styles,
// } = ClientSyncStyledDefault

const PokerBoardComponent: React.FC<PokerBoardComponentProps> = (props) => {
  const { pointOptions, currentlyVoting, activePlayers, observers, showVotes } = props.board

  const changeCurrentlyVoting = (textareaValue: string) => {
    props.changeCurrentlyVoting(textareaValue)
  }

  const onClickVote = (vote: number) => {
    props.makeVote(vote)
  }

  const voteResults: VoteResults = showVotes ? { average: props.board.averageVote, voteCounts: props.board.voteCounts } : null

  return (
    <div>
      <Title level={TitleLevel.LEVEL6} style={{ lineHeight: 'inherit' }}>
        Currently Voting
      </Title>
      <form action='#' className='mb-2'>
        <div className='form-group'>
          <Textarea className='form-control' defaultValue={currentlyVoting} onChange={(event) => changeCurrentlyVoting(event.textareaValue)} />
        </div>
        <div className='grid gap-4 grid-rows-1 grid-cols-2' style={{ maxWidth: '30vw' }}>
          <Button variant={VariantState.SECONDARY} onClick={() => props.clearVotes}>
            Clear Votes
          </Button>
          <Button variant={VariantState.SECONDARY} onClick={() => props.showVotes}>
            Show Votes
          </Button>
        </div>
      </form>
      <Title level={TitleLevel.LEVEL6} style={{ lineHeight: 'inherit' }}>
        Vote:
      </Title>
      <div className='voteOptions mb-3'>
        {pointOptions.map((it: PointOption) => (
          // <Button className={`btn btn-primary mr-2${it === props.myPlayer.vote ? ' active' : ''}`} key={it} onClick={props.makeVote}>
          //   {it}
          // </Button>
          <Button
            small
            markup={ButtonMarkup.BUTTON}
            variant={VariantState.TERTIARY}
            className={classNames(styles.isInverted, `mr-2${it === props.myPlayer.vote ? ' active' : ''}`)}
            key={it}
            onClick={() => onClickVote(it as number)}
          >
            {it}
          </Button>
        ))}
      </div>
      <div className='row'>
        <div className='col'>
          <Title level={TitleLevel.LEVEL6} style={{ lineHeight: 'inherit' }}>
            Players
          </Title>
          <table className='table table-responsive'>
            <thead>
              <tr>
                <th>Name</th>
                <th>Vote</th>
              </tr>
            </thead>
            <tbody>
              {activePlayers
                .filter((player: PokerPlayer) => props.board.isTableJoined(player, props.activeTable?.tableId))
                .map((player: PokerPlayer) => (
                  <tr key={player.id}>
                    <td>{player.joining ? 'Incoming player...' : player.name}</td>
                    <td>{showVotes || player === props.myPlayer ? player.vote : player.vote !== null ? '...' : ''}</td>
                  </tr>
                ))}
            </tbody>
          </table>
          {!!observers.length && (
            <>
              <h3>Observers</h3>
              <p>{observers.map((it: PokerPlayer) => it.name).join(', ')}</p>
            </>
          )}
        </div>
        <div className='col'>
          {voteResults != null && (
            <>
              <h3>Results</h3>
              <p>Average: {voteResults.average.toFixed(2)}</p>
              <table className='table table-responsive'>
                <thead>
                  <tr>
                    <th>Vote</th>
                    <th>Count</th>
                  </tr>
                </thead>
                <tbody>
                  {voteResults.voteCounts.map((it) => (
                    <tr key={it.vote}>
                      <td>{it.vote}</td>
                      <td>{it.count}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </>
          )}
        </div>
      </div>
    </div>
  )
}

export default PokerBoardComponent
