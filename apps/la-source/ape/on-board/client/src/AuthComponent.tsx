'use-client'

/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable camelcase */
import '@aws-amplify/ui-react/styles.css' // default theme
import React from 'react'
import classNames from 'classnames'
// import { formatPhoneNumberIntl } from 'react-phone-number-input'
// import PhoneInput from 'react-phone-number-input/input'
import { toJS } from 'mobx'
import { observer } from 'mobx-react-lite'

import { DataStore } from 'aws-amplify/datastore'

import { isObjectEmpty } from '@flexiness/domain-utils'

import { OnBoardEventApiTypes } from 'flexiness'

import { AMPLIFY_CLIENT, AmplifyAuthTypes, AmplifyUIReactTypes, initAmplifyConfigClient } from '@flexiness/aws'

const { AmplifyAuth, AmplifyUIReact, AmplifyUtils } = AMPLIFY_CLIENT

const {
  Authenticator,
  // withAuthenticator,
  translations,
  useAuthenticator,
  // useTheme,
  ThemeProvider,
  // View,
  // Button,
  Image,
  // Flex
} = AmplifyUIReact

const {
  // autoSignIn,
  // confirmSignUp,
  // signUp,
  // fetchAuthSession,
  fetchUserAttributes,
} = AmplifyAuth

// const {
//   SignUp
// } = Authenticator

const { I18n, Hub } = AmplifyUtils

// import {
//   Button,
//   Link,
//   Title,
//   View,
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import { Button, Link, Title, View } from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

// const { ClientSyncStyledDefault } = await import('flex_design_system_react_ts_styled_default')
// const {
//   Link,
//   Title,
//   View,
//   styles,
// } = ClientSyncStyledDefault

import { getOnBoardEventsStore, getUIStore } from '@flexiness/domain-store'
const CreateOnBoardEvent = getOnBoardEventsStore()
const UIStore = getUIStore()
const { setCreateUser } = CreateOnBoardEvent
const {
  setModalCreateOnBoardEventOpen,
  // setTriggerAuthentication
} = UIStore

Hub.listen('auth', ({ payload }) => {
  switch (payload.event) {
    case 'signedIn':
      console.log('user have been signedIn successfully.')
      break
    case 'signedOut':
      console.log('user have been signedOut successfully.')
      break
    case 'tokenRefresh':
      console.log('auth tokens have been refreshed.')
      break
    case 'tokenRefresh_failure':
      console.log('failure while refreshing auth tokens.')
      break
    case 'signInWithRedirect':
      console.log('signInWithRedirect API has successfully been resolved.')
      break
    case 'signInWithRedirect_failure':
      console.log('failure while trying to resolve signInWithRedirect API.')
      break
    // case 'customOAuthState':
    //   logger.info('custom state returned from CognitoHosted UI')
    //   break
    default:
      break
  }
})

I18n.putVocabularies(translations)
I18n.setLanguage('fr')
I18n.putVocabularies({
  // https://github.com/aws-amplify/amplify-ui/blob/main/packages/ui/src/i18n/dictionaries/authenticator/fr.ts
  fr: {
    // SignIn
    'Sign In': 'Connexion sécurisée', // Tab header
    'Sign in': 'Se connecter', // Button label
    'Sign in to your account': 'Connectez-vous à votre compte', // Header text
    'Username (Label)': `Nom d'utilisateur`,
    'Username (Placeholder)': `Saisissez votre nom d'utilisateur`,
    'Password (Label)': 'Mot de passe',
    'Password (Placeholder)': 'Saisissez votre mot de passe',
    'Forgot your password?': 'Réinitialiser votre mot de passe',
    'There is already a signed in user.': 'Vous êtes déjà connecté. Actualiser la page.',
    // SignUp
    'Create Account': `Créer mon compte`, // Tab header
    'Sign Up': `S'inscrire`, // Button label
    'Create a new account': 'Créer un nouveau compte', // Header text
    'Email (Label)': 'Adresse email',
    'Email (Placeholder)': 'Saisissez votre adresse email',
    'Phone Number (Label)': 'Numéro mobile',
    'Phone Number (Placeholder)': 'Saisissez votre numéro de mobile',
    'Confirm Password (Label)': 'Confirmation de mot de passe',
    'Confirm Password (Placeholder)': 'Saisissez votre mot de passe de nouveau',
    'GivenName (Label)': 'Prénom',
    'GivenName (Placeholder)': 'Saisissez votre prénom',
    'FamilyName (Label)': 'Nom',
    'FamilyName (Placeholder)': 'Saisissez votre nom',
    'username is required to signUp': 'Vous devez saisir une adresse email',
    // 'username is required to signUp': 'Il faut renseigner une adresse email ou un numéro de téléphone',
    // VerifyUser
    'Account recovery requires verified contact information': 'La récupération du compte nécessite des informations de contact vérifiées',
    Skip: 'Passer cet étape',
    // Forgot Password
    'Reset your password': 'Mot de passe oublié ?', // Link text
    'Forgotten password': 'Réinitialisez votre mot de passe', // Header text
    'Account username (Label)': `Nom d'utilisateur lié au compte`,
    'Enter your account username (Placeholder)': `Saisissez votre nom d'utilisateur`,
    // 'Enter your username (Placeholder)': `Saisissez votre nom d'utilisateur ou votre adresse e-mail`,
    'Send code': 'Envoyer le code',
    'Back to Sign In': 'Retour à la connexion',
    'Enter Information': 'Saisir les informations',
    // ConfirmResetPassword
    'Confirm code (Label)': 'Code de vérification',
    'Confirm code (Placeholder)': 'Entrez votre code de vérification',
    'Send code again': 'Renvoyer le code à nouveau',
    'Account password (Label)': `Nouveau mot de passe`,
    'Enter your account password (Placeholder)': `Saisissez votre nouveau mot de passe`,
    'Confirm Account password (Label)': `Confirmation de mot de passe`,
    'Confirm account password (Placeholder)': `Confirmez votre nouveau mot de passe`,
    // Validation Errors
    'Password must have at least 8 characters': 'Le mot de passe doit comporter au moins 8 caractères',
    "Cannot read properties of undefined (reading 'replace')": 'Veuillez fournir un numéro de mobile',
    'Attribute value for phone_number must not be null': 'Veuillez fournir un numéro de mobile',
    'Invalid phone number format.': "Le format du numéro mobile n'est pas valide",
    'Username cannot be of email format, since user pool is configured for email alias.':
      "Veuillez fournir un nom d'utilisateur qui diffère de l'adresse email",
  },
})

// let hasCustomFormError: string = ''

const components = {
  Header() {
    return (
      <View className={classNames('bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500', 'flex flex-col items-center')}>
        <Image alt='APE CA logo' src={`${process.env.FLEX_CONTENT_HOST}/assets/png/Logo1.png`} style={{ maxWidth: '75px' }} />
      </View>
    )
  },

  // Footer() {
  //   return (
  //     <View className='flex flex-col items-center'>
  //       {hasCustomFormError?.length && (
  //         <Icon content={hasCustomFormError} size={IconSize.SMALL} position={IconPosition.LEFT} name={IconName.EXCLAMATION_TRIANGLE} />
  //       )}
  //     </View>
  //   )
  // },

  SignIn: {
    Header() {
      return (
        <View className='flex flex-col items-center'>
          <Title level={3}>{I18n.get('Sign in to your account')}</Title>
        </View>
      )
    },
    Footer() {
      const { toForgotPassword } = useAuthenticator()
      return (
        <View className='flex flex-col items-center' style={{ marginBottom: '1rem' }}>
          <Link onClick={() => toForgotPassword()}>{I18n.get('Reset your password')}</Link>
        </View>
      )
    },
  },

  SignUp: {
    Header() {
      return (
        <View className='flex flex-col items-center'>
          <Title level={3}>{I18n.get('Create a new account')}</Title>
        </View>
      )
    },
    Footer() {
      const { toSignIn } = useAuthenticator()
      return (
        <View className='flex flex-col items-center' style={{ marginBottom: '1rem' }}>
          <Link onClick={() => toSignIn()}>{I18n.get('Back to Sign In')}</Link>
        </View>
      )
    },
  },

  ConfirmSignUp: {
    Header() {
      return (
        <View className='flex flex-col items-center'>
          <Title level={3}>{I18n.get('Enter Information')}</Title>
        </View>
      )
    },
    // Footer() {
    //   return <Text>Footer Information</Text>
    // },
  },

  VerifyUser: {
    Header() {
      return (
        <View className='flex flex-col items-center'>
          <Title level={3} className={classNames(styles.hasTextCentered)} style={{ marginBottom: '1rem' }}>
            {I18n.get('Account recovery requires verified contact information')}
          </Title>
        </View>
      )
    },
    // FormFields() {
    //   const { skipVerification } = useAuthenticator()
    //   return <Link onClick={() => skipVerification()}>{I18n.get('Skip')}</Link>
    // },
    // Footer() {
    //   return <Text>Footer Information</Text>
    // },
  },

  SetupTotp: {
    Header() {
      return (
        <View className='flex flex-col items-center'>
          <Title level={3}>{I18n.get('Enter Information')}</Title>
        </View>
      )
    },
    // Footer() {
    //   return <Text>Footer Information</Text>
    // },
  },

  ConfirmSignIn: {
    Header() {
      return (
        <View className='flex flex-col items-center'>
          <Title level={3}>{I18n.get('Enter Information')}</Title>
        </View>
      )
    },
    // Footer() {
    //   return <Text>Footer Information</Text>
    // },
  },

  ForgotPassword: {
    Header() {
      return (
        <View className='flex flex-col items-center'>
          <Title level={3}>{I18n.get('Forgotten password')}</Title>
        </View>
      )
    },
    Footer() {
      const { toSignIn } = useAuthenticator()
      return (
        <View className='flex flex-col items-center' style={{ marginTop: '1rem' }}>
          <Link onClick={() => toSignIn()}>{I18n.get('Back to Sign In')}</Link>
        </View>
      )
    },
  },

  ConfirmResetPassword: {
    Header() {
      return (
        <View className='flex flex-col items-center'>
          <Title level={3}>{I18n.get('Enter Information')}</Title>
        </View>
      )
    },
    // Footer() {
    //   return <Text>Footer Information</Text>
    // },
  },
}

// const validateUsername = () => {
//   const containsDigit = /\d/.test(e.currentTarget.value);
//   setHasError(!containsDigit);
// };

const FormFields = () => {
  const { validationErrors } = useAuthenticator()
  return {
    signIn: {
      // username: {
      //   label: `${I18n.get('Username (Label)')}`,
      //   placeholder: `${I18n.get('Username (Placeholder)')}`,
      //   isRequired: true,
      //   inputMode: 'text',
      //   type: 'text',
      //   autoComplete: 'username',
      // },
      password: {
        label: `${I18n.get('Password (Label)')}`,
        placeholder: `${I18n.get('Password (Placeholder)')}`,
        isRequired: true,
        inputMode: 'text',
        type: 'password',
        autoComplete: 'current-password',
      },
    },
    signUp: {
      // username: {
      //   label: `${I18n.get('Username (Label)')}`,
      //   placeholder: `${I18n.get('Username (Placeholder)')}`,
      //   order: 1,
      //   isRequired: true,
      //   inputMode: 'text',
      //   type: 'text',
      //   autoComplete: 'username',
      // },
      email: {
        label: `${I18n.get('Email (Label)')}`,
        placeholder: `${I18n.get('Email (Placeholder)')}`,
        order: 2,
        isRequired: false,
        inputMode: 'email',
        type: 'email',
        autoComplete: 'username',
        errorMessage: validationErrors?.email as string,
        hasError: !!validationErrors?.email,
      },
      phone_number: {
        label: `${I18n.get('Phone Number (Label)')}`,
        placeholder: `${I18n.get('Phone Number (Placeholder)')}`,
        defaultDialCode: '+33',
        dialCodeList: ['+33'],
        order: 3,
        isRequired: false,
        inputMode: 'tel',
        type: 'tel',
        autoComplete: 'username',
        errorMessage: validationErrors?.phone_number as string,
        hasError: !!validationErrors?.phone_number,
        maxLength: 10,
        // backgroundColor: 'var(--flex-input-fill, #f9f9f9)',
        // borderColor: 'var(--flex-input-border-color, rgba(37, 70, 95, 0.4))'
      },
      given_name: {
        label: `${I18n.get('GivenName (Label)')}`,
        placeholder: `${I18n.get('GivenName (Placeholder)')}`,
        order: 4,
        isRequired: true,
        inputMode: 'text',
        type: 'text',
        autoComplete: 'given-name',
      },
      family_name: {
        label: `${I18n.get('FamilyName (Label)')}`,
        placeholder: `${I18n.get('FamilyName (Placeholder)')}`,
        order: 5,
        isRequired: true,
        inputMode: 'text',
        type: 'text',
        autoComplete: 'family-name',
      },
      password: {
        label: `${I18n.get('Password (Label)')}`,
        placeholder: `${I18n.get('Password (Placeholder)')}`,
        order: 6,
        isRequired: true,
        inputMode: 'text',
        type: 'password',
        autoComplete: 'new-password',
        errorMessage: validationErrors?.password as string,
        hasError: !!validationErrors?.password,
      },
      confirm_password: {
        label: `${I18n.get('Confirm Password (Label)')}`,
        placeholder: `${I18n.get('Confirm Password (Placeholder)')}`,
        order: 7,
        isRequired: true,
        inputMode: 'text',
        type: 'password',
        autoComplete: 'new-password',
        errorMessage: validationErrors?.confirm_password as string,
        hasError: !!validationErrors?.confirm_password,
      },
    },
    forceNewPassword: {
      password: {
        label: `${I18n.get('Password (Label)')}`,
        placeholder: `${I18n.get('Password (Placeholder)')}`,
      },
    },
    forgotPassword: {
      username: {
        label: `${I18n.get('Account username (Label)')}`,
        placeholder: `${I18n.get('Enter your account username (Placeholder)')}`,
        isRequired: true,
      },
    },
    confirmResetPassword: {
      confirmation_code: {
        label: `${I18n.get('Confirm code (Label)')}`,
        placeholder: `${I18n.get('Confirm code (Placeholder)')}`,
        isRequired: true,
      },
      password: {
        label: `${I18n.get('Account password (Label)')}`,
        placeholder: `${I18n.get('Enter your account password (Placeholder)')}`,
        isRequired: true,
      },
      confirm_password: {
        label: `${I18n.get('Confirm Account password (Label)')}`,
        placeholder: `${I18n.get('Confirm account password (Placeholder)')}`,
        isRequired: true,
      },
    },
    setupTotp: {
      QR: {
        totpIssuer: 'test issuer',
        totpUsername: 'amplify_qr_test_user',
      },
      confirmation_code: {
        label: `${I18n.get('Confirm code (Label)')}`,
        placeholder: `${I18n.get('Confirm code (Placeholder)')}`,
        isRequired: false,
      },
    },
    confirmSignIn: {
      confirmation_code: {
        label: `${I18n.get('Confirm code (Label)')}`,
        placeholder: `${I18n.get('Confirm code (Placeholder)')}`,
        isRequired: false,
      },
    },
  }
}

const theme: AmplifyUIReactTypes.Theme = {
  name: 'flex-override-theme',
  tokens: {
    components: {
      button: {
        primary: {
          color: { value: 'var(--flex-primary-invert, #fff)' },
          borderColor: { value: 'var(--flex-primary, #fe544b)' },
          backgroundColor: { value: 'var(--flex-primary, #fe544b)' },
          _hover: {
            borderColor: { value: '#fe544bcc' },
            backgroundColor: { value: '#fe544bcc' },
          },
        },
      },
      input: {
        color: { value: 'var(--flex-input-color, #25465f)' },
        borderColor: { value: 'var(--flex-input-border-color, rgba(37, 70, 95, 0.4))' },
        // backgroundColor: { value: 'var(--flex-input-border-color, rgba(37, 70, 95, 0.4))'},
        _focus: {
          borderColor: { value: 'var(--flex-link-hover, #109db9)' },
          // borderColor: { value: 'var(--flex-input-hover-border-color, #25465f)' },
        },
      },
    },
  },
}

async function handleFetchUserAttributes() {
  try {
    const userAttributes = await fetchUserAttributes()
    if (process.env.DEBUG === 'true') console.log('userAttributes: ', userAttributes)
    setCreateUser({
      username: userAttributes.sub,
      email: userAttributes?.email,
      phoneNumber: userAttributes?.phone_number,
      firstName: userAttributes?.given_name,
      lastName: userAttributes?.family_name,
    } as OnBoardEventApiTypes.User)
  } catch (error) {
    console.log(error)
  }
}

// async function handleFetchSessionResults() {
//   try {
//     const fetchSessionResult = await fetchAuthSession() // will return the credentials
//     console.log('fetchSessionResult: ', fetchSessionResult)
//   } catch (error) {
//     console.log(error)
//   }
// }

const AuthSignedIn = () => {
  const { user, signOut } = useAuthenticator((context) => [context.user])
  return (
    <main>
      <Title level={6}>Hello {user?.username}</Title>
      <Link
        onClick={() => {
          signOut()
          void DataStore.clear()
        }}
      >
        Se déconnecter
      </Link>
    </main>
  )
}

// https://docs.amplify.aws/javascript/build-a-backend/auth/auth-migration-guide/#authsignup
// // 1. Sign Up with autoSignIn enabled
// const handleSignUp = async ({ username, password, email, phone_number, validationData }) => {
//   const { isSignUpComplete, userId, nextStep } = await signUp({
//     username,
//     password,
//     options: {
//       userAttributes: {
//         email,
//         phone_number,
//       },
//       validationData,
//     },
//     autoSignIn: true,
//   })
// }

// 2. Confirm Sign Up
// const handleConfirmSignUp = async ({ username, confirmationCode }: AmplifyAuthTypes.ConfirmSignUpInput) => {
//   const { isSignUpComplete, userId, nextStep } = await confirmSignUp({
//     username,
//     confirmationCode,
//   })
//   // 3. Trigger autoSignIn event - will not be triggered automatically
//   const { isSignedIn } = await autoSignIn()
// }

async function handleAutoSignIn() {
  try {
    // 3. Trigger autoSignIn event - will not be triggered automatically
    const isSignedIn = await AmplifyAuth.autoSignIn()
    console.log(isSignedIn)
    if (isSignedIn) {
      console.log('isSignedIn', isSignedIn)
      window.location.reload()
    }
  } catch (error) {
    console.log(error)
  }
}

const AuthSteps = () => {
  return (
    <ThemeProvider theme={theme} colorMode={'dark'} nonce={globalThis.__webpack_nonce__}>
      <Authenticator
        variation='modal'
        formFields={FormFields()}
        components={components}
        services={{
          // eslint-disable-next-line @typescript-eslint/require-await
          async validateCustomSignUp(formData) {
            if (!formData.phone_number.length && !formData.email.length) {
              return {
                phone_number: '⚠️ Il faut renseigner une adresse email ou un numéro de téléphone',
              }
            }
            // if (!formData.phone_number) {
            //   return {
            //     phone_number: 'Phone number is required',
            //   }
            // }
            if (formData.phone_number.length) {
              if (!/^(?:\+|\+?(\d(?:\s*))+)$/.test(formData.phone_number)) {
                return {
                  phone_number: 'Le numéro de téléphone ne doit contenir que des chiffres',
                }
              }
            }
          },
          // 1. Sign Up with autoSignIn enabled
          async handleSignUp(formData: AmplifyAuthTypes.SignUpInput) {
            const { username, password, options } = formData
            // custom username
            // username = username.toLowerCase()
            // console.log(options)

            // https://regex101.com/r/dsi0Pw/2
            const regexPhoneNumberModify = /^(\+33)+(\s*0?)(\s*[1-9])(([\s]*\d{2}){4})$/gim
            const phone_number_modified = options?.userAttributes?.phone_number?.replace(regexPhoneNumberModify, '+33$3$4')
            console.log('phone_number', options?.userAttributes?.phone_number)
            console.log('modified phone_number', phone_number_modified)
            return AmplifyAuth.signUp({
              username,
              password,
              options: {
                userAttributes: {
                  ...(options?.userAttributes?.email?.length && {
                    email: options?.userAttributes?.email,
                  }),
                  // TODO : monkey patch for issue https://github.com/aws-amplify/amplify-ui/issues/4794
                  // TODO : currently unable to update @aws-amplify@next to retrieve fix for this bug
                  // https://github.com/aws-amplify/amplify-ui/pull/4801/files/8bfbbac3cb0816eb0b67711ba7813055e689d143
                  // due to dependancy errors with @aws-amplify/core
                  ...(options?.userAttributes?.phone_number?.length && {
                    phone_number: phone_number_modified,
                  }),
                  family_name: options?.userAttributes?.family_name,
                  given_name: options?.userAttributes?.given_name,
                },
                autoSignIn: {
                  enabled: true,
                },
              },
            })
          },
          // 2. Confirm Sign Up
          async handleConfirmSignUp(formData: AmplifyAuthTypes.ConfirmSignUpInput) {
            const { username, confirmationCode, options } = formData
            return AmplifyAuth.confirmSignUp({
              username,
              confirmationCode,
              options,
            }).then((data: AmplifyAuthTypes.ConfirmSignUpOutput) => {
              const { isSignUpComplete, userId, nextStep } = data
              if (process.env.DEBUG === 'true') console.log('nextStep', `${userId} - ${nextStep.signUpStep}`)
              // if (nextStep?.signUpStep === 'DONE' || nextStep?.signUpStep === 'COMPLETE_AUTO_SIGN_IN') {
              //   handleAutoSignIn()
              // }
              if (isSignUpComplete) void handleAutoSignIn()
              return data
            })
          },
        }}
        // The following parameters are defined by AMPLIFY_AUTH_CONFIG_V2
        loginMechanisms={['email', 'phone_number']}
        signUpAttributes={['phone_number', 'family_name', 'given_name']}
        socialProviders={['google']}
        // hideSignUp={true}
      >
        {/* {route === 'authenticated' &&
          (({ signOut, user }) => (
            <main>
              <h1>Hello {user?.username}</h1>
              <button onClick={signOut}>Se déconnecter</button>
            </main>
          ))} */}
      </Authenticator>
    </ThemeProvider>
  )
}

const AuthComponent = observer(() => {
  const { route } = useAuthenticator((context) => [context.route])
  const { authenticationOnLoad, triggerAuthentication } = UIStore
  const { createOnBoardEvent } = CreateOnBoardEvent
  const [showAuthSteps, setShowAuthSteps] = React.useState<boolean>(true)

  React.useEffect(() => {
    void (async () => {
      try {
        if (process.env.DEBUG === 'true') console.log('Authenticator route : ', route)
        if (route === 'authenticated') {
          void handleFetchUserAttributes()
          await initAmplifyConfigClient('API-userPool')
          // void handleFetchSessionResults()
          if (!authenticationOnLoad && triggerAuthentication && !isObjectEmpty(toJS(createOnBoardEvent))) {
            void setModalCreateOnBoardEventOpen(true)
          }
        }
      } catch (err) {
        console.error(err)
      }
    })()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [route])

  return route === 'authenticated' ? (
    <AuthSignedIn />
  ) : (
    <>
      {showAuthSteps && (
        <>
          <div style={{ position: 'absolute', right: '2rem', top: '1rem', zIndex: '1000' }}>
            <Button onClick={() => setShowAuthSteps(false)}>X</Button>
          </div>
          <AuthSteps />
        </>
      )}
    </>
  )

  // if (authenticationOnLoad) {
  //   return route === 'authenticated' ? <AuthSignedIn /> : <AuthSteps />
  // } else {
  //   return route === 'authenticated' ? <AuthSignedIn /> : triggerAuthentication ? <AuthSteps /> : null
  // }
})

// export default withAuthenticator(AuthComponent)
export default AuthComponent
