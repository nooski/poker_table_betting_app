/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { action, makeObservable, observable } from 'mobx'
import { type PokerTables as PokerTablesType, type PokerBoard as PokerBoardType, type PokerTimer as PokerTimerType } from 'on-board-event-common'
const { PokerBoard, PokerTables, PokerTimer, getBaseTableState, updateTablesReq, createTableReq, deleteTableReq } = await import(
  /* webpackFetchPriority: "high" */
  'on-board-event-common'
)

const { getOnBoardEventsStore } = await import('@flexiness/domain-store')
const CreateOnBoardEvent = getOnBoardEventsStore()
const { setDisplayedOnBoardEventDataMap, addDisplayedOnBoardEventDataMap } = CreateOnBoardEvent

// import * as WebSocket from 'ws'
import { Table, TableData, OnBoardEventApiTypes, IPokerMsg, IPokerAction, PokerAction, Window } from 'flexiness'

declare let window: Window

/**
 * Provides a client to the poker backend.
 */
export default class PokerClient {
  nextSeq = 1

  clientId: number | null = null

  pokerClass: null | string = null

  messages: (IPokerMsg | string)[] = []

  disconnected = false

  pendingActions: PokerAction[] = []

  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-explicit-any
  board: any = new PokerBoard()

  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  serverBoard = this.board

  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-explicit-any
  tables: any = new PokerTables([])

  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  serverTables = this.tables

  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-explicit-any
  timers: any = new PokerTimer(new Date())

  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  serverTimers = this.timers

  ws

  /**
   * Constructs a new poker board client.
   *
   * @param {string=} url
   *   if provided, the websocket URL to connect with. If not provided, forms a URL based on window.location
   *   assuming the websocket is hosted at relative URL "ws", but with ws (if http) and wss (if https) protocol.
   *   In the development environment we assume port 8080, else we use port from window.location (if any)
   */
  constructor(url: string) {
    // Call it here
    // makeAutoObservable(this)
    makeObservable(this, {
      clientId: observable,
      pokerClass: observable,
      board: observable.ref,
      serverBoard: observable.ref,
      tables: observable.ref,
      serverTables: observable.ref,
      timers: observable.ref,
      serverTimers: observable.ref,
      messages: observable,
      disconnected: observable,
      pendingActions: observable.shallow,
      setClientId: action,
      setPokerClass: action,
      setBoard: action,
      setTables: action,
      setTimers: action,
      processBoardActions: action,
      processTablesActions: action,
      processTimersActions: action,
      processServerBoardActions: action,
      processServerTablesActions: action,
      processServerTimersActions: action,
      pushMessage: action,
      filterPendingActions: action,
      addPendingAction: action,
      setDisconnectedState: action,
    })
    if (!url) {
      const parsedUrl = new URL(window.location as string | URL)
      parsedUrl.protocol = `${process.env.FLEX_PROTOCOL}` === 'http://' ? 'ws' : 'wss'
      parsedUrl.port = `${process.env.FLEX_POKER_BACK_PORT}`
      // console.log(parsedUrl)
      url = new URL('./ws', parsedUrl).href
      // console.log(url)
    }
    this.ws = new WebSocket(url)

    this.ws.addEventListener('open', () => {
      this.pushMessage('Connection opened')
    })

    // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/no-misused-promises
    this.ws.addEventListener('message', async (m: MessageEvent<any>) => {
      // console.log(m.data)
      this.pushMessage(m.data as string)
      const message = JSON.parse(m.data as string) as IPokerMsg

      if (message.ack !== undefined) {
        // Remove all acknowledged actions
        this.filterPendingActions(message.ack as number)
      } else if (!this.clientId && message.clientId) {
        // initial message
        this.setClientId(message.clientId as number)
        this.updateBoardFromServer(message.snapshot as PokerAction)
        await this.updateTableFromServer(message.initSnapshotTables as PokerAction)
      } else {
        if (message.source === 'PokerBoard') {
          this.updateBoardFromServer(message as IPokerAction)
        } else if (message.source === 'PokerTables') {
          await this.updateTableFromServer(message as IPokerAction)
        } else if (message.source === 'PokerTimer') {
          this.updateTimerFromServer(message as IPokerAction)
        }
      }
    })

    this.ws.addEventListener('close', () => {
      this.pushMessage('Connection closed')
      this.setDisconnectedState(true)
    })
  }

  close() {
    this?.ws?.close?.()
  }

  /**
   * Set disconnected state.
   */
  setDisconnectedState(bool: boolean) {
    this.disconnected = bool
  }

  /**
   * Set ClientId.
   */
  setClientId(id: number) {
    this.clientId = id
  }

  /**
   * Push Message.
   */
  pushMessage(data: string | IPokerMsg) {
    void this.messages.push(data)
  }

  /**
   * Set board.
   */
  setBoard(board: PokerBoardType) {
    this.board = board
  }

  /**
   * Set table.
   */
  setTables(tables: PokerTablesType) {
    this.tables = tables
  }

  /**
   * Set timers.
   */
  setTimers(timers: PokerTimerType) {
    this.timers = timers
  }

  /**
   * Process board actions.
   */
  processBoardActions(action: PokerAction) {
    this.board = (this.board as PokerBoardType).processAction(action)
  }

  /**
   * Process tables actions.
   */
  processTablesActions(action: PokerAction) {
    this.tables = (this.tables as PokerTablesType).processAction(action)
  }

  /**
   * Process timers actions.
   */
  processTimersActions(action: PokerAction) {
    this.timers = (this.timers as PokerTimerType).processAction(action)
  }

  /**
   * Process server board actions.
   */
  processServerBoardActions(action: PokerAction) {
    this.serverBoard = (this.serverBoard as PokerBoardType).processAction(action)
  }

  /**
   * Process server tables actions.
   */
  processServerTablesActions(action: PokerAction) {
    this.serverTables = (this.serverTables as PokerTablesType).processAction(action)
  }

  /**
   * Process server timers actions.
   */
  processServerTimersActions(action: PokerAction) {
    this.serverTimers = (this.serverTimers as PokerTimerType).processAction(action)
  }

  /**
   * Sets the pokerClass to route processAction to correct class endpoint.
   */
  setPokerClass(newPokerClass: string) {
    this.pokerClass = newPokerClass
  }

  /**
   * Adds actions to class endpoint.
   */
  setActions(action: PokerAction) {
    if (this.pokerClass === 'PokerBoard') {
      this.processBoardActions(action)
    } else if (this.pokerClass === 'PokerTables') {
      this.processTablesActions(action)
    } else if (this.pokerClass === 'PokerTimer') {
      this.processTimersActions(action)
    }
  }

  /**
   * Remove all acknowledged actions
   */
  filterPendingActions(messageAcknowledged: number) {
    this.pendingActions = this.pendingActions.filter((it) => (it.seq ?? 0) > messageAcknowledged)
  }

  /**
   * Add to pending actions.
   */
  addPendingAction(action: PokerAction) {
    this.pendingActions.push(action)
  }

  /**
   * Sends an action to the server, tagging the client ID and sequence to the message.
   */
  sendAction(msg: IPokerMsg) {
    // We should improve this experience at some point...
    if (this.clientId === null) throw new Error('Not yet connected')

    const action: PokerAction = {
      ...msg,
      id: this.clientId,
      seq: this.nextSeq++,
    }
    if (action) this.setActions(action)
    this.addPendingAction(action)
    this.ws.send(JSON.stringify(action))
  }

  updateBoardFromServer(action: PokerAction) {
    if (action.source !== 'PokerBoard') return
    this.processServerBoardActions(action)

    // rebuild our local board with any pending actions since server's state
    const board = this.serverBoard as PokerBoardType
    this.pendingActions.forEach((a) => {
      this.processBoardActions(a)
    })

    this.setBoard(board)
  }

  updateTableFromServer = async (action: PokerAction) => {
    if (action.source !== 'PokerTables') return
    console.log('updateTableFromServer', action)
    if (action.action === 'initSnapshotTables') {
      console.log('PokerClient initSnapshotTables', action)
    }
    if (action.action === 'snapshotTables') {
      console.log('PokerClient snapshotTables')
      await getBaseTableState({ limit: 20 }).then((tables: Table[] | undefined) => {
        if (tables !== null && tables !== undefined) {
          action = { ...action, ...tables }
        }
      })
    }
    if (action.action === 'updateTables') {
      console.log('PokerClient updateTables')
      await updateTablesReq().then((data: OnBoardEventApiTypes.OnBoardEvent[] | undefined) => {
        if (typeof data !== 'undefined') {
          setDisplayedOnBoardEventDataMap(data)
        }
      })
    }
    if (action.action === 'createTable') {
      await createTableReq().then((data: OnBoardEventApiTypes.OnBoardEvent | undefined) => {
        if (typeof data != 'undefined') {
          addDisplayedOnBoardEventDataMap([data])
        }
      })
    }
    if (action.action === 'deleteTable' && (action.tableData as TableData).id != undefined) {
      await deleteTableReq((action.tableData as TableData).id)
    }
    if (action.action === 'activateTable' && (action.tableData as TableData).id != undefined) {
      // activateTable(action.tableData as TableData)
    }
    this.processServerTablesActions(action)

    // rebuild our local board with any pending actions since server's state
    const tables = this.serverTables as PokerTablesType
    this.pendingActions.forEach((a) => {
      this.processTablesActions(a)
    })

    this.setTables(tables)
  }

  updateTimerFromServer(action: PokerAction) {
    if (action.source !== 'PokerTimer') return
    this.processServerTimersActions(action)

    // rebuild our local board with any pending actions since server's state
    const timers = this.serverTimers as PokerTimerType
    this.pendingActions.forEach((a) => {
      this.processTimersActions(a)
    })

    this.setTimers(timers)
  }
}
