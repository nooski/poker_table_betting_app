'use-client'

/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */
// https://ui.docs.amplify.aws/react/connected-components/authenticator/advanced

import { isServer } from '@flexiness/domain-utils'
import { enableStaticRendering } from 'mobx-react-lite'
enableStaticRendering(isServer)
import { extendZodWithMobxZodForm } from '@monoid-dev/mobx-zod-form'
import fr from 'date-fns/locale/fr'
import React from 'react'
import { registerLocale, setDefaultLocale } from 'react-datepicker'
registerLocale('fr', fr)
setDefaultLocale('fr')
import { z } from 'zod'
// Necessary step to setup zod with mobx-zod-form power!
// eslint-disable-next-line @typescript-eslint/no-unsafe-call
extendZodWithMobxZodForm(z)

const {
  initAmplifyConfigClient,
  AMPLIFY_CLIENT,
  // getCustomCredentialsProvider
} = await import('@flexiness/aws')

const { AmplifyUIReact } = AMPLIFY_CLIENT

await initAmplifyConfigClient('API-userPool')

// Create an instance of your custom provider
// const customCredentialsProvider = await getCustomCredentialsProvider()
// Amplify.configure(AMPLIFY_AUTH_CONFIG_V2, {
//   Auth: {
//     // Supply the custom credentials provider to Amplify
//     credentialsProvider: customCredentialsProvider,
//   },
// })

const { Authenticator, View } = AmplifyUIReact

// import AuthComponent from './AuthComponent'
const { default: AuthComponent } = await import('./AuthComponent')

import { StoreProvider, getStores } from '@flexiness/domain-store'

import OnBoarding from './OnBoarding.jsx'

// import { runStandalone } from '@flexiness/domain-utils/style-loader-esm'

import { AppProps } from 'flexiness'

import '@flexiness/domain-tailwind/globals.css'
import 'react-datepicker/dist/react-datepicker.css'
import './styles/poker-app.css'

const App: React.FC<AppProps> = ({ isStandalone = false }) => {
  // React.useEffect(() => {
  //   if (!isStandalone || isServer) {
  //     return
  //   }
  //   // If we receive the isStandAlone prop we will initialize the style loader in standalone mode
  //   runStandalone()
  // }, [isStandalone])

  return (
    <Authenticator.Provider>
      <StoreProvider value={{ stores: getStores() }}>
        <View>
          {isStandalone && <AuthComponent />}
          <OnBoarding isStandalone={isStandalone} />
        </View>
      </StoreProvider>
    </Authenticator.Provider>
  )
}

export default App
