import {
  setDefaultOptions,
  format,
  formatISO,
  parseISO,
  // isValid,
  startOfDay,
  // endOfDay,
  subYears,
  addYears,
  isAfter,
} from 'date-fns'
import { fr } from 'date-fns/locale'
setDefaultOptions({ locale: fr })

const today = new Date()
// tests
// const today = new Date(2024, 6, 1) // Current month (0-11, January is 0)
// const today = new Date(2024, 6, 3) // Current month (0-11, January is 0)

const septStr = format(startOfDay(today), 'yyyy-09-04', { locale: fr })
const septDate = parseISO(septStr)
// console.log('septDate', isValid(septDate))

const julyStr = format(startOfDay(today), 'yyyy-07-02', { locale: fr })
const julyDate = parseISO(julyStr)
// console.log('julyDate', isValid(julyDate))

const nextSeptDate = septDate
// console.log('nextSeptDate', isValid(nextSeptDate))
const prevSeptDate = subYears(septDate, 1)
// console.log('prevSeptDate', isValid(prevSeptDate))

const nextJulyDate = addYears(julyDate, 1)
// console.log('nextJulyDate', isValid(nextJulyDate))
const prevJulyDate = julyDate
// console.log('prevJulyDate', isValid(prevJulyDate))

const startOfSchoolYear = () => {
  const nextSeptDateISO = parseISO(formatISO(nextSeptDate, { representation: 'complete' }))
  // console.log('nextSeptDateISO', isValid(nextSeptDateISO))
  const nextSeptStrISO = formatISO(nextSeptDateISO)

  const prevSeptDateISO = parseISO(formatISO(prevSeptDate, { representation: 'complete' }))
  // console.log('prevSeptDateISO', isValid(prevSeptDateISO))
  const prevSeptStrISO = formatISO(prevSeptDateISO)

  const data = {
    string: isAfter(today, julyDate) ? nextSeptStrISO : prevSeptStrISO,
    date: isAfter(today, julyDate) ? nextSeptDateISO : prevSeptDateISO,
  }

  return data
}

const endOfSchoolYear = () => {
  const nextJulyDateISO = parseISO(formatISO(nextJulyDate, { representation: 'complete' }))
  // console.log('nextJulyDateISO', isValid(nextJulyDateISO))
  const nextJulyStrISO = formatISO(nextJulyDateISO)

  const prevJulyDateISO = parseISO(formatISO(prevJulyDate, { representation: 'complete' }))
  // console.log('prevJulyDateISO', isValid(prevJulyDateISO))
  const prevJulyStrISO = formatISO(prevJulyDateISO)

  const data = {
    string: isAfter(today, julyDate) ? nextJulyStrISO : prevJulyStrISO,
    date: isAfter(today, julyDate) ? nextJulyDateISO : prevJulyDateISO,
  }

  return data
}

export { startOfSchoolYear, endOfSchoolYear }
