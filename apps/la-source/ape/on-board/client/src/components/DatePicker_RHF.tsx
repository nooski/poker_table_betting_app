/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */

import React from 'react'
// import { observer } from 'mobx-react-lite'
// import { toJS } from 'mobx'
// import { format, formatISO, parseISO, startOfDay, endOfDay } from 'date-fns'
import { zodResolver } from '@hookform/resolvers/zod'
import DatePicker from 'react-datepicker'
import {
  // Control,
  Controller,
  EventType,
  // UseFormRegisterReturn,
  // RefCallBack,
  UseFormRegister,
  useForm as useFormRHF,
} from 'react-hook-form'
import { z } from 'zod'

// import { addDays } from 'date-fns'
import { fr } from 'date-fns/locale'

import classNames from 'classnames'

// import {
//   IconName,
//   Input,
//   type InputWebEvents,
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import { IconName, Input, type InputWebEvents } from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

// import type { InputWebEvents } from '@flex-design-system/react-ts/client-sync-styled-named'

// const { ClientSyncStyledDefault } = await import('flex_design_system_react_ts_styled_default')
// const {
//   IconName,
//   Input,
//   styles,
// } = ClientSyncStyledDefault

type HandleSubmitRHF = {
  handleSubmitRHF: () => boolean
}

interface WatchData {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  value: { [x: string]: any }
  name?: string
  type?: EventType
}

interface IReactDatePickerInputProps {
  value?: string
  onClick?: () => InputWebEvents
  placeholderText: string
  marginless?: boolean
}

interface IDateInputProps {
  schema: z.ZodTypeAny
  // schema: z.ZodType<z.ZodDate>
  label: string
  name: string
  value?: string
  callback?: (data: Date) => void
  register?: UseFormRegister<FormData>
  marginless?: boolean
}

const ReactDatePickerInput = React.forwardRef(({ value, onClick, placeholderText, marginless = false }: IReactDatePickerInputProps, ref) => {
  return (
    <Input
      onClick={onClick}
      hasIcon
      customIcon={IconName.CALENDAR_INFO_CIRCLE}
      value={value}
      placeholder={placeholderText}
      className={classNames(marginless && styles.isMarginless)}
    />
  )
})
ReactDatePickerInput.displayName = 'ReactDatePickerInput'

const DateInput = React.forwardRef(({ schema, label, name, value, callback, marginless = false }: IDateInputProps, ref) => {
  const { control, watch } = useFormRHF({
    resolver: zodResolver(schema),
  })

  // Callback version of watch. It's your responsibility to unsubscribe when done.
  React.useEffect(() => {
    const subscription = watch((value, { name, type }) => {
      console.log(value, name, type)
    })
    return () => subscription.unsubscribe()
  }, [watch])

  return (
    <Controller
      control={control}
      name={name}
      rules={{ required: true }}
      render={({ field }) => (
        <DatePicker
          withPortal
          dateFormat='dd/MM/yyyy'
          locale={fr}
          placeholderText={label}
          onChange={(date: Date) => {
            field.onChange(date)
            callback?.(date)
          }}
          selected={field.value as Date}
          ref={null}
          value={value}
          customInput={<ReactDatePickerInput placeholderText={label} marginless={marginless} />}
        />
      )}
    />
  )
})
DateInput.displayName = 'DateInput'

const ReactTimePickerInput = React.forwardRef(({ value, onClick, placeholderText, marginless = false }: IReactDatePickerInputProps, ref) => {
  return (
    <Input
      onClick={onClick}
      hasIcon
      customIcon={IconName.STOPWATCH}
      value={value}
      placeholder={placeholderText}
      className={classNames('w-full', marginless && styles.isMarginless)}
    />
  )
})
ReactTimePickerInput.displayName = 'ReactTimePickerInput'

const TimeInput = React.forwardRef(({ schema, label, name, value, callback, marginless = false }: IDateInputProps, ref) => {
  const { control, watch } = useFormRHF({
    resolver: zodResolver(schema),
  })

  // const [startDate, setStartDate] = React.useState<Date>(addDays(new Date(), 1))

  // Callback version of watch. It's your responsibility to unsubscribe when done.
  React.useEffect(() => {
    const subscription = watch((value, { name, type }) => {
      console.log(value, name, type)
    })
    return () => subscription.unsubscribe()
  }, [watch])

  return (
    <Controller
      control={control}
      name={name}
      rules={{ required: true }}
      render={({ field }) => (
        <DatePicker
          withPortal
          portalId='root-portal'
          dateFormat='HH:mm'
          locale={fr}
          placeholderText={label}
          showTimeSelect
          showTimeSelectOnly
          timeIntervals={15}
          timeCaption={label}
          timeClassName={() => 'w-full text-lg'}
          onChange={(date: Date) => {
            field.onChange(date)
            callback?.(date)
          }}
          selected={field.value as Date}
          // onChange={(date: Date) => {
          //   field.onChange(setStartDate(date))
          //   callback?.(startDate)
          // }}
          // selected={startDate}
          ref={null}
          value={value}
          customInput={<ReactTimePickerInput placeholderText={label} marginless={marginless} />}
        />
      )}
    />
  )
})
TimeInput.displayName = 'TimeInput'

export { DateInput, TimeInput, type HandleSubmitRHF }
