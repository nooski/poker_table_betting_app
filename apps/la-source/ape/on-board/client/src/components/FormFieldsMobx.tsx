/* eslint-disable @typescript-eslint/no-unused-vars */

import { motion } from 'framer-motion'
import React from 'react'
// import useCallbackState from '../utility/hooks/useCallbackState'
import { getForm } from '@monoid-dev/mobx-zod-form-react'
import { observer } from 'mobx-react-lite'

// import { isObjectEmpty } from '@flexiness/domain-utils'

import { MobxFormFieldEnum, MobxFormFieldInput, MobxFormObject } from 'flexiness'

import classNames from 'classnames'

// import {
//   Input,
//   Icon,
//   IconName,
//   IconSize,
//   IconPosition,
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import { Input, Icon, IconName, IconSize, IconPosition } from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

// import type { InputProps } from '@flex-design-system/react-ts/client-sync-styled-named'

// const { ClientSyncStyledDefault } = await import('flex_design_system_react_ts_styled_default')
// const {
//   Input,
//   Icon,
//   IconName,
//   IconSize,
//   IconPosition,
//   styles,
// } = ClientSyncStyledDefault

interface FormProps {
  form: MobxFormObject
  // form: ReturnType<typeof useFormMobx>
}

interface meta {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any
  fieldValue: string
  label?: () => React.JSX.Element
  info?: () => React.JSX.Element
}

type HandleMobxInputChange = {
  handleMobxInputChange: () => boolean
}

type MobxFormFieldInputProps = {
  field: MobxFormFieldInput
  placeholder?: string
  marginless?: boolean
  metas?: meta[]
  callback?: (inputValue: string) => void
}

type MobxFormFieldEnumProps = {
  field: MobxFormFieldEnum
  fieldErrorOverride?: boolean
  options?: string[]
  metas?: meta[]
  callback?: (inputValue: string) => void
}

const ErrorDisplay = ({ message }: { message: string }) => {
  return (
    <div className={classNames(styles.help, styles.isDanger, styles.isFullwidth)} style={{ margin: '0.5rem 0' }}>
      <Icon content={message} size={IconSize.SMALL} position={IconPosition.LEFT} name={IconName.EXCLAMATION_CIRCLE} />
    </div>
  )
}

const TextInput = observer(({ field, placeholder, marginless = false, metas, callback }: MobxFormFieldInputProps) => {
  const _id = `${field.uniqueId}`
  const _placeholder = placeholder || field.path.at(-1)?.toString()
  const [inputValue, setInputValue] = React.useState<string | null>('')
  const [info, setInfo] = React.useState<React.ReactNode | null>(null)
  React.useEffect(() => {
    if (inputValue) {
      setInfo(metas?.find((meta) => meta.fieldValue === inputValue)?.info?.())
      callback?.(inputValue)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inputValue])
  return (
    <div className={classNames(styles.field, styles.isFullwidth, marginless && styles.isMarginless)}>
      <div
        className={classNames(styles.control, styles.hasDynamicPlaceholder, field.errorMessages.length && styles.isDanger)}
        {...getForm(field).bindField(field)}
      >
        <input
          id={_id}
          placeholder={_placeholder}
          className={classNames(styles.input)}
          value={`${field.rawInput}`}
          onChange={() => {
            setInputValue(`${field.rawInput}`)
          }}
        />
        <label htmlFor={_id}>{_placeholder}</label>
      </div>
      {info && (
        <div className={classNames(styles.help, styles.isSuccess, styles.isFullwidth)}>
          <motion.div initial={{ x: '-50%', opacity: 0 }} animate={{ x: 0, opacity: 1 }} transition={{ duration: 0.3, ease: 'easeInOut' }}>
            {info}
          </motion.div>
        </div>
      )}
      {!field._extraErrorMessages.length && field.errorMessages.map((e, i) => <ErrorDisplay key={i} message={e} />)}
    </div>
  )
})

const TextInputRef = observer(
  React.forwardRef<HTMLInputElement, MobxFormFieldInputProps>(({ field, placeholder, marginless = false, metas, callback }, ref) => {
    const _id = `${field.uniqueId}`
    const _placeholder = placeholder || field.path.at(-1)?.toString()
    const [inputValue, setInputValue] = React.useState<string | null>(null)
    const [info, setInfo] = React.useState<React.ReactNode | null>(null)
    React.useEffect(() => {
      if (inputValue) {
        setInfo(metas?.find((meta) => meta.fieldValue === inputValue)?.info?.())
        // callback?.(inputValue)
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [inputValue])

    return (
      <div className={classNames(styles.field, styles.isFullwidth, marginless && styles.isMarginless)}>
        <div
          className={classNames(styles.control, styles.hasDynamicPlaceholder, field.errorMessages.length && styles.isDanger)}
          {...getForm(field).bindField(field)}
        >
          <input
            id={_id}
            placeholder={_placeholder}
            className={classNames(styles.input)}
            defaultValue={`${field.rawInput}`}
            onKeyUp={() => {
              setInputValue(`${field.rawInput}`)
            }}
            // Mobile touch device
            onBlur={() => {
              if (inputValue) callback?.(inputValue)
            }}
            // Web device
            onMouseOut={() => {
              if (inputValue) callback?.(inputValue)
            }}
            ref={ref}
          />
          <label htmlFor={_id}>{_placeholder}</label>
        </div>
        {info && (
          <div className={classNames(styles.help, styles.isSuccess, styles.isFullwidth)}>
            <motion.div initial={{ x: '-50%', opacity: 0 }} animate={{ x: 0, opacity: 1 }} transition={{ duration: 0.3, ease: 'easeInOut' }}>
              {info}
            </motion.div>
          </div>
        )}
        {!field._extraErrorMessages.length && field.errorMessages.map((e, i) => <ErrorDisplay key={i} message={e} />)}
      </div>
    )
  }),
)

const TextArea = observer(({ field, placeholder, marginless = false, metas, callback }: MobxFormFieldInputProps) => {
  const _id = `${field.uniqueId}`
  const _placeholder = placeholder || field.path.at(-1)?.toString()
  const [inputValue, setInputValue] = React.useState<string | null>(null)
  const [info, setInfo] = React.useState<React.ReactNode | null>(null)
  React.useEffect(() => {
    if (inputValue) {
      setInfo(metas?.find((meta) => meta.fieldValue === inputValue)?.info?.())
      callback?.(inputValue)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inputValue])
  return (
    <div className={classNames(styles.field, styles.isFullwidth, marginless && styles.isMarginless)}>
      <div
        className={classNames(styles.control, styles.hasDynamicPlaceholder, field.errorMessages.length && styles.isDanger)}
        {...getForm(field).bindField(field)}
      >
        <textarea
          id={_id}
          placeholder={_placeholder}
          className={classNames(styles.textarea)}
          value={`${field.rawInput}`}
          onChange={() => {
            setInputValue(`${field.rawInput}`)
          }}
        />
        <label htmlFor={_id}>{_placeholder}</label>
      </div>
      {info && (
        <div className={classNames(styles.help, styles.isSuccess, styles.isFullwidth)}>
          <motion.div initial={{ x: '-50%', opacity: 0 }} animate={{ x: 0, opacity: 1 }} transition={{ duration: 0.3, ease: 'easeInOut' }}>
            {info}
          </motion.div>
        </div>
      )}
      {!field._extraErrorMessages.length && field.errorMessages.map((e, i) => <ErrorDisplay key={i} message={e} />)}
    </div>
  )
})

const OptionsEnumFlex = observer(({ field, fieldErrorOverride, options, metas, callback }: MobxFormFieldEnumProps) => {
  const _fieldName = field.path.at(0)?.toString()
  const _enum = options ?? getEnumValues(field.type.Values)
  const [inputValue, setInputValue] = React.useState<string | null>(null)
  const [info, setInfo] = React.useState<React.ReactNode | null>(null)
  const mainLayoutClasses = classNames(
    // FlexBox Styles
    [styles.isFlexDirectionRow, styles.isFlexWrapWrap, styles.isAlignItemsCenter, styles.isJustifiedCenter, styles.isAlignContentCenter],
    [styles.isFullwidth],
  )
  // const _initialInput = field.form._options.initialOutput?.[`${_fieldName}`]
  React.useEffect(() => {
    if (inputValue) {
      setInfo(metas?.find((meta) => meta.fieldValue === inputValue)?.info?.())
      callback?.(inputValue)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inputValue])
  return (
    <div className={classNames(styles.options, mainLayoutClasses)} {...getForm(field).bindField(field)} style={{ marginBottom: '1rem' }}>
      {_enum.map((value, index: number) => {
        const _id = `${_fieldName}_${value.toLowerCase()}`
        return (
          <div key={index} className={classNames(styles.field, 'w-full md:w-fit')} style={{ margin: '0', padding: '0 0.25rem' }}>
            <div className={classNames(styles.control, styles.isFlex)}>
              <input
                type='radio'
                id={_id}
                value={`${value}`}
                name={`${_fieldName}`}
                checked={Boolean(value === field.rawInput)}
                onChange={() => {
                  // https://stackoverflow.com/questions/56247433/how-to-use-setstate-callback-on-react-hooks
                  setInputValue(value)
                  // Promise.resolve()
                  //   .then(() => setInfo(metas?.find((meta) => meta.fieldValue === value)?.info?.()))
                  //   .then(() => callback?.(value))
                }}
              />
              <label
                className={classNames(
                  [styles.isFullwidth, 'break-all', 'mb-1'],
                  [styles.isFlex, styles.isAlignItemsCenter, styles.isJustifiedCenter],
                )}
                style={{ userSelect: 'none' }}
                htmlFor={_id}
              >
                <div>{metas?.find((meta) => meta.fieldValue === value)?.label?.() ?? value}</div>
              </label>
            </div>
          </div>
        )
      })}
      {info && (
        <div className={classNames(styles.help, styles.isSuccess, styles.isFullwidth)}>
          <motion.div initial={{ x: '-50%', opacity: 0 }} animate={{ x: 0, opacity: 1 }} transition={{ duration: 0.3, ease: 'easeInOut' }}>
            {info}
          </motion.div>
        </div>
      )}
      {!field._extraErrorMessages.length && field.errorMessages.map((e, i) => <ErrorDisplay key={i} message={e} />)}
    </div>
  )
})

const OptionsEnumGrid = observer(({ field, fieldErrorOverride, options, metas, callback }: MobxFormFieldEnumProps) => {
  // console.log(field)
  // console.log(options)
  // console.log(getEnumValues(field.type.Values))
  // console.log(field.form.root.type)
  const _fieldName = field.path.at(0)?.toString()
  const _enum = options ?? getEnumValues(field.type.Values)
  const [inputValue, setInputValue] = React.useState<string | null>(null)
  const [info, setInfo] = React.useState<React.ReactNode | null>(null)
  const mainLayoutClasses = classNames(
    // Grid Styles
    [styles.useGrid, 'gap-1', 'grid-cols-1 sm:grid-cols-2 md:grid-cols-3', 'auto-cols-fr', 'place-items-center', 'place-content-evenly'],
  )
  // console.log(`${_fieldName} field rawInput`, field.rawInput)
  // console.log(`${_fieldName} field initialOutput`, field.form._options.initialOutput?.[`${_fieldName}`])
  // const _initialInput = field.form._options.initialOutput?.[`${_fieldName}`]
  React.useEffect(() => {
    if (inputValue) {
      setInfo(metas?.find((meta) => meta.fieldValue === inputValue)?.info?.())
      callback?.(inputValue)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inputValue])
  return (
    <div {...getForm(field).bindField(field)} style={{ marginBottom: '1rem' }}>
      <div className={classNames(styles.options, mainLayoutClasses)}>
        {_enum.map((value, index: number) => {
          // console.log(`${_fieldName} value`, value)
          const _id = `${_fieldName}_${value.toLowerCase()}`
          return (
            <div key={index} className={classNames(styles.field, styles.isFlex, styles.isFullwidth, styles.isFullheight)} style={{ margin: '0' }}>
              <div className={classNames(styles.control, styles.isFlex, styles.hasTextCentered)}>
                <input
                  type='radio'
                  id={_id}
                  value={`${value}`}
                  name={`${_fieldName}`}
                  checked={Boolean(value === field.rawInput)}
                  onChange={() => {
                    setInputValue(value)
                    // Promise.resolve()
                    //   .then(() => setInfo(metas?.find((meta) => meta.fieldValue === value)?.info?.()))
                    //   .then(() => callback?.(value))
                  }}
                />
                <label
                  className={classNames(
                    [styles.isFullwidth, 'break-all'],
                    [styles.isFlex, styles.isAlignItemsCenter, styles.isJustifiedCenter, styles.isMarginless],
                  )}
                  style={{ userSelect: 'none' }}
                  htmlFor={_id}
                >
                  <div>{metas?.find((meta) => meta.fieldValue === value)?.label?.() ?? value}</div>
                </label>
              </div>
            </div>
          )
        })}
      </div>
      {info && (
        <div className={classNames(styles.help, styles.isSuccess, styles.isFullwidth)}>
          <motion.div initial={{ x: '-50%', opacity: 0 }} animate={{ x: 0, opacity: 1 }} transition={{ duration: 0.3, ease: 'easeInOut' }}>
            {info}
          </motion.div>
        </div>
      )}
      {!field._extraErrorMessages.length && field.errorMessages.map((e, i) => <ErrorDisplay key={i} message={e} />)}
    </div>
  )
})

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function getEnumValues<T extends Record<string, any>>(obj: T) {
  return Object.values(obj) as [(typeof obj)[keyof T]]
}

export { ErrorDisplay, OptionsEnumFlex, OptionsEnumGrid, TextArea, TextInput, TextInputRef, getEnumValues }
