/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */
// https://ui.docs.amplify.aws/react/connected-components/authenticator/advanced

import { isServer } from '@flexiness/domain-utils'
import { extendZodWithMobxZodForm } from '@monoid-dev/mobx-zod-form'
import fr from 'date-fns/locale/fr'
import React from 'react'
import { registerLocale, setDefaultLocale } from 'react-datepicker'
import { z } from 'zod'
// Necessary step to setup zod with mobx-zod-form power!
// eslint-disable-next-line @typescript-eslint/no-unsafe-call
extendZodWithMobxZodForm(z)

const {
  // CustomCredentialsProvider
  initAmplifyConfigClient,
  AMPLIFY_CLIENT,
} = await import('@flexiness/aws')

const { AmplifyUIReact } = AMPLIFY_CLIENT

await initAmplifyConfigClient('API-userPool')

// Create an instance of your custom provider
// const customCredentialsProvider = new CustomCredentialsProvider()
// Amplify.configure(AMPLIFY_AUTH_CONFIG_V2, {
//   Auth: {
//     // Supply the custom credentials provider to Amplify
//     credentialsProvider: customCredentialsProvider,
//   },
// })

const { Authenticator, View } = AmplifyUIReact
registerLocale('fr', fr)
setDefaultLocale('fr')

// import AuthComponent from './AuthComponent'
const { default: AuthComponent } = await import('./AuthComponent')

import { StoreProvider, getStores } from '@flexiness/domain-store'

import OnBoarding from './OnBoarding.jsx'

import { AppProps } from 'flexiness'

// import '@flexiness/domain-tailwind/globals.css'
// import 'react-datepicker/dist/react-datepicker.css'
// import './styles/poker-app.css'

const AppSSR: React.FC<AppProps> = () => {
  return (
    // <Authenticator.Provider>
    //   <StoreProvider value={{ stores: getStores() }}>
    //     <View>
    //       {!isServer && <AuthComponent />}
    //       <OnBoarding />
    //     </View>
    //   </StoreProvider>
    // </Authenticator.Provider>
    <OnBoarding />
  )
}

export default AppSSR
