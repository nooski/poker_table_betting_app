// https://stackoverflow.com/questions/28143487/preferred-way-to-represent-time-of-day-in-json-javascript
// https://bobbyhadz.com/blog/javascript-add-leading-zeros-to-number

function padWithZero(num: number) {
  const str = String(num)

  if (str.length > 1) {
    return str
  }

  return `0${str}`
}

class Time {
  private date: Date

  constructor(...args: [number] | [number, number]) {
    if (args.length === 1) {
      this.date = new Date(args[0])
    } else {
      const hours = args[0]
      const minutes = args[1]
      this.date = new Date(Date.UTC(1970, 0, 1, hours, minutes))
    }
  }

  getHours() {
    return this.date.getUTCHours()
  }

  getMinutes() {
    return this.date.getUTCMinutes()
  }

  getTime() {
    return this.date.getTime()
  }

  toString() {
    return `${padWithZero(this.getHours())}:${padWithZero(this.getMinutes())}`
  }
}

export { Time }
