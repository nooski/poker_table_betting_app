export default (html: string) => `
  <!DOCTYPE html>
    <html>
    <head>
      <meta charset="utf-8" />
      <title>Pointing Poker</title>
      <link rel="icon" href="favicon.ico" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="theme-color" content="#000000" />
      <meta
        name="description"
        content="A live pointing poker application in JS and React"
      />
      <link rel="apple-touch-icon" href="logo192.png" />
      <link rel="manifest" href="manifest.json" />
      <script nonce='<?=nonce?>'>globalThis.__webpack_nonce__='<?=nonce?>'</script>
      <% for (key in htmlWebpackPlugin.files.js) { %>
        <script type="text/javascript" defer="defer" nonce='<?=nonce?>' src="<%= htmlWebpackPlugin.files.js[key] %>"></script>
      <% } %>
    </head>
    <body>
      <noscript>You need to enable JavaScript to run this app.</noscript>
      <div id="root">${html}</div>
    </body>
  </html>
`
