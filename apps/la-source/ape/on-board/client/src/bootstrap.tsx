import React from 'react'
import { createRoot } from 'react-dom/client'
const container: HTMLElement = document.getElementById('root')!
const root = createRoot(container)
import App from './App'
import * as serviceWorker from './serviceWorker.js'

root.render(
  <React.StrictMode>
    <App isStandalone />
  </React.StrictMode>,
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
