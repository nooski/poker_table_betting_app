/* eslint-disable camelcase */
/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */

import { empty } from '@monoid-dev/mobx-zod-form'
import { useForm as useFormMobx } from '@monoid-dev/mobx-zod-form-react'
import { runInAction } from 'mobx'
import { observer } from 'mobx-react-lite'
import React, { useMemo } from 'react'
import { ErrorBoundary } from 'react-error-boundary'
import { z } from 'zod'

import { Form0, Form1, Form2, Form3 } from './form'

const { defaultDesc, generateTableName } = await import(
  /* webpackFetchPriority: "high" */
  'on-board-event-common'
)

const { EAttendeeRole, EActivity, EDayOfWeek, EEventType, ELifeCycle, EWhichWay } = await import(
  /* webpackFetchPriority: "high" */
  'on-board-event-api'
)

// console.log(EActivity)

import { OnBoardEventApiTypes, PokerTablesModalCreateProps, AtttendeesInitialOutput } from 'flexiness'

// import { getEnumValues } from '../components/FormFieldsMobx'
import { getEnumValues } from '@flexiness/domain-utils'

import { getOnBoardEventsStore, getUIStore } from '@flexiness/domain-store'
const CreateOnBoardEvent = getOnBoardEventsStore()
const UIStore = getUIStore()
const { setCurrentMobxFormByStep, setCurrentMobxFormByKey } = CreateOnBoardEvent
const { setModalCreateOnBoardEventOpen } = UIStore

import classNames from 'classnames'

// import {
//   Modal,
//   Title,
//   TitleLevel,
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import { Modal, Title, TitleLevel } from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

// const { ClientSyncStyledDefault } = await import('flex_design_system_react_ts_styled_default')
// const {
//   Modal,
//   Title,
//   TitleLevel,
//   styles,
// } = ClientSyncStyledDefault

const enumErrorMapChooseOption = (issue: z.ZodIssueOptionalMessage, _ctx: z.ErrorMapCtx) => {
  switch (issue.code) {
    case 'invalid_type':
      return { message: "Veuillez séléctionner l'une des options" }
    case 'invalid_enum_value':
      return { message: "Veuillez séléctionner l'une des options" }
    case 'invalid_return_type':
      return { message: "Veuillez séléctionner l'une des options" }
    default:
      return { message: "Veuillez séléctionner l'une des options" }
  }
}

const enumErrorMapChooseDay = (issue: z.ZodIssueOptionalMessage, _ctx: z.ErrorMapCtx) => {
  switch (issue.code) {
    case 'invalid_type':
      return { message: 'Veuillez sélectionner au moins un des jours' }
    case 'invalid_enum_value':
      return { message: 'Veuillez sélectionner au moins un des jours' }
    case 'invalid_return_type':
      return { message: 'Veuillez sélectionner au moins un des jours' }
    default:
      return { message: 'Veuillez sélectionner au moins un des jours' }
  }
}

const EnumEventTypeConst = z.enum(getEnumValues(EEventType), {
  // errorMap: () => ({
  //   message: "Veuillez séléctionner l'une des options",
  // }),
  errorMap: (issue, _ctx) => enumErrorMapChooseOption(issue, _ctx),
})

const EnumActivityConst = z.enum(getEnumValues(EActivity), {
  errorMap: (issue, _ctx) => enumErrorMapChooseOption(issue, _ctx),
})

const EnumWhichWayConst = z.enum(getEnumValues(EWhichWay), {
  errorMap: (issue, _ctx) => enumErrorMapChooseOption(issue, _ctx),
})

const EnumLifeCycleConst = z.enum(getEnumValues(ELifeCycle), {
  errorMap: (issue, _ctx) => enumErrorMapChooseOption(issue, _ctx),
})

const EnumDayOfWeekConst = z.enum(getEnumValues(EDayOfWeek), {
  errorMap: (issue, _ctx) => enumErrorMapChooseDay(issue, _ctx),
})

// function getEnumValues<T extends Record<string, any>>(obj: T) {
//   console.log(obj)
//   return Object.values(obj) as [(typeof obj)[keyof T]]
// }

const generateAtttendeesInitialOutput = () => {
  const { createOnBoardEvent, currentAttendeeIndex } = CreateOnBoardEvent
  const _initialOutput: AtttendeesInitialOutput[] = []
  // const _initialOutput: OnBoardEventApiTypes.TAttendee[] = []
  // eslint-disable-next-line prettier/prettier
  for (let index = 0; index < (currentAttendeeIndex + 1); index++) {
    _initialOutput.push({
      __typename: 'TAttendee',
      displayName: empty,
      // firstName: createOnBoardEvent?.eventType === EEventType.PROPOSAL ? getName('firstName') : empty,
      firstName: createOnBoardEvent?.eventType === EEventType.PROPOSAL ? getName('firstName') : `Inoe ${index + 1}`,
      lastName: getName('lastName'),
      startParticipation: empty,
      endParticipation: empty,
      role: getRole(),
    })
  }
  // console.log('_initialOutput', _initialOutput)
  return _initialOutput
}

const getWhichWay = () => {
  const { createOnBoardEvent } = CreateOnBoardEvent
  switch (createOnBoardEvent.activity) {
    case EActivity.PEDIBUS:
      return {
        schema: z.enum([EWhichWay.FROM_SCHOOL, EWhichWay.TO_SCHOOL], {
          errorMap: (issue, _ctx) => enumErrorMapChooseOption(issue, _ctx),
        }),
        default: EWhichWay.FROM_SCHOOL,
      }
    case EActivity.COVOITURAGE:
      return {
        schema: z.enum([EWhichWay.FROM_SCHOOL, EWhichWay.TO_SCHOOL], {
          errorMap: (issue, _ctx) => enumErrorMapChooseOption(issue, _ctx),
        }),
        default: empty,
      }
    case EActivity.BABYSITTING:
      return {
        schema: z.enum([EWhichWay.AT_HOME, EWhichWay.FROM_SCHOOL], {
          errorMap: (issue, _ctx) => enumErrorMapChooseOption(issue, _ctx),
        }),
        default: EWhichWay.AT_HOME,
      }
    case EActivity.SOUTIEN_DEVOIRS:
      return {
        schema: z.enum([EWhichWay.AT_SCHOOL, EWhichWay.AT_HOME], {
          errorMap: (issue, _ctx) => enumErrorMapChooseOption(issue, _ctx),
        }),
        default: empty,
      }
    case EActivity.AUTRE:
      return {
        schema: EnumWhichWayConst,
        default: empty,
      }
    default:
      return {
        schema: EnumWhichWayConst,
        default: empty,
      }
  }
}

const getDays = () => {
  const { createOnBoardEvent } = CreateOnBoardEvent
  switch (createOnBoardEvent.whichWay) {
    case EWhichWay.AT_SCHOOL:
    case EWhichWay.FROM_SCHOOL:
    case EWhichWay.TO_SCHOOL:
      // console.log('registering only school days')
      return EnumDayOfWeekConst.exclude([EDayOfWeek.SATURDAY, EDayOfWeek.SUNDAY])
    default:
      // console.log('registering all days of the week')
      return EnumDayOfWeekConst
  }
}
// console.log(getDays().enum)

const getRole = () => {
  const { currentStep, createOnBoardEvent } = CreateOnBoardEvent
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/no-unsafe-return
  if (currentStep < 2) return empty
  switch (createOnBoardEvent.eventType) {
    case EEventType.PROPOSAL:
      return EAttendeeRole.RESPONSABLE
    case EEventType.REQUEST:
      return EAttendeeRole.PARTICIPANT
    default:
      return EAttendeeRole.PARTICIPANT
  }
}

const getName = (key: keyof OnBoardEventApiTypes.User) => {
  const { createUser } = CreateOnBoardEvent
  // console.log(createUser?.[key] ?? undefined)
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/no-unsafe-return
  return createUser?.[key] ?? empty
}

const PokerTablesModalCreate: React.FC<PokerTablesModalCreateProps> = observer((props) => {
  const { currentStep, createOnBoardEvent, currentAttendeeIndex, currentMobxFormStep } = CreateOnBoardEvent
  const { modalCreateOnBoardEventOpen } = UIStore
  // const [tableName, tableDesc] = useMemo(() => {
  //   const tableName = createOnBoardEvent?.name ?? generateTableName()
  //   const tableDesc = createOnBoardEvent?.description ?? defaultDesc()
  //   return [
  //     tableName,
  //     tableDesc
  //   ]
  // }, [props])

  const ModalMarkUp = () => {
    const form0 = runInAction(() => {
      return useFormMobx(
        z.object({
          name: z.string().min(1, { message: 'champ requis' }).max(50, { message: 'champ limité à un maxiume de 50 caractères' }),
          description: z.string().min(1, { message: 'champ requis' }).max(300, { message: 'champ limité à un maxiume de 300 caractères' }),
          eventType: EnumEventTypeConst,
        }),
        {
          initialOutput: {
            name: createOnBoardEvent?.name ?? generateTableName(),
            description: createOnBoardEvent?.description ?? defaultDesc(),
            eventType: createOnBoardEvent?.eventType ?? empty,
          },
        },
      )
    })

    const form1 = runInAction(() => {
      return useFormMobx(
        z.object({
          activity: EnumActivityConst,
          whichWay: getWhichWay().schema as typeof EnumWhichWayConst,
        }),
        {
          initialOutput: {
            activity: createOnBoardEvent?.activity ?? empty,
            whichWay: createOnBoardEvent?.whichWay ?? (getWhichWay().default as OnBoardEventApiTypes.EWhichWay),
          },
        },
      )
    })

    // console.log('form1 activity', form1?.schema?.shape?.activity?.options)
    // console.log('form1 whichWay', form1?.schema?.shape?.whichWay?.options)

    const form2 = runInAction(() => {
      return useFormMobx(
        z.object({
          lifeCycle: EnumLifeCycleConst,
        }),
        {
          initialOutput: {
            lifeCycle: createOnBoardEvent?.lifeCycle ?? empty,
          },
        },
      )
    })

    const form3a = runInAction(() => {
      // console.log('updating form3a')
      return useFormMobx(
        z
          .object({
            day: getDays(),
            attendees: z
              .object({
                displayName: z.string().optional(),
                firstName: z
                  .string()
                  .min(1, { message: 'le champ prénom est requis' })
                  .max(30, { message: 'le champ prénom est limité à un maxiume de 30 caractères' }),
                lastName: z
                  .string()
                  .min(1, { message: 'le champ nom est requis' })
                  .max(30, { message: 'le champ nom est limité à un maxiume de 30 caractères' }),
                startParticipation: z.string().optional(),
                endParticipation: z.string().optional(),
              })
              .array()
              .refine(
                // https://www.answeroverflow.com/m/1066438470932897932
                (entries) => {
                  const firstName = new Set(entries.map((e) => e.firstName))
                  return firstName.size === entries.length
                },
                {
                  message: 'Les prénoms doivent être unique',
                },
              ),
          })
          .array()
          .refine(
            (entries) => {
              const day = new Set(entries.map((e) => e.day))
              return day.size === entries.length
            },
            {
              message: 'Les jours doivent être unique',
            },
          ),
        {
          initialOutput: [
            {
              day: empty,
              attendees: [...generateAtttendeesInitialOutput()],
            },
          ],
        },
      )
    })

    const form3b = runInAction(() => {
      return useFormMobx(
        z
          .object({
            day: getDays(),
            attendees: z
              .object({
                displayName: z.string().optional(),
                firstName: z
                  .string()
                  .min(1, { message: 'le champ prénom est requis' })
                  .max(30, { message: 'le champ prénom est limité à un maxiume de 30 caractères' }),
                lastName: z
                  .string()
                  .min(1, { message: 'le champ nom est requis' })
                  .max(30, { message: 'le champ nom est limité à un maxiume de 30 caractères' }),
                startParticipation: z.string().optional(),
                endParticipation: z.string().optional(),
                role: z.string().optional(),
              })
              .array(),
          })
          .array()
          .refine(
            (entries) => {
              const day = new Set(entries.map((e) => e.day))
              return day.size === entries.length
            },
            {
              message: 'Les jours doivent être unique',
            },
          ),
        {
          initialOutput: [
            {
              day: empty,
              attendees: [
                {
                  // displayName: empty,
                  // firstName: 'Inoe 1',
                  // lastName: getName('lastName'),
                  // startParticipation: empty,
                  // endParticipation: empty,
                  displayName: empty,
                  firstName: 'Inoe 1',
                  lastName: getName('lastName'),
                  startParticipation: '2024-02-22T08:15:00+01:00',
                  endParticipation: '2024-02-22T08:30:00+01:00',
                  // role: EAttendeeRole.PARTICIPANT,
                  role: getRole(),
                },
              ],
            },
          ],
        },
      )
    })

    const form3c = runInAction(() => {
      return useFormMobx(
        z
          .object({
            day: getDays(),
            attendees: z
              .object({
                displayName: z.string().optional(),
                firstName: z
                  .string()
                  .min(1, { message: 'le champ prénom est requis' })
                  .max(30, { message: 'le champ prénom est limité à un maxiume de 30 caractères' }),
                lastName: z
                  .string()
                  .min(1, { message: 'le champ nom est requis' })
                  .max(30, { message: 'le champ nom est limité à un maxiume de 30 caractères' }),
                startParticipation: z.string().optional(),
                endParticipation: z.string().optional(),
                role: z.string().optional(),
              })
              .array()
              .refine(
                (entries) => {
                  const firstName = new Set(entries.map((e) => e.firstName))
                  return firstName.size === entries.length
                },
                {
                  message: 'Les prénoms doivent être unique',
                },
              ),
          })
          .array()
          .refine(
            (entries) => {
              const day = new Set(entries.map((e) => e.day))
              return day.size === entries.length
            },
            {
              message: 'Les jours doivent être unique',
            },
          ),
        {
          initialOutput: [
            {
              day: empty,
              attendees: [
                {
                  // displayName: empty,
                  // firstName: 'Inoe 1',
                  // lastName: getName('lastName'),
                  // startParticipation: empty,
                  // endParticipation: empty,
                  displayName: empty,
                  firstName: 'Inoe 1',
                  lastName: getName('lastName'),
                  startParticipation: '2024-02-22T08:15:00+01:00',
                  endParticipation: '2024-02-22T08:30:00+01:00',
                  // role: EAttendeeRole.PARTICIPANT,
                  role: getRole(),
                },
                {
                  // displayName: empty,
                  // firstName: 'Inoe 2',
                  // lastName: getName('lastName'),
                  // startParticipation: empty,
                  // endParticipation: empty,
                  displayName: empty,
                  firstName: 'Inoe 2',
                  lastName: getName('lastName'),
                  startParticipation: '2024-02-22T08:15:00+01:00',
                  endParticipation: '2024-02-22T08:30:00+01:00',
                  // role: EAttendeeRole.PARTICIPANT,
                  role: getRole(),
                },
              ],
            },
          ],
        },
      )
    })

    const form3d = runInAction(() => {
      return useFormMobx(
        z
          .object({
            day: getDays(),
            attendees: z
              .object({
                displayName: z.string().optional(),
                firstName: z
                  .string()
                  .min(1, { message: 'le champ prénom est requis' })
                  .max(30, { message: 'le champ prénom est limité à un maxiume de 30 caractères' }),
                lastName: z
                  .string()
                  .min(1, { message: 'le champ nom est requis' })
                  .max(30, { message: 'le champ nom est limité à un maxiume de 30 caractères' }),
                startParticipation: z.string().optional(),
                endParticipation: z.string().optional(),
                role: z.string().optional(),
              })
              .array()
              .refine(
                (entries) => {
                  const firstName = new Set(entries.map((e) => e.firstName))
                  return firstName.size === entries.length
                },
                {
                  message: 'Les prénoms doivent être unique',
                },
              ),
          })
          .array()
          .refine(
            (entries) => {
              const day = new Set(entries.map((e) => e.day))
              return day.size === entries.length
            },
            {
              message: 'Les jours doivent être unique',
            },
          ),
        {
          initialOutput: [
            {
              day: empty,
              attendees: [
                {
                  displayName: empty,
                  firstName: 'Inoe 1',
                  lastName: getName('lastName'),
                  startParticipation: empty,
                  endParticipation: empty,
                  // role: EAttendeeRole.PARTICIPANT,
                  role: getRole(),
                  // displayName: empty,
                  // firstName: 'Inoe 1',
                  // lastName: getName('lastName'),
                  // startParticipation: '2024-02-22T08:15:00+01:00',
                  // endParticipation: '2024-02-22T08:30:00+01:00',
                },
                {
                  displayName: empty,
                  firstName: 'Inoe 2',
                  lastName: getName('lastName'),
                  startParticipation: empty,
                  endParticipation: empty,
                  // role: EAttendeeRole.PARTICIPANT,
                  role: getRole(),
                  // displayName: empty,
                  // firstName: 'Inoe 2',
                  // lastName: getName('lastName'),
                  // startParticipation: '2024-02-22T08:15:00+01:00',
                  // endParticipation: '2024-02-22T08:30:00+01:00',
                },
                {
                  displayName: empty,
                  firstName: 'Inoe 3',
                  lastName: getName('lastName'),
                  startParticipation: empty,
                  endParticipation: empty,
                  // role: EAttendeeRole.PARTICIPANT,
                  role: getRole(),
                },
              ],
            },
          ],
        },
      )
    })

    const form3e = runInAction(() => {
      return useFormMobx(
        z
          .object({
            day: getDays(),
            attendees: z
              .object({
                displayName: z.string().optional(),
                firstName: z
                  .string()
                  .min(1, { message: 'le champ prénom est requis' })
                  .max(30, { message: 'le champ prénom est limité à un maxiume de 30 caractères' }),
                lastName: z
                  .string()
                  .min(1, { message: 'le champ nom est requis' })
                  .max(30, { message: 'le champ nom est limité à un maxiume de 30 caractères' }),
                startParticipation: z.string().optional(),
                endParticipation: z.string().optional(),
                role: z.string().optional(),
              })
              .array(),
          })
          .array()
          .refine(
            (entries) => {
              const day = new Set(entries.map((e) => e.day))
              return day.size === entries.length
            },
            {
              message: 'Les jours doivent être unique',
            },
          ),
        {
          initialOutput: [
            {
              day: empty,
              attendees: [
                {
                  // displayName: empty,
                  // firstName: getName('firstName'),
                  // lastName: getName('lastName'),
                  // startParticipation: empty,
                  // endParticipation: empty,
                  displayName: empty,
                  firstName: getName('firstName'),
                  lastName: getName('lastName'),
                  startParticipation: '2024-02-22T08:15:00+01:00',
                  endParticipation: '2024-02-22T08:30:00+01:00',
                  // role: EAttendeeRole.RESPONSABLE,
                  role: getRole(),
                },
              ],
            },
          ],
        },
      )
    })

    React.useEffect(() => {
      // console.log('registering updated form steps')

      setCurrentMobxFormByKey({ form: form0, formTypeKey: 'MobxFormObject', key: 'form0' })

      setCurrentMobxFormByKey({ form: form1, formTypeKey: 'MobxFormObject', key: 'form1' })

      setCurrentMobxFormByKey({ form: form2, formTypeKey: 'MobxFormObject', key: 'form2' })

      setCurrentMobxFormByKey({ form: form3a, formTypeKey: 'MobxFormArray', key: 'form3a' })
      setCurrentMobxFormByKey({ form: form3b, formTypeKey: 'MobxFormArray', key: 'form3b' })
      setCurrentMobxFormByKey({ form: form3c, formTypeKey: 'MobxFormArray', key: 'form3c' })
      setCurrentMobxFormByKey({ form: form3d, formTypeKey: 'MobxFormArray', key: 'form3d' })
      setCurrentMobxFormByKey({ form: form3e, formTypeKey: 'MobxFormArray', key: 'form3e' })
    }, [currentStep, currentAttendeeIndex, currentMobxFormStep, props])

    React.useEffect(() => {
      // console.log('registering initial form steps')

      setCurrentMobxFormByStep({ form: form0, formTypeKey: 'MobxFormObject', currentStep: 0 })
      setCurrentMobxFormByKey({ form: form0, formTypeKey: 'MobxFormObject', key: 'form0' })

      setCurrentMobxFormByStep({ form: form1, formTypeKey: 'MobxFormObject', currentStep: 1 })
      setCurrentMobxFormByKey({ form: form1, formTypeKey: 'MobxFormObject', key: 'form1' })

      setCurrentMobxFormByStep({ form: form2, formTypeKey: 'MobxFormObject', currentStep: 2 })
      setCurrentMobxFormByKey({ form: form2, formTypeKey: 'MobxFormObject', key: 'form2' })

      // setCurrentMobxFormByStep({ form: form3a, formTypeKey: 'MobxFormArray', currentStep: 3 })
      setCurrentMobxFormByStep({ form: form3b, formTypeKey: 'MobxFormArray', currentStep: 3 })
      setCurrentMobxFormByKey({ form: form3a, formTypeKey: 'MobxFormArray', key: 'form3a' })
      setCurrentMobxFormByKey({ form: form3b, formTypeKey: 'MobxFormArray', key: 'form3b' })
      setCurrentMobxFormByKey({ form: form3c, formTypeKey: 'MobxFormArray', key: 'form3c' })
      setCurrentMobxFormByKey({ form: form3d, formTypeKey: 'MobxFormArray', key: 'form3d' })
      setCurrentMobxFormByKey({ form: form3e, formTypeKey: 'MobxFormArray', key: 'form3e' })
    }, [])

    return (
      <div
        className={classNames(styles.isFlex, styles.isFlexDirectionColumn, styles.isAlignContentCenter, styles.isFullwidth)}
        style={{ minHeight: '4rem', padding: '2rem 0', verticalAlign: 'center' }}
      >
        <Title level={TitleLevel.LEVEL3} className={classNames(styles.isInline)} style={{ lineHeight: 'inherit' }}>
          Créer un nouveau événement
        </Title>
        <div className={classNames('mb-2', styles.isInline)} style={{ padding: '0 2rem', overflowX: 'hidden' }}>
          {currentStep === 0 && <Form0 />}
          {currentStep === 1 && <Form1 />}
          {currentStep === 2 && <Form2 />}
          {currentStep === 3 && <Form3 createTable={props.createTable} updateTables={props.updateTables} />}
        </div>
      </div>
    )
  }

  // const tableName = useMemo(() => createOnBoardEvent.name ?? generateTableName(), [props])
  // const children = useMemo(() => <ModalMarkUp tableName={tableName} />, [tableName]);
  // const tableName = createOnBoardEvent.name ?? generateTableName()

  return (
    // <ErrorBoundary fallback={<div>Something went wrong</div>}>
    <Modal
      active={modalCreateOnBoardEventOpen}
      title='title modal'
      content='Modal content description 1'
      ctaContent='Action'
      onClose={() => setModalCreateOnBoardEventOpen(false)}
      // active={props?.active}
      // title='title modal'
      // content='Modal content description 1'
      // ctaContent='Action'
      // onClose={() => props?.setModalShow?.(false)}
    >
      <ModalMarkUp />
      {/* {children} */}
    </Modal>
    // </ErrorBoundary>
  )
})

export { PokerTablesModalCreate }
