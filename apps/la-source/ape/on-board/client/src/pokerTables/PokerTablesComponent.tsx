/* eslint-disable no-console */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { lazy, Suspense } from 'react'
import { observer } from 'mobx-react-lite'
import { PokerClickEvent, PokerTablesComponentProps, Table } from 'flexiness'
import { ErrorBoundary } from 'react-error-boundary'
// import { PokerTablesModalCreate } from './PokerTablesModalCreate'
// https://dev.to/iamandrewluca/react-lazy-without-default-export-4b65
const PokerTablesModalCreate = lazy(() => import('./PokerTablesModalCreate').then((module) => ({ default: module.PokerTablesModalCreate })))
// const { PokerTablesModalCreate } = await import('./PokerTablesModalCreate')

import { getUIStore } from '@flexiness/domain-store'
const UIStore = getUIStore()
const { setModalCreateOnBoardEventOpen, setActiveTableId } = UIStore

import classNames from 'classnames'

// import {
//   Button,
//   Title,
//   TitleLevel,
//   VariantState,
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import { Button, Title, TitleLevel, VariantState } from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

// const { ClientSyncStyledDefault } = await import('flex_design_system_react_ts_styled_default')
// const {
//   Button,
//   Title,
//   TitleLevel,
//   VariantState,
//   styles,
// } = ClientSyncStyledDefault

const PokerTablesComponent: React.FC<PokerTablesComponentProps> = observer((props) => {
  // const [modalShow, setModalShow] = React.useState<boolean>(false)

  function Loading() {
    return <h2>Loading...</h2>
  }

  const activateTable = (e: PokerClickEvent) => {
    // console.log('activateTable id', e.id)
    // console.log('activateTable tableId', e.tableId)
    setActiveTableId(e.id)
    props.activateTable(e.id, e.tableId)
  }

  const deleteTable = (id: Table['id'], tableId: Table['tableId']) => {
    console.log(id)
    props.deleteTable(id || '', tableId)
  }

  // const updateTables = () => {
  //   props.updateTables()
  // }

  const { allTables } = props
  return (
    <>
      <div
        className={classNames(
          // [styles.isFlex, styles.isAlignContentCenter, styles.isJustifiedCenter, styles.isFullwidth],
          ['grid', 'gap-1', 'grid-cols-1 sm:grid-cols-3 md:grid-cols-6', 'auto-cols-fr', 'place-items-center', 'place-content-evenly'],
        )}
        style={{ minHeight: '4rem', padding: '2rem 0', verticalAlign: 'center' }}
      >
        <div className={classNames(['grid grid-cols-subgrid col-span-1 sm:col-span-2 md:col-span-4'])}>
          <Title
            level={TitleLevel.LEVEL3}
            className={classNames([styles.isMarginless], ['grid col-span-full text-center sm:text-left'])}
            style={{ lineHeight: 'inherit' }}
          >
            <span className='px-2'>Rejoindre un événement ou en créer un nouveau</span>
          </Title>
        </div>
        <div className={classNames(['grid grid-cols-subgrid col-span-1 sm:col-span-1 md:col-span-2'])}>
          <div className={classNames(['grid col-span-full'])} style={{ padding: '1rem', minWidth: '200px', border: '1ps solid green' }}>
            <Button
              variant={VariantState.PRIMARY}
              onClick={() => {
                setModalCreateOnBoardEventOpen(true)
                props?.updateTables?.()
                // setModalShow(true)
              }}
            >
              Créer un événement
            </Button>
          </div>
        </div>
      </div>

      <div className='grid gap-4 grid-cols-2 sm:grid-cols-2 lg:grid-cols-3'>
        {allTables?.map?.((table) => (
          <div
            className={classNames(
              'card',
              table.active && 'border-info',
              // table.active && 'bg-warning'
            )}
            style={{ width: '100%' }}
            key={table.tableId}
            onClick={() => activateTable({ id: table.id, tableId: table.tableId })}
          >
            <div className='card-body'>
              <Title level={TitleLevel.LEVEL7} className={classNames('card-title')} style={{ lineHeight: 'inherit', marginBottom: '1rem' }}>
                {table.name}
              </Title>
              <p className={classNames('card-text', table.active && 'text-info')}>{table.description}</p>
              <br />
              <Button
                variant={table.active ? VariantState.SECONDARY : VariantState.TERTIARY}
                className={classNames(styles.isSmall)}
                onClick={() => deleteTable(table.id, table.tableId)}
              >
                Delete this table
              </Button>
            </div>
          </div>
        ))}
      </div>
      <ErrorBoundary fallback={<div>Something went wrong</div>}>
        <Suspense fallback={<Loading />}>
          <PokerTablesModalCreate
            // active={modalShow} setModalShow={setModalShow}
            createTable={props.createTable}
            updateTables={props.updateTables}
          />
        </Suspense>
        {/* <PokerTablesModalCreate
          // active={modalShow} setModalShow={setModalShow}
          createTable={props.createTable}
        /> */}
      </ErrorBoundary>
    </>
  )
})

export { PokerTablesComponent }
