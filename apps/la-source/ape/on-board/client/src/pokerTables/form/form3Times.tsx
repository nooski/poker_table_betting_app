/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */

import React from 'react'

import { z } from 'zod'
import { ErrorDisplay } from '../../components/FormFieldsMobx'

import { zodResolver } from '@hookform/resolvers/zod'
import { addDays, format, formatISO, parseISO } from 'date-fns'
import { fr } from 'date-fns/locale'
import { FieldErrors, FieldValues, SubmitErrorHandler, SubmitHandler, UseFormRegisterReturn, useForm as useFormReactHook } from 'react-hook-form'
import { fromZodError } from 'zod-validation-error'
import { HandleSubmitRHF, TimeInput } from '../../components/DatePicker_RHF'

import { OnBoardEventApiTypes } from 'flexiness'

import { getOnBoardEventsStore } from '@flexiness/domain-store'
const CreateOnBoardEvent = getOnBoardEventsStore()
const { updateAllAttendees, setInvalidateStep } = CreateOnBoardEvent

import classNames from 'classnames'

type TimeFormType = {
  endParticipation: Date
  startParticipation: Date
}

type Props = {
  callback?: (startParticipation: string, endParticipation: string) => void
  errorCallback?: (isValidState: boolean) => void
  showAsList?: boolean
  keyIndex: number
}

const customDateErrorMap: z.ZodErrorMap = (issue, ctx) => {
  if (issue.code === z.ZodIssueCode.invalid_date) {
    return { message: 'Veuillez préciser les heures de participation de début et de fin' }
  }
  return { message: ctx.defaultError }
}

// This will be the schema for the form, it'll be used to validate the form when submitting.
// z.coerce.date() is used to convert the string to a date object.
const schemaTimes: z.ZodType<TimeFormType> = z
  .object({
    startParticipation: z.coerce
      .date({ errorMap: customDateErrorMap })
      .refine((data) => data > new Date(), { message: "L'heure de début doit être dans le futur." }),
    endParticipation: z.coerce.date({ errorMap: customDateErrorMap }),
  })
  .required()
  .refine((data) => data.endParticipation > data.startParticipation, {
    message: "L'heure de fin ne peut pas être antérieure à l'heure de début.",
    path: ['endParticipation'],
  })

const Form3Times = React.forwardRef<HandleSubmitRHF, Props>(({ callback, errorCallback, showAsList, keyIndex }, ref) => {
  const { createTimes, currentStep, currentDay, createAttendees, currentAttendeeIndex, currentKeyStep } = CreateOnBoardEvent
  const [errorsDate, setErrorsDate] = React.useState<FieldErrors<FieldValues> | null>(null)

  // const getDefaultValues = () => {
  //   if (!createTimes.size) return {
  //     startParticipation: createAttendees?.attendees?.[keyIndex]?.startParticipation || '',
  //     endParticipation: createAttendees?.attendees?.[keyIndex]?.endParticipation || '',
  //   }
  //   return {
  //     endParticipation: createTimes.get(currentDay as EDayOfWeek)?.attendees?.[keyIndex].endParticipation || '',
  //     startParticipation: createTimes.get(currentDay as EDayOfWeek)?.attendees?.[keyIndex].startParticipation || '',
  //   }
  // }

  const {
    register,
    setValue,
    handleSubmit,
    formState: { errors },
  } = useFormReactHook({
    resolver: zodResolver(schemaTimes, { errorMap: customDateErrorMap }),
    defaultValues: {
      startParticipation: createAttendees?.attendees?.[keyIndex]?.startParticipation || '',
      endParticipation: createAttendees?.attendees?.[keyIndex]?.endParticipation || '',
    },
    // defaultValues: getDefaultValues()
  })

  type StartDate = Partial<UseFormRegisterReturn<'startParticipation'>>
  const restStartDate: StartDate = register('startParticipation', {
    valueAsDate: true,
    required: 'Veuillez choisir une date de début',
  })
  delete restStartDate['ref']

  type EndDate = Partial<UseFormRegisterReturn<'endParticipation'>>
  const restEndDate: EndDate = register('endParticipation', {
    valueAsDate: true,
    required: 'Veuillez choisir une date de fin',
  })
  delete restEndDate['ref']

  const onValidate = (data: { endParticipation?: string | null; startParticipation?: string | null }) => {
    try {
      schemaTimes.parse(data)
    } catch (err) {
      if (err instanceof z.ZodError) {
        const validationError = fromZodError(err)
        // console.log(validationError.details)
        return validationError
      }
    }
  }

  const onSubmit: SubmitHandler<FieldValues> = (data) => {
    // console.log('onSubmit', data)

    // console.log('onSubmit valid data.endParticipation', isValid(data.endParticipation))

    // const _tempDate = parseISO(format(data.endParticipation as Date, 'HH:mm', { locale: fr }))
    // console.log('onSubmit valid _tempDate', isValid(_tempDate))
    // const _tempDate2 = parseISO(formatISO(data.endParticipation as Date, { representation: 'time' }))
    // console.log('onSubmit valid _tempDate2', isValid(_tempDate2))

    setInvalidateStep(false)

    // console.log('Form3Times keyIndex', keyIndex)

    const newAttendeeData = [...(createAttendees?.attendees ?? [])]
    newAttendeeData[keyIndex] = {
      ...newAttendeeData?.[keyIndex],
      startParticipation: formatISO(data.startParticipation as Date),
      endParticipation: formatISO(data.endParticipation as Date),
    } as OnBoardEventApiTypes.TAttendee

    updateAllAttendees(newAttendeeData)

    callback?.(formatISO(data.startParticipation as Date), formatISO(data.endParticipation as Date))

    errorCallback?.(true)
  }

  const onError: SubmitErrorHandler<FieldValues> = (data) => {
    // console.log('onError', data)
    setInvalidateStep(true)
    setErrorsDate(data)
    errorCallback?.(false)
  }

  React.useImperativeHandle(ref, () => ({
    handleSubmitRHF() {
      handleSubmit(onSubmit, onError)()
        .then(() => {
          console.log('mobx validating step 3 - form3Times')
        })
        .catch((e) => {
          throw e
        })
      return true
    },
  }))

  return (
    <>
      <TimeInput
        schema={schemaTimes}
        {...restStartDate}
        label='Heure de début'
        name='startParticipation'
        /* eslint-disable prettier/prettier */
        {...((createAttendees?.attendees?.[keyIndex ]?.startParticipation && !errorsDate) && {
          value: format(parseISO(createAttendees.attendees[keyIndex ].startParticipation as string), 'HH:mm', { locale: fr }),
          // value: createAttendees.attendees[currentAttendeeIndex].startParticipation as string,
          // value: formatISO(
          //   parseISO(createAttendees.attendees[currentAttendeeIndex].startParticipation as string),
          //   {
          //     format: 'extended',
          //     representation: 'time'
          //   }
          // )
        })}
        /* eslint-enable prettier/prettier */
        callback={(date: Date) => {
          setErrorsDate(null)

          const _dateValue = addDays(date, 1)
          const _dateValueStr = formatISO(_dateValue)
          // const _dateValueStr2 = _dateValue.toISOString()
          // console.log(_dateValueStr)
          // console.log(_dateValueStr2)

          // console.log(isValid(_dateValue))
          const data = {
            startParticipation: _dateValueStr,
            endParticipation: createAttendees?.attendees?.[currentAttendeeIndex]?.endParticipation,
          }
          const validationData = schemaTimes.safeParse(data)
          console.log(validationData)
          onValidate(data)

          setValue('startParticipation', _dateValueStr)
          void handleSubmit(onSubmit, onError)()
        }}
        marginless={true}
        key={`${currentKeyStep.get(currentStep)}_startParticipation_${keyIndex}`}
      />
      <TimeInput
        schema={schemaTimes}
        {...restEndDate}
        label='Heure de fin'
        name='endParticipation'
        /* eslint-disable prettier/prettier */
        {...((createAttendees?.attendees?.[keyIndex ]?.endParticipation && !errorsDate?.endParticipation) && {
          value: format(parseISO(createAttendees.attendees[keyIndex ].endParticipation as string), 'HH:mm', { locale: fr }),
          // value: createAttendees.attendees[currentAttendeeIndex].endParticipation as string,
          // value: formatISO(
          //   parseISO(createAttendees.attendees[currentAttendeeIndex].endParticipation as string),
          //   {
          //     format: 'extended',
          //     representation: 'time'
          //   }
          // )
        })}
        /* eslint-enable prettier/prettier */
        callback={(date: Date) => {
          setErrorsDate(null)

          const _dateValue = addDays(date, 1)
          const _dateValueStr = formatISO(_dateValue)
          // const _dateValueStr2 = _dateValue.toISOString()
          // console.log(_dateValueStr)
          // console.log(_dateValueStr2)

          // console.log(isValid(_dateValue))
          // const data = {
          //   startParticipation: createAttendees?.attendees?.[currentAttendeeIndex]?.startParticipation,
          //   endParticipation: _dateValueStr,
          // }
          // const validationData = schemaTimes.safeParse(data)
          // console.log(validationData)
          // onValidate(data)

          setValue('endParticipation', _dateValueStr)
          void handleSubmit(onSubmit, onError)()
        }}
        marginless={true}
        key={`${currentKeyStep.get(currentStep)}_endParticipation_${keyIndex}`}
      />
      {errors.startParticipation?.message && (
        <div className={classNames(['grid grid-cols-subgrid col-span-1 sm:col-span-2'])}>
          <div className={classNames(['grid col-span-full text-center'])}>
            <ErrorDisplay message={errors.startParticipation?.message} />
          </div>
        </div>
      )}
      {errors.endParticipation?.message && (
        <div className={classNames(['grid grid-cols-subgrid col-span-1 sm:col-span-2'])}>
          <div className={classNames(['grid col-span-full text-center'])}>
            <ErrorDisplay message={errors.endParticipation?.message} />
          </div>
        </div>
      )}
    </>
  )
})
Form3Times.displayName = 'Form3Times'

export { Form3Times }
