import React from 'react'

import classNames from 'classnames'

// import {
//   Box, Icon, IconName, IconSize, IconPosition, List, ListItem, Title, Text
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import { Box, Icon, IconName, IconSize, IconPosition, List, ListItem, Title, Text } from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

// const { ClientSyncStyledDefault } = await import('flex_design_system_react_ts_styled_default')
// const {
//   Box, Icon, IconName, IconSize, IconPosition, List, ListItem, Title, Text, styles
// } = ClientSyncStyledDefault

const metas = {
  eventType: [
    {
      fieldValue: 'PROPOSAL',
      label: () => <p>{"Je propose de l'aide"}</p>,
      info: () => (
        <Box className={classNames(styles.hasTextLeft, styles.hasTextInfo)}>
          <List>
            <ListItem>
              <Title level={7}>Je propose mes services à des créneaux prcécis</Title>
              <Text className={classNames(styles.hasTextRight, styles.hasTextWeightMedium)}>
                {"... pour accompagner d'enfants à la sortie d'école par exemple"}
              </Text>
            </ListItem>
            <br />
            <ListItem>
              <Title level={7}>{"D'autres parents pourront se joindre à cet événement"}</Title>
              <Text className={classNames(styles.hasTextRight, styles.hasTextWeightMedium)}>
                {'... pour inscrire et faire participer leurs enfants'}
              </Text>
            </ListItem>
            <br />
            <ListItem>
              <Title level={7}>{"Un cas d'exemple :"}</Title>
              <List>
                <ListItem>
                  <Icon
                    content={"Je suis un élève qui s'engage d'être responsable pour gagner un peu d'argent de poche."}
                    size={IconSize.SMALL}
                    position={IconPosition.LEFT}
                    name={IconName.SUN_EURO}
                  />
                </ListItem>
                <ListItem>
                  <Icon
                    content={"Je suis assez autonome pour rentrer chez moi tout seul... pourquoi pas déposer d'autres enfants en route."}
                    size={IconSize.SMALL}
                    position={IconPosition.LEFT}
                    name={IconName.DISTANCE}
                  />
                </ListItem>
              </List>
            </ListItem>
          </List>
        </Box>
      ),
    },
    {
      fieldValue: 'REQUEST',
      label: () => <p>{"Je cherche de l'aide"}</p>,
      info: () => (
        <Box className={classNames(styles.hasTextLeft, styles.hasTextInfo)}>
          <List>
            <ListItem>
              <Title level={7}>{"Je sollicite de l'aide pour des créneaux prcécis"}</Title>
              <Text className={classNames(styles.hasTextRight, styles.hasTextWeightMedium)}>
                {"...pour assurer la garde de mes enfants à la maison après l'école par exemple"}
              </Text>
            </ListItem>
            <br />
            <ListItem>
              <Title level={7}>{"D'autres élèves ou parents pourront se joindre à cet événement"}</Title>
              <Text className={classNames(styles.hasTextRight, styles.hasTextWeightMedium)}>
                {"...en s'engageant comme responsable pour satisfaire un ou plusieurs crénaux"}
              </Text>
            </ListItem>
            <br />
            <ListItem>
              <Title level={7}>{"Un cas d'exemple :"}</Title>
              <List>
                <ListItem>
                  <Icon
                    content={
                      "Je suis un parent qui cherche à cumuler avec d'autres parents, la garde de leurs enfants sur la semaine de manière récurrente"
                    }
                    size={IconSize.SMALL}
                    position={IconPosition.LEFT}
                    name={IconName.CHILDREN_ACCESS}
                  />
                </ListItem>
                <ListItem>
                  <Icon
                    content={
                      'En se joignant en groupe on peut se porter responsable à tour de rôle et couvrir plus facilement les besoins de la semaine'
                    }
                    size={IconSize.SMALL}
                    position={IconPosition.LEFT}
                    name={IconName.HANDS_HELPING}
                  />
                </ListItem>
              </List>
            </ListItem>
          </List>
        </Box>
      ),
    },
  ],
  activity: [
    {
      fieldValue: 'PEDIBUS',
      label: () => <p>Pedibus</p>,
    },
    {
      fieldValue: 'BABYSITTING',
      label: () => <p>Babysitting</p>,
    },
    {
      fieldValue: 'COVOITURAGE',
      label: () => <p>Covoiturage</p>,
    },
    {
      fieldValue: 'SOUTIEN_DEVOIRS',
      label: () => <p>Soutien devoirs</p>,
    },
    {
      fieldValue: 'AUTRE',
      label: () => <p>Autre</p>,
    },
  ],
  whichWay: [
    {
      fieldValue: 'AT_HOME',
      label: () => <p>À la maison</p>,
    },
    {
      fieldValue: 'AT_SCHOOL',
      label: () => <p>{"À l'école"}</p>,
    },
    {
      fieldValue: 'FROM_SCHOOL',
      label: () => (
        <>
          <p>En partant de</p>
          <p>{"l'école"}</p>
        </>
      ),
    },
    {
      fieldValue: 'TO_SCHOOL',
      label: () => (
        <>
          <p>En allant</p>
          <p>{"vers l'école"}</p>
        </>
      ),
    },
  ],
  lifeCycle: [
    {
      fieldValue: 'ALL_YEAR',
      label: () => (
        <span className={classNames(styles.hasTextCentered, styles.isFullwidth)}>
          <p>De manière récurrente</p>
          <p>{"pendant toute l'année scolaire"}</p>
        </span>
      ),
    },
    {
      fieldValue: 'DATE_RANGE',
      label: () => (
        <span className={classNames(styles.hasTextCentered, styles.isFullwidth)}>
          <p>Pour une période spécifique</p>
          <p>entre deux dates</p>
        </span>
      ),
    },
  ],
  day: [
    {
      fieldValue: 'MONDAY',
      label: () => <p>Lundi</p>,
    },
    {
      fieldValue: 'TUESDAY',
      label: () => <p>Mardi</p>,
    },
    {
      fieldValue: 'WEDNESDAY',
      label: () => <p>Mercredi</p>,
    },
    {
      fieldValue: 'THURSDAY',
      label: () => <p>Jeudi</p>,
    },
    {
      fieldValue: 'FRIDAY',
      label: () => <p>Vendredi</p>,
    },
    {
      fieldValue: 'SATURDAY',
      label: () => <p>Samedi</p>,
    },
    {
      fieldValue: 'SUNDAY',
      label: () => <p>Dimanche</p>,
    },
  ],
}

export { metas }
