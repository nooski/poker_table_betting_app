/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */

import { observer } from 'mobx-react-lite'
import React, { MouseEvent } from 'react'

import { MobxFormObject, OnBoardEventApiTypes } from 'flexiness'

const { EEventType } = await import(
  /* webpackFetchPriority: "high" */
  'on-board-event-api'
)

import { OptionsEnumFlex, TextArea, TextInput } from '../../components/FormFieldsMobx'
import { metas } from './'

import { getOnBoardEventsStore } from '@flexiness/domain-store'
const CreateOnBoardEvent = getOnBoardEventsStore()
const { setCreateOnBoardEvent, setInvalidateStep, setValidatedCurrentStep } = CreateOnBoardEvent

// import classNames from 'classnames'

// import {
//   Button,
//   VariantState,
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import { Button, VariantState } from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

// const { ClientSyncStyledDefault } = await import('flex_design_system_react_ts_styled_default')
// const {
//   Button,
//   VariantState,
//   styles,
// } = ClientSyncStyledDefault

// const Form0: React.FC<FormProps> = observer(({ form }) => {
// const Form0 = observer(({ form }: { form: MobxFormObject }) => {
const Form0 = observer(() => {
  const { createOnBoardEvent, currentStep, currentMobxFormStep } = CreateOnBoardEvent
  const form = currentMobxFormStep.get(currentStep) as MobxFormObject
  if (!form) return <p>...loading</p>
  const { fields } = form.root

  // React.useEffect(() => {
  //   console.log('changing')
  //   const { createOnBoardEvent } = CreateOnBoardEvent
  //   form.root.fields.name.setOutput(createOnBoardEvent.name ?? form.root.fields.name.rawInput)
  //   form.root.fields.description.setOutput(createOnBoardEvent.description ?? form.root.fields.description.rawInput)
  //   form.root.fields.eventType.setOutput(createOnBoardEvent.eventType ?? form.root.fields.description.rawInput)
  // }, [])

  return (
    <form {...form.bindForm()}>
      <fieldset>
        <TextInput field={fields.name} placeholder={'Titre'} />
        <TextArea field={fields.description} placeholder={'Description'} />
        <OptionsEnumFlex
          field={fields.eventType}
          // options={form.schema.shape.eventType.options as string[]}
          callback={(inputValue: string) => {
            setCreateOnBoardEvent({
              ...createOnBoardEvent,
              ...{ name: form.root.fields.name.rawInput as string },
              ...{ description: form.root.fields.description.rawInput as string },
              ...{ eventType: inputValue as unknown as OnBoardEventApiTypes.EEventType },
            })
          }}
          metas={metas.eventType}
        />
      </fieldset>
      <Button
        variant={VariantState.PRIMARY}
        type={'submit'}
        onClick={() => {
          void form.handleSubmit(() => {
            console.info(form.parsed)
            if (form.parsed.success && currentStep === 0) {
              setInvalidateStep(false)
              setValidatedCurrentStep(1) // increment by 1 to go to next step
              const { data } = form.parsed
              setCreateOnBoardEvent({
                ...createOnBoardEvent,
                ...(data as OnBoardEventApiTypes.OnBoardEvent),
              })
            }
          })
        }}
      >
        Suite
      </Button>
    </form>
  )
})

export { Form0 }
