/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */

import React from 'react'
import { z } from 'zod'

import { zodResolver } from '@hookform/resolvers/zod'
import {
  // RefCallBack,
  // UseFormRegister,
  // EventType,
  // DeepMap,
  // FieldError,
  FieldErrors,
  FieldValues,
  SubmitErrorHandler,
  // useFormState,
  // Resolver,
  SubmitHandler,
  UseFormRegisterReturn,
  // Control,
  // Controller,
  useForm as useFormReactHook,
} from 'react-hook-form'

import { fromZodError } from 'zod-validation-error'

import { endOfDay, format, formatISO, parseISO, startOfDay } from 'date-fns'

import { DateInput, HandleSubmitRHF } from '../../components/DatePicker_RHF'
import { ErrorDisplay } from '../../components/FormFieldsMobx'

import { getOnBoardEventsStore } from '@flexiness/domain-store'
const CreateOnBoardEvent = getOnBoardEventsStore()
const { setCreateOnBoardEvent, setInvalidateStep } = CreateOnBoardEvent

import classNames from 'classnames'

// // https://stackoverflow.com/questions/75913987/validating-dates-with-zod-react-start-date-end-date
// // https://stackoverflow.com/questions/74790564/validate-field-in-discriminated-union-based-on-other-field-in-zod
type DateFormType = {
  startDate: Date
  endDate: Date
}

type Props = {
  errorCallback?: (isValidState: boolean) => void
}

const customDateErrorMap: z.ZodErrorMap = (issue, ctx) => {
  if (issue.code === z.ZodIssueCode.invalid_date) {
    return { message: 'Veuillez séléctionner deux dates' }
  }
  return { message: ctx.defaultError }
}

// This will be the schema for the form, it'll be used to validate the form when submitting.
// z.coerce.date() is used to convert the string to a date object.
const schemaDates: z.ZodType<DateFormType> = z
  .object({
    startDate: z.coerce
      .date({ errorMap: customDateErrorMap })
      .refine((data) => data > new Date(), { message: 'La date de début doit être dans le futur.' }),
    endDate: z.coerce.date({ errorMap: customDateErrorMap }),
  })
  .required()
  .refine((data) => data.endDate > data.startDate, {
    message: 'La date de fin ne peut pas être antérieure à la date de début.',
    path: ['endDate'],
  })

// const Form2Dates = React.forwardRef(({ errorCallback }: Props, ref: ForwardedRef<HandleSubmitRHF>) => {
const Form2Dates = React.forwardRef<HandleSubmitRHF, Props>(({ errorCallback }, ref) => {
  const { createOnBoardEvent } = CreateOnBoardEvent
  const [errorsDate, setErrorsDate] = React.useState<FieldErrors<FieldValues> | null>(null)

  const {
    register,
    setValue,
    // getValues,
    handleSubmit,
    formState: { errors },
  } = useFormReactHook({
    resolver: zodResolver(schemaDates, { errorMap: customDateErrorMap }),
    defaultValues: {
      startDate: createOnBoardEvent?.startDate || '',
      endDate: createOnBoardEvent?.endDate || '',
    },
  })

  type StartDate = Partial<UseFormRegisterReturn<'startDate'>>
  const restStartDate: StartDate = register('startDate', {
    valueAsDate: true,
    required: 'Veuillez choisir une date de début',
  })
  delete restStartDate['ref']

  type EndDate = Partial<UseFormRegisterReturn<'endDate'>>
  const restEndDate: EndDate = register('endDate', {
    valueAsDate: true,
    required: 'Veuillez choisir une date de fin',
  })
  delete restEndDate['ref']

  const onValidate = (data: { startDate?: string | null; endDate?: string | null }) => {
    try {
      schemaDates.parse(data)
    } catch (err) {
      if (err instanceof z.ZodError) {
        const validationError = fromZodError(err)
        // the error is now readable by the user
        // you may print it to console
        // console.log(validationError.message)
        // console.log(validationError)
        // or return it as an actual error
        return validationError
      }
    }
  }

  const onSubmit: SubmitHandler<FieldValues> = (data) => {
    // console.log('onSubmit', data)
    setInvalidateStep(false)
    setCreateOnBoardEvent({
      ...createOnBoardEvent,
      ...{ startDate: formatISO(startOfDay(data.startDate as Date)) },
      ...{ endDate: formatISO(endOfDay(data.endDate as Date)) },
    })
    errorCallback?.(true)
  }

  const onError: SubmitErrorHandler<FieldValues> = (data) => {
    // console.log('onError', data)
    setInvalidateStep(true)
    setErrorsDate(data)
    errorCallback?.(false)
  }

  React.useImperativeHandle(ref, () => ({
    handleSubmitRHF() {
      handleSubmit(onSubmit, onError)()
        .then(() => {
          console.log('mobx validating step 2 - form2Dates')
        })
        .catch((e) => {
          throw e
        })
      return true
    },
  }))

  return (
    <fieldset>
      <div
        className={classNames(
          ['grid-default'],
          ['grid', 'gap-1', 'grid-cols-1 sm:grid-cols-2', 'auto-cols-fr', 'place-items-center', 'place-content-evenly'],
        )}
        style={{ minHeight: '4rem', padding: '0 0.5rem 1rem', verticalAlign: 'center' }}
      >
        <DateInput
          schema={schemaDates}
          // {...register('startDate', { valueAsDate: true })}
          {...restStartDate}
          label='Date de début'
          name='startDate'
          /* eslint-disable prettier/prettier */
          {...((createOnBoardEvent.startDate && !errorsDate) && {
            value: format(parseISO(createOnBoardEvent.startDate), 'dd/MM/yyyy'),
          })}
          /* eslint-enable prettier/prettier */

          // ref={React.useRef}
          // ref={(e) => {
          //   ref(e)
          //   startDate.current = e // you can still assign to ref
          // }}
          callback={(date: Date) => {
            // console.log('Is a date obj ? :', date instanceof Date)
            setErrorsDate(null)

            const _dateValue = startOfDay(date)
            const _dateValueStr = formatISO(_dateValue)

            // console.log(isValid(_dateValue))
            // const data = {
            //   startDate: _dateValueStr,
            //   endDate: createOnBoardEvent.endDate,
            // }
            // const validationData = schemaDates.safeParse(data)
            // console.log(validationData)
            // onValidate(data)

            setValue('startDate', _dateValueStr)
            void handleSubmit(onSubmit, onError)()
          }}
        />
        <DateInput
          schema={schemaDates}
          // {...register('endDate', { valueAsDate: true })}
          {...restEndDate}
          label='Date de fin'
          name='endDate'
          /* eslint-disable prettier/prettier */
          {...((createOnBoardEvent.endDate && !errorsDate?.endDate) && {
            value: format(parseISO(createOnBoardEvent.endDate), 'dd/MM/yyyy'),
          })}
          /* eslint-enable prettier/prettier */

          callback={(date: Date) => {
            // console.log('Is a date obj ? :', date instanceof Date)
            setErrorsDate(null)

            const _dateValue = endOfDay(date)
            const _dateValueStr = formatISO(_dateValue)

            // console.log(isValid(_dateValue))
            // const data = {
            //   startDate: createOnBoardEvent.startDate,
            //   endDate: _dateValueStr,
            // }
            // const validationData = schemaDates.safeParse(data)
            // console.log(validationData)
            // onValidate(data)

            setValue('endDate', _dateValueStr)
            void handleSubmit(onSubmit, onError)()
          }}
        />
      </div>
      {errors.startDate?.message && <ErrorDisplay message={errors.startDate?.message} />}
      {errors.endDate?.message && <ErrorDisplay message={errors.endDate?.message} />}
    </fieldset>
  )
})
Form2Dates.displayName = 'Form2Dates'

export { Form2Dates, customDateErrorMap, schemaDates, type DateFormType }
