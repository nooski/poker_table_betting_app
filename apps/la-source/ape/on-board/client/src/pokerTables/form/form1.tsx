/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */

import { observer } from 'mobx-react-lite'
import React from 'react'

import { OptionsEnumGrid } from '../../components/FormFieldsMobx'
import { metas } from './'

const { EActivity, EWhichWay } = await import(
  /* webpackFetchPriority: "high" */
  'on-board-event-api'
)

import { MobxFormObject, OnBoardEventApiTypes } from 'flexiness'

import { getOnBoardEventsStore } from '@flexiness/domain-store'
const CreateOnBoardEvent = getOnBoardEventsStore()
const { setCreateOnBoardEvent, setCurrentMobxFormByStep, setInvalidateStep, setValidatedCurrentStep } = CreateOnBoardEvent

import classNames from 'classnames'

// import {
//   Button,
//   Columns,
//   ColumnsItem,
//   Title,
//   VariantState,
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import { Button, Columns, ColumnsItem, Title, VariantState } from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

// const { ClientSyncStyledDefault } = await import('flex_design_system_react_ts_styled_default')
// const {
//   Button,
//   Columns,
//   ColumnsItem,
//   Title,
//   VariantState,
//   styles,
// } = ClientSyncStyledDefault

// https://github.com/colinhacks/zod/issues/93
// const formValidator = <T extends z.ZodType<any,any>>(schema:T)=>(values:any)=>{
//   try{
//     schema.parse(values)
//     return {}
//   } catch(err){
//     return (err as z.ZodError).formErrors.fieldErrors;
//   }
// }

// const Form1: React.FC<FormProps> = observer(({ form }) => {
const Form1 = observer(() => {
  const { createOnBoardEvent, currentStep, currentMobxFormStep } = CreateOnBoardEvent
  const form = currentMobxFormStep.get(currentStep) as MobxFormObject
  if (!form) return <p>...loading</p>
  // console.log(form)
  const { fields } = form.root
  // console.log(fields)

  // const switchForm1 = () => {
  //   const { createOnBoardEvent, currentMobxFormStep, currentMobxFormKey } = CreateOnBoardEvent
  //   switch (true) {
  //     case createOnBoardEvent.activity === EActivity.PEDIBUS:
  //     case createOnBoardEvent.activity === EActivity.COVOITURAGE:
  //       console.log('form1b')
  //       void setCurrentMobxFormByStep({
  //         form: currentMobxFormKey.get('form1b'),
  //         formTypeKey: 'MobxFormObject',
  //         currentStep: 1,
  //       })
  //       break
  //     case createOnBoardEvent.activity === EActivity.BABYSITTING:
  //       console.log('form1c')
  //       void setCurrentMobxFormByStep({
  //         form: currentMobxFormKey.get('form1c'),
  //         formTypeKey: 'MobxFormObject',
  //         currentStep: 1,
  //       })
  //       break
  //     case createOnBoardEvent.activity === EActivity.SOUTIEN_DEVOIRS:
  //       console.log('form1d')
  //       void setCurrentMobxFormByStep({
  //         form: currentMobxFormKey.get('form1d'),
  //         formTypeKey: 'MobxFormObject',
  //         currentStep: 1,
  //       })
  //       break
  //     case createOnBoardEvent.activity === EActivity.AUTRE:
  //       console.log('form1a')
  //       void setCurrentMobxFormByStep({
  //         form: currentMobxFormKey.get('form1a'),
  //         formTypeKey: 'MobxFormObject',
  //         currentStep: 1,
  //       })
  //       break
  //     default:
  //       // console.log('form1a')
  //       void setCurrentMobxFormByStep({
  //         form: currentMobxFormKey.get('form1a'),
  //         formTypeKey: 'MobxFormObject',
  //         currentStep: 1,
  //       })
  //       break
  //   }
  //   // return (
  //   //   (currentMobxFormStep.get(currentStep) as MobxFormObject)?.root.fields.activity.setOutput(createOnBoardEvent.activity),
  //   //   (currentMobxFormStep.get(currentStep) as MobxFormObject)?.root.fields.whichWay.setOutput(createOnBoardEvent.whichWay)
  //   // )
  // }

  // const setForm = () => {
  //   const { createOnBoardEvent, currentMobxFormStep } = CreateOnBoardEvent
  //   return (
  //     // (currentMobxFormStep.get(currentStep) as MobxFormObject)?.root.fields.activity.setOutput(createOnBoardEvent.activity),
  //     // (currentMobxFormStep.get(currentStep) as MobxFormObject)?.root.fields.whichWay.setOutput(createOnBoardEvent.whichWay)
  //     form.root.fields.activity.setOutput(createOnBoardEvent.activity),
  //     form.root.fields.whichWay.setOutput(createOnBoardEvent.whichWay)
  //   )
  // }

  // React.useEffect(() => {
  //   const { createOnBoardEvent } = CreateOnBoardEvent
  //   // form.root.fields.activity.setOutput(createOnBoardEvent.activity),
  //   // form.root.fields.whichWay.setOutput(createOnBoardEvent.whichWay)
  // }, [])

  return (
    <form {...form.bindForm()}>
      <fieldset>
        <Title level={7}>{"Quel type d'actvité ?"}</Title>
        <br />
        <OptionsEnumGrid
          field={fields.activity}
          // options={form.schema.shape.activity.options as string[]}
          callback={(inputValue: string) => {
            setCreateOnBoardEvent({
              ...createOnBoardEvent,
              ...{ activity: inputValue as unknown as OnBoardEventApiTypes.EActivity },
            })
            // switchForm1()
            // setForm()
          }}
          metas={metas.activity}
        />
        <Title level={7}>Où a-t-il lieu ?</Title>
        <br />
        <OptionsEnumGrid
          field={fields.whichWay}
          // options={form.schema.shape.whichWay.options}
          callback={(inputValue: string) => {
            setCreateOnBoardEvent({
              ...createOnBoardEvent,
              ...{ whichWay: inputValue as unknown as OnBoardEventApiTypes.EWhichWay },
            })
          }}
          metas={metas.whichWay}
        />
      </fieldset>

      <Columns className={classNames(styles.isFullwidth, styles.isAlignItemsCenter, styles.isMarginless)}>
        <ColumnsItem>
          <Button variant={VariantState.SECONDARY} onClick={() => setValidatedCurrentStep(-1)}>
            Retour
          </Button>
        </ColumnsItem>
        <ColumnsItem>
          <Button
            variant={VariantState.PRIMARY}
            type={'submit'}
            onClick={() => {
              void form.handleSubmit(() => {
                console.info(form.parsed)
                if (form.parsed.success && currentStep === 1) {
                  setInvalidateStep(false)
                  setValidatedCurrentStep(1) // increment by 1 to go to next step
                  const { data } = form.parsed
                  setCreateOnBoardEvent({
                    ...createOnBoardEvent,
                    ...(data as OnBoardEventApiTypes.OnBoardEvent),
                  })
                }
              })
            }}
          >
            Suite
          </Button>
        </ColumnsItem>
      </Columns>
    </form>
  )
})

export { Form1 }
