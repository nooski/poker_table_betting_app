/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */

import { observer } from 'mobx-react-lite'
import React from 'react'

// import { z } from 'zod'
// import { errorMap, fromZodError, ValidationError, isValidationErrorLike } from 'zod-validation-error'

import { MobxFormObject, OnBoardEventApiTypes } from 'flexiness'

const { ELifeCycle } = await import(
  /* webpackFetchPriority: "high" */
  'on-board-event-api'
)

import { HandleSubmitRHF } from '../../components/DatePicker_RHF'
import { OptionsEnumFlex } from '../../components/FormFieldsMobx'
import { endOfSchoolYear, startOfSchoolYear } from '../../constants'
import { Form2Dates, metas } from './'

import { getOnBoardEventsStore } from '@flexiness/domain-store'
const CreateOnBoardEvent = getOnBoardEventsStore()
const { setCreateOnBoardEvent, setInvalidateStep, setValidatedCurrentStep } = CreateOnBoardEvent

import classNames from 'classnames'

// import {
//   Button,
//   Columns,
//   ColumnsItem,
//   Title,
//   VariantState,
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import { Button, Columns, ColumnsItem, Title, VariantState } from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

// const { ClientSyncStyledDefault } = await import('flex_design_system_react_ts_styled_default')
// const {
//   Button,
//   Columns,
//   ColumnsItem,
//   Title,
//   VariantState,
//   styles,
// } = ClientSyncStyledDefault

// const Form1: React.FC<FormProps> = observer(({ form }) => {
const Form2 = observer(() => {
  const { createOnBoardEvent, currentStep, currentMobxFormStep } = CreateOnBoardEvent
  const form = currentMobxFormStep.get(currentStep) as MobxFormObject
  if (!form) return <p>...loading</p>
  const { fields } = form.root
  const [isValidState, setValidState] = React.useState<boolean>(true)
  const form2DatesRef = React.useRef<HandleSubmitRHF>(null)

  return (
    <form {...form.bindForm()}>
      <fieldset>
        <Title level={7}>Comment cet événément se repête-il ?</Title>
        <br />
        <OptionsEnumFlex
          field={fields.lifeCycle}
          // options={form.schema.shape.lifeCycle.options as string[]}
          callback={(inputValue: string) => {
            setCreateOnBoardEvent({
              ...createOnBoardEvent,
              ...{ lifeCycle: inputValue as unknown as OnBoardEventApiTypes.ELifeCycle },
            })
          }}
          metas={metas.lifeCycle}
        />
      </fieldset>

      {createOnBoardEvent.lifeCycle === ELifeCycle.DATE_RANGE && (
        <Form2Dates
          ref={form2DatesRef}
          errorCallback={(isValidState) => {
            // console.log('errorCallback', isValidState)
            setValidState(isValidState)
          }}
        />
      )}

      <Columns className={classNames(styles.isFullwidth, styles.isAlignItemsCenter, styles.isMarginless)}>
        <ColumnsItem>
          <Button variant={VariantState.SECONDARY} onClick={() => setValidatedCurrentStep(-1)}>
            Retour
          </Button>
        </ColumnsItem>
        <ColumnsItem>
          <Button
            variant={VariantState.PRIMARY}
            type={'submit'}
            // disabled={Boolean(createOnBoardEvent.lifeCycle === ELifeCycle.DATE_RANGE && !isValidState)}
            onClick={() => {
              void form.handleSubmit(() => {
                console.info(form.parsed)
                if (form.parsed.success && currentStep === 2) {
                  const { data } = form.parsed
                  if (createOnBoardEvent.lifeCycle === ELifeCycle.DATE_RANGE && form2DatesRef.current) {
                    // https://stackoverflow.com/a/69292925/10159170
                    form2DatesRef.current.handleSubmitRHF()
                    setValidatedCurrentStep(1) // increment by 1 to go to next step
                    setCreateOnBoardEvent({
                      ...createOnBoardEvent,
                      ...(data as OnBoardEventApiTypes.OnBoardEvent),
                    })
                  } else {
                    setInvalidateStep(false)
                    setValidatedCurrentStep(1) // increment by 1 to go to next step
                    setCreateOnBoardEvent({
                      ...createOnBoardEvent,
                      ...(data as OnBoardEventApiTypes.OnBoardEvent),
                      ...{ startDate: startOfSchoolYear().string },
                      ...{ endDate: endOfSchoolYear().string },
                    })
                  }
                }
              })
            }}
          >
            Valider
          </Button>
        </ColumnsItem>
      </Columns>
    </form>
  )
})

export { Form2 }
