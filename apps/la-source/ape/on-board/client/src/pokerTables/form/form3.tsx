/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable unused-imports/no-unused-imports */

import { AnimatePresence, motion, useAnimation } from 'framer-motion'
import { runInAction, toJS } from 'mobx'
import { observer } from 'mobx-react-lite'
import React, { ReactNode } from 'react'
import { isMobile } from 'react-device-detect'

import { MobxZodObjectField } from '@monoid-dev/mobx-zod-form'
import { format, parseISO } from 'date-fns'
import { fr } from 'date-fns/locale'
import { z } from 'zod'
import { fromZodError } from 'zod-validation-error'

import { Form3Props, MobxFormArray, MobxFormFieldEnum, MobxFormFieldInput, OnBoardEventApiTypes } from 'flexiness'

const { EDayOfWeek, EEventType, EWhichWay } = await import(
  /* webpackFetchPriority: "high" */
  'on-board-event-api'
)

import { getEnumValues } from '@flexiness/domain-utils'
import { resetModalCreateOnBoardEvent } from 'on-board-event-common'
import { HandleSubmitRHF } from '../../components/DatePicker_RHF'
import { ErrorDisplay, OptionsEnumGrid, TextInputRef } from '../../components/FormFieldsMobx'
import { Form3Times, metas } from './'

import { Message } from '@aws-amplify/ui-react'

const { AMPLIFY_CLIENT } = await import('@flexiness/aws')

const { AmplifyUIReact } = AMPLIFY_CLIENT

const { useAuthenticator } = AmplifyUIReact

import { getOnBoardEventsStore, getUIStore } from '@flexiness/domain-store'
const CreateOnBoardEvent = getOnBoardEventsStore()
const UIStore = getUIStore()
const {
  setCreateOnBoardEvent,
  setCreateTime,
  unsetCreateTimes,
  deleteTime,
  setCurrentAttendeeIndex,
  deleteAttendee,
  updateAllAttendees,
  setCurrentMobxFormByStep,
  setInvalidateStep,
  setValidatedCurrentStep,
  setCurrentDay,
} = CreateOnBoardEvent
const { setTriggerAuthentication } = UIStore

import classNames from 'classnames'

// import {
//   Box,
//   Button,
//   Columns,
//   ColumnsItem,
//   Icon,
//   IconName,
//   IconPosition,
//   IconSize,
//   Link,
//   Sticker,
//   Tag,
//   TagList,
//   TagVariant,
//   Title,
//   VariantState,
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import {
  Box,
  Button,
  Columns,
  ColumnsItem,
  Icon,
  IconName,
  IconPosition,
  IconSize,
  Link,
  Sticker,
  Tag,
  TagList,
  TagVariant,
  Title,
  VariantState,
} from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

// const { ClientSyncStyledDefault } = await import('flex_design_system_react_ts_styled_default')
// const {
//   Box,
//   Button,
//   Columns,
//   ColumnsItem,
//   Icon,
//   IconName,
//   IconPosition,
//   IconSize,
//   Link,
//   Sticker,
//   Tag,
//   TagList,
//   TagVariant,
//   Title,
//   VariantState,
//   styles
// } = ClientSyncStyledDefault

type Mutable<Type> = {
  -readonly [Key in keyof Type]: Type[Key]
}

let form: MobxFormArray
let formKey: string

type MobxFormElements = Mutable<MobxZodObjectField<z.AnyZodObject>[]>

const showAsList = false

const getRandomTransformOrigin = () => {
  const value = (16 + 40 * Math.random()) / 100
  const value2 = (15 + 36 * Math.random()) / 100
  return {
    originX: value,
    originY: value2,
  }
}
const getRandomDelay = () => -(Math.random() * 0.7 + 0.05)
const randomDuration = () => Math.random() * 0.07 + 0.23
const variantShake = {
  start: (i: number) => ({
    x: i % 2 === 0 ? [-100, 150, 0] : [100, -150, 0],
    transition: {
      delay: getRandomDelay(),
      repeat: 5,
      duration: randomDuration(),
    },
  }),
  reset: {
    x: 0,
  },
}
const variantConfirm = {
  start: (i: number) => ({
    x: '-50%',
    opacity: 0,
    transition: {
      delay: 0.1,
      duration: 0.3,
      ease: 'easeInOut',
    },
  }),
  reset: {
    x: 0,
    opacity: 0,
  },
}

// const Form3: React.FC<FormProps> = observer(({ form }) => {
// const Form3 = observer(({ getDays }: { getDays?: () => EDayOfWeek }) => {
const Form3: React.FC<Form3Props> = observer((props) => {
  const { route } = useAuthenticator((context) => [context.route])
  const { createTimes, createAttendees, currentStep, currentDay, createOnBoardEvent } = CreateOnBoardEvent
  const { authenticationOnLoad, modalCreateOnBoardEventOpen, triggerAuthentication } = UIStore

  let mobxFormElements: MobxFormElements

  // const [currentDay, setCurrentDay] = React.useState<OnBoardEventApiTypes.TTime['day'] | null>(null)
  // const [currentAttendees, setCurrentAttendees] = React.useState<OnBoardEventApiTypes.TAttendee[] | null>(null)
  const [hasTempInputErrorState, toggleTempInputErrorState] = React.useState<boolean>(false)
  // const [isValidState, setValidState] = React.useState<boolean>(true)
  const [hasTempInputAttendeeErrorState, countTempInputAttendeeErrorState] = React.useState<number>(0)
  // const [daysOfWeek, setDaysOfWeek] = React.useState<'only school days' | 'all days of week'>('only school days')

  let stickerElements: React.ReactNode[] = []
  const form2TimesRef = React.useRef<HandleSubmitRHF>(null)

  const controlShakeDays = useAnimation()
  const controlShakeAttendee = useAnimation()

  const switchForm3 = (formIndex: number) => {
    const { createOnBoardEvent, currentStep, currentMobxFormStep, currentMobxFormKey } = CreateOnBoardEvent
    let _prevForm: MobxFormArray | null = null
    let fieldAttendeesRawPrev: OnBoardEventApiTypes.TTime['attendees'] = []
    let fieldAttendeesRawNew: OnBoardEventApiTypes.TTime['attendees'] = []
    // let fieldDayRawPrev: OnBoardEventApiTypes.TTime['day']
    // let fieldDayRawNew: OnBoardEventApiTypes.TTime['day']
    if (form) {
      _prevForm = form
      fieldAttendeesRawPrev = (_prevForm.rawInput as OnBoardEventApiTypes.TTime[])[0]?.attendees
      // fieldDayRawPrev = (form.rawInput as OnBoardEventApiTypes.TTime[])[0]?.day
      // console.log('switchForm3 | prev | form rawInput attendees', toJS(fieldAttendeesRawPrev))
    }

    switch (true) {
      case createOnBoardEvent.eventType === EEventType.PROPOSAL:
        formKey = 'form3e'
        // console.log(formKey)
        void setCurrentMobxFormByStep({
          form: currentMobxFormKey.get(formKey),
          formTypeKey: 'MobxFormObject',
          currentStep: 3,
        })
        break
      case formIndex === 0:
        formKey = 'form3b'
        // console.log(formKey)
        void setCurrentMobxFormByStep({
          form: currentMobxFormKey.get(formKey),
          formTypeKey: 'MobxFormObject',
          currentStep: 3,
        })
        break
      case formIndex === 1:
        formKey = 'form3c'
        // console.log(formKey)
        void setCurrentMobxFormByStep({
          form: currentMobxFormKey.get(formKey),
          formTypeKey: 'MobxFormObject',
          currentStep: 3,
        })
        break
      case formIndex === 2:
        formKey = 'form3d'
        // console.log(formKey)
        void setCurrentMobxFormByStep({
          form: currentMobxFormKey.get(formKey),
          formTypeKey: 'MobxFormObject',
          currentStep: 3,
        })
        break
      default:
        formKey = 'form3a'
        // console.log(formKey)
        void setCurrentMobxFormByStep({
          form: currentMobxFormKey.get(formKey),
          formTypeKey: 'MobxFormObject',
          currentStep: 3,
        })
        break
    }

    form = currentMobxFormStep.get(currentStep) as unknown as MobxFormArray
    // if (!form) return (<p>...loading</p>)
    // console.log(form)

    // fieldDayRawNew = (form.rawInput as OnBoardEventApiTypes.TTime[])[0]?.day
    // console.log(fieldDayRawPrev)
    // form._setRawInputAt([0, 'day'], fieldDayRawPrev)

    fieldAttendeesRawNew = (form.rawInput as OnBoardEventApiTypes.TTime[])[0]?.attendees
    // console.log('switchForm3 | new | form rawInput attendees', toJS(fieldAttendeesRawNew))

    // Deleting Attendee
    if (fieldAttendeesRawPrev.length > fieldAttendeesRawNew.length) {
      fieldAttendeesRawNew.map((attendee: OnBoardEventApiTypes.TAttendee, index: number) => {
        form._setRawInputAt([0, 'attendees', index], {
          ...attendee,
        })
      })
    }

    // Adding Attendee
    if (fieldAttendeesRawPrev.length < fieldAttendeesRawNew.length) {
      fieldAttendeesRawPrev.map((attendee: OnBoardEventApiTypes.TAttendee, index: number) => {
        form._setRawInputAt([0, 'attendees', index], {
          ...attendee,
        })
      })
    }

    updateAllAttendees(fieldAttendeesRawNew)
    // console.log('switchForm3 | createAttendees?.attendees', toJS(createAttendees?.attendees))
  }

  // const shouldAnimateAttendeeInput = () => {
  //   if (currentDay) return false
  //   if (form.root._extraErrorMessages.length) return false
  //   return true
  // }

  const _setCurrentAttendeeIndex = (newIndex: number) => {
    // console.log('_setCurrentAttendeeIndex',newIndex)
    setCurrentAttendeeIndex(newIndex)
    void switchForm3(newIndex)
  }

  const setExtraErrorMessagesDay = (messages: string[]) => {
    runInAction(() => {
      mobxFormElements?.[0].fields.day.setExtraErrorMessages(messages)
      form.root.setExtraErrorMessages(messages)
    })
  }

  const getDays = () => {
    const { createOnBoardEvent } = CreateOnBoardEvent
    const EDayOfWeekConst = z.enum(getEnumValues(EDayOfWeek))
    switch (createOnBoardEvent.whichWay) {
      case EWhichWay.AT_SCHOOL:
      case EWhichWay.FROM_SCHOOL:
      case EWhichWay.TO_SCHOOL:
        // console.log('registering only school days')
        // setDaysOfWeek('only school days')
        return getEnumValues(EDayOfWeekConst.exclude([EDayOfWeek.SATURDAY, EDayOfWeek.SUNDAY]).Values) as unknown as string[]
      default:
        // console.log('registering all days of week')
        // setDaysOfWeek('all days of week')
        return getEnumValues(EDayOfWeekConst.Values) as unknown as string[]
    }
  }

  const DaysInputEnum = ({ field, type, index }: { field: MobxFormFieldEnum; type: z.SomeZodObject; index: number }) => {
    return (
      <fieldset>
        <OptionsEnumGrid
          field={field}
          options={getDays()}
          callback={(inputValue: string) => {
            // console.log('CurrentDay | clicked', toJS(createAttendees?.attendees))

            void form.handleSubmit(() => {
              // console.info(form.parsed)
              if (form.parsed.success && currentStep === 3) {
                setCurrentDay(inputValue as unknown as OnBoardEventApiTypes.EDayOfWeek)
              } else {
                toggleTempInputErrorState(!hasTempInputErrorState)
                mobxFormElements?.[0].fields.day.setRawInput(null)
                mobxFormElements?.[0].fields.day.setTouched(false)
                // form.flushValidationTasks()
                setExtraErrorMessagesDay(['Veuillez corriger les erreurs au-dessus avant de continuer'])

                // console.log(mobxFormElements?.[0].fields.day.errorMessages)
                // console.log(mobxFormElements?.[0].fields.day.extraErrorMessages)
                // console.log(form.root.errorMessages)
                // console.log(form.root.extraErrorMessages)
              }
            })
          }}
          metas={metas.day}
        />
      </fieldset>
    )
  }

  React.useEffect(() => {
    const { CurrentAttendeeIndex, createTimes } = CreateOnBoardEvent
    switchForm3(CurrentAttendeeIndex)
    // if (createTimes.size) {
    //   mobxFormElements?.[0].fields.day.setRawInput(null)
    //   mobxFormElements?.[0].fields.day.setTouched(false)
    // }
  }, [])

  React.useEffect(() => {
    if (!form?.root?._extraErrorMessages?.length) return
    void controlShakeDays.start('start')
    setTimeout(() => {
      void controlShakeDays.stop()
      void controlShakeDays.set('reset')
    }, 350)
  }, [hasTempInputErrorState])

  React.useEffect(() => {
    if (hasTempInputAttendeeErrorState === 0) return
    try {
      const { CurrentAttendeeIndex } = CreateOnBoardEvent
      let _hasAttendeeError: boolean = false
      if (!form.parsed.success) {
        fromZodError(form.parsed.error as unknown as z.ZodError).details.map((error) => {
          // console.log(Object.values(error.path))
          if (Object.values(error.path).includes('attendees')) {
            _hasAttendeeError = true
          }
        })
        if (hasTempInputAttendeeErrorState > 0) form.validate() // show error issues
      }
      if (!_hasAttendeeError) {
        _setCurrentAttendeeIndex(CurrentAttendeeIndex + 1)
      } else {
        void controlShakeAttendee.start('start')
        setTimeout(() => {
          void controlShakeAttendee.stop()
          void controlShakeAttendee.set('reset')
        }, 350)
      }
    } catch (err) {
      console.error(err)
    }
  }, [hasTempInputAttendeeErrorState])

  React.useEffect(() => {
    const { CurrentAttendeeIndex, createTime, AllAttendees } = CreateOnBoardEvent
    if (!currentDay) return

    // console.log(form)

    // console.log('useEffect | currentDay | mobxFormElements', mobxFormElements)

    // console.log('debug mobxFormElements day', mobxFormElements?.[0].fields.day.rawInput)
    // console.log('debug mobxFormElements attendees', toJS(mobxFormElements?.[0].fields.attendees.rawInput))

    const fieldAttendeesRaw = mobxFormElements?.[0].fields.attendees.rawInput as OnBoardEventApiTypes.TAttendee[]
    // console.log('useEffect | currentDay | fieldAttendeesRaw', toJS(fieldAttendeesRaw))

    // updateCurrentAttendee(fieldAttendeesRaw[CurrentAttendeeIndex])

    // setCreateTime({
    //   ...createTime,
    //   ...{
    //     day: currentDay as unknown as OnBoardEventApiTypes.EDayOfWeek,
    //     attendees: [...fieldAttendeesRaw],
    //   },
    // })

    updateAllAttendees(fieldAttendeesRaw)

    setCreateTime({
      ...createTime,
      ...{
        day: currentDay as unknown as OnBoardEventApiTypes.EDayOfWeek,
        attendees: [...fieldAttendeesRaw],
      },
    })
  }, [currentDay])

  const attendeeStickerSwitch = (index: number) => {
    switch (index) {
      case 2:
        return {
          stickerColor: VariantState.SECONDARY,
          borderColor: styles.isFlatSecondary,
          textColor: styles.hasTextSecondary,
        }
      case 1:
        return {
          stickerColor: VariantState.TERTIARY,
          borderColor: styles.isFlatTertiary,
          textColor: styles.hasTextTertiary,
        }
      default:
        return {
          stickerColor: VariantState.FLEX_PINK,
          borderColor: styles.isFlatFlexPink,
          textColor: styles.hasTextFlexPink,
        }
    }
  }

  const initStickersMenu = (length: number) => {
    const { CurrentAttendeeIndex } = CreateOnBoardEvent
    // const isSmall = isMobile
    const isSmall = true
    stickerElements = []
    for (let index = 0; index < length; index++) {
      stickerElements.push(
        <div
          key={`${formKey}_${index}`}
          style={{
            position: 'relative',
            cursor: 'pointer',
            userSelect: 'none',
            /* eslint-disable prettier/prettier */
            ...(isSmall
              ? {
                zoom: CurrentAttendeeIndex === index ? '120%' : '100%',
                top: CurrentAttendeeIndex === index ? '0px' : '4px',
                padding: '0 1px',
              } : {
                zoom: CurrentAttendeeIndex === index ? '107.5%' : '100%',
                top: CurrentAttendeeIndex === index ? '0px' : '2.2px',
                padding: '0 1px',
                marginLeft: '2px',
              }),
            /* eslint-enable prettier/prettier */
          }}
          onClick={() => {
            setCurrentAttendeeIndex(index)
            // console.log('stickerZIndex2 changed on sticker menu', index)
          }}
        >
          <Sticker
            className={classNames(CurrentAttendeeIndex !== index && !isSmall && styles.isOutlined)}
            small={isSmall}
            variant={attendeeStickerSwitch(index).stickerColor}
          >
            {createOnBoardEvent.eventType === EEventType.PROPOSAL ? (
              <span>{`# Responsable`}</span>
            ) : (
              <span>
                <span className={classNames('inline sm:hidden')}>{`# ${index + 1}`}</span>
                <span className={classNames('hidden sm:inline')}>{`Participant # ${index + 1}`}</span>
              </span>
            )}
          </Sticker>
        </div>,
      )
    }
  }

  const getStickersMenuItems = () => {
    return stickerElements
  }

  const displayDeleteAttendeeItem = (index?: number, textColor: string | null = null) => {
    const { CurrentAttendeeIndex } = CreateOnBoardEvent
    return (
      <Link
        className={classNames(
          [textColor ? textColor : attendeeStickerSwitch(CurrentAttendeeIndex).textColor],
          [styles.isAlignSelfFlexEnd, styles.isSmall],
        )}
        onClick={() => {
          // console.log('displayDeleteAttendeeItem | createAttendees?.attendees before', toJS(createAttendees?.attendees))
          if (createAttendees?.attendees?.length) {
            if (showAsList) {
              deleteAttendee(index as number)
              _setCurrentAttendeeIndex((index as number) - 1)
            } else {
              deleteAttendee(CurrentAttendeeIndex)
              _setCurrentAttendeeIndex(CurrentAttendeeIndex - 1)
            }
          }
          // console.log('displayDeleteAttendeeItem | createAttendees?.attendees after', toJS(createAttendees?.attendees))
          // _setCurrentAttendeeIndex(CurrentAttendeeIndex - 1)
        }}
      >
        Supprimer
      </Link>
    )
  }

  const AttendeesInputFieldArray = observer(({ root, type, index }: { root: MobxFormArray['root']; type: z.SomeZodObject; index: number }) => {
    const { CurrentAttendeeIndex } = CreateOnBoardEvent

    if (root) {
      const ref = React.useRef(null)
      const { elements, form } = root

      // console.log('root', root)
      // console.log('CurrentAttendeeIndex', CurrentAttendeeIndex)
      // console.log('elements length', elements?.length)
      // console.log('elements', elements)

      const activeAttendee = elements?.[CurrentAttendeeIndex]

      if (!activeAttendee) return <p>...loading</p>
      // console.log('activeAttendee', activeAttendee)

      initStickersMenu(elements?.length)

      const { fields } = activeAttendee

      const renderAddParticipantBtn = () => {
        if (createOnBoardEvent.eventType === EEventType.PROPOSAL) return
        if (!showAsList) {
          // eslint-disable-next-line prettier/prettier
          if (CurrentAttendeeIndex < 2 && CurrentAttendeeIndex === (elements?.length - 1)) {
            return (
              <div className={classNames(['grid grid-cols-subgrid col-span-full col-start-1 sm:col-span-1 sm:col-start-2'])}>
                <motion.div
                  style={{ ...getRandomTransformOrigin() }}
                  custom={CurrentAttendeeIndex}
                  variants={variantShake}
                  animate={controlShakeAttendee}
                >
                  <Button
                    small
                    variant={attendeeStickerSwitch(CurrentAttendeeIndex).stickerColor}
                    onClick={() => {
                      countTempInputAttendeeErrorState(hasTempInputAttendeeErrorState + 1)
                      // _setCurrentAttendeeIndex(CurrentAttendeeIndex + 1)
                    }}
                  >
                    Ajouter un autre particpant
                  </Button>
                </motion.div>
              </div>
            )
          }
        }
        if (showAsList) {
          if (CurrentAttendeeIndex < 2) {
            return (
              <div className={classNames(['grid grid-cols-subgrid col-span-full col-start-1 sm:col-span-1 sm:col-start-2'])}>
                <motion.div
                  style={{ ...getRandomTransformOrigin() }}
                  custom={CurrentAttendeeIndex}
                  variants={variantShake}
                  animate={controlShakeAttendee}
                >
                  <Button
                    small
                    variant={VariantState.SECONDARY}
                    onClick={() => {
                      countTempInputAttendeeErrorState(hasTempInputAttendeeErrorState + 1)
                      // _setCurrentAttendeeIndex(CurrentAttendeeIndex + 1)
                    }}
                  >
                    Ajouter un autre particpant
                  </Button>
                </motion.div>
              </div>
            )
          }
        }
        return null
      }

      return (
        <fieldset>
          {!showAsList && (
            <div className={classNames(styles.isFullwidth, styles.isFlex, styles.isAlignContentSpaceBetween)}>
              <div
                className={classNames(
                  styles.isFullwidth,
                  styles.isFlex,
                  styles.isAlignContentEnd,
                  styles.isJustifyContentStart,
                  styles.isJustifiedStart,
                )}
              >
                {getStickersMenuItems()}
              </div>
              {/* eslint-disable-next-line prettier/prettier */}
              {((CurrentAttendeeIndex === (elements.length - 1)) && (CurrentAttendeeIndex > 0)) && displayDeleteAttendeeItem()}
            </div>
          )}
          <div
            className={classNames([
              'w-full',
              'grid',
              'gap-0',
              'grid-cols-1 sm:grid-cols-2',
              'auto-cols-fr',
              'place-items-center',
              'place-content-evenly',
            ])}
            style={{
              position: 'relative',
              padding: '0 0 1.5rem 0',
              ...(!isMobile && {
                marginTop: '-2px',
              }),
            }}
          >
            {!showAsList && (
              <>
                <Box
                  className={classNames(
                    [styles.isFlat, styles.isThick],
                    [attendeeStickerSwitch(CurrentAttendeeIndex).borderColor],
                    ['w-full grid grid-cols-subgrid col-span-1 sm:col-span-2'],
                  )}
                >
                  <div className={classNames(['w-full', 'grid col-span-full', 'place-items-center'])} style={{ overflow: 'hidden' }}>
                    <motion.div
                    // {...(shouldAnimateAttendeeInput() && {
                    //   initial: { x: '-50%', opacity: 0 },
                    //   animate: { x: 0, opacity: 1 },
                    //   transition: { duration: 0.3, ease: 'easeInOut' },
                    // })}
                    // {...{
                    //   initial: { x: '-50%', opacity: 0 },
                    //   animate: { x: 0, opacity: 1 },
                    //   transition: { duration: 0.3, ease: 'easeInOut' },
                    // }}
                    // style={{ x: '-50%' }}
                    // custom={CurrentAttendeeIndex}
                    // variants={variantAttendee}
                    // animate={controlAttendee}
                    >
                      <div
                        className={classNames(
                          ['grid-default'],
                          ['grid', 'gap-1', 'grid-cols-1 sm:grid-cols-2', 'auto-cols-fr', 'place-items-start', 'place-content-evenly'],
                        )}
                        style={{ minHeight: '4rem', verticalAlign: 'top' }}
                      >
                        <div className={classNames(['w-full row-start-1 gap-0'])}>
                          <TextInputRef
                            field={fields.lastName as MobxFormFieldInput}
                            placeholder={'Nom'}
                            marginless={true}
                            callback={(inputValue: string) => {
                              if (inputValue.length && form.root._extraErrorMessages.length) setExtraErrorMessagesDay([])
                              // const newAttendeeData = [...(createAttendees?.attendees ?? [])]
                              // newAttendeeData[CurrentAttendeeIndex as number] = {
                              //   lastName: inputValue,
                              // } as OnBoardEventApiTypes.TAttendee
                              // void updateAllAttendees(newAttendeeData)
                            }}
                            key={`${formKey}_lastName_${CurrentAttendeeIndex}`}
                            ref={ref}
                          />
                        </div>
                        <div className={classNames(['w-full row-start-2 gap-0'])}>
                          <TextInputRef
                            field={fields.firstName as MobxFormFieldInput}
                            placeholder={'Prénom'}
                            marginless={true}
                            callback={(inputValue: string) => {
                              if (inputValue.length && form.root._extraErrorMessages.length) setExtraErrorMessagesDay([])
                              // const newAttendeeData = [...(createAttendees?.attendees ?? [])]
                              // newAttendeeData[CurrentAttendeeIndex as number] = {
                              //   firstName: inputValue,
                              // } as OnBoardEventApiTypes.TAttendee
                              // void updateAllAttendees(newAttendeeData)
                            }}
                            key={`${formKey}_firstName_${CurrentAttendeeIndex}`}
                            ref={ref}
                          />
                        </div>
                        <Form3Times
                          callback={(startParticipation: string, endParticipation: string) => {
                            fields.startParticipation.setRawInput(startParticipation)
                            fields.endParticipation.setRawInput(endParticipation)
                          }}
                          showAsList={showAsList}
                          keyIndex={CurrentAttendeeIndex}
                          ref={form2TimesRef}
                        />
                      </div>
                    </motion.div>
                  </div>
                </Box>
                {/* eslint-disable-next-line prettier/prettier */}
                {/* {(CurrentAttendeeIndex < 2 && CurrentAttendeeIndex === (elements?.length - 1)) && (
                  <div className={classNames(['grid grid-cols-subgrid col-span-full col-start-1 sm:col-span-1 sm:col-start-2'])}>
                    <motion.div
                      style={{ ...getRandomTransformOrigin() }}
                      custom={CurrentAttendeeIndex}
                      variants={variantShake}
                      animate={controlShakeAttendee}
                    >
                      <Button
                        small
                        variant={attendeeStickerSwitch(CurrentAttendeeIndex).stickerColor}
                        onClick={() => {
                          void countTempInputAttendeeErrorState(hasTempInputAttendeeErrorState + 1)
                          // _setCurrentAttendeeIndex(CurrentAttendeeIndex + 1)
                        }}
                      >
                        Ajouter un autre particpant
                      </Button>
                    </motion.div>
                  </div>
                )} */}
                {/* eslint-disable-next-line prettier/prettier */}
                {/* {(CurrentAttendeeIndex < 2 && CurrentAttendeeIndex === (elements?.length - 1)) && renderAddParticipantBtn()} */}
                {renderAddParticipantBtn()}
              </>
            )}

            {showAsList && (
              <>
                {elements.map((root: MobxZodObjectField<z.AnyZodObject>, index: number) => {
                  const { fields } = root
                  return (
                    <React.Fragment key={index}>
                      <div className={classNames(['w-full grid col-span-full'])} style={{ overflow: 'hidden', padding: '0 0 1rem 0' }}>
                        <motion.div
                        // {...(!currentDay && {
                        //   initial: { x: '-50%', opacity: 0 },
                        //   animate: { x: 0, opacity: 1 },
                        //   transition: { duration: 0.3, ease: 'easeInOut' },
                        // })}
                        // style={{ x: '-50%' }}
                        // custom={index}
                        // variants={variantAttendee}
                        // animate={controlAttendee}
                        >
                          {/* eslint-disable-next-line prettier/prettier */}
                          {((index === (elements.length - 1)) && index > 0) && (
                            <div
                              className={classNames(styles.isFullwidth, styles.isFlex, styles.isJustifyContentEnd)}
                              style={{ padding: '0 0 0.5rem 0' }}
                            >
                              {displayDeleteAttendeeItem(index, styles.hasTextSecondary)}
                            </div>
                          )}
                          <div
                            className={classNames(
                              ['grid-default'],
                              ['grid', 'gap-1', 'grid-cols-1 sm:grid-cols-2', 'auto-cols-fr', 'place-items-start', 'place-content-evenly'],
                            )}
                            style={{ minHeight: '4rem', verticalAlign: 'top' }}
                          >
                            <div className={classNames(['w-full row-start-1 gap-0'])}>
                              <TextInputRef
                                field={fields.lastName as MobxFormFieldInput}
                                placeholder={'Nom'}
                                marginless={true}
                                callback={(inputValue: string) => {
                                  if (inputValue.length && form.root._extraErrorMessages.length) setExtraErrorMessagesDay([])
                                  // const newAttendeeData = [...(createAttendees?.attendees ?? [])]
                                  // newAttendeeData[index as number] = {
                                  //   lastName: inputValue,
                                  // } as OnBoardEventApiTypes.TAttendee
                                  // void updateAllAttendees(newAttendeeData)
                                }}
                                key={`${formKey}_lastName_${index}`}
                                ref={ref}
                              />
                            </div>
                            <div className={classNames(['w-full row-start-2 gap-0'])}>
                              <TextInputRef
                                field={fields.firstName as MobxFormFieldInput}
                                placeholder={'Prénom'}
                                marginless={true}
                                callback={(inputValue: string) => {
                                  if (inputValue.length && form.root._extraErrorMessages.length) setExtraErrorMessagesDay([])
                                  // const newAttendeeData = [...(createAttendees?.attendees ?? [])]
                                  // newAttendeeData[index as number] = {
                                  //   firstName: inputValue,
                                  // } as OnBoardEventApiTypes.TAttendee
                                  // void updateAllAttendees(newAttendeeData)
                                }}
                                key={`${formKey}_firstName_${index}`}
                                ref={ref}
                              />
                            </div>
                            <Form3Times
                              callback={(startParticipation: string, endParticipation: string) => {
                                fields.startParticipation.setRawInput(startParticipation)
                                fields.endParticipation.setRawInput(endParticipation)
                              }}
                              showAsList={showAsList}
                              keyIndex={index}
                              ref={form2TimesRef}
                            />
                          </div>
                        </motion.div>
                      </div>
                    </React.Fragment>
                  )
                })}
                {/* {CurrentAttendeeIndex < 2 && (
                  <div className={classNames(['grid grid-cols-subgrid col-span-full col-start-1 sm:col-span-1 sm:col-start-2'])}>
                    <motion.div
                      style={{ ...getRandomTransformOrigin() }}
                      custom={CurrentAttendeeIndex}
                      variants={variantShake}
                      animate={controlShakeAttendee}
                    >
                      <Button
                        small
                        variant={VariantState.SECONDARY}
                        onClick={() => {
                          countTempInputAttendeeErrorState(hasTempInputAttendeeErrorState + 1)
                          // _setCurrentAttendeeIndex(CurrentAttendeeIndex + 1)
                        }}
                      >
                        Ajouter un autre particpant
                      </Button>
                    </motion.div>
                  </div>
                )} */}
                {/* {CurrentAttendeeIndex < 2 && renderAddParticipantBtn()} */}
                {renderAddParticipantBtn()}
              </>
            )}
          </div>
        </fieldset>
      )
    }
    return <p>...loading</p>
  })

  const ConfirmTimes = observer(() => {
    const { createTimes } = CreateOnBoardEvent
    const [hoveredDay, setHoveredDay] = React.useState<OnBoardEventApiTypes.EDayOfWeek | null>(null)
    // https://www.js-howto.com/looping-through-javascript-map-object-in-react/
    React.useEffect(() => {
      if (!createTimes.size) return
      if (!hoveredDay) return
    }, [hoveredDay])
    const getDayLabel = (value: string) => metas.day.find((day) => day.fieldValue === value)?.label?.() as unknown as ReactNode
    let entriesArray = [...createTimes.entries()]
    const mappedDayValues = entriesArray.map(([key, value]) => {
      return (
        <Tag
          key={key}
          deletable
          onClick={() => {
            deleteTime(value.day)

            if (!createTimes.size) {
              mobxFormElements?.[0].fields.day.setRawInput(null)
              mobxFormElements?.[0].fields.day.setTouched(false)
            } else {
              // https://gist.github.com/tizmagik/19ba6516064a046a37aff57c7c65c9cd
              entriesArray = [...createTimes.entries()]
              const [, lastValue] = [...entriesArray].at(-1) || []
              // console.log(lastValue?.day)
              mobxFormElements?.[0].fields.day.setRawInput(lastValue?.day)
            }

            setCurrentDay(null)
          }}
          onMouseEnter={() => setHoveredDay(value.day)}
          onMouseLeave={() => setHoveredDay(null)}
          variant={TagVariant.SECONDARY}
        >
          {getDayLabel(value.day as string)}
        </Tag>
      )
    })

    const messageContentAttendee = (day: OnBoardEventApiTypes.EDayOfWeek, attendees: OnBoardEventApiTypes.TAttendee[]) => {
      return attendees.map((attendee: OnBoardEventApiTypes.TAttendee, index: number) => {
        const startParticipation = attendee.startParticipation ? format(parseISO(attendee.startParticipation), 'HH:mm', { locale: fr }) : null
        const endParticipation = attendee.endParticipation ? format(parseISO(attendee.endParticipation), 'HH:mm', { locale: fr }) : null
        return (
          <React.Fragment key={`${day}-${index}`}>
            <div
              className={classNames(['w-full', 'grid', 'gap-1', 'grid-flow-col', 'col-span-3 sm:col-span-5', 'col-start-2', 'place-items-start'])}
              style={{ verticalAlign: 'top' }}
            >
              <div className={classNames('px-1')}>
                {`
                  ${attendee.firstName}
                  ${attendee.lastName}
                  ${attendee.startParticipation && attendee.endParticipation && `de ${startParticipation} à ${endParticipation}`}
                `}
              </div>
            </div>
          </React.Fragment>
        )
      })
    }

    const mappedAttendeeValues = entriesArray.map(([key, value]) => {
      return (
        <motion.div
          key={key}
          initial={{ y: 50, opacity: 0, scale: 0.3 }}
          animate={{ y: 0, opacity: 1, scale: 1 }}
          exit={{ opacity: 0, scale: 0.5, transition: { duration: 0.2 } }}
          transition={{ duration: 0.3, ease: 'easeInOut' }}
        >
          <Message colorTheme='info' variation='filled'>
            <div className={classNames(['w-full'], ['grid', 'gap-0', 'grid-flow-col', 'place-items-stretch'])}>
              <div
                className={classNames(['w-full', 'grid', 'gap-1', 'row-span-3', 'grid-flow-col', 'col-span-1', 'col-start-1'])}
                style={{ verticalAlign: 'top', height: '100%' }}
              >
                <div className={classNames('truncate', 'min-w-auto sm:min-w-[80px]')}>{getDayLabel(value.day as string)}</div>
              </div>
              {messageContentAttendee(value.day, value.attendees)}
            </div>
          </Message>
        </motion.div>
      )
    })

    const activeAttendeeValue = (day: OnBoardEventApiTypes.EDayOfWeek) => {
      const attendees = createTimes.get(day)?.attendees ?? []
      return (
        <motion.div
          initial={{ x: '-50%', opacity: 0, scale: 0.3 }}
          animate={{ x: 0, opacity: 1, scale: 1 }}
          exit={{ x: 0, opacity: 0, scale: 0.5 }}
          transition={{ duration: 0.3, ease: 'easeInOut' }}
        >
          <Message colorTheme='info' variation='filled'>
            <div className={classNames(['w-full', 'sm:w-auto'], ['grid', 'gap-0', 'grid-flow-col', 'place-items-stretch'])}>
              <div
                className={classNames(['w-full', 'grid', 'gap-1', 'row-span-3', 'grid-flow-col', 'col-span-1', 'col-start-1'])}
                style={{ verticalAlign: 'top', height: '100%' }}
              >
                <div className={classNames('truncate', 'min-w-auto sm:min-w-[80px]')}>{getDayLabel(day as string)}</div>
              </div>
              {messageContentAttendee(day, attendees)}
            </div>
          </Message>
        </motion.div>
      )
    }

    if (!createTimes.size) return

    return (
      <div
        style={{
          padding: '0 0 1.5rem 0',
        }}
      >
        <Title level={7}>{'Récapitulatif de vos choix'}</Title>
        <br />
        <div className='w-full'>
          <TagList>{mappedDayValues}</TagList>
          <div
            className={classNames([
              'w-full',
              'grid',
              'gap-1',
              'grid-cols-1',
              'auto-cols-fr',
              'place-items-center',
              'justify-stretch',
              'place-content-evenly',
            ])}
            style={{
              padding: '0 1rem 0',
            }}
          >
            <AnimatePresence>
              {mappedAttendeeValues}
              {/* {createTimes.size && currentDay && !hoveredDay && activeAttendeeValue(currentDay)}
              {createTimes.size && hoveredDay && activeAttendeeValue(hoveredDay)} */}
            </AnimatePresence>
          </div>
        </div>
        <div className={classNames(styles.help, styles.isWarning, styles.isFullwidth)}>
          <Icon
            content={`Vous pourriez encore affiner ces choix ultérieurement`}
            size={IconSize.SMALL}
            position={IconPosition.LEFT}
            name={IconName.EXCLAMATION_TRIANGLE}
          />
        </div>
      </div>
    )
  })

  if (form) {
    // console.log(form)

    mobxFormElements = form.root.elements as MobxFormElements

    // console.log('form3a schema elements length', mobxFormElements.length)

    return (
      <form {...form.bindForm()}>
        {createOnBoardEvent.eventType === EEventType.PROPOSAL ? (
          <Title level={7}>{'Definissez vos heures de disponibilité'}</Title>
        ) : (
          <Title level={7}>{'Assignez un ou plusieurs participants'}</Title>
        )}
        <br />
        {mobxFormElements.map((root: MobxZodObjectField<z.SomeZodObject>, index: number) => {
          // console.log('root', root)
          const { fields, type } = root
          // console.log('fields.attendees', fields.attendees)
          return (
            <div key={`${formKey}_${index}`} style={{ padding: '0 0 1rem 0' }}>
              {fields.attendees && (
                <AttendeesInputFieldArray root={root.fields.attendees as unknown as MobxFormArray['root']} type={type} index={index} />
              )}
            </div>
          )
        })}

        {form.root._extraErrorMessages && form.root.extraErrorMessages.map((message, index) => <ErrorDisplay key={index} message={message} />)}

        {createOnBoardEvent.eventType === EEventType.PROPOSAL ? (
          <Title level={7}>{'Definissez vos jours de disponibilité'}</Title>
        ) : (
          <Title level={7}>{'Choisissez un ou plusieurs jours'}</Title>
        )}
        <br />
        {mobxFormElements.map((root: MobxZodObjectField<z.SomeZodObject>, index: number) => {
          const { fields, type } = root
          // console.log(fields)
          return (
            <div key={`${formKey}_${index}`} style={{ padding: '0 0 1rem 0' }}>
              <motion.div style={{ ...getRandomTransformOrigin() }} custom={index} variants={variantShake} animate={controlShakeDays}>
                {fields.day && <DaysInputEnum field={root.fields.day as MobxFormFieldEnum} type={type} index={index} />}
              </motion.div>
            </div>
          )
        })}

        <ConfirmTimes />

        <Columns className={classNames(styles.isFullwidth, styles.isAlignItemsCenter, styles.isMarginless)}>
          <ColumnsItem>
            <Button
              variant={VariantState.SECONDARY}
              onClick={() => {
                setExtraErrorMessagesDay([])
                // unsetCreateAttendees()
                unsetCreateTimes()
                mobxFormElements?.[0].fields.day.setRawInput(null)
                mobxFormElements?.[0].fields.day.setTouched(false)
                setCurrentDay(null)
                setValidatedCurrentStep(-1)
              }}
            >
              Retour
            </Button>
          </ColumnsItem>
          <ColumnsItem>
            <Button
              disabled={authenticationOnLoad && route !== 'authenticated'}
              variant={VariantState.PRIMARY}
              // https://stackoverflow.com/questions/42053775/getting-error-form-submission-canceled-because-the-form-is-not-connected
              type={'submit'}
              onClick={() => {
                if (route !== 'authenticated') {
                  if (!authenticationOnLoad) {
                    void setTriggerAuthentication(true)
                    console.log('trigger authentication')
                  }
                  return
                } else {
                  void form.handleSubmit(async () => {
                    console.info(form.parsed)
                    if (form.parsed.success && currentStep === 3) {
                      setInvalidateStep(false)
                      // setValidatedCurrentStep(1) // increment by 1 to go to next step

                      // const { data } = form.parsed

                      const sendPayload = true

                      setCreateOnBoardEvent({
                        ...createOnBoardEvent,
                      })

                      if (!sendPayload) void resetModalCreateOnBoardEvent()

                      if (sendPayload) {
                        await props?.createTable?.(createOnBoardEvent.name, createOnBoardEvent.description)

                        // const result = await props?.createTable?.(createOnBoardEvent.name, createOnBoardEvent.description)
                        // if (result) props?.updateTables?.()
                        // await resetModalCreateOnBoardEvent()

                        // await props?.createTable?.(createOnBoardEvent.name, createOnBoardEvent.description).then((result) => {
                        //   if (result) props?.updateTables?.()
                        // })
                      }
                    }
                  })
                }
              }}
            >
              Suite
            </Button>
          </ColumnsItem>
        </Columns>
      </form>
    )
  }
  return <p>...loading</p>
})

export { Form3 }
