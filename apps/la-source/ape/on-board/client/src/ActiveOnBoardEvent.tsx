/* eslint-disable camelcase */

// https://medium.com/@adamgerhant/keeping-multi-user-web-app-in-sync-with-database-aws-amplify-react-41130139abe5
// https://github.com/adamgerhant/Synchronous-Todo-App/tree/master

// https://gerard-sans.medium.com/offline-first-made-easy-with-graphql-amplify-datastore-and-vue-f9f7eec03c2d

// https://github.com/ErikCH/CodeFirstTypeSafety/blob/main/src/components/auth/Auth.tsx

// https://docs.amplify.aws/gen1/javascript/build-a-backend/more-features/datastore/example-application/

// https://dev.to/codebeast/an-in-depth-guide-on-amplify-graphql-api-authorization-14ng

'use client'

// import { isServer, isObjectEmpty } from '@flexiness/domain-utils'
import React, { useEffect, useState } from 'react'
import { observer } from 'mobx-react-lite'

import { AMPLIFY_CLIENT } from '@flexiness/aws'

const {
  // AmplifyUIReact,
  AmplifyAuth,
} = AMPLIFY_CLIENT
// const {
//   useAuthenticator,
// } = AmplifyUIReact
const {
  // autoSignIn,
  // confirmSignUp,
  // signUp,
  fetchAuthSession,
  // fetchUserAttributes,
} = AmplifyAuth
async function handleFetchSessionResults() {
  try {
    const { userSub } = await fetchAuthSession() // will return the credentials
    return userSub
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error)
  }
}

import {
  // getOnBoardEventsStore,
  getUIStore,
} from '@flexiness/domain-store'
// const CreateOnBoardEvent = getOnBoardEventsStore()
const UIStore = getUIStore()

// import { DataStore } from '@aws-amplify/datastore'
import { DataStore, PersistentModelConstructor } from 'aws-amplify/datastore'

// import type { Schema } from "@root/amplify/data/resource"
// import { generateClient } from "aws-amplify/data"

import { OnBoardEventApiTypes } from 'flexiness'

import {
  // Attendees,
  // Times,
  OnBoardEvent,
  // Post,
  // Comment,
  // User,
  // Tag,
  // OnBoardEventTags,
  // OnBoardEventUsers,
  // EActivity,
  // EDayOfWeek,
  // EEventType,
  // ELifeCycle,
  // EPostStatus,
  // ESchool,
  // ESchoolLevel,
  // EStatus,
  // EWhichWay,
  // EAttendeeRole,
  // TAddress,
  // TCalendarDateTime,
  // TPoint,
  // TPointList,
  // TAttendee,
  // TTime
} from '@flexiness/aws/models'

// import { getOnBoardEventTableById } from 'on-board-event-common'
const { getByAPIKeyOnBoardEventTableById } = await import(
  /* webpackFetchPriority: "high" */
  'on-board-event-common'
)

// const client = generateClient<Schema>()

import { materialRenderers, materialCells } from '@jsonforms/material-renderers'
import { JsonForms } from '@jsonforms/react'

const schema = {
  type: 'object',
  properties: {
    name: {
      type: 'string',
      minLength: 1,
    },
    done: {
      type: 'boolean',
    },
    due_date: {
      type: 'string',
      format: 'date',
    },
    recurrence: {
      type: 'string',
      enum: ['Never', 'Daily', 'Weekly', 'Monthly'],
    },
  },
  required: ['name', 'due_date'],
}

const uischema = {
  type: 'VerticalLayout',
  elements: [
    {
      type: 'Control',
      label: false,
      scope: '#/properties/done',
    },
    {
      type: 'Control',
      scope: '#/properties/name',
    },
    {
      type: 'HorizontalLayout',
      elements: [
        {
          type: 'Control',
          scope: '#/properties/due_date',
        },
        {
          type: 'Control',
          scope: '#/properties/recurrence',
        },
      ],
    },
  ],
}

const initialData = {}

const ActiveOnBoardEvent = observer(() => {
  // const { route } = useAuthenticator((context) => [context.route])
  const { activeTableId } = UIStore
  const [events, setEvents] = useState<OnBoardEventApiTypes.OnBoardEvent[] | null>(null)
  const [data, setData] = useState(initialData)

  useEffect(() => {
    if (activeTableId === null) return
    // console.log('activeTableId: ', activeTableId)
    // console.log('Authenticator route : ', route)
    void handleFetchSessionResults().then((response) => {
      if (response) {
        // console.log('ActiveOnBoardEvent : Amplify | UserPool | DataStore RealTime')

        // DataStore.configure({
        //   // authModeStrategyType: AuthModeStrategyType.MULTI_AUTH
        //   // ...DataStoreConfig,
        //   // ...AmplifyConfig,
        // })

        // const subscription = DataStore.observe(OnBoardEvent).subscribe((msg) => {
        //   console.log(msg.model, msg.opType, msg.element)
        // })

        // https://docs.aws.amazon.com/whitepapers/latest/amplify-datastore-implementation/amplify-datastore-best-practices.html
        // const original = await DataStore.query(Whitepaper, "123");
        // await DataStore.save(
        //   Whitepaper.copyOf(original, updated => {
        //     updated.title = `title ${Date.now()}`;
        //   })
        // );

        // const originalTable = await DataStore.query(OnBoardEvent, activeTableId);
        // await DataStore.save(
        //   OnBoardEvent.copyOf(originalTable, updated => {
        //     updated.title = `title ${Date.now()}`;
        //   })
        // )

        /**
         * This keeps `OnBoardEvent` fresh.
         */
        const sub = DataStore.observeQuery(OnBoardEvent as unknown as PersistentModelConstructor<OnBoardEventApiTypes.OnBoardEvent>, (c) =>
          c.id.eq(activeTableId),
        ).subscribe((snapshot) => {
          // const { items, isSynced } = snapshot
          const { items } = snapshot
          setEvents(items)
        })

        return () => {
          // subscription.unsubscribe()
          sub.unsubscribe()
        }
      } else {
        // console.log('ActiveOnBoardEvent : Amplify | APIKey | AppSync GraphQL')
        void getByAPIKeyOnBoardEventTableById(activeTableId).then((items) => {
          setEvents(items)
        })
      }
    })
  }, [activeTableId])

  return (
    <div>
      <h1>OnBoardEvents</h1>
      {/* <button onClick={async () => {
        // create a new Todo with the following attributes
        const { errors, data: newTodo } = await client.models.OnBoardEvent.create({
          // prompt the user to enter the title
          name: window.prompt("title"),
          status: false,
          activity: 'BABYSITTING'
        })
        console.log(errors, newTodo)
      }}>Create </button> */}
      {activeTableId && (
        <>
          <ul>
            {events?.map((event) => (
              <React.Fragment key={event.id}>
                <li key={`${event.id}${event.name}`}>{event.name}</li>
                <li key={`${event.id}${event.activity}`}>{event.activity}</li>
              </React.Fragment>
            ))}
          </ul>
          <JsonForms
            schema={schema}
            uischema={uischema}
            data={data}
            renderers={materialRenderers}
            cells={materialCells}
            onChange={({
              data,
              // errors
            }) => setData(data)}
            config={{
              nonce: globalThis.__webpack_nonce__,
            }}
          />
        </>
      )}
    </div>
  )
})

export { ActiveOnBoardEvent }
