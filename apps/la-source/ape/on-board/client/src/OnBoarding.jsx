import { autorun } from 'mobx'
import React from 'react'
import DebuggingComponent from './DebuggingComponent.js'
// import JoinComponent from './JoinComponent.js'
// import PokerBoardComponent from './PokerBoardComponent'
import PokerClient from './PokerClient.ts'
import { ActiveOnBoardEvent } from './ActiveOnBoardEvent.js'
import { PokerTablesComponent } from './pokerTables/PokerTablesComponent'
// import { PokerBoard, PokerTables, PokerTimer } from 'on-board-event-common'
const { PokerBoard, PokerTables, PokerTimer } = await import(
  /* webpackFetchPriority: "high" */
  'on-board-event-common'
)

import classNames from 'classnames'

// import {
//   Button,
//   View,
//   Text,
//   Title,
//   Stepper,
//   StepperStep,
//   StepperStepMarkup,
//   TitleLevel,
//   VariantState,
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import { View, Title, Stepper, StepperStep, StepperStepMarkup, TitleLevel } from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

// const { ClientSyncStyledDefault } = await import('flex_design_system_react_ts_styled_default')
// const {
//   Button,
//   View,
//   Text,
//   Title,
//   Stepper,
//   StepperStep,
//   StepperStepMarkup,
//   TitleLevel,
//   VariantState,
//   styles,
// } = ClientSyncStyledDefault

export default class OnBoarding extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      messages: [],
      pendingActions: [],
      board: new PokerBoard(),
      serverBoard: new PokerBoard(),
      tables: new PokerTables([]),
      serverTables: new PokerTables([]),
      timers: new PokerTimer(new Date()),
      serverTimers: new PokerTimer(new Date()),
      clientId: null,
      disconnected: false,
    }

    this.joinCompRef = React.createRef()
    this.boardCompRef = React.createRef()
  }

  componentDidMount() {
    this.client = new PokerClient()

    autorun(() => {
      this.setState({
        messages: this.client.messages,
        pendingActions: this.client.pendingActions,
        board: this.client.board,
        serverBoard: this.client.serverBoard,
        tables: this.client.tables,
        serverTables: this.client.serverTables,
        timers: this.client.timers,
        serverTimers: this.client.serverTimers,
        clientId: this.client.clientId,
        disconnected: this.client.disconnected,
      })
    })
  }

  makeVote = (points) => {
    // console.log(points)
    // console.log(typeof points)
    this.client.setPokerClass('PokerBoard')
    this.client.sendAction({
      source: 'PokerBoard',
      action: PokerBoard.ACTION_VOTE,
      vote: typeof points === 'number' ? points : 0,
    })
  }

  changeCurrentlyVoting = (currentlyVoting) => {
    this.client.setPokerClass('PokerBoard')
    this.client.sendAction({
      source: 'PokerBoard',
      action: PokerBoard.ACTION_CURRENTLY_VOTING,
      currentlyVoting,
    })
  }

  clearVotes = () => {
    this.client.setPokerClass('PokerBoard')
    this.client.sendAction({
      source: 'PokerBoard',
      action: PokerBoard.ACTION_CLEAR_VOTES,
    })
  }

  showVotes = () => {
    this.client.setPokerClass('PokerBoard')
    this.client.sendAction({
      source: 'PokerBoard',
      action: PokerBoard.ACTION_SHOW_VOTES,
    })
  }

  componentWillUnmount() {
    this.client && this.client.close()
  }

  completeJoin = (name, observer, activeTable) => {
    this.client.setPokerClass('PokerBoard')
    this.client.sendAction({
      source: 'PokerBoard',
      action: PokerBoard.ACTION_COMPLETE_JOIN,
      name,
      observer,
      tableData: {
        id: activeTable.id,
        tableId: activeTable.tableId,
      },
      nextPlayerAction: this.state.timers.timeUTC,
    })
    window.sessionStorage.setItem('name', name)
  }

  /**
   * Fetches Table items from graphQL backend
   * @returns {OnBoardEvent[]} Returns an array of OnBoardEvent objects
   */
  updateTables = () => {
    this.client.setPokerClass('PokerTables')
    this.client.sendAction({
      source: 'PokerTables',
      action: PokerTables.ACTION_UPDATE_TABLES,
    })
  }

  /**
   * Creates a new Table item
   * @param {string} tableName The table name
   * @param {string} tableDesc The table description
   * @returns {OnBoardEvent} Returns an OnBoardEvent object
   */
  createTable = (tableName, tableDesc) => {
    this.client.setPokerClass('PokerTables')
    this.client.sendAction({
      source: 'PokerTables',
      action: PokerTables.ACTION_CREATE_TABLE,
      tableData: {
        tableName,
        tableDesc,
      },
    })
  }

  /**
   * Deletes a Table item
   * @param {string} id The OnBoardEvent id
   * @param {number} tableId The current table index
   */
  deleteTable = (id, tableId) => {
    // console.log(id)
    this.client.setPokerClass('PokerTables')
    this.client.sendAction({
      source: 'PokerTables',
      action: PokerTables.ACTION_DELETE_TABLE,
      tableData: {
        id,
        tableId,
      },
    })
  }

  activateTable = (id, tableId) => {
    this.client.setPokerClass('PokerTables')
    this.client.sendAction({
      source: 'PokerTables',
      action: PokerTables.ACTION_ACTIVATE_TABLE,
      tableData: {
        id,
        tableId,
      },
    })
  }

  scrollJoinComp = () => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    this.joinCompRef.current.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'nearest' })
  }

  scrollBoardComp = () => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    this.boardCompRef.current.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' })
  }

  render() {
    // const myself = this.state.board.getPlayerById(this.state.clientId)
    // const activeTable = this.state.tables.getActiveTable()
    // const joinedTables = myself?.joinedTables || []
    // const isTableJoined = this.state.board.isTableJoined(myself, activeTable?.tableId)

    // console.log(this.state.timers.timeReadable)

    return (
      <View className='mx-2 sm:mx-0'>
        <div className='container mx-auto'>
          <div
            className={classNames(styles.isFullwidth, styles.isFlex, styles.isFlexDirectionColumn)}
            style={{ minHeight: '4rem', padding: '2rem 0', verticalAlign: 'center' }}
          >
            <Title level={TitleLevel.LEVEL2} className={classNames(styles.isCentered)} style={{ lineHeight: 'inherit' }}>
              An after school multiplayer websocket app
            </Title>
            <Stepper
              centered
              className={classNames(styles.isFullwidth, styles.isFlex, styles.isJustifiedCenter, styles.isHiddenMobile, styles.isPaddingless)}
              style={{ marginTop: '1.5rem' }}
            >
              {/*
              <StepperStep markup={StepperStepMarkup.DIV} done label="Jira Service Management" step={1} />
              <StepperStep markup={StepperStepMarkup.DIV} done label='AWS AppSync GraphQL' step={2} />
              <StepperStep active current label='Multiplayer websocket' step={3} />
              */}
              <StepperStep markup={StepperStepMarkup.DIV} done label='Rechercher des événements qui vous correspondent' step={1} />
              <StepperStep markup={StepperStepMarkup.DIV} done label='Rejoindre ou créer un nouveau événement ' step={2} />
              <StepperStep active current label={'Souscrire des participants à des événements'} step={3} />
            </Stepper>
          </div>

          {!this.state.disconnected && (
            <div className='px-2 sm:px-2 md:px-0' onClick={(e) => this.scrollJoinComp(e)}>
              <PokerTablesComponent
                allTables={this.state.tables.allTables}
                // allTables={myself?.tables?.allTables || this.state.tables.allTables}
                // allTables={myself.tables.allTables}
                createTable={this.createTable}
                deleteTable={this.deleteTable}
                activateTable={this.activateTable}
                updateTables={this.updateTables}
              />
            </div>
          )}

          <div ref={this.joinCompRef} onClick={(e) => this.scrollBoardComp(e)}>
            {/*
              {
                !this.state.disconnected && myself && (myself.joining || (!myself.joining && !isTableJoined)) && (
                  <JoinComponent
                    onSubmit={this.completeJoin}
                    defaultValue={window.sessionStorage.getItem('name') || 'New Player'}
                    activeTable={activeTable}
                    joinedTables={joinedTables}
                    isTableJoined={isTableJoined}
                  />
              )}
            */}
          </div>

          <div ref={this.boardCompRef}>
            {/*
              {
                !this.state.disconnected && myself && !myself.joining && isTableJoined && (
                  <div style={{ marginTop: '2rem' }}>
                    <Title level={TitleLevel.LEVEL3} className={classNames(styles.isInline, styles.isMarginless )}
                      style={{ lineHeight: 'inherit' }}>
                      {myself.name}
                    </Title>
                    <PokerBoardComponent board={this.state.board}
                      myPlayer={myself}
                      activeTable={activeTable}
                      joinedTables={joinedTables}
                      makeVote={myself.observer ? null : this.makeVote}
                      changeCurrentlyVoting={this.changeCurrentlyVoting}
                      clearVotes={this.clearVotes}
                      showVotes={this.showVotes}
                    />
                  </div>
              )}
            */}
            <ActiveOnBoardEvent />
          </div>

          {this.state.disconnected && (
            <div
              className={classNames(styles.isFlex, styles.isAlignContentCenter, styles.isFullwidth)}
              style={{ minHeight: '4rem', padding: '2rem 0', verticalAlign: 'center' }}
            >
              <Title level={TitleLevel.LEVEL3} className={classNames(styles.isInline, styles.isMarginless)} style={{ lineHeight: 'inherit' }}>
                {/* Disconnected from Server. Please Reload the page. */}
                Déconnecté du serveur. Veuillez rafraichir la page.
              </Title>
            </div>
          )}

          {process.env.FLEX_MODE === 'development' && this.props.isStandalone && (
            <DebuggingComponent pendingActions={this.state.pendingActions} messages={this.state.messages} />
          )}
        </div>

        {this.props.isStandalone && (
          <footer className='footer'>
            <span>
              Powered by Flexiness <div className='logoBadge' />
            </span>
          </footer>
        )}

        <div id='root-portal' />
      </View>
    )
  }
}
