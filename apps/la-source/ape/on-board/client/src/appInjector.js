import App from './App.tsx'
import React from 'react'
import ReactDOM from 'react-dom'
import { createShadowContainer, deleteShadowContainer } from '@flexiness/domain-utils/style-loader-esm'

export const inject = (parentElementId) => {
  const appPlaceholder = createShadowContainer(parentElementId)
  ReactDOM.render(<App />, appPlaceholder)
}

export const cleanup = (parentElementId) => {
  deleteShadowContainer(parentElementId)
  ReactDOM.unmountComponentAtNode(document.getElementById(parentElementId))
}
