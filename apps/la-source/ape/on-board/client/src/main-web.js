import 'core-js'
import React from 'react'
import { hydrate } from 'react-dom'
import { loadableReady } from '@loadable/component'
import AppSSR from './AppSSR.tsx'

// console.log('waiting for application ready...')
void loadableReady(() => {
  // console.log('application is ready...')
  const root = document.getElementById('root')
  hydrate(<AppSSR />, root)
})
