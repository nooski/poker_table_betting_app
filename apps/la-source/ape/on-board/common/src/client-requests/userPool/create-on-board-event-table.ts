/* eslint-disable no-console */

import { isObjectEmpty } from '@flexiness/domain-utils'
import { toJS } from 'mobx'

import { OnBoardEventApiTypes } from 'flexiness'

const { createEvent, createEventAttendees, createEventTimes, createEventUser, createEventUsers } = await import(
  /* webpackFetchPriority: "high" */
  'on-board-event-api'
)

import { getOnBoardEventsStore, getUIStore } from '@flexiness/domain-store'
const CreateOnBoardEvent = getOnBoardEventsStore()
const UIStore = getUIStore()
const { unsetCreateOnBoardEvent, unsetCreateTimes, unsetCreateAttendees, resetCurrentStep } = CreateOnBoardEvent
const { setModalCreateOnBoardEventOpen, setTriggerAuthentication } = UIStore

const resetModalCreateOnBoardEvent = async () => {
  await Promise.all([
    setTriggerAuthentication(false),
    unsetCreateAttendees(),
    unsetCreateTimes(),
    unsetCreateOnBoardEvent(),
    resetCurrentStep(),
    setModalCreateOnBoardEventOpen(false),
  ])
}

const createOnBoardEventTable = async () => {
  try {
    const { createUser, createTimes, createAttendees, createOnBoardEvent } = CreateOnBoardEvent

    const TTimeArray = Array.from(toJS(createTimes), ([day, attendees]) => ({ day, attendees: attendees.attendees })) as OnBoardEventApiTypes.TTime[]
    // console.log('TTimeArray', TTimeArray)

    const _user = await createEventUser({
      ...toJS(createUser),
    } as OnBoardEventApiTypes.CreateUserInput).then((resultData) => {
      console.log('_user', resultData)
      return resultData
    })

    const _onBoardEvent = await createEvent({
      ...toJS(createOnBoardEvent),
      onBoardEventCreatorId: _user?.id,
      onBoardEventOrganizerId: _user?.id,
    } as OnBoardEventApiTypes.CreateOnBoardEventInput).then(async (resultData) => {
      console.log('_onBoardEvent', resultData)

      const _times = await createEventTimes({
        times: TTimeArray,
        onBoardEventTimesId: resultData?.id,
        timesUserId: _user?.id,
      } as OnBoardEventApiTypes.CreateTimesInput).then((resultData) => {
        console.log('_times', resultData)
        return resultData
      })

      await createEventAttendees({
        ...toJS(createAttendees),
        onBoardEventAttendeesId: resultData?.id,
        attendeesUserId: _user?.id,
        timesAttendeesId: _times?.id,
      } as OnBoardEventApiTypes.CreateAttendeesInput).then((resultData) => {
        console.log('_attendees', resultData)
        return resultData
      })

      return resultData
    })

    const _createEventUsers = await createEventUsers({
      userId: _user?.id,
      onBoardEventId: _onBoardEvent?.id,
    } as OnBoardEventApiTypes.CreateOnBoardEventUsersInput).then(async (resultData) => {
      console.log('_createEventUsers', resultData)
      await resetModalCreateOnBoardEvent()
      return resultData
    })

    if (_createEventUsers != null && !isObjectEmpty(_createEventUsers)) return Promise.resolve(_onBoardEvent as OnBoardEventApiTypes.OnBoardEvent)
    return Promise.reject(false)
  } catch (e) {
    console.log(e)
    return Promise.reject(e)
  }
}

export { createOnBoardEventTable, resetModalCreateOnBoardEvent }
