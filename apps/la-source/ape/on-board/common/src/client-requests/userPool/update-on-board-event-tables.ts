/* eslint-disable no-console */

import { isObjectEmpty } from '@flexiness/domain-utils'

import { OnBoardEventApiTypes } from 'flexiness'

const { listEvents } = await import(
  /* webpackFetchPriority: "high" */
  'on-board-event-api'
)

// import { getOnBoardEventsStore, getUIStore } from '@flexiness/domain-store'
// const CreateOnBoardEvent = getOnBoardEventsStore()
// const UIStore = getUIStore()
// const { } = CreateOnBoardEvent
// const { } = UIStore

const updateOnBoardEventTables = async () => {
  try {
    const _events = await listEvents({
      limit: 20,
    } as OnBoardEventApiTypes.ListOnBoardEventsQueryVariables).then((resultData) => {
      console.log('_events', resultData)
      return resultData
    })

    if (_events != null && !isObjectEmpty(_events)) return Promise.resolve(_events as OnBoardEventApiTypes.OnBoardEvent[])
    return Promise.reject(false)
  } catch (e) {
    console.log(e)
    return Promise.reject(e)
  }
}

export { updateOnBoardEventTables }
