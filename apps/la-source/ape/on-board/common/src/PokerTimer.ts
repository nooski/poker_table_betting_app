import { immerable, produce } from 'immer'
import { PokerAction } from 'flexiness'

const getRandomArbitrary = (min: number, max: number) => {
  return Math.floor((Math.random() * (max - min + 1)) << 0)
}

const actions = {
  nextPlayerAction: produce<PokerTimer>((draft, action) => {
    const newDate = action.date
    draft.second = newDate.setSeconds(newDate.getSeconds() + getRandomArbitrary(15, 30))
  }),

  // tick: produce<PokerTimer>((draft) => {
  //   draft.minute++
  // }),
}
export class PokerTimer extends Date {
  // export class PokerTimer {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [action: string]: any

  [immerable] = true

  date: Date = new Date()

  hour: number

  minute: number

  second: number

  constructor(...args: ConstructorParameters<typeof Date>) {
    super(...args)
    this.hour = this.date.getHours()
    this.minute = this.date.getMinutes()
    this.second = this.date.getSeconds()
  }

  get timeReadable() {
    return `${this.hour}:${this.minute}:${this.second}`
  }

  get timeUTC() {
    return this.date
  }

  get nextPlayerAction() {
    return {
      source: 'PokerTimer',
      action: PokerTimer.ACTION_NEXT_TIME_UTC,
      timers: PokerTimer,
    }
  }

  // get tick() {
  //   return {
  //     source: 'PokerTimer',
  //     action: PokerTimer.ACTION_TICK,
  //     timers: PokerTimer,
  //   }
  // }

  tick() {
    return produce(this, (draft) => {
      draft.minute++
    })
  }

  static get ACTION_TIME_READABLE() {
    return 'timeReadable'
  }

  static get ACTION_TIME_UTC() {
    return 'timeUTC'
  }

  static get ACTION_NEXT_TIME_UTC() {
    return 'nextPlayerAction'
  }

  static get ACTION_TICK() {
    return 'tick'
  }

  processAction(action: PokerAction) {
    if (!action || !action.action) throw new Error('action is undefined')

    // @ts-expect-error - Requires better Immer typing ?
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const producer: typeof produce = actions[action?.action as string]
    if (!producer) throw new Error(`unknown action ${action.action as string}`)

    // @ts-expect-error - Requires better Immer typing ?
    return producer(this, action)
  }
}
