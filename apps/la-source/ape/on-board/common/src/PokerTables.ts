/* eslint-disable no-console */

import { immerable, produce, enableMapSet } from 'immer'
enableMapSet()
import { adjectives, animals, colors, starWars, uniqueNamesGenerator } from 'unique-names-generator'
import { PokerAction, Table, TableData, OnBoardEventApiTypes } from 'flexiness'

// import {
//   deleteEventLegacy,
//   listEventsLegacy,
// } from 'on-board-event-api'
const {
  deleteEventLegacy,
  // deleteEventServer,
  listEventsLegacy,
  // listEventsServer,
} = await import(
  /* webpackFetchPriority: "high" */
  'on-board-event-api'
)

// import { createOnBoardEventTable, updateOnBoardEventTables } from './client-requests/userPool/index.js'
const { createOnBoardEventTable, updateOnBoardEventTables } = await import(
  /* webpackFetchPriority: "high" */
  './client-requests/index.js'
)

// import { enrich } from '@flexiness/domain-utils'

const {
  getOnBoardEventsStore,
  // getUIStore,
} = await import('@flexiness/domain-store')
const CreateOnBoardEvent = getOnBoardEventsStore()
// const { setTables } = CreateOnBoardEvent
// const UIStore = getUIStore()
// const { setActiveTableId } = UIStore

export const generateTableName = () => {
  const i = Math.floor(Math.random() * Math.floor(2))
  const nouns = [animals, starWars]
  const name = uniqueNamesGenerator({
    dictionaries: [adjectives, colors, nouns[i]],
    length: Math.floor(Math.random() * Math.floor(4)),
    separator: ' ',
    style: 'capital',
  })
  return `${name} Table`
}

export const defaultDesc = () => {
  return "Some quick example text to build on the card title and make up the bulk of the card's content."
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const baseTableState = [
  {
    id: '',
    tableId: 1,
    name: generateTableName(),
    description: defaultDesc(),
    order: 9999,
    active: false,
    created: '',
    updated: '',
    __typename: 'OnBoardEvent',
  },
  {
    id: '',
    tableId: 2,
    name: generateTableName(),
    description: defaultDesc(),
    order: 9999,
    active: false,
    created: '',
    updated: '',
    __typename: 'OnBoardEvent',
  },
  {
    id: '',
    tableId: 3,
    name: generateTableName(),
    description: defaultDesc(),
    order: 9999,
    active: false,
    created: '',
    updated: '',
    __typename: 'OnBoardEvent',
  },
  {
    id: '',
    tableId: 4,
    name: generateTableName(),
    description: defaultDesc(),
    order: 9999,
    active: false,
    created: '',
    updated: '',
    __typename: 'OnBoardEvent',
  },
]

// eslint-disable-next-line @typescript-eslint/no-unused-vars
async function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

// https://stackoverflow.com/questions/43431550/async-await-class-constructor
// https://dev.to/somedood/the-proper-way-to-write-async-constructors-in-javascript-1o8c
export const getBaseTableState = async (variables: { limit: number }) => {
  try {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    const _events =
      (await listEventsLegacy(variables).catch((err: Error) => {
        console.log(err)
      })) || []
    if (_events !== null) {
      console.log('Init server events', _events[0])
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      const _tables: Table[] = _events?.map((_event: unknown, index: number) => {
        return {
          ...(_event as OnBoardEventApiTypes.OnBoardEvent),
          tableId: index,
          order: 9999,
          active: false,
        }
      })
      return _tables
    }

    // await sleep(500)
    // return baseTableState
  } catch (e) {
    console.log(e)
  }
}

export const createTableReq = async () => {
  try {
    // Nothing to see here, move along now... !!
    // Just forwrding promise, no await here
    const res = createOnBoardEventTable()
    return res
  } catch (e) {
    console.log(e)
  }
}

export const deleteTableReq = async (id: string) => {
  try {
    const _id: OnBoardEventApiTypes.DeleteOnBoardEventInput = { id }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    await deleteEventLegacy(_id)
  } catch (e) {
    console.log(e)
  }
}

export const updateTablesReq = async () => {
  try {
    // Nothing to see here, move along now... !!
    // Just forwrding promise, no await here
    const res = updateOnBoardEventTables()
    return res
  } catch (e) {
    console.log(e)
  }
}

const actions = {
  initSnapshotTables: produce<PokerTables>((draft, action) => {
    // setTables(action.tables)
    draft.tables = action.tables
    // console.log('initSnapshotTables client', draft.tables)
  }),

  snapshotTables: produce<PokerTables>((draft, action) => {
    draft.tables = action.tables
  }),

  updateTables: produce<PokerTables>((draft) => {
    const { AllTablesDataArray } = CreateOnBoardEvent
    draft.tables = AllTablesDataArray
    console.log('updateTables client', draft.tables)
  }),

  createTable: produce<PokerTables>((draft) => {
    const { AllTablesDataArray } = CreateOnBoardEvent
    draft.tables = AllTablesDataArray
    console.log('createTable client', draft.tables)
  }),

  deleteTable: produce<PokerTables>((draft, action) => {
    // if (action.id !== '') onDeleteOnBoardEvent({id: action.tableData.id})
    const index = draft.tables.findIndex((table) => table.tableId === (action.tableData as TableData).tableId)
    if (index !== -1) draft.tables.splice(index, 1)
  }),

  activateTable: produce<PokerTables>((draft, action) => {
    // setActiveTableId(action.tableData.id)
    draft.tables.map((table) => {
      table.active = table.tableId === (action.tableData as TableData).tableId ?? false
    })
    // console.log('activateTable client', draft.tables)
  }),
}

export class PokerTables {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [action: string]: any

  [immerable] = true

  tables: Table[] = []
  // tables: Set<Table> = new Set()

  constructor(_tables: Table[]) {
    if (_tables) this.tables = _tables
    // if (_tables) this.tables = new Set([..._tables])
    // console.log('class PokerTables constructor',this.tables)
  }

  get allTables() {
    return this.tables
    // return Array.from(this.tables)
  }

  get initSnapshotTables() {
    // console.log('initSnapshotTables server',this.tables)
    return {
      source: 'PokerTables',
      action: PokerTables.ACTION_INIT_SNAPSHOT,
      tables: this.tables,
    }
  }

  get snapshotTables() {
    // console.log('snapshotTables',this.tables)
    return {
      source: 'PokerTables',
      action: PokerTables.ACTION_SNAPSHOT,
      tables: this.tables,
    }
  }

  get newTableName() {
    return generateTableName()
  }

  static get ACTION_INIT_SNAPSHOT() {
    return 'initSnapshotTables'
  }

  static get ACTION_SNAPSHOT() {
    return 'snapshotTables'
  }

  static get ACTION_UPDATE_TABLES() {
    return 'updateTables'
  }

  static get ACTION_CREATE_TABLE() {
    return 'createTable'
  }

  static get ACTION_DELETE_TABLE() {
    return 'deleteTable'
  }

  static get ACTION_ACTIVATE_TABLE() {
    return 'activateTable'
  }

  getActiveTable() {
    // return this.tables.find((it) => it.active === true)
    return Array.from(this.tables).find((it) => it.active === true)
  }

  getTableByID(TableId: PokerTables['id']) {
    // return this.tables.find((it) => it.id === TableId)
    return Array.from(this.tables).find((it) => it.id === TableId)
  }

  processAction(action: PokerAction) {
    if (!action || !action.action) throw new Error('action is undefined')

    // @ts-expect-error - Requires better Immer typing ?
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const producer: typeof produce = actions[action?.action as string]
    if (!producer) throw new Error(`unknown action ${action.action as string}`)

    // @ts-expect-error - Requires better Immer typing ?
    return producer(this, action)
  }
}
