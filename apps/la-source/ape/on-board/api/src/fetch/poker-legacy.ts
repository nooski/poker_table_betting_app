/* eslint-disable no-console */

import {
  GraphQLQuery,
  GraphQLResult,
  // AmplifyTypes,
  // AmplifyAPITypes
} from '@flexiness/aws'

const {
  AMPLIFY_API_CONFIG,
  // AMPLIFY_AUTH_CONFIG_V2,
  AMPLIFY_CLIENT,
  AMPLIFY_SERVER,
} = await import('@flexiness/aws')

const { AmplifyAPI } = AMPLIFY_CLIENT

const { Amplify } = AMPLIFY_SERVER

Amplify.configure(AMPLIFY_API_CONFIG)

const { generateClient } = AmplifyAPI

const client = generateClient()

// import {
//   createISSUE,
//   // updateISSUE,
//   deleteISSUE,
//   // getISSUE,
//   listISSUES,
// } from '../graphql/poker/includes/index.js';
const {
  // getISSUE,
  listISSUES,
  createISSUE,
  // updateISSUE,
  deleteISSUE,
} = await import('../graphql/poker/includes/index.js')

import {
  CreateISSUEInput,
  CreateISSUEMutation,
  // UpdateISSUEInput,
  // UpdateISSUEMutation,
  DeleteISSUEInput,
  DeleteISSUEMutation,
  ISSUE,
  ListISSUESQuery,
  ListISSUESQueryVariables,
} from '../graphql/poker/API.js'

type GetListISSUESQueryResult = {
  listISSUES: {
    items: ISSUE[]
    nextToken: string
  }
}

type CreateISSUEQueryResult = {
  createISSUE: ISSUE
}

// export const onListIssue = async (variables: ListISSUESQueryVariables) => {
//   // Fetch first 20 records
//   // const variables: ListTodosQueryVariables = {
//   //   limit: 20,
//   //   // add filter as needed
//   // }

//   try {
//     const result: GraphQLResult = await client.graphql<GraphQLQuery<ListISSUESQuery>>({
//       query: listISSUES,
//       variables: { input: variables },
//     })

//     // setIssueList((issues: Issue[]) => [...issues, { ...newIssue }])

//     // console.log('Successfully fetched page 1 of Issues!')

//     if (result?.data?.['listISSUES']) {
//       // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unused-vars
//       const { items, nextToken }: { items: ISSUE[]; nextToken: string } = result.data['listISSUES']
//       return items
//     }
//     return []
//   } catch (err) {
//     console.log('error: ', err)
//   }
// }

// https://stackoverflow.com/questions/54603992/how-do-i-set-typescript-types-for-an-aws-amplify-graphql-response-in-react
export const onListIssue = async (variables: ListISSUESQueryVariables) => {
  try {
    const result: GraphQLResult<GetListISSUESQueryResult> = (await client?.graphql<GraphQLQuery<ListISSUESQuery>>({
      query: listISSUES,
      variables: { input: variables },
    })) as {
      data: GetListISSUESQueryResult
    }

    if (result?.data?.listISSUES) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { items, nextToken } = result.data.listISSUES
      return items
    }
    return []
  } catch (err) {
    console.log('error: ', err)
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////

// export const onCreateIssue = async (newIssue: CreateISSUEInput) => {
//   // const newIssue = {
//   //   name: issue.name,
//   //   desc: issue.desc,
//   // }
//   try {
//     const result: GraphQLResult = await API.graphql<GraphQLQuery<CreateISSUEMutation>>({
//       query: createISSUE,
//       variables: { input: newIssue },
//     })

//     // setIssueList((issues: Issue[]) => [...issues, { ...newIssue }])

//     // console.log('Successfully created a new Issue!')

//     if (result?.data?.['createISSUE']) {
//       return result.data.createISSUE as ISSUE
//     }
//   } catch (err) {
//     console.log('error: ', err)
//   }
// }

export const onCreateIssue = async (newIssue: CreateISSUEInput) => {
  try {
    const result: GraphQLResult<CreateISSUEQueryResult> = (await client?.graphql<GraphQLQuery<CreateISSUEMutation>>({
      query: createISSUE,
      variables: { input: newIssue },
    })) as {
      data: CreateISSUEQueryResult
    }

    if (result?.data?.createISSUE) {
      return result.data.createISSUE
    }
  } catch (err) {
    console.log('error: ', err)
  }
}

// export const onCreateIssue = async (query: GraphQLQuery<CreateISSUEMutation>, newIssue: CreateISSUEInput) => {
//   try {
//     const result: GraphQLResult<CreateISSUEQueryResult> = (await client.graphql(
//       graphqlOperation({
//         query,
//         variables: { input: newIssue },
//       }),
//     )) as {
//       data: CreateISSUEQueryResult
//     }

//     if (result?.data?.createISSUE) {
//       return result.data.createISSUE
//     }
//   } catch (err) {
//     console.log('error: ', err)
//   }
// }

// ////////////////////////////////////////////////////////////////////////////////////////////////

export const onDeleteIssue = async (issueDetails: DeleteISSUEInput) => {
  // const issueDetails = {
  //   id: 'some_id'
  // }

  try {
    await client?.graphql<GraphQLQuery<DeleteISSUEMutation>>({
      query: deleteISSUE,
      variables: { input: issueDetails },
    })

    // setIssueList((issues: Issue[]) => [...issues, { ...newIssue }])

    console.log('Successfully deleted an existing Issue!')
  } catch (err) {
    console.log('error: ', err)
  }
}
