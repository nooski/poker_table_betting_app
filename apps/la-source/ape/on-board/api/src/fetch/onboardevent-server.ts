// /* eslint-disable no-console */

// import { isServer } from '@flexiness/domain-utils';
// import { Response } from 'node-fetch';

// class HTTPResponseError extends Error {
// 	constructor(response: Response) {
// 		super(`HTTP Error Response: ${response.status} ${response.statusText}`)
//     // @ts-expect-error
// 		this.response = response
// 	}
// }

// const checkStatus = (response: Response) => {
//   if (response.ok) {
//     // response.status >= 200 && response.status < 300
//     return response;
//   } else {
//     throw new HTTPResponseError(response);
//   }
// }

// import {
//   GraphQLQuery,
//   GraphQLResult
// } from '@flexiness/aws';

// import {
//   InitAmplifyConfigServer,
// } from '@flexiness/aws'

// const {
//   AMPLIFY_AUTH_CONFIG_V2,
//   AMPLIFY_SERVER,
//   initAmplifyConfigServer,
//   // cookieBasedClient,
//   reqResBasedClient,
//   runWithAmplifyServerContext,
// } = await import('@flexiness/aws')

// let clientSSR: InitAmplifyConfigServer['clientSSR']

// if (isServer) {
//   const { clientSSR, tokenProvider, credentialsProvider } = initAmplifyConfigServer('API-apiKey')
//   console.log('Graphql server client', clientSSR)
// }

// const {
//   Amplify,
//   AmplifyAuthServer,
//   AmplifyAPIServer,
//   AmplifyUtils,
//   // runWithAmplifyServerContext,
//   // signedFetch
// } = AMPLIFY_SERVER;

// // import { createOnBoardEvent, deleteOnBoardEvent, listOnBoardEvents } from '../graphql/onboardevent/includes/index.js';
// const { listOnBoardEvents, createOnBoardEvent, deleteOnBoardEvent } = await import('../graphql/onboardevent/includes/index.js')

// import {
//   ListOnBoardEventsQuery,
//   ListOnBoardEventsQueryVariables,
//   DeleteOnBoardEventInput,
//   DeleteOnBoardEventMutation,
// } from '../graphql/onboardevent/API.js';

// // https://stackoverflow.com/questions/54603992/how-do-i-set-typescript-types-for-an-aws-amplify-graphql-response-in-react
// // export const listEventsServer = async (variables: ListOnBoardEventsQueryVariables) => {
// //   try {
// //     const result: GraphQLResult<ListOnBoardEventsQuery> = (await clientSSR?.graphql<GraphQLQuery<ListOnBoardEventsQuery>>({
// //       query: listOnBoardEvents,
// //       variables: { input: variables },
// //     })) as {
// //       data: ListOnBoardEventsQuery
// //     }

// //     if (result?.data?.listOnBoardEvents) {
// //       // eslint-disable-next-line @typescript-eslint/no-unused-vars
// //       const { items, nextToken } = result.data.listOnBoardEvents
// //       return items
// //     }
// //     return []
// //   } catch (err) {
// //     console.log('error: ', err)
// //   }

//   // ////////////////////////////////////////////////////

//   //   const result: AmplifyAPITypes.GraphQLResult<ListOnBoardEventsQuery> = await runWithAmplifyServerContext({
//   //     nextServerContext: {
//   //       request: req as unknown as NextApiRequest,
//   //       response: res as unknown as NextApiResponse<ListOnBoardEventsQuery>,
//   //     },
//   //     operation: async (contextSpec) => {
//   //       return reqResBasedClient.graphql<AmplifyAPITypes.GraphQLQuery<ListOnBoardEventsQuery>>(contextSpec, {
//   //         query: listOnBoardEvents,
//   //         variables: { input: variables },
//   //       });
//   //     }
//   //   });

//   //   const result: AmplifyAPITypes.GraphQLResult<ListOnBoardEventsQuery> = await cookieBasedClient.graphql<AmplifyAPITypes.GraphQLQuery<ListOnBoardEventsQuery>>({
//   //     query: listOnBoardEvents,
//   //     variables: { input: variables },
//   //   })

//   //   if (result?.data?.listOnBoardEvents) {
//   //     // eslint-disable-next-line @typescript-eslint/no-unused-vars
//   //     const { items, nextToken } = result.data.listOnBoardEvents
//   //     return items
//   //   }
//   //   return []
//   // } catch (err) {
//   //   console.log('error: ', err)
//   // }

//   // ////////////////////////////////////////////////////

//   // const options = {
//   //   method: 'POST',
//   //   headers: {
//   //     'x-api-key': AMPLIFY_AUTH_CONFIG_V2.API?.GraphQL?.apiKey!,
//   //     'Content-Type': 'application/json'
//   //   },
//   //   body: JSON.stringify({
//   //     query: listOnBoardEvents,
//   //     variables: { input: variables },
//   //    })
//   // } as RequestInit

//   // const request = new Request(AMPLIFY_AUTH_CONFIG_V2.API?.GraphQL?.endpoint!, options)

//   // const response = await fetch(request)

//   // try {
//   //   checkStatus(response)
//   // } catch (error) {
//   //   console.error(error)
//   //   const errorBody = await (error as any).response.text()
//   //   console.error(`Error body: ${errorBody}`)
//   // }

//   // if (response.ok) JSON.stringify(await response.json())

//   // ////////////////////////////////////////////////////

//   // const response = await signedFetch({
//   //   query: listOnBoardEvents,
//   //   variables: { input: variables },
//   // }).then((res) => {
//   //   console.log(JSON.stringify(res))
//   //   return JSON.stringify(res)
//   // })

//   // return []

//   // ////////////////////////////////////////////////////

//   // let statusCode = 200
//   // let body: unknown
//   // let response: Response

//   // try {
//   //   response = await fetch(request)
//   //   body = await response.json()
//   //   if ((body as any).errors) statusCode = 400
//   // } catch (error: any) {
//   //   statusCode = 400
//   //   body = {
//   //     errors: [
//   //       {
//   //         // @ts-expect-error
//   //         status: response.status,
//   //         message: error.message,
//   //         stack: error.stack
//   //       }
//   //     ]
//   //   };
//   // }

//   // return {
//   //   statusCode,
//   //   body: JSON.stringify(body)
//   // }
// // }

// // ////////////////////////////////////////////////////////////////////////////////////////////////

// // export const createEventServer = async (variables: CreateOnBoardEventInput) => {
// //   try {
// //     const result: AmplifyAPITypes.GraphQLResult<CreateOnBoardEventMutation> = (await clientSSR?.graphql<AmplifyAPITypes.GraphQLQuery<CreateOnBoardEventMutation>>({
// //       query: createOnBoardEvent,
// //       variables: { input: variables },
// //     })) as {
// //       data: CreateOnBoardEventMutation
// //     }

// //     if (result?.data?.createOnBoardEvent) {
// //       return result.data.createOnBoardEvent
// //     }
// //     return null
// //   } catch (err) {
// //     console.log('error: ', err)
// //   }
// // }

// // // ////////////////////////////////////////////////////////////////////////////////////////////////

// // export const deleteEventServer = async (variables: DeleteOnBoardEventInput) => {
// //   try {
// //     await clientSSR?.graphql<GraphQLQuery<DeleteOnBoardEventMutation>>({
// //       query: deleteOnBoardEvent,
// //       variables: { input: variables },
// //     })

// //     console.log('Successfully deleted an existing Issue!')
// //   } catch (err) {
// //     console.log('error: ', err)
// //   }
// // }
