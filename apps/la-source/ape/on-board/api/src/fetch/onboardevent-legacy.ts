/* eslint-disable no-console */

// import { isServer } from '@flexiness/domain-utils'

// import { GraphQLQuery, GraphQLResult } from '@aws-amplify/api'
// import { API, Amplify } from 'aws-amplify'
// "@aws-amplify/api": "5.4.4",
// "@aws-amplify/api-graphql": "3.4.10",
// "@aws-amplify/auth": "5.6.4",
// "@aws-amplify/cache": "5.1.10",
// "@aws-amplify/cli": "^12.4.0",
// "@aws-amplify/core": "5.8.4",
// "@aws-amplify/datastore": "4.7.4",
// "@aws-crypto/sha256-browser": "5.0.0",
// "@aws-crypto/sha256-js": "5.0.0",
// "@aws-sdk/util-utf8-browser": "3.259.0",
// "aws-amplify": "5.3.10",

// @aws-amplify/cache@npm:5.1.12, @aws-amplify/core@npm:5.8.6, @aws-crypto/ie11-detection@npm:1.0.0, @aws-crypto/sha256-browser@npm:1.2.2, and 62 more.
// "@aws-amplify/api": "6.0.19",
// "@aws-amplify/api-graphql": "4.0.19",
// "@aws-amplify/auth": "6.0.19",
// "@aws-amplify/cache": "5.1.12",
// "@aws-amplify/core": "6.0.19",
// "@aws-amplify/datastore": "5.0.19",
// "@aws-crypto/sha256-browser": "5.2.0",
// "@aws-crypto/sha256-js": "5.2.0",
// "@aws-sdk/util-utf8-browser": "3.259.0",
// "@types/node": "20.12.7",
// "aws-amplify": "6.0.19",
// import { GraphQLQuery, GraphQLResult } from '@aws-amplify/api'
// import { GraphQLResult } from '@aws-amplify/api-graphql'

// import { Amplify, ResourcesConfig  } from 'aws-amplify';
// import { generateClient, Client } from 'aws-amplify/api';

// import { generateClient } from 'aws-amplify/api';
// import { V6Client } from '@aws-amplify/api-graphql';

import { AmplifyAPITypes, GraphQLQuery, GraphQLResult } from '@flexiness/aws'

const { initAmplifyConfigClient } = await import('@flexiness/aws')

let client: AmplifyAPITypes.Client | undefined

const { listOnBoardEvents, createOnBoardEvent, deleteOnBoardEvent } = await import('../graphql/onboardevent/includes/index.js')

import {
  CreateOnBoardEventInput,
  CreateOnBoardEventMutation,
  // UpdateOnBoardEventInput,
  // UpdateOnBoardEventMutation,
  DeleteOnBoardEventInput,
  DeleteOnBoardEventMutation,
  ListOnBoardEventsQuery,
  ListOnBoardEventsQueryVariables,
} from '../graphql/onboardevent/API.js'

// https://stackoverflow.com/questions/54603992/how-do-i-set-typescript-types-for-an-aws-amplify-graphql-response-in-react
export const listEventsLegacy = async (variables: ListOnBoardEventsQueryVariables) => {
  if (!client) client = await initAmplifyConfigClient('API-apiKey')
  try {
    const result: GraphQLResult<ListOnBoardEventsQuery> = (await client?.graphql<GraphQLQuery<ListOnBoardEventsQuery>>({
      query: listOnBoardEvents,
      variables,
    })) as {
      data: ListOnBoardEventsQuery
    }

    if (result?.data?.listOnBoardEvents) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { items, nextToken } = result.data.listOnBoardEvents
      return items
    }
    return []
  } catch (err) {
    console.log('error: ', err)
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////

export const createEventLegacy = async (variables: CreateOnBoardEventInput) => {
  if (!client) client = await initAmplifyConfigClient('API-apiKey')
  try {
    const result: GraphQLResult<CreateOnBoardEventMutation> = (await client?.graphql<GraphQLQuery<CreateOnBoardEventMutation>>({
      query: createOnBoardEvent,
      variables: { input: variables },
    })) as {
      data: CreateOnBoardEventMutation
    }

    if (result?.data?.createOnBoardEvent) {
      return result.data.createOnBoardEvent
    }
    return null
  } catch (err) {
    console.log('error: ', err)
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////

export const deleteEventLegacy = async (variables: DeleteOnBoardEventInput) => {
  if (!client) client = await initAmplifyConfigClient('API-apiKey')
  try {
    await client?.graphql<GraphQLQuery<DeleteOnBoardEventMutation>>({
      query: deleteOnBoardEvent,
      variables: { input: variables },
    })

    console.log('Successfully deleted an existing Issue!')
  } catch (err) {
    console.log('error: ', err)
  }
}
