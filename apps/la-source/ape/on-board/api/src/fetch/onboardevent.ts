/* eslint-disable no-console */
import { AmplifyAPITypes, GraphQLQuery, GraphQLResult } from '@flexiness/aws'

const { initAmplifyConfigClient } = await import('@flexiness/aws')

let client: AmplifyAPITypes.Client | undefined

const {
  listOnBoardEvents,
  createOnBoardEvent,
  updateOnBoardEvent,
  deleteOnBoardEvent,
  createUser,
  createOnBoardEventUsers,
  createTimes,
  createAttendees,
} = await import('../graphql/onboardevent/includes/index.js')

import {
  CreateAttendeesInput,
  CreateAttendeesMutation,
  CreateOnBoardEventInput,
  CreateOnBoardEventMutation,
  CreateOnBoardEventUsersInput,
  CreateOnBoardEventUsersMutation,
  CreateTimesInput,
  CreateTimesMutation,
  CreateUserInput,
  CreateUserMutation,
  DeleteOnBoardEventInput,
  DeleteOnBoardEventMutation,
  ListOnBoardEventsQuery,
  ListOnBoardEventsQueryVariables,
  UpdateOnBoardEventInput,
  UpdateOnBoardEventMutation,
} from '../graphql/onboardevent/API.js'

// https://stackoverflow.com/questions/54603992/how-do-i-set-typescript-types-for-an-aws-amplify-graphql-response-in-react
export const listEvents = async (variables: ListOnBoardEventsQueryVariables) => {
  if (!client) client = await initAmplifyConfigClient('API-userPool')
  try {
    const result: GraphQLResult<ListOnBoardEventsQuery> = (await client?.graphql<GraphQLQuery<ListOnBoardEventsQuery>>({
      query: listOnBoardEvents,
      variables,
    })) as {
      data: ListOnBoardEventsQuery
    }

    if (result?.data?.listOnBoardEvents) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { items, nextToken } = result.data.listOnBoardEvents
      return items
    }
    return []
  } catch (err) {
    console.log('error: ', err)
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////

export const createEvent = async (variables: CreateOnBoardEventInput) => {
  if (!client) client = await initAmplifyConfigClient('API-userPool')
  try {
    const result: GraphQLResult<CreateOnBoardEventMutation> = (await client?.graphql<GraphQLQuery<CreateOnBoardEventMutation>>({
      query: createOnBoardEvent,
      variables: { input: variables },
    })) as {
      data: CreateOnBoardEventMutation
    }

    if (result?.data?.createOnBoardEvent) {
      return result.data.createOnBoardEvent
    }
    return null
  } catch (err) {
    console.log('error: ', err)
  }
}

export const updateEvent = async (variables: UpdateOnBoardEventInput) => {
  if (!client) client = await initAmplifyConfigClient('API-userPool')
  try {
    const result: GraphQLResult<UpdateOnBoardEventMutation> = (await client?.graphql<GraphQLQuery<UpdateOnBoardEventMutation>>({
      query: updateOnBoardEvent,
      variables: { input: variables },
    })) as {
      data: UpdateOnBoardEventMutation
    }

    if (result?.data?.updateOnBoardEvent) {
      return result.data.updateOnBoardEvent
    }
    return null
  } catch (err) {
    console.log('error: ', err)
  }
}

export const createEventTimes = async (variables: CreateTimesInput) => {
  if (!client) client = await initAmplifyConfigClient('API-userPool')
  try {
    const result: GraphQLResult<CreateTimesMutation> = (await client?.graphql<GraphQLQuery<CreateTimesMutation>>({
      query: createTimes,
      variables: { input: variables },
    })) as {
      data: CreateTimesMutation
    }

    if (result?.data?.createTimes) {
      return result.data.createTimes
    }
    return null
  } catch (err) {
    console.log('error: ', err)
  }
}

export const createEventAttendees = async (variables: CreateAttendeesInput) => {
  if (!client) client = await initAmplifyConfigClient('API-userPool')
  try {
    const result: GraphQLResult<CreateAttendeesMutation> = (await client?.graphql<GraphQLQuery<CreateAttendeesMutation>>({
      query: createAttendees,
      variables: { input: variables },
    })) as {
      data: CreateAttendeesMutation
    }

    if (result?.data?.createAttendees) {
      return result.data.createAttendees
    }
    return null
  } catch (err) {
    console.log('error: ', err)
  }
}

export const createEventUser = async (variables: CreateUserInput) => {
  if (!client) client = await initAmplifyConfigClient('API-userPool')
  try {
    const result: GraphQLResult<CreateUserMutation> = (await client?.graphql<GraphQLQuery<CreateUserMutation>>({
      query: createUser,
      variables: { input: variables },
    })) as {
      data: CreateUserMutation
    }

    if (result?.data?.createUser) {
      return result.data.createUser
    }
    return null
  } catch (err) {
    console.log('error: ', err)
  }
}

export const createEventUsers = async (variables: CreateOnBoardEventUsersInput) => {
  if (!client) client = await initAmplifyConfigClient('API-userPool')
  try {
    const result: GraphQLResult<CreateOnBoardEventUsersMutation> = (await client?.graphql<GraphQLQuery<CreateOnBoardEventUsersMutation>>({
      query: createOnBoardEventUsers,
      variables: { input: variables },
    })) as {
      data: CreateOnBoardEventUsersMutation
    }

    if (result?.data?.createOnBoardEventUsers) {
      return result.data.createOnBoardEventUsers
    }
    return null
  } catch (err) {
    console.log('error: ', err)
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////

export const deleteEvent = async (variables: DeleteOnBoardEventInput) => {
  if (!client) client = await initAmplifyConfigClient('API-userPool')
  try {
    await client?.graphql<GraphQLQuery<DeleteOnBoardEventMutation>>({
      query: deleteOnBoardEvent,
      variables: { input: variables },
    })

    console.log('Successfully deleted an existing Issue!')
  } catch (err) {
    console.log('error: ', err)
  }
}
