/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

import * as APITypes from "../API";
type GeneratedQuery<InputType, OutputType> = string & {
  __generatedQueryInput: InputType;
  __generatedQueryOutput: OutputType;
};

export const getAttendees = /* GraphQL */ `query GetAttendees($id: ID!) {
  getAttendees(id: $id) {
    id
    created
    updated
    attendees {
      role
      address {
        city
        itinerary {
          __typename
        }
        location {
          latitude
          longitude
          __typename
        }
        postcode
        street1
        street2
        additional
        __typename
      }
      displayName
      firstName
      lastName
      school
      schoolLevel
      organizer
      status
      self
      endParticipation
      startParticipation
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    timesAttendeesId
    onBoardEventAttendeesId
    attendeesUserId
    owner
    __typename
  }
}
` as GeneratedQuery<
  APITypes.GetAttendeesQueryVariables,
  APITypes.GetAttendeesQuery
>;
export const listAttendees = /* GraphQL */ `query ListAttendees(
  $filter: ModelAttendeesFilterInput
  $limit: Int
  $nextToken: String
) {
  listAttendees(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      created
      updated
      attendees {
        role
        address {
          city
          postcode
          street1
          street2
          additional
          __typename
        }
        displayName
        firstName
        lastName
        school
        schoolLevel
        organizer
        status
        self
        endParticipation
        startParticipation
        __typename
      }
      event {
        id
        created
        updated
        description
        name
        activity
        eventType
        whichWay
        lifeCycle
        reoccur
        startDate
        endDate
        end {
          dateTime
          timeZone
          __typename
        }
        start {
          dateTime
          timeZone
          __typename
        }
        recurrence
        attendees {
          nextToken
          startedAt
          __typename
        }
        times {
          nextToken
          startedAt
          __typename
        }
        tags {
          nextToken
          startedAt
          __typename
        }
        creator {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        organizer {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        users {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventCreatorId
        onBoardEventOrganizerId
        owner
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      timesAttendeesId
      onBoardEventAttendeesId
      attendeesUserId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.ListAttendeesQueryVariables,
  APITypes.ListAttendeesQuery
>;
export const syncAttendees = /* GraphQL */ `query SyncAttendees(
  $filter: ModelAttendeesFilterInput
  $limit: Int
  $nextToken: String
  $lastSync: AWSTimestamp
) {
  syncAttendees(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
    lastSync: $lastSync
  ) {
    items {
      id
      created
      updated
      attendees {
        role
        address {
          city
          postcode
          street1
          street2
          additional
          __typename
        }
        displayName
        firstName
        lastName
        school
        schoolLevel
        organizer
        status
        self
        endParticipation
        startParticipation
        __typename
      }
      event {
        id
        created
        updated
        description
        name
        activity
        eventType
        whichWay
        lifeCycle
        reoccur
        startDate
        endDate
        end {
          dateTime
          timeZone
          __typename
        }
        start {
          dateTime
          timeZone
          __typename
        }
        recurrence
        attendees {
          nextToken
          startedAt
          __typename
        }
        times {
          nextToken
          startedAt
          __typename
        }
        tags {
          nextToken
          startedAt
          __typename
        }
        creator {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        organizer {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        users {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventCreatorId
        onBoardEventOrganizerId
        owner
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      timesAttendeesId
      onBoardEventAttendeesId
      attendeesUserId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.SyncAttendeesQueryVariables,
  APITypes.SyncAttendeesQuery
>;
export const getTimes = /* GraphQL */ `query GetTimes($id: ID!) {
  getTimes(id: $id) {
    id
    created
    updated
    times {
      day
      attendees {
        role
        address {
          city
          postcode
          street1
          street2
          additional
          __typename
        }
        displayName
        firstName
        lastName
        school
        schoolLevel
        organizer
        status
        self
        endParticipation
        startParticipation
        __typename
      }
      __typename
    }
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventTimesId
    timesUserId
    owner
    __typename
  }
}
` as GeneratedQuery<APITypes.GetTimesQueryVariables, APITypes.GetTimesQuery>;
export const listTimes = /* GraphQL */ `query ListTimes(
  $filter: ModelTimesFilterInput
  $limit: Int
  $nextToken: String
) {
  listTimes(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      created
      updated
      times {
        day
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        __typename
      }
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      event {
        id
        created
        updated
        description
        name
        activity
        eventType
        whichWay
        lifeCycle
        reoccur
        startDate
        endDate
        end {
          dateTime
          timeZone
          __typename
        }
        start {
          dateTime
          timeZone
          __typename
        }
        recurrence
        attendees {
          nextToken
          startedAt
          __typename
        }
        times {
          nextToken
          startedAt
          __typename
        }
        tags {
          nextToken
          startedAt
          __typename
        }
        creator {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        organizer {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        users {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventCreatorId
        onBoardEventOrganizerId
        owner
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventTimesId
      timesUserId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<APITypes.ListTimesQueryVariables, APITypes.ListTimesQuery>;
export const syncTimes = /* GraphQL */ `query SyncTimes(
  $filter: ModelTimesFilterInput
  $limit: Int
  $nextToken: String
  $lastSync: AWSTimestamp
) {
  syncTimes(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
    lastSync: $lastSync
  ) {
    items {
      id
      created
      updated
      times {
        day
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        __typename
      }
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      event {
        id
        created
        updated
        description
        name
        activity
        eventType
        whichWay
        lifeCycle
        reoccur
        startDate
        endDate
        end {
          dateTime
          timeZone
          __typename
        }
        start {
          dateTime
          timeZone
          __typename
        }
        recurrence
        attendees {
          nextToken
          startedAt
          __typename
        }
        times {
          nextToken
          startedAt
          __typename
        }
        tags {
          nextToken
          startedAt
          __typename
        }
        creator {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        organizer {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        users {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventCreatorId
        onBoardEventOrganizerId
        owner
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventTimesId
      timesUserId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<APITypes.SyncTimesQueryVariables, APITypes.SyncTimesQuery>;
export const getOnBoardEvent = /* GraphQL */ `query GetOnBoardEvent($id: ID!) {
  getOnBoardEvent(id: $id) {
    id
    created
    updated
    description
    name
    activity
    eventType
    whichWay
    lifeCycle
    reoccur
    startDate
    endDate
    end {
      dateTime
      timeZone
      __typename
    }
    start {
      dateTime
      timeZone
      __typename
    }
    recurrence
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    times {
      items {
        id
        created
        updated
        times {
          day
          __typename
        }
        attendees {
          nextToken
          startedAt
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventTimesId
        timesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    tags {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    creator {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    organizer {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    users {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventCreatorId
    onBoardEventOrganizerId
    owner
    __typename
  }
}
` as GeneratedQuery<
  APITypes.GetOnBoardEventQueryVariables,
  APITypes.GetOnBoardEventQuery
>;
export const listOnBoardEvents = /* GraphQL */ `query ListOnBoardEvents(
  $filter: ModelOnBoardEventFilterInput
  $limit: Int
  $nextToken: String
) {
  listOnBoardEvents(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.ListOnBoardEventsQueryVariables,
  APITypes.ListOnBoardEventsQuery
>;
export const syncOnBoardEvents = /* GraphQL */ `query SyncOnBoardEvents(
  $filter: ModelOnBoardEventFilterInput
  $limit: Int
  $nextToken: String
  $lastSync: AWSTimestamp
) {
  syncOnBoardEvents(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
    lastSync: $lastSync
  ) {
    items {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.SyncOnBoardEventsQueryVariables,
  APITypes.SyncOnBoardEventsQuery
>;
export const onboardEventByName = /* GraphQL */ `query OnboardEventByName(
  $name: String!
  $sortDirection: ModelSortDirection
  $filter: ModelOnBoardEventFilterInput
  $limit: Int
  $nextToken: String
) {
  onboardEventByName(
    name: $name
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnboardEventByNameQueryVariables,
  APITypes.OnboardEventByNameQuery
>;
export const onboardEventByActivity = /* GraphQL */ `query OnboardEventByActivity(
  $activity: EActivity!
  $sortDirection: ModelSortDirection
  $filter: ModelOnBoardEventFilterInput
  $limit: Int
  $nextToken: String
) {
  onboardEventByActivity(
    activity: $activity
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnboardEventByActivityQueryVariables,
  APITypes.OnboardEventByActivityQuery
>;
export const onboardEventByEventType = /* GraphQL */ `query OnboardEventByEventType(
  $eventType: EEventType!
  $sortDirection: ModelSortDirection
  $filter: ModelOnBoardEventFilterInput
  $limit: Int
  $nextToken: String
) {
  onboardEventByEventType(
    eventType: $eventType
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnboardEventByEventTypeQueryVariables,
  APITypes.OnboardEventByEventTypeQuery
>;
export const onboardEventByWhichWay = /* GraphQL */ `query OnboardEventByWhichWay(
  $whichWay: EWhichWay!
  $sortDirection: ModelSortDirection
  $filter: ModelOnBoardEventFilterInput
  $limit: Int
  $nextToken: String
) {
  onboardEventByWhichWay(
    whichWay: $whichWay
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnboardEventByWhichWayQueryVariables,
  APITypes.OnboardEventByWhichWayQuery
>;
export const getPost = /* GraphQL */ `query GetPost($id: ID!) {
  getPost(id: $id) {
    id
    created
    updated
    name
    poststatus
    comments {
      items {
        id
        content
        post {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        created
        updated
        _version
        _deleted
        _lastChangedAt
        postCommentsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    userPostsId
    owner
    __typename
  }
}
` as GeneratedQuery<APITypes.GetPostQueryVariables, APITypes.GetPostQuery>;
export const listPosts = /* GraphQL */ `query ListPosts(
  $filter: ModelPostFilterInput
  $limit: Int
  $nextToken: String
) {
  listPosts(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      created
      updated
      name
      poststatus
      comments {
        items {
          id
          content
          created
          updated
          _version
          _deleted
          _lastChangedAt
          postCommentsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      userPostsId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<APITypes.ListPostsQueryVariables, APITypes.ListPostsQuery>;
export const syncPosts = /* GraphQL */ `query SyncPosts(
  $filter: ModelPostFilterInput
  $limit: Int
  $nextToken: String
  $lastSync: AWSTimestamp
) {
  syncPosts(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
    lastSync: $lastSync
  ) {
    items {
      id
      created
      updated
      name
      poststatus
      comments {
        items {
          id
          content
          created
          updated
          _version
          _deleted
          _lastChangedAt
          postCommentsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      userPostsId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<APITypes.SyncPostsQueryVariables, APITypes.SyncPostsQuery>;
export const getComment = /* GraphQL */ `query GetComment($id: ID!) {
  getComment(id: $id) {
    id
    content
    post {
      id
      created
      updated
      name
      poststatus
      comments {
        items {
          id
          content
          created
          updated
          _version
          _deleted
          _lastChangedAt
          postCommentsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      userPostsId
      owner
      __typename
    }
    created
    updated
    _version
    _deleted
    _lastChangedAt
    postCommentsId
    owner
    __typename
  }
}
` as GeneratedQuery<
  APITypes.GetCommentQueryVariables,
  APITypes.GetCommentQuery
>;
export const listComments = /* GraphQL */ `query ListComments(
  $filter: ModelCommentFilterInput
  $limit: Int
  $nextToken: String
) {
  listComments(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      content
      post {
        id
        created
        updated
        name
        poststatus
        comments {
          nextToken
          startedAt
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        userPostsId
        owner
        __typename
      }
      created
      updated
      _version
      _deleted
      _lastChangedAt
      postCommentsId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.ListCommentsQueryVariables,
  APITypes.ListCommentsQuery
>;
export const syncComments = /* GraphQL */ `query SyncComments(
  $filter: ModelCommentFilterInput
  $limit: Int
  $nextToken: String
  $lastSync: AWSTimestamp
) {
  syncComments(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
    lastSync: $lastSync
  ) {
    items {
      id
      content
      post {
        id
        created
        updated
        name
        poststatus
        comments {
          nextToken
          startedAt
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        userPostsId
        owner
        __typename
      }
      created
      updated
      _version
      _deleted
      _lastChangedAt
      postCommentsId
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.SyncCommentsQueryVariables,
  APITypes.SyncCommentsQuery
>;
export const getUser = /* GraphQL */ `query GetUser($id: ID!) {
  getUser(id: $id) {
    id
    created
    updated
    username
    displayName
    firstName
    lastName
    email
    phoneNumber
    posts {
      items {
        id
        created
        updated
        name
        poststatus
        comments {
          nextToken
          startedAt
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        userPostsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    events {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedQuery<APITypes.GetUserQueryVariables, APITypes.GetUserQuery>;
export const listUsers = /* GraphQL */ `query ListUsers(
  $filter: ModelUserFilterInput
  $limit: Int
  $nextToken: String
) {
  listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<APITypes.ListUsersQueryVariables, APITypes.ListUsersQuery>;
export const syncUsers = /* GraphQL */ `query SyncUsers(
  $filter: ModelUserFilterInput
  $limit: Int
  $nextToken: String
  $lastSync: AWSTimestamp
) {
  syncUsers(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
    lastSync: $lastSync
  ) {
    items {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<APITypes.SyncUsersQueryVariables, APITypes.SyncUsersQuery>;
export const onboardUsersByUsername = /* GraphQL */ `query OnboardUsersByUsername(
  $username: String!
  $sortDirection: ModelSortDirection
  $filter: ModelUserFilterInput
  $limit: Int
  $nextToken: String
) {
  onboardUsersByUsername(
    username: $username
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnboardUsersByUsernameQueryVariables,
  APITypes.OnboardUsersByUsernameQuery
>;
export const onboardUsersByDisplayName = /* GraphQL */ `query OnboardUsersByDisplayName(
  $displayName: String!
  $sortDirection: ModelSortDirection
  $filter: ModelUserFilterInput
  $limit: Int
  $nextToken: String
) {
  onboardUsersByDisplayName(
    displayName: $displayName
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnboardUsersByDisplayNameQueryVariables,
  APITypes.OnboardUsersByDisplayNameQuery
>;
export const onboardUsersByFirstName = /* GraphQL */ `query OnboardUsersByFirstName(
  $firstName: String!
  $sortDirection: ModelSortDirection
  $filter: ModelUserFilterInput
  $limit: Int
  $nextToken: String
) {
  onboardUsersByFirstName(
    firstName: $firstName
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnboardUsersByFirstNameQueryVariables,
  APITypes.OnboardUsersByFirstNameQuery
>;
export const onboardUsersByLastName = /* GraphQL */ `query OnboardUsersByLastName(
  $lastName: String!
  $sortDirection: ModelSortDirection
  $filter: ModelUserFilterInput
  $limit: Int
  $nextToken: String
) {
  onboardUsersByLastName(
    lastName: $lastName
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnboardUsersByLastNameQueryVariables,
  APITypes.OnboardUsersByLastNameQuery
>;
export const onboardUsersByEmail = /* GraphQL */ `query OnboardUsersByEmail(
  $email: AWSEmail!
  $sortDirection: ModelSortDirection
  $filter: ModelUserFilterInput
  $limit: Int
  $nextToken: String
) {
  onboardUsersByEmail(
    email: $email
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnboardUsersByEmailQueryVariables,
  APITypes.OnboardUsersByEmailQuery
>;
export const onboardUsersByPhoneNumber = /* GraphQL */ `query OnboardUsersByPhoneNumber(
  $phoneNumber: AWSPhone!
  $sortDirection: ModelSortDirection
  $filter: ModelUserFilterInput
  $limit: Int
  $nextToken: String
) {
  onboardUsersByPhoneNumber(
    phoneNumber: $phoneNumber
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnboardUsersByPhoneNumberQueryVariables,
  APITypes.OnboardUsersByPhoneNumberQuery
>;
export const getTag = /* GraphQL */ `query GetTag($id: ID!) {
  getTag(id: $id) {
    id
    created
    updated
    name
    events {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedQuery<APITypes.GetTagQueryVariables, APITypes.GetTagQuery>;
export const listTags = /* GraphQL */ `query ListTags($filter: ModelTagFilterInput, $limit: Int, $nextToken: String) {
  listTags(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      created
      updated
      name
      events {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<APITypes.ListTagsQueryVariables, APITypes.ListTagsQuery>;
export const syncTags = /* GraphQL */ `query SyncTags(
  $filter: ModelTagFilterInput
  $limit: Int
  $nextToken: String
  $lastSync: AWSTimestamp
) {
  syncTags(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
    lastSync: $lastSync
  ) {
    items {
      id
      created
      updated
      name
      events {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<APITypes.SyncTagsQueryVariables, APITypes.SyncTagsQuery>;
export const getOnBoardEventTags = /* GraphQL */ `query GetOnBoardEventTags($id: ID!) {
  getOnBoardEventTags(id: $id) {
    id
    onBoardEventId
    tagId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    tag {
      id
      created
      updated
      name
      events {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedQuery<
  APITypes.GetOnBoardEventTagsQueryVariables,
  APITypes.GetOnBoardEventTagsQuery
>;
export const listOnBoardEventTags = /* GraphQL */ `query ListOnBoardEventTags(
  $filter: ModelOnBoardEventTagsFilterInput
  $limit: Int
  $nextToken: String
) {
  listOnBoardEventTags(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      onBoardEventId
      tagId
      onBoardEvent {
        id
        created
        updated
        description
        name
        activity
        eventType
        whichWay
        lifeCycle
        reoccur
        startDate
        endDate
        end {
          dateTime
          timeZone
          __typename
        }
        start {
          dateTime
          timeZone
          __typename
        }
        recurrence
        attendees {
          nextToken
          startedAt
          __typename
        }
        times {
          nextToken
          startedAt
          __typename
        }
        tags {
          nextToken
          startedAt
          __typename
        }
        creator {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        organizer {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        users {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventCreatorId
        onBoardEventOrganizerId
        owner
        __typename
      }
      tag {
        id
        created
        updated
        name
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.ListOnBoardEventTagsQueryVariables,
  APITypes.ListOnBoardEventTagsQuery
>;
export const syncOnBoardEventTags = /* GraphQL */ `query SyncOnBoardEventTags(
  $filter: ModelOnBoardEventTagsFilterInput
  $limit: Int
  $nextToken: String
  $lastSync: AWSTimestamp
) {
  syncOnBoardEventTags(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
    lastSync: $lastSync
  ) {
    items {
      id
      onBoardEventId
      tagId
      onBoardEvent {
        id
        created
        updated
        description
        name
        activity
        eventType
        whichWay
        lifeCycle
        reoccur
        startDate
        endDate
        end {
          dateTime
          timeZone
          __typename
        }
        start {
          dateTime
          timeZone
          __typename
        }
        recurrence
        attendees {
          nextToken
          startedAt
          __typename
        }
        times {
          nextToken
          startedAt
          __typename
        }
        tags {
          nextToken
          startedAt
          __typename
        }
        creator {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        organizer {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        users {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventCreatorId
        onBoardEventOrganizerId
        owner
        __typename
      }
      tag {
        id
        created
        updated
        name
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.SyncOnBoardEventTagsQueryVariables,
  APITypes.SyncOnBoardEventTagsQuery
>;
export const onBoardEventTagsByOnBoardEventId = /* GraphQL */ `query OnBoardEventTagsByOnBoardEventId(
  $onBoardEventId: ID!
  $sortDirection: ModelSortDirection
  $filter: ModelOnBoardEventTagsFilterInput
  $limit: Int
  $nextToken: String
) {
  onBoardEventTagsByOnBoardEventId(
    onBoardEventId: $onBoardEventId
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      onBoardEventId
      tagId
      onBoardEvent {
        id
        created
        updated
        description
        name
        activity
        eventType
        whichWay
        lifeCycle
        reoccur
        startDate
        endDate
        end {
          dateTime
          timeZone
          __typename
        }
        start {
          dateTime
          timeZone
          __typename
        }
        recurrence
        attendees {
          nextToken
          startedAt
          __typename
        }
        times {
          nextToken
          startedAt
          __typename
        }
        tags {
          nextToken
          startedAt
          __typename
        }
        creator {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        organizer {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        users {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventCreatorId
        onBoardEventOrganizerId
        owner
        __typename
      }
      tag {
        id
        created
        updated
        name
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnBoardEventTagsByOnBoardEventIdQueryVariables,
  APITypes.OnBoardEventTagsByOnBoardEventIdQuery
>;
export const onBoardEventTagsByTagId = /* GraphQL */ `query OnBoardEventTagsByTagId(
  $tagId: ID!
  $sortDirection: ModelSortDirection
  $filter: ModelOnBoardEventTagsFilterInput
  $limit: Int
  $nextToken: String
) {
  onBoardEventTagsByTagId(
    tagId: $tagId
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      onBoardEventId
      tagId
      onBoardEvent {
        id
        created
        updated
        description
        name
        activity
        eventType
        whichWay
        lifeCycle
        reoccur
        startDate
        endDate
        end {
          dateTime
          timeZone
          __typename
        }
        start {
          dateTime
          timeZone
          __typename
        }
        recurrence
        attendees {
          nextToken
          startedAt
          __typename
        }
        times {
          nextToken
          startedAt
          __typename
        }
        tags {
          nextToken
          startedAt
          __typename
        }
        creator {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        organizer {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        users {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventCreatorId
        onBoardEventOrganizerId
        owner
        __typename
      }
      tag {
        id
        created
        updated
        name
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnBoardEventTagsByTagIdQueryVariables,
  APITypes.OnBoardEventTagsByTagIdQuery
>;
export const getOnBoardEventUsers = /* GraphQL */ `query GetOnBoardEventUsers($id: ID!) {
  getOnBoardEventUsers(id: $id) {
    id
    onBoardEventId
    userId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedQuery<
  APITypes.GetOnBoardEventUsersQueryVariables,
  APITypes.GetOnBoardEventUsersQuery
>;
export const listOnBoardEventUsers = /* GraphQL */ `query ListOnBoardEventUsers(
  $filter: ModelOnBoardEventUsersFilterInput
  $limit: Int
  $nextToken: String
) {
  listOnBoardEventUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      onBoardEventId
      userId
      onBoardEvent {
        id
        created
        updated
        description
        name
        activity
        eventType
        whichWay
        lifeCycle
        reoccur
        startDate
        endDate
        end {
          dateTime
          timeZone
          __typename
        }
        start {
          dateTime
          timeZone
          __typename
        }
        recurrence
        attendees {
          nextToken
          startedAt
          __typename
        }
        times {
          nextToken
          startedAt
          __typename
        }
        tags {
          nextToken
          startedAt
          __typename
        }
        creator {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        organizer {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        users {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventCreatorId
        onBoardEventOrganizerId
        owner
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.ListOnBoardEventUsersQueryVariables,
  APITypes.ListOnBoardEventUsersQuery
>;
export const syncOnBoardEventUsers = /* GraphQL */ `query SyncOnBoardEventUsers(
  $filter: ModelOnBoardEventUsersFilterInput
  $limit: Int
  $nextToken: String
  $lastSync: AWSTimestamp
) {
  syncOnBoardEventUsers(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
    lastSync: $lastSync
  ) {
    items {
      id
      onBoardEventId
      userId
      onBoardEvent {
        id
        created
        updated
        description
        name
        activity
        eventType
        whichWay
        lifeCycle
        reoccur
        startDate
        endDate
        end {
          dateTime
          timeZone
          __typename
        }
        start {
          dateTime
          timeZone
          __typename
        }
        recurrence
        attendees {
          nextToken
          startedAt
          __typename
        }
        times {
          nextToken
          startedAt
          __typename
        }
        tags {
          nextToken
          startedAt
          __typename
        }
        creator {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        organizer {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        users {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventCreatorId
        onBoardEventOrganizerId
        owner
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.SyncOnBoardEventUsersQueryVariables,
  APITypes.SyncOnBoardEventUsersQuery
>;
export const onBoardEventUsersByOnBoardEventId = /* GraphQL */ `query OnBoardEventUsersByOnBoardEventId(
  $onBoardEventId: ID!
  $sortDirection: ModelSortDirection
  $filter: ModelOnBoardEventUsersFilterInput
  $limit: Int
  $nextToken: String
) {
  onBoardEventUsersByOnBoardEventId(
    onBoardEventId: $onBoardEventId
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      onBoardEventId
      userId
      onBoardEvent {
        id
        created
        updated
        description
        name
        activity
        eventType
        whichWay
        lifeCycle
        reoccur
        startDate
        endDate
        end {
          dateTime
          timeZone
          __typename
        }
        start {
          dateTime
          timeZone
          __typename
        }
        recurrence
        attendees {
          nextToken
          startedAt
          __typename
        }
        times {
          nextToken
          startedAt
          __typename
        }
        tags {
          nextToken
          startedAt
          __typename
        }
        creator {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        organizer {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        users {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventCreatorId
        onBoardEventOrganizerId
        owner
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnBoardEventUsersByOnBoardEventIdQueryVariables,
  APITypes.OnBoardEventUsersByOnBoardEventIdQuery
>;
export const onBoardEventUsersByUserId = /* GraphQL */ `query OnBoardEventUsersByUserId(
  $userId: ID!
  $sortDirection: ModelSortDirection
  $filter: ModelOnBoardEventUsersFilterInput
  $limit: Int
  $nextToken: String
) {
  onBoardEventUsersByUserId(
    userId: $userId
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      onBoardEventId
      userId
      onBoardEvent {
        id
        created
        updated
        description
        name
        activity
        eventType
        whichWay
        lifeCycle
        reoccur
        startDate
        endDate
        end {
          dateTime
          timeZone
          __typename
        }
        start {
          dateTime
          timeZone
          __typename
        }
        recurrence
        attendees {
          nextToken
          startedAt
          __typename
        }
        times {
          nextToken
          startedAt
          __typename
        }
        tags {
          nextToken
          startedAt
          __typename
        }
        creator {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        organizer {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        users {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventCreatorId
        onBoardEventOrganizerId
        owner
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    nextToken
    startedAt
    __typename
  }
}
` as GeneratedQuery<
  APITypes.OnBoardEventUsersByUserIdQueryVariables,
  APITypes.OnBoardEventUsersByUserIdQuery
>;
