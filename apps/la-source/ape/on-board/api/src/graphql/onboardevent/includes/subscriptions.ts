/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

import * as APITypes from "../API";
type GeneratedSubscription<InputType, OutputType> = string & {
  __generatedSubscriptionInput: InputType;
  __generatedSubscriptionOutput: OutputType;
};

export const onCreateAttendees = /* GraphQL */ `subscription OnCreateAttendees(
  $filter: ModelSubscriptionAttendeesFilterInput
  $owner: String
) {
  onCreateAttendees(filter: $filter, owner: $owner) {
    id
    created
    updated
    attendees {
      role
      address {
        city
        itinerary {
          __typename
        }
        location {
          latitude
          longitude
          __typename
        }
        postcode
        street1
        street2
        additional
        __typename
      }
      displayName
      firstName
      lastName
      school
      schoolLevel
      organizer
      status
      self
      endParticipation
      startParticipation
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    timesAttendeesId
    onBoardEventAttendeesId
    attendeesUserId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnCreateAttendeesSubscriptionVariables,
  APITypes.OnCreateAttendeesSubscription
>;
export const onUpdateAttendees = /* GraphQL */ `subscription OnUpdateAttendees(
  $filter: ModelSubscriptionAttendeesFilterInput
  $owner: String
) {
  onUpdateAttendees(filter: $filter, owner: $owner) {
    id
    created
    updated
    attendees {
      role
      address {
        city
        itinerary {
          __typename
        }
        location {
          latitude
          longitude
          __typename
        }
        postcode
        street1
        street2
        additional
        __typename
      }
      displayName
      firstName
      lastName
      school
      schoolLevel
      organizer
      status
      self
      endParticipation
      startParticipation
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    timesAttendeesId
    onBoardEventAttendeesId
    attendeesUserId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnUpdateAttendeesSubscriptionVariables,
  APITypes.OnUpdateAttendeesSubscription
>;
export const onDeleteAttendees = /* GraphQL */ `subscription OnDeleteAttendees(
  $filter: ModelSubscriptionAttendeesFilterInput
  $owner: String
) {
  onDeleteAttendees(filter: $filter, owner: $owner) {
    id
    created
    updated
    attendees {
      role
      address {
        city
        itinerary {
          __typename
        }
        location {
          latitude
          longitude
          __typename
        }
        postcode
        street1
        street2
        additional
        __typename
      }
      displayName
      firstName
      lastName
      school
      schoolLevel
      organizer
      status
      self
      endParticipation
      startParticipation
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    timesAttendeesId
    onBoardEventAttendeesId
    attendeesUserId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnDeleteAttendeesSubscriptionVariables,
  APITypes.OnDeleteAttendeesSubscription
>;
export const onCreateTimes = /* GraphQL */ `subscription OnCreateTimes(
  $filter: ModelSubscriptionTimesFilterInput
  $owner: String
) {
  onCreateTimes(filter: $filter, owner: $owner) {
    id
    created
    updated
    times {
      day
      attendees {
        role
        address {
          city
          postcode
          street1
          street2
          additional
          __typename
        }
        displayName
        firstName
        lastName
        school
        schoolLevel
        organizer
        status
        self
        endParticipation
        startParticipation
        __typename
      }
      __typename
    }
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventTimesId
    timesUserId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnCreateTimesSubscriptionVariables,
  APITypes.OnCreateTimesSubscription
>;
export const onUpdateTimes = /* GraphQL */ `subscription OnUpdateTimes(
  $filter: ModelSubscriptionTimesFilterInput
  $owner: String
) {
  onUpdateTimes(filter: $filter, owner: $owner) {
    id
    created
    updated
    times {
      day
      attendees {
        role
        address {
          city
          postcode
          street1
          street2
          additional
          __typename
        }
        displayName
        firstName
        lastName
        school
        schoolLevel
        organizer
        status
        self
        endParticipation
        startParticipation
        __typename
      }
      __typename
    }
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventTimesId
    timesUserId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnUpdateTimesSubscriptionVariables,
  APITypes.OnUpdateTimesSubscription
>;
export const onDeleteTimes = /* GraphQL */ `subscription OnDeleteTimes(
  $filter: ModelSubscriptionTimesFilterInput
  $owner: String
) {
  onDeleteTimes(filter: $filter, owner: $owner) {
    id
    created
    updated
    times {
      day
      attendees {
        role
        address {
          city
          postcode
          street1
          street2
          additional
          __typename
        }
        displayName
        firstName
        lastName
        school
        schoolLevel
        organizer
        status
        self
        endParticipation
        startParticipation
        __typename
      }
      __typename
    }
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventTimesId
    timesUserId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnDeleteTimesSubscriptionVariables,
  APITypes.OnDeleteTimesSubscription
>;
export const onCreateOnBoardEvent = /* GraphQL */ `subscription OnCreateOnBoardEvent(
  $filter: ModelSubscriptionOnBoardEventFilterInput
  $owner: String
) {
  onCreateOnBoardEvent(filter: $filter, owner: $owner) {
    id
    created
    updated
    description
    name
    activity
    eventType
    whichWay
    lifeCycle
    reoccur
    startDate
    endDate
    end {
      dateTime
      timeZone
      __typename
    }
    start {
      dateTime
      timeZone
      __typename
    }
    recurrence
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    times {
      items {
        id
        created
        updated
        times {
          day
          __typename
        }
        attendees {
          nextToken
          startedAt
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventTimesId
        timesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    tags {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    creator {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    organizer {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    users {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventCreatorId
    onBoardEventOrganizerId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnCreateOnBoardEventSubscriptionVariables,
  APITypes.OnCreateOnBoardEventSubscription
>;
export const onUpdateOnBoardEvent = /* GraphQL */ `subscription OnUpdateOnBoardEvent(
  $filter: ModelSubscriptionOnBoardEventFilterInput
  $owner: String
) {
  onUpdateOnBoardEvent(filter: $filter, owner: $owner) {
    id
    created
    updated
    description
    name
    activity
    eventType
    whichWay
    lifeCycle
    reoccur
    startDate
    endDate
    end {
      dateTime
      timeZone
      __typename
    }
    start {
      dateTime
      timeZone
      __typename
    }
    recurrence
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    times {
      items {
        id
        created
        updated
        times {
          day
          __typename
        }
        attendees {
          nextToken
          startedAt
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventTimesId
        timesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    tags {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    creator {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    organizer {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    users {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventCreatorId
    onBoardEventOrganizerId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnUpdateOnBoardEventSubscriptionVariables,
  APITypes.OnUpdateOnBoardEventSubscription
>;
export const onDeleteOnBoardEvent = /* GraphQL */ `subscription OnDeleteOnBoardEvent(
  $filter: ModelSubscriptionOnBoardEventFilterInput
  $owner: String
) {
  onDeleteOnBoardEvent(filter: $filter, owner: $owner) {
    id
    created
    updated
    description
    name
    activity
    eventType
    whichWay
    lifeCycle
    reoccur
    startDate
    endDate
    end {
      dateTime
      timeZone
      __typename
    }
    start {
      dateTime
      timeZone
      __typename
    }
    recurrence
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    times {
      items {
        id
        created
        updated
        times {
          day
          __typename
        }
        attendees {
          nextToken
          startedAt
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventTimesId
        timesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    tags {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    creator {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    organizer {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    users {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventCreatorId
    onBoardEventOrganizerId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnDeleteOnBoardEventSubscriptionVariables,
  APITypes.OnDeleteOnBoardEventSubscription
>;
export const onCreatePost = /* GraphQL */ `subscription OnCreatePost(
  $filter: ModelSubscriptionPostFilterInput
  $owner: String
) {
  onCreatePost(filter: $filter, owner: $owner) {
    id
    created
    updated
    name
    poststatus
    comments {
      items {
        id
        content
        post {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        created
        updated
        _version
        _deleted
        _lastChangedAt
        postCommentsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    userPostsId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnCreatePostSubscriptionVariables,
  APITypes.OnCreatePostSubscription
>;
export const onUpdatePost = /* GraphQL */ `subscription OnUpdatePost(
  $filter: ModelSubscriptionPostFilterInput
  $owner: String
) {
  onUpdatePost(filter: $filter, owner: $owner) {
    id
    created
    updated
    name
    poststatus
    comments {
      items {
        id
        content
        post {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        created
        updated
        _version
        _deleted
        _lastChangedAt
        postCommentsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    userPostsId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnUpdatePostSubscriptionVariables,
  APITypes.OnUpdatePostSubscription
>;
export const onDeletePost = /* GraphQL */ `subscription OnDeletePost(
  $filter: ModelSubscriptionPostFilterInput
  $owner: String
) {
  onDeletePost(filter: $filter, owner: $owner) {
    id
    created
    updated
    name
    poststatus
    comments {
      items {
        id
        content
        post {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        created
        updated
        _version
        _deleted
        _lastChangedAt
        postCommentsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    userPostsId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnDeletePostSubscriptionVariables,
  APITypes.OnDeletePostSubscription
>;
export const onCreateComment = /* GraphQL */ `subscription OnCreateComment(
  $filter: ModelSubscriptionCommentFilterInput
  $owner: String
) {
  onCreateComment(filter: $filter, owner: $owner) {
    id
    content
    post {
      id
      created
      updated
      name
      poststatus
      comments {
        items {
          id
          content
          created
          updated
          _version
          _deleted
          _lastChangedAt
          postCommentsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      userPostsId
      owner
      __typename
    }
    created
    updated
    _version
    _deleted
    _lastChangedAt
    postCommentsId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnCreateCommentSubscriptionVariables,
  APITypes.OnCreateCommentSubscription
>;
export const onUpdateComment = /* GraphQL */ `subscription OnUpdateComment(
  $filter: ModelSubscriptionCommentFilterInput
  $owner: String
) {
  onUpdateComment(filter: $filter, owner: $owner) {
    id
    content
    post {
      id
      created
      updated
      name
      poststatus
      comments {
        items {
          id
          content
          created
          updated
          _version
          _deleted
          _lastChangedAt
          postCommentsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      userPostsId
      owner
      __typename
    }
    created
    updated
    _version
    _deleted
    _lastChangedAt
    postCommentsId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnUpdateCommentSubscriptionVariables,
  APITypes.OnUpdateCommentSubscription
>;
export const onDeleteComment = /* GraphQL */ `subscription OnDeleteComment(
  $filter: ModelSubscriptionCommentFilterInput
  $owner: String
) {
  onDeleteComment(filter: $filter, owner: $owner) {
    id
    content
    post {
      id
      created
      updated
      name
      poststatus
      comments {
        items {
          id
          content
          created
          updated
          _version
          _deleted
          _lastChangedAt
          postCommentsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      userPostsId
      owner
      __typename
    }
    created
    updated
    _version
    _deleted
    _lastChangedAt
    postCommentsId
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnDeleteCommentSubscriptionVariables,
  APITypes.OnDeleteCommentSubscription
>;
export const onCreateUser = /* GraphQL */ `subscription OnCreateUser(
  $filter: ModelSubscriptionUserFilterInput
  $owner: String
) {
  onCreateUser(filter: $filter, owner: $owner) {
    id
    created
    updated
    username
    displayName
    firstName
    lastName
    email
    phoneNumber
    posts {
      items {
        id
        created
        updated
        name
        poststatus
        comments {
          nextToken
          startedAt
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        userPostsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    events {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnCreateUserSubscriptionVariables,
  APITypes.OnCreateUserSubscription
>;
export const onUpdateUser = /* GraphQL */ `subscription OnUpdateUser(
  $filter: ModelSubscriptionUserFilterInput
  $owner: String
) {
  onUpdateUser(filter: $filter, owner: $owner) {
    id
    created
    updated
    username
    displayName
    firstName
    lastName
    email
    phoneNumber
    posts {
      items {
        id
        created
        updated
        name
        poststatus
        comments {
          nextToken
          startedAt
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        userPostsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    events {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnUpdateUserSubscriptionVariables,
  APITypes.OnUpdateUserSubscription
>;
export const onDeleteUser = /* GraphQL */ `subscription OnDeleteUser(
  $filter: ModelSubscriptionUserFilterInput
  $owner: String
) {
  onDeleteUser(filter: $filter, owner: $owner) {
    id
    created
    updated
    username
    displayName
    firstName
    lastName
    email
    phoneNumber
    posts {
      items {
        id
        created
        updated
        name
        poststatus
        comments {
          nextToken
          startedAt
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        userPostsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    events {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnDeleteUserSubscriptionVariables,
  APITypes.OnDeleteUserSubscription
>;
export const onCreateTag = /* GraphQL */ `subscription OnCreateTag(
  $filter: ModelSubscriptionTagFilterInput
  $owner: String
) {
  onCreateTag(filter: $filter, owner: $owner) {
    id
    created
    updated
    name
    events {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnCreateTagSubscriptionVariables,
  APITypes.OnCreateTagSubscription
>;
export const onUpdateTag = /* GraphQL */ `subscription OnUpdateTag(
  $filter: ModelSubscriptionTagFilterInput
  $owner: String
) {
  onUpdateTag(filter: $filter, owner: $owner) {
    id
    created
    updated
    name
    events {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnUpdateTagSubscriptionVariables,
  APITypes.OnUpdateTagSubscription
>;
export const onDeleteTag = /* GraphQL */ `subscription OnDeleteTag(
  $filter: ModelSubscriptionTagFilterInput
  $owner: String
) {
  onDeleteTag(filter: $filter, owner: $owner) {
    id
    created
    updated
    name
    events {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnDeleteTagSubscriptionVariables,
  APITypes.OnDeleteTagSubscription
>;
export const onCreateOnBoardEventTags = /* GraphQL */ `subscription OnCreateOnBoardEventTags(
  $filter: ModelSubscriptionOnBoardEventTagsFilterInput
  $owner: String
) {
  onCreateOnBoardEventTags(filter: $filter, owner: $owner) {
    id
    onBoardEventId
    tagId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    tag {
      id
      created
      updated
      name
      events {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnCreateOnBoardEventTagsSubscriptionVariables,
  APITypes.OnCreateOnBoardEventTagsSubscription
>;
export const onUpdateOnBoardEventTags = /* GraphQL */ `subscription OnUpdateOnBoardEventTags(
  $filter: ModelSubscriptionOnBoardEventTagsFilterInput
  $owner: String
) {
  onUpdateOnBoardEventTags(filter: $filter, owner: $owner) {
    id
    onBoardEventId
    tagId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    tag {
      id
      created
      updated
      name
      events {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnUpdateOnBoardEventTagsSubscriptionVariables,
  APITypes.OnUpdateOnBoardEventTagsSubscription
>;
export const onDeleteOnBoardEventTags = /* GraphQL */ `subscription OnDeleteOnBoardEventTags(
  $filter: ModelSubscriptionOnBoardEventTagsFilterInput
  $owner: String
) {
  onDeleteOnBoardEventTags(filter: $filter, owner: $owner) {
    id
    onBoardEventId
    tagId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    tag {
      id
      created
      updated
      name
      events {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnDeleteOnBoardEventTagsSubscriptionVariables,
  APITypes.OnDeleteOnBoardEventTagsSubscription
>;
export const onCreateOnBoardEventUsers = /* GraphQL */ `subscription OnCreateOnBoardEventUsers(
  $filter: ModelSubscriptionOnBoardEventUsersFilterInput
  $owner: String
) {
  onCreateOnBoardEventUsers(filter: $filter, owner: $owner) {
    id
    onBoardEventId
    userId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnCreateOnBoardEventUsersSubscriptionVariables,
  APITypes.OnCreateOnBoardEventUsersSubscription
>;
export const onUpdateOnBoardEventUsers = /* GraphQL */ `subscription OnUpdateOnBoardEventUsers(
  $filter: ModelSubscriptionOnBoardEventUsersFilterInput
  $owner: String
) {
  onUpdateOnBoardEventUsers(filter: $filter, owner: $owner) {
    id
    onBoardEventId
    userId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnUpdateOnBoardEventUsersSubscriptionVariables,
  APITypes.OnUpdateOnBoardEventUsersSubscription
>;
export const onDeleteOnBoardEventUsers = /* GraphQL */ `subscription OnDeleteOnBoardEventUsers(
  $filter: ModelSubscriptionOnBoardEventUsersFilterInput
  $owner: String
) {
  onDeleteOnBoardEventUsers(filter: $filter, owner: $owner) {
    id
    onBoardEventId
    userId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedSubscription<
  APITypes.OnDeleteOnBoardEventUsersSubscriptionVariables,
  APITypes.OnDeleteOnBoardEventUsersSubscription
>;
