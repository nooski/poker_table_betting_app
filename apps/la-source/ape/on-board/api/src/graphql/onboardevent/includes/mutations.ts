/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

import * as APITypes from "../API";
type GeneratedMutation<InputType, OutputType> = string & {
  __generatedMutationInput: InputType;
  __generatedMutationOutput: OutputType;
};

export const createAttendees = /* GraphQL */ `mutation CreateAttendees(
  $input: CreateAttendeesInput!
  $condition: ModelAttendeesConditionInput
) {
  createAttendees(input: $input, condition: $condition) {
    id
    created
    updated
    attendees {
      role
      address {
        city
        itinerary {
          __typename
        }
        location {
          latitude
          longitude
          __typename
        }
        postcode
        street1
        street2
        additional
        __typename
      }
      displayName
      firstName
      lastName
      school
      schoolLevel
      organizer
      status
      self
      endParticipation
      startParticipation
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    timesAttendeesId
    onBoardEventAttendeesId
    attendeesUserId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.CreateAttendeesMutationVariables,
  APITypes.CreateAttendeesMutation
>;
export const updateAttendees = /* GraphQL */ `mutation UpdateAttendees(
  $input: UpdateAttendeesInput!
  $condition: ModelAttendeesConditionInput
) {
  updateAttendees(input: $input, condition: $condition) {
    id
    created
    updated
    attendees {
      role
      address {
        city
        itinerary {
          __typename
        }
        location {
          latitude
          longitude
          __typename
        }
        postcode
        street1
        street2
        additional
        __typename
      }
      displayName
      firstName
      lastName
      school
      schoolLevel
      organizer
      status
      self
      endParticipation
      startParticipation
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    timesAttendeesId
    onBoardEventAttendeesId
    attendeesUserId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.UpdateAttendeesMutationVariables,
  APITypes.UpdateAttendeesMutation
>;
export const deleteAttendees = /* GraphQL */ `mutation DeleteAttendees(
  $input: DeleteAttendeesInput!
  $condition: ModelAttendeesConditionInput
) {
  deleteAttendees(input: $input, condition: $condition) {
    id
    created
    updated
    attendees {
      role
      address {
        city
        itinerary {
          __typename
        }
        location {
          latitude
          longitude
          __typename
        }
        postcode
        street1
        street2
        additional
        __typename
      }
      displayName
      firstName
      lastName
      school
      schoolLevel
      organizer
      status
      self
      endParticipation
      startParticipation
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    timesAttendeesId
    onBoardEventAttendeesId
    attendeesUserId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.DeleteAttendeesMutationVariables,
  APITypes.DeleteAttendeesMutation
>;
export const createTimes = /* GraphQL */ `mutation CreateTimes(
  $input: CreateTimesInput!
  $condition: ModelTimesConditionInput
) {
  createTimes(input: $input, condition: $condition) {
    id
    created
    updated
    times {
      day
      attendees {
        role
        address {
          city
          postcode
          street1
          street2
          additional
          __typename
        }
        displayName
        firstName
        lastName
        school
        schoolLevel
        organizer
        status
        self
        endParticipation
        startParticipation
        __typename
      }
      __typename
    }
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventTimesId
    timesUserId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.CreateTimesMutationVariables,
  APITypes.CreateTimesMutation
>;
export const updateTimes = /* GraphQL */ `mutation UpdateTimes(
  $input: UpdateTimesInput!
  $condition: ModelTimesConditionInput
) {
  updateTimes(input: $input, condition: $condition) {
    id
    created
    updated
    times {
      day
      attendees {
        role
        address {
          city
          postcode
          street1
          street2
          additional
          __typename
        }
        displayName
        firstName
        lastName
        school
        schoolLevel
        organizer
        status
        self
        endParticipation
        startParticipation
        __typename
      }
      __typename
    }
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventTimesId
    timesUserId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.UpdateTimesMutationVariables,
  APITypes.UpdateTimesMutation
>;
export const deleteTimes = /* GraphQL */ `mutation DeleteTimes(
  $input: DeleteTimesInput!
  $condition: ModelTimesConditionInput
) {
  deleteTimes(input: $input, condition: $condition) {
    id
    created
    updated
    times {
      day
      attendees {
        role
        address {
          city
          postcode
          street1
          street2
          additional
          __typename
        }
        displayName
        firstName
        lastName
        school
        schoolLevel
        organizer
        status
        self
        endParticipation
        startParticipation
        __typename
      }
      __typename
    }
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    event {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventTimesId
    timesUserId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.DeleteTimesMutationVariables,
  APITypes.DeleteTimesMutation
>;
export const createOnBoardEvent = /* GraphQL */ `mutation CreateOnBoardEvent(
  $input: CreateOnBoardEventInput!
  $condition: ModelOnBoardEventConditionInput
) {
  createOnBoardEvent(input: $input, condition: $condition) {
    id
    created
    updated
    description
    name
    activity
    eventType
    whichWay
    lifeCycle
    reoccur
    startDate
    endDate
    end {
      dateTime
      timeZone
      __typename
    }
    start {
      dateTime
      timeZone
      __typename
    }
    recurrence
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    times {
      items {
        id
        created
        updated
        times {
          day
          __typename
        }
        attendees {
          nextToken
          startedAt
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventTimesId
        timesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    tags {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    creator {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    organizer {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    users {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventCreatorId
    onBoardEventOrganizerId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.CreateOnBoardEventMutationVariables,
  APITypes.CreateOnBoardEventMutation
>;
export const updateOnBoardEvent = /* GraphQL */ `mutation UpdateOnBoardEvent(
  $input: UpdateOnBoardEventInput!
  $condition: ModelOnBoardEventConditionInput
) {
  updateOnBoardEvent(input: $input, condition: $condition) {
    id
    created
    updated
    description
    name
    activity
    eventType
    whichWay
    lifeCycle
    reoccur
    startDate
    endDate
    end {
      dateTime
      timeZone
      __typename
    }
    start {
      dateTime
      timeZone
      __typename
    }
    recurrence
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    times {
      items {
        id
        created
        updated
        times {
          day
          __typename
        }
        attendees {
          nextToken
          startedAt
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventTimesId
        timesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    tags {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    creator {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    organizer {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    users {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventCreatorId
    onBoardEventOrganizerId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.UpdateOnBoardEventMutationVariables,
  APITypes.UpdateOnBoardEventMutation
>;
export const deleteOnBoardEvent = /* GraphQL */ `mutation DeleteOnBoardEvent(
  $input: DeleteOnBoardEventInput!
  $condition: ModelOnBoardEventConditionInput
) {
  deleteOnBoardEvent(input: $input, condition: $condition) {
    id
    created
    updated
    description
    name
    activity
    eventType
    whichWay
    lifeCycle
    reoccur
    startDate
    endDate
    end {
      dateTime
      timeZone
      __typename
    }
    start {
      dateTime
      timeZone
      __typename
    }
    recurrence
    attendees {
      items {
        id
        created
        updated
        attendees {
          role
          displayName
          firstName
          lastName
          school
          schoolLevel
          organizer
          status
          self
          endParticipation
          startParticipation
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        timesAttendeesId
        onBoardEventAttendeesId
        attendeesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    times {
      items {
        id
        created
        updated
        times {
          day
          __typename
        }
        attendees {
          nextToken
          startedAt
          __typename
        }
        event {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        onBoardEventTimesId
        timesUserId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    tags {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    creator {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    organizer {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    users {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    onBoardEventCreatorId
    onBoardEventOrganizerId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.DeleteOnBoardEventMutationVariables,
  APITypes.DeleteOnBoardEventMutation
>;
export const createPost = /* GraphQL */ `mutation CreatePost(
  $input: CreatePostInput!
  $condition: ModelPostConditionInput
) {
  createPost(input: $input, condition: $condition) {
    id
    created
    updated
    name
    poststatus
    comments {
      items {
        id
        content
        post {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        created
        updated
        _version
        _deleted
        _lastChangedAt
        postCommentsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    userPostsId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.CreatePostMutationVariables,
  APITypes.CreatePostMutation
>;
export const updatePost = /* GraphQL */ `mutation UpdatePost(
  $input: UpdatePostInput!
  $condition: ModelPostConditionInput
) {
  updatePost(input: $input, condition: $condition) {
    id
    created
    updated
    name
    poststatus
    comments {
      items {
        id
        content
        post {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        created
        updated
        _version
        _deleted
        _lastChangedAt
        postCommentsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    userPostsId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.UpdatePostMutationVariables,
  APITypes.UpdatePostMutation
>;
export const deletePost = /* GraphQL */ `mutation DeletePost(
  $input: DeletePostInput!
  $condition: ModelPostConditionInput
) {
  deletePost(input: $input, condition: $condition) {
    id
    created
    updated
    name
    poststatus
    comments {
      items {
        id
        content
        post {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        created
        updated
        _version
        _deleted
        _lastChangedAt
        postCommentsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    userPostsId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.DeletePostMutationVariables,
  APITypes.DeletePostMutation
>;
export const createComment = /* GraphQL */ `mutation CreateComment(
  $input: CreateCommentInput!
  $condition: ModelCommentConditionInput
) {
  createComment(input: $input, condition: $condition) {
    id
    content
    post {
      id
      created
      updated
      name
      poststatus
      comments {
        items {
          id
          content
          created
          updated
          _version
          _deleted
          _lastChangedAt
          postCommentsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      userPostsId
      owner
      __typename
    }
    created
    updated
    _version
    _deleted
    _lastChangedAt
    postCommentsId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.CreateCommentMutationVariables,
  APITypes.CreateCommentMutation
>;
export const updateComment = /* GraphQL */ `mutation UpdateComment(
  $input: UpdateCommentInput!
  $condition: ModelCommentConditionInput
) {
  updateComment(input: $input, condition: $condition) {
    id
    content
    post {
      id
      created
      updated
      name
      poststatus
      comments {
        items {
          id
          content
          created
          updated
          _version
          _deleted
          _lastChangedAt
          postCommentsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      userPostsId
      owner
      __typename
    }
    created
    updated
    _version
    _deleted
    _lastChangedAt
    postCommentsId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.UpdateCommentMutationVariables,
  APITypes.UpdateCommentMutation
>;
export const deleteComment = /* GraphQL */ `mutation DeleteComment(
  $input: DeleteCommentInput!
  $condition: ModelCommentConditionInput
) {
  deleteComment(input: $input, condition: $condition) {
    id
    content
    post {
      id
      created
      updated
      name
      poststatus
      comments {
        items {
          id
          content
          created
          updated
          _version
          _deleted
          _lastChangedAt
          postCommentsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      user {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      userPostsId
      owner
      __typename
    }
    created
    updated
    _version
    _deleted
    _lastChangedAt
    postCommentsId
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.DeleteCommentMutationVariables,
  APITypes.DeleteCommentMutation
>;
export const createUser = /* GraphQL */ `mutation CreateUser(
  $input: CreateUserInput!
  $condition: ModelUserConditionInput
) {
  createUser(input: $input, condition: $condition) {
    id
    created
    updated
    username
    displayName
    firstName
    lastName
    email
    phoneNumber
    posts {
      items {
        id
        created
        updated
        name
        poststatus
        comments {
          nextToken
          startedAt
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        userPostsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    events {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.CreateUserMutationVariables,
  APITypes.CreateUserMutation
>;
export const updateUser = /* GraphQL */ `mutation UpdateUser(
  $input: UpdateUserInput!
  $condition: ModelUserConditionInput
) {
  updateUser(input: $input, condition: $condition) {
    id
    created
    updated
    username
    displayName
    firstName
    lastName
    email
    phoneNumber
    posts {
      items {
        id
        created
        updated
        name
        poststatus
        comments {
          nextToken
          startedAt
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        userPostsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    events {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.UpdateUserMutationVariables,
  APITypes.UpdateUserMutation
>;
export const deleteUser = /* GraphQL */ `mutation DeleteUser(
  $input: DeleteUserInput!
  $condition: ModelUserConditionInput
) {
  deleteUser(input: $input, condition: $condition) {
    id
    created
    updated
    username
    displayName
    firstName
    lastName
    email
    phoneNumber
    posts {
      items {
        id
        created
        updated
        name
        poststatus
        comments {
          nextToken
          startedAt
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        userPostsId
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    events {
      items {
        id
        onBoardEventId
        userId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        user {
          id
          created
          updated
          username
          displayName
          firstName
          lastName
          email
          phoneNumber
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.DeleteUserMutationVariables,
  APITypes.DeleteUserMutation
>;
export const createTag = /* GraphQL */ `mutation CreateTag(
  $input: CreateTagInput!
  $condition: ModelTagConditionInput
) {
  createTag(input: $input, condition: $condition) {
    id
    created
    updated
    name
    events {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.CreateTagMutationVariables,
  APITypes.CreateTagMutation
>;
export const updateTag = /* GraphQL */ `mutation UpdateTag(
  $input: UpdateTagInput!
  $condition: ModelTagConditionInput
) {
  updateTag(input: $input, condition: $condition) {
    id
    created
    updated
    name
    events {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.UpdateTagMutationVariables,
  APITypes.UpdateTagMutation
>;
export const deleteTag = /* GraphQL */ `mutation DeleteTag(
  $input: DeleteTagInput!
  $condition: ModelTagConditionInput
) {
  deleteTag(input: $input, condition: $condition) {
    id
    created
    updated
    name
    events {
      items {
        id
        onBoardEventId
        tagId
        onBoardEvent {
          id
          created
          updated
          description
          name
          activity
          eventType
          whichWay
          lifeCycle
          reoccur
          startDate
          endDate
          recurrence
          _version
          _deleted
          _lastChangedAt
          onBoardEventCreatorId
          onBoardEventOrganizerId
          owner
          __typename
        }
        tag {
          id
          created
          updated
          name
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      nextToken
      startedAt
      __typename
    }
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.DeleteTagMutationVariables,
  APITypes.DeleteTagMutation
>;
export const createOnBoardEventTags = /* GraphQL */ `mutation CreateOnBoardEventTags(
  $input: CreateOnBoardEventTagsInput!
  $condition: ModelOnBoardEventTagsConditionInput
) {
  createOnBoardEventTags(input: $input, condition: $condition) {
    id
    onBoardEventId
    tagId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    tag {
      id
      created
      updated
      name
      events {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.CreateOnBoardEventTagsMutationVariables,
  APITypes.CreateOnBoardEventTagsMutation
>;
export const updateOnBoardEventTags = /* GraphQL */ `mutation UpdateOnBoardEventTags(
  $input: UpdateOnBoardEventTagsInput!
  $condition: ModelOnBoardEventTagsConditionInput
) {
  updateOnBoardEventTags(input: $input, condition: $condition) {
    id
    onBoardEventId
    tagId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    tag {
      id
      created
      updated
      name
      events {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.UpdateOnBoardEventTagsMutationVariables,
  APITypes.UpdateOnBoardEventTagsMutation
>;
export const deleteOnBoardEventTags = /* GraphQL */ `mutation DeleteOnBoardEventTags(
  $input: DeleteOnBoardEventTagsInput!
  $condition: ModelOnBoardEventTagsConditionInput
) {
  deleteOnBoardEventTags(input: $input, condition: $condition) {
    id
    onBoardEventId
    tagId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    tag {
      id
      created
      updated
      name
      events {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.DeleteOnBoardEventTagsMutationVariables,
  APITypes.DeleteOnBoardEventTagsMutation
>;
export const createOnBoardEventUsers = /* GraphQL */ `mutation CreateOnBoardEventUsers(
  $input: CreateOnBoardEventUsersInput!
  $condition: ModelOnBoardEventUsersConditionInput
) {
  createOnBoardEventUsers(input: $input, condition: $condition) {
    id
    onBoardEventId
    userId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.CreateOnBoardEventUsersMutationVariables,
  APITypes.CreateOnBoardEventUsersMutation
>;
export const updateOnBoardEventUsers = /* GraphQL */ `mutation UpdateOnBoardEventUsers(
  $input: UpdateOnBoardEventUsersInput!
  $condition: ModelOnBoardEventUsersConditionInput
) {
  updateOnBoardEventUsers(input: $input, condition: $condition) {
    id
    onBoardEventId
    userId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.UpdateOnBoardEventUsersMutationVariables,
  APITypes.UpdateOnBoardEventUsersMutation
>;
export const deleteOnBoardEventUsers = /* GraphQL */ `mutation DeleteOnBoardEventUsers(
  $input: DeleteOnBoardEventUsersInput!
  $condition: ModelOnBoardEventUsersConditionInput
) {
  deleteOnBoardEventUsers(input: $input, condition: $condition) {
    id
    onBoardEventId
    userId
    onBoardEvent {
      id
      created
      updated
      description
      name
      activity
      eventType
      whichWay
      lifeCycle
      reoccur
      startDate
      endDate
      end {
        dateTime
        timeZone
        __typename
      }
      start {
        dateTime
        timeZone
        __typename
      }
      recurrence
      attendees {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          timesAttendeesId
          onBoardEventAttendeesId
          attendeesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      times {
        items {
          id
          created
          updated
          _version
          _deleted
          _lastChangedAt
          onBoardEventTimesId
          timesUserId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      tags {
        items {
          id
          onBoardEventId
          tagId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      creator {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      organizer {
        id
        created
        updated
        username
        displayName
        firstName
        lastName
        email
        phoneNumber
        posts {
          nextToken
          startedAt
          __typename
        }
        events {
          nextToken
          startedAt
          __typename
        }
        _version
        _deleted
        _lastChangedAt
        owner
        __typename
      }
      users {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      onBoardEventCreatorId
      onBoardEventOrganizerId
      owner
      __typename
    }
    user {
      id
      created
      updated
      username
      displayName
      firstName
      lastName
      email
      phoneNumber
      posts {
        items {
          id
          created
          updated
          name
          poststatus
          _version
          _deleted
          _lastChangedAt
          userPostsId
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      events {
        items {
          id
          onBoardEventId
          userId
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
          __typename
        }
        nextToken
        startedAt
        __typename
      }
      _version
      _deleted
      _lastChangedAt
      owner
      __typename
    }
    createdAt
    updatedAt
    _version
    _deleted
    _lastChangedAt
    owner
    __typename
  }
}
` as GeneratedMutation<
  APITypes.DeleteOnBoardEventUsersMutationVariables,
  APITypes.DeleteOnBoardEventUsersMutation
>;
