// @ts-nocheck
const { optionsHTTPS } = require('@flexiness/certs')
const http = require('http')
const https = require('https')
const express = require('express')
const path = require('path')
const WebSocket = require('ws')
const cors = require('cors')
const regexEscape = require('regex-escape')
const { PokerBoard, PokerTables, getBaseTableState, PokerTimer } = import('on-board-event-common/dist/index.js')

const { FLEX_SERVER_RUNNING } = process.env
const PORT = process.env.FLEX_POKER_BACK_PORT
const HOST = `${process.env.FLEX_PROTOCOL}${process.env.FLEX_POKER_BACK_HOSTNAME}:${PORT}`

const app = express()
const server = `${process.env.FLEX_PROTOCOL}` === 'http://' ? http.createServer(app) : https.createServer(optionsHTTPS(), app)
// : https.createServer({ ...optionsHTTPS(), rejectUnauthorized: false }, app) // debug ssl only !!
// console.log('SERVER', server)

const wss = new WebSocket.Server({ server, path: '/ws' })
// const wss = new WebSocket.Server({ server, path: `${process.env.FLEX_PROTOCOL === 'http://' ? 'ws' : 'wss'}` })
// console.log('WEBSOCKET SERVER', wss)

const delay = 0

const corsOptions = {
  /* eslint-disable indent */
  ...(process.env.FLEX_MODE === 'development'
    ? { origin: '*' }
    : {
        origin: [
          new RegExp(`${regexEscape(process.env.FLEX_DOMAIN_NAME ?? '')}`),
          new RegExp(`${regexEscape(`.${process.env.FLEX_BASE_DOMAIN ?? ''}`)}$`),
          new RegExp(`${regexEscape(process.env.FLEX_HOST_IP ?? '')}$`),
        ],
      }),
  /* eslint-enable indent */
  methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'],
  allowedHeaders: ['Content-Type', 'X-Requested-With', 'Authorization'],
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
}

const servePath = path.join(__dirname, '../..', 'client', 'build', 'web')

app.use(cors(corsOptions))
app.use(express.static(servePath))
app.get('/', function (req, res) {
  res.sendFile(path.join(servePath, 'index.html'))
})

let nextClientId = 1

let board = new PokerBoard()

const _tables = Promise.resolve(getBaseTableState(20))
let tables = new PokerTables(_tables)

let timers = new PokerTimer()

function sendToAll(obj) {
  const actionSerialized = JSON.stringify(obj)
  wss.clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      setTimeout(() => client.send(actionSerialized), delay)
    }
  })
}

function processAction(action) {
  console.log(action)
  if (action.source === 'PokerBoard') {
    board = board.processAction(action)
  } else if (action.source === 'PokerTables') {
    tables = tables.processAction(action)
  } else if (action.source === 'PokerTimer') {
    timers = timers.processAction(action)
  }
  sendToAll(action)
}

wss.on('connection', (ws) => {
  function send(obj) {
    setTimeout(() => ws.send(JSON.stringify(obj)), delay)
  }

  const myClientId = nextClientId++

  console.log(`ws connected: client ${myClientId}`)

  send({
    clientId: myClientId,
    snapshot: board.snapshot,
    initSnapshotTables: tables.initSnapshotTables,
  })
  void processAction({ id: myClientId, source: 'PokerBoard', action: PokerBoard.ACTION_JOIN })
  // processAction({ id: myClientId, source: 'PokerTables', action: PokerTables.ACTION_UPDATE_TABLES })

  ws.on('message', (m) => {
    // eslint-disable-next-line @typescript-eslint/no-base-to-string
    console.log(`received: ${m}`)
    const parsed = JSON.parse(m)
    if (typeof parsed === 'object') {
      send({
        ack: parsed.seq,
      })

      parsed.id = myClientId // not strictly needed but this is a "security" measure
      delete parsed.seq

      void processAction(parsed)
    }
  })

  ws.on('ping', () => {
    console.log('received a ping!')
  })

  ws.on('pong', () => {
    console.log('received a pong!')
  })

  ws.on('close', () => {
    console.log(`ws closed; client ${myClientId} departing`)
    void processAction({
      id: myClientId,
      source: 'PokerBoard',
      action: PokerBoard.ACTION_DEPART,
    })
  })
})

server.listen(PORT, `${process.env.FLEX_POKER_BACK_HOSTNAME}`, 34, (err) => {
  if (err) throw err
  console.log(`[${process.env.FLEX_PROTOCOL.slice(0, -3).toUpperCase()}] : ${HOST} :`, server.address())
  console.log(`${FLEX_SERVER_RUNNING} ${HOST}`)
})
