/* eslint-disable no-console */

import path from 'path'
// __dirname is not defined in ES module scope
import * as http from 'http'
import * as https from 'https'
import { fileURLToPath } from 'url'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
// // require.resolve for ES modules
// import { createRequire } from 'module'
// const require = createRequire(import.meta.url)

import subprocess from 'node:child_process'
import { promisify } from 'node:util'
const execPromise = promisify(subprocess.exec)

import { promises as fsPromise } from 'fs'
// import { PathLike, promises as fsPromise } from 'fs'

let _gitCommitSHA = ''
async function getGitCommitSHA() {
  if ('GIT_COMMIT_SHA' in process.env) {
    return process.env.GIT_COMMIT_SHA
  } else {
    const result = await execPromise(`git rev-parse --short HEAD`)
    const { stdout } = result
    if (!stdout) return result
    _gitCommitSHA = stdout.trim()
    console.log(`Git Commit SHA :`, _gitCommitSHA)
    return _gitCommitSHA
  }
}
_gitCommitSHA = (await getGitCommitSHA()) as string

// const accessFile = async (path: PathLike) => {
//   try {
//     await fsPromise.access(path)
//     // console.log(path)
//     // console.log('*.html path good')
//     return path
//   } catch {
//     return false
//   }
// }

import express from 'express'
// import express, { Request, Response } from 'express'

// import { CognitoJwtVerifier } from 'aws-jwt-verify'
// // Create the verifier outside your route handlers,
// // so the cache is persisted and can be shared amongst them.
// const jwtVerifier = CognitoJwtVerifier.create({
//   userPoolId: process.env.FLEX_AWS_COGNITO_USER_POOL_ID!,
//   tokenUse: 'access',
//   clientId: process.env.FLEX_AWS_COGNITO_USER_POOL_APP_CLIENT_ID!,
//   scope: 'read',
// });

// const { getOnBoardEventsStore } = await import('@flexiness/domain-store')
// const OnBoardEventData = getOnBoardEventsStore()
// const { setCurrentOnBoardEventDataSet } = OnBoardEventData

const servePath = path.join(
  __dirname,
  '../..',
  'client',
  'build',
  `${process.env.FLEX_POKER_CLIENT_BUILD_SYS}`,
  `${process.env.FLEX_POKER_CLIENT_TARGET}`,
)

// import { ChunkExtractor } from '@loadable/server'
// const clientStats = await accessFile(path.join(servePath, 'loadable-stats.json'))
// let clientExtractor
// let flexFrameworkStylesAsset
// if (clientStats) {
//   clientExtractor = new ChunkExtractor({ statsFile: clientStats, entrypoints: [`mainEntry_${process.env.FLEX_POKER_CLIENT_NAME}_${_gitCommitSHA}`], })
//   console.log(clientExtractor['stats']['assetsByChunkName'])
//   flexFrameworkStylesAsset = clientExtractor['stats']['assetsByChunkName']['flex-framework-styles']
//   console.log('flex-framework-styles : ', flexFrameworkStylesAsset)
// }

const { optionsHTTPS } = await import('@flexiness/certs')
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const { checkIsRoute, getContentSecurityPolicy, setFlexCSPNonce } = await import('@flexiness/domain-utils')
import cors from 'cors'
import ejs from 'ejs'
ejs.delimiter = '?' // Means instead use __webpack_nonce__ = '<?=nonce?>'
import * as _JSON from 'json-typescript'
// import { type PokerTables as PokerTablesType } from 'on-board-event-common'
const { PokerBoard, PokerTables, PokerTimer, getBaseTableState } = await import('on-board-event-common')
import regexEscape from 'regex-escape'
import { WebSocketServer } from 'ws'

// import findWorkspaceRoot from 'find-yarn-workspace-root'
// const workspacePath = findWorkspaceRoot(__dirname)
// const rootLocation = path.relative(__dirname, workspacePath!)
const rootLocation = process.env.FLEX_PROJ_ROOT
console.log('rootLocation', rootLocation)

import { PokerAction, Table } from 'flexiness'

let _nonce = ''
interface Nonce {
  nonce: string
}
const readNonce = async () => {
  const nonceLocation = `${rootLocation}/apps/la-source/ape/gateway/nonce.json`
  const nonceJsonFile = await fsPromise.readFile(nonceLocation)
  const nonceParsed = JSON.parse(nonceJsonFile.toString('utf8')) as Nonce
  _nonce = nonceParsed.nonce
  setFlexCSPNonce(_nonce)
  // await parseHTML(_nonce)
  // console.log('_nonce', `${getFlexCSPNonce()}`)
}

const port = 8080
const app = express()
const server = `${process.env.FLEX_PROTOCOL}` === 'http://' ? http.createServer(app) : https.createServer(optionsHTTPS(), app)
const wss = new WebSocketServer({ server, path: '/ws' })
const delay = 0

const corsOptions = {
  /* eslint-disable indent */
  ...(process.env.FLEX_MODE === 'development'
    ? { origin: '*' }
    : {
        origin: [
          new RegExp(`${regexEscape(process.env.FLEX_DOMAIN_NAME ?? '')}`),
          new RegExp(`${regexEscape(`.${process.env.FLEX_BASE_DOMAIN ?? ''}`)}$`),
          new RegExp(`${regexEscape(process.env.FLEX_HOST_IP ?? '')}$`),
        ],
      }),
  /* eslint-enable indent */
  methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'],
  allowedHeaders: ['Content-Type', 'X-Requested-With', 'Authorization'],
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
}

// let tables: PokerTablesType
let _tables: Table[] = []
// let _currentRoute = ''

async function getTables() {
  console.log('hello dolly')
  try {
    _tables = (await getBaseTableState({ limit: 20 })) || []
  } catch (error) {
    console.log(error)
  } finally {
    // eslint-disable-next-line no-unsafe-finally
    return _tables
  }
}

/* eslint-disable @typescript-eslint/no-misused-promises */
app.engine('ejs', ejs.renderFile)
app.set('views', servePath)
// app.use('/', express.static(servePath))
// app.use('/styles', express.static(servePath))

app.use(cors(corsOptions))
app.use(async (req, res, next) => {
  // console.log(`req port : ${req.socket.localPort}`)
  if (checkIsRoute(req.path)) {
    // console.log(`req path : ${req.path}`)
    await readNonce()
    // await generateNonce()
    // await sedNonceStaticHTML(req.path)
    // await sedNonceJS()
    // _currentRoute = req.path
  }
  res.locals.cspNonce = _nonce
  next()
})

// // if (process.env.FLEX_MODE === 'production') {
// app.use((req, res, next) => {
//   return getContentSecurityPolicy(req, res, next, _nonce)
// })
// // }

// app.get('/', async(req: Request, res: Response) => {
app.get('/', async () => {
  // ejs.renderFile(path.join(servePath, 'index.ejs'), { nonce: res.locals.cspNonce, }, {}, function(err, str){
  //   // str => Rendered HTML string
  //   if (err) {
  //     console.log(err)
  //   } else {
  //     console.log(str)
  //     res.sendFile(str)
  //   }
  // })
  await getTables().then(() => {
    // res.render(path.join(servePath, 'index.ejs'), {
    //   nonce: res.locals.cspNonce as string,
    //   ...(flexFrameworkStylesAsset !== 'undefined' &&
    //     {
    //       flexFrameworkStyles: `/styles/${flexFrameworkStylesAsset}`
    //     }
    //   )
    // })
  })
  // tables = new PokerTables(_tables)
  // res.render(path.join(servePath, 'index.ejs'), { nonce: res.locals.cspNonce as string, })
})

// app.get('/', (req, res) => {
//   res.render(path.join(servePath, 'index.ejs'), { nonce: res.locals.cspNonce, })
// })
/* eslint-enable @typescript-eslint/no-misused-promises */

let nextClientId = 1

const board = new PokerBoard()

// _tables = await getTables()
// tables = new PokerTables(_tables)
// const _tables = await getTables()
_tables = await getTables()
// setCurrentOnBoardEventDataSet(_tables)
const tables = new PokerTables(_tables)

// const timers = new PokerTimer()
const timers = new PokerTimer(new Date())

function sendToAll(obj: Record<string, unknown>) {
  const actionSerialized = JSON.stringify(obj)
  wss.clients.forEach((client) => {
    // 1 equivalent to WebSocket.OPEN
    if (client.readyState === 1) {
      setTimeout(() => client.send(actionSerialized), delay)
    }
  })
}

function processAction(action: PokerAction) {
  console.log(action)
  if (action.source === 'PokerBoard') {
    board.processAction(action)
  } else if (action.source === 'PokerTables') {
    tables.processAction(action)
  } else if (action.source === 'PokerTimer') {
    timers.processAction(action)
  }
  sendToAll(action)
}

wss.on('connection', (ws) => {
  function send(obj: Record<string, unknown>) {
    setTimeout(() => ws.send(JSON.stringify(obj)), delay)
  }

  const myClientId = nextClientId++

  console.log(`ws connected: client ${myClientId}`)

  send({
    clientId: myClientId,
    snapshot: board.snapshot,
    initSnapshotTables: tables.initSnapshotTables,
  })
  // void processAction({ id: myClientId, source: 'PokerTables', action: PokerTables.ACTION_UPDATE_TABLES })
  void processAction({ id: myClientId, source: 'PokerBoard', action: PokerBoard.ACTION_JOIN })

  ws.on('message', (m: string) => {
    console.log(`received: ${m}`)
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const parsed: _JSON.Object = JSON.parse(m)
    if (typeof parsed === 'object') {
      send({
        ack: parsed.seq,
      })

      parsed.id = myClientId // not strictly needed but this is a 'security' measure
      delete parsed.seq

      void processAction(parsed as PokerAction)
    }
  })

  ws.on('ping', () => {
    console.log('received a ping!')
  })

  ws.on('pong', () => {
    console.log('received a pong!')
  })

  ws.on('close', () => {
    console.log(`ws closed; client ${myClientId} departing`)
    void processAction({
      id: myClientId,
      source: 'PokerBoard',
      action: PokerBoard.ACTION_DEPART,
    })
  })
})

server.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})
