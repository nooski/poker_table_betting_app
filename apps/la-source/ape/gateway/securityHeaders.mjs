// deferred to helmet -csp in express server
const activateContentSecurityPolicy = false

const setSecurityHeaders = (scope = 'default')  => {
  const whiteList = {
    styleSrc: [
      'https://fonts.googleapis.com',
      'https://api.iconify.design'
    ],
    styleSrcElem: [
      'https://fonts.googleapis.com',
      'https://api.iconify.design'
    ],
    styleSrcAttr: [
      'https://fonts.googleapis.com',
      'https://api.iconify.design'
    ],
    fontSrc: [
      'https://fonts.gstatic.com'
    ],
    imgSrc: [
    ],
    connectSrc: [
    ],
  }
  const whiteListFlexDomain = `${prod ? `*.${parsedDomain}:*` : 'localhost:*'}`
  const ContentSecurityPolicyDev =
  `
    default-src 'self' ${whiteListFlexDomain};
    base-uri 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    style-src 'self' ${whiteListFlexDomain} ${whiteList.styleSrc.join(' ')} 'nonce-${_nonce}' 'unsafe-inline';
    font-src 'self' ${whiteListFlexDomain} ${whiteList.fontSrc.join(' ')} data: 'nonce-${_nonce}';
    img-src 'self' ${whiteListFlexDomain} ${whiteList.imgSrc.join(' ')} data: 'nonce-${_nonce}';
    prefetch-src 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    manifest-src 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    child-src 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    connect-src 'self' ${whiteListFlexDomain} ${whiteList.connectSrc.join(' ')} 'nonce-${_nonce}';
    navigate-to 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    form-action 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    frame-ancestors 'self' ${whiteListFlexDomain};
    script-src-elem 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    script-src-attr 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    script-src 'self' ${whiteListFlexDomain} 'nonce-${_nonce}' 'strict-dynamic' 'unsafe-eval';
    report-uri ${process.env.FLEX_GATEWAY_HOST}/api/report-csp;
  `
  const ContentSecurityPolicyProd =
  `
    default-src 'self' ${whiteListFlexDomain};
    base-uri 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    style-src 'self' ${whiteListFlexDomain} ${whiteList.styleSrc.join(' ')} 'nonce-${_nonce}';
    font-src 'self' ${whiteListFlexDomain} ${whiteList.fontSrc.join(' ')} data: 'nonce-${_nonce}';
    img-src 'self' ${whiteListFlexDomain} ${whiteList.imgSrc.join(' ')} data: 'nonce-${_nonce}';
    prefetch-src 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    manifest-src 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    child-src 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    connect-src 'self' ${whiteListFlexDomain} ${whiteList.connectSrc.join(' ')} 'nonce-${_nonce}';
    navigate-to 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    form-action 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    frame-ancestors 'self' ${whiteListFlexDomain};
    script-src-elem 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    script-src-attr 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    script-src 'self' ${whiteListFlexDomain} 'nonce-${_nonce}' 'strict-dynamic';
  `
  const ContentSecurityPolicyPermissive =
  `
    default-src 'self' ${whiteListFlexDomain};
    base-uri 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    style-src 'self' ${whiteListFlexDomain} ${whiteList.styleSrc.join(' ')} 'nonce-${_nonce}';
    font-src 'self' ${whiteListFlexDomain} ${whiteList.fontSrc.join(' ')} data: 'nonce-${_nonce}';
    img-src 'self' ${whiteListFlexDomain} ${whiteList.imgSrc.join(' ')} data: 'nonce-${_nonce}';
    prefetch-src 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    manifest-src 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    child-src 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    connect-src 'self' ${whiteListFlexDomain} ${whiteList.connectSrc.join(' ')} 'nonce-${_nonce}';
    navigate-to 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    form-action 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    frame-ancestors 'self' ${whiteListFlexDomain};
    script-src-elem 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    script-src-attr 'self' ${whiteListFlexDomain} 'nonce-${_nonce}';
    script-src 'self' ${whiteListFlexDomain} 'nonce-${_nonce}' 'strict-dynamic' 'unsafe-eval';
  `
  const ContentSecurityPolicy = () => {
    switch (true) {
      case scope === 'permissive': {
        return ContentSecurityPolicyPermissive
      }
      case scope === 'default' && process.env.FLEX_MODE === 'development': {
        return ContentSecurityPolicyDev
      }
      case scope === 'default' && process.env.FLEX_MODE === 'production': {
        return ContentSecurityPolicyProd
      }
      default: {
        return ContentSecurityPolicyProd
      }
    }
  }

  // Apply to nginx headers as well
  const baseHeaders = [
    {
      key: 'Access-Control-Allow-Origin',
      value: `${process.env.FLEX_GATEWAY_HOST}`,
    },
    {
      key: 'Access-Control-Allow-Methods',
      value: 'GET, POST, PUT, DELETE, PATCH, OPTIONS'
    },
    {
      key: 'Access-Control-Allow-Headers',
      value: 'Origin, Content-Type, Accept, Authorization'
    },
    {
      key: 'Referrer-Policy',
      value: 'no-referrer, strict-origin-when-cross-origin'
    },
    // https://blog.qualys.com/vulnerabilities-threat-research/2016/03/28/the-importance-of-a-proper-http-strict-transport-security-implementation-on-your-web-server
    {
      key: 'Strict-Transport-Security',
      value: 'max-age=63072000; includeSubDomains; preload'
    },
    {
      key: 'X-Content-Type-Options',
      value: 'nosniff'
    },
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Permissions_Policy
    {
      key: 'Permissions-Policy',
      value: 'geolocation=(),midi=(),sync-xhr=(),microphone=(),camera=(),magnetometer=(),gyroscope=(),fullscreen=(self),payment=()'
    }
  ]

  const securityHeaders = [
    ...baseHeaders,
    ...(activateContentSecurityPolicy
      ? [{
        key: 'Content-Security-Policy',
        value: ContentSecurityPolicy().replace(/\s{2,}/g, ' ').trim()
      }] : []
    )
  ]

  return securityHeaders
}

export { setSecurityHeaders }
