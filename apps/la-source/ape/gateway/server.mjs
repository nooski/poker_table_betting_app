/* eslint-disable no-useless-escape */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-console */

// https://nextjs.org/docs/app/building-your-application/configuring/content-security-policy#reading-the-nonce
// https://blog.logrocket.com/using-cors-next-js-handle-cross-origin-requests/


// __dirname is not defined in ES module scope
import * as path from 'path'
import { fileURLToPath, parse } from 'url'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

// require.resolve for ES modules
import { createRequire } from 'module'
const require = createRequire(import.meta.url)

// const tracer = require('dd-trace').init()
// const ddOptions = {
//   // eslint-disable-next-line camelcase
//   response_code: true,
//   tags: [
//     `app:${process.env.FLEX_GATEWAY_NAME}`
//   ]
// }
// const connectDatadog = require('connect-datadog')(ddOptions)

import subprocess from 'node:child_process'
import { promisify } from 'node:util'
const execPromise = promisify(subprocess.exec)

import { promises as fs } from 'fs'

import express from 'express'
import http from 'http'
import https from 'https'
import Cors from 'cors'
import accepts from 'accepts'
import createNextServer from 'next'
import escapeStringRegexp from 'escape-string-regexp'

const nextConfig = await import('./next.config.mjs')
  .then(module => module.default || module)
  .then(config => {
    return config()
  })

// const NextI18NextInstance = require('@flexiness/languages/dist/next-i18next.cjs')
import { i18nextServerSide } from '@flexiness/languages/dist/i18n-serverside.mjs'
import * as i18nextMiddleware from 'i18next-http-middleware'
import { initLang } from '@flexiness/languages/dist/i18n-init-lang.js'
import {
  locales,
  // defaultLocale
} from '@flexiness/languages/dist/i18n-constants.js'

const accessFile = async (path) => {
  try {
    await fs.access(path)
    return true
  } catch {
    return false
  }
}

const psl = require('psl')
// const nocache = require('nocache')
const { optionsHTTPS } = require('@flexiness/certs')
const { checkIsRoute, getContentSecurityPolicy, generateFlexCSPNonce, getFlexCSPNonce, setFlexCSPNonce } = await import('@flexiness/domain-utils')

const corsOptions = Cors({
  ...(process.env.FLEX_MODE === 'development'
    ? { origin: '*' }
    : { origin: [
      new RegExp(`${escapeStringRegexp(`${process.env.FLEX_DOMAIN_NAME}`)}`),
      new RegExp(`${escapeStringRegexp(`.${process.env.FLEX_BASE_DOMAIN}`)}$`),
      new RegExp(`${escapeStringRegexp(`${process.env.FLEX_HOST_IP}`)}$`),
      new RegExp(`${escapeStringRegexp(`webpack://_N_E`)}`)
    ] }
  ),
  // methods: ['POST', 'GET', 'HEAD'],
  methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'],
  allowedHeaders: ['Content-Type', 'X-Requested-With', 'Authorization'],
  // preflightContinue: true,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
})

const bypassVerifySSL = execPromise(`
  set -o allexport source ${process.env.FLEX_PROJ_ROOT}/env/public/.env.${process.env.FLEX_MODE} set +o allexport \
  ${process.env.FLEX_PROJ_ROOT}/bin/run-bypass-verify-ssl.sh
`)
  .then(result => {
    const { stdout } = result
    if (!stdout) return result
    console.log(`[SSL] proxy-server.js bypassVerifySSL : ${Boolean(stdout)}`)
    return stdout
  })
  .catch(err => {
    console.log(err)
  })

let _nonce = ''
const readNonce = async () => {
  _nonce = JSON.parse(`${await fs.readFile(`./nonce.json`)}`)['nonce']
}

const generateNonce = async ()  => {
  _nonce = await generateFlexCSPNonce()
  const save = await execPromise(`
    jq -n --arg base64 ${_nonce} '{"nonce":$base64}' > ${path.resolve(`${__dirname}`, 'nonce.json')} && \
    echo ${_nonce} > ${path.resolve(`${__dirname}`, 'nonce.txt')} && \
    echo export const __webpack_nonce__ = \\'${_nonce}\\' > ${path.resolve(`${__dirname}`, 'nonce_webpack.js')} && \
    export FLEX_CSP_NONCE=${_nonce} && \
    echo ${_nonce}
  `)
  const { stdout, stderr } = save
  if (stdout) {
    setFlexCSPNonce(stdout)
    if (process.env.DEBUG === 'true') console.log(`Gateway Express Custom Server csp nonce : ${getFlexCSPNonce()}`)
  }
  if (stderr) {
    console.log(stderr)
    Promise.reject(new Error('Cannot write nonce to json file'))
  }
}

const sedNonceStaticHTML = async (reqRoute)  => {
  let route = reqRoute
  if (reqRoute === '/') route = '/store'
  // console.log(`${__dirname}/build/server/pages`)
  // const replace = await execPromise(`
  //   find ${__dirname}/build/server/pages/ -name '*.html' -print
  // `)

  // const replace = await execPromise(`
  //   sed -i -E 's~(property=\\"csp-nonce\\"\\\scontent=|__webpack_nonce__=|nonce=)(\\")(---CSP_NONCE---|.{43}=)(\\")~\\1"${_nonce}"~g' \
  //   ${__dirname}/build/server/pages/404.html
  // `)

  // https://unix.stackexchange.com/questions/195939/what-is-meaning-of-in-finds-exec-command
  // SED on all HTML pages - synchronous blocking operation - maybe too expensive in ressources overtime
  // const replace = await execPromise(`
  //   find ${__dirname}/build/server/pages/ -name '*.html' -exec \
  //   sed -i -E 's~(property=\\"csp-nonce\\"\\\scontent=|__webpack_nonce__=|nonce=)(\\")(---CSP_NONCE---|.{43}=)(\\")~\\1"${_nonce}"~g' {} ';'
  // `)

  const htmlFileExists = await accessFile(`${__dirname}/build/server/pages/${route}.html`)
  if (!htmlFileExists) return

  // SED only on HTML landing page called by initial url route
  const replace = await execPromise(`
    sed -i -E 's~(property=\\"csp-nonce\\"\\\scontent=|__webpack_nonce__=|nonce=)(\\")(---CSP_NONCE---|.{43}=)(\\")~\\1"${_nonce}"~g' \
    ${__dirname}/build/server/pages/${route}.html
  `)

  const { stdout, stderr } = replace
  if (stdout) {
    console.log(`Gateway Express sed html successful`)
  }
  if (stderr) {
    console.log(stderr)
    Promise.reject(new Error('Cannot write replace nonce value in html files'))
  }
  return
}

const sedNonceJS = async ()  => {
  const replace = await execPromise(`
    sed -i -E "s~(nonce:\\\s')(---CSP_NONCE---|.{43}=)(')~\\1${_nonce}'~g" \
    ${__dirname}/build/server/pages/_document.js
  `)

  // const replace = await execPromise(`
  //   sed -i -E 's~(nonce:\\\s)(\\")(---CSP_NONCE---|.{43}=)(\\")~\\1"${_nonce}"~g' \
  //   ${__dirname}/build/server/pages/_document.js
  // `)

  // const replace = await execPromise(`
  //   sed -i -E "s~(nonce:\\\s')(---CSP_NONCE---|.{43}=)(')~\\1${_nonce}'~g" \
  //   ${__dirname}/build/server/pages/_document.js && \
  //   sed -i -E "s~(nonce=')(---CSP_NONCE---|.{43}=)(')~\\1${_nonce}'~g" \
  //   ${__dirname}/src/pages/_document.tsx
  // `)

  const { stdout, stderr } = replace
  if (stdout) {
    console.log(`Gateway Express sed js successful`)
  }
  if (stderr) {
    console.log(stderr)
    Promise.reject(new Error('Cannot write replace nonce value in js files'))
  }
  return
}

const saveInitLang = async (_initLng)  => {
  const save = await execPromise(`
    jq -n --arg language ${_initLng} '{"initLng":$language}' > ${path.resolve(`${__dirname}`, 'initLng.json')} && \
    echo ${_initLng} > ${path.resolve(`${__dirname}`, 'initLng.txt')} && \
    echo ${_initLng}
  `)
  const { stderr } = save
  if (stderr) {
    console.log(stderr)
    Promise.reject(new Error('Cannot write _initLng to json file'))
  }
}

const dev = Boolean(process.env.FLEX_MODE !== 'production')
const hostname = process.env.FLEX_GATEWAY_HOSTNAME
const port = process.env.FLEX_GATEWAY_PORT

// when using middleware `hostname` and `port` must be provided below
const nextServer = createNextServer({ dev, hostname, port, conf: nextConfig })
const nextRequestHandler = nextServer.getRequestHandler()

let _currentRoute = ''
const app = express()
app.use(
  i18nextMiddleware.handle(i18nextServerSide)
)

app.use(corsOptions)
app.use(connectDatadog)
app.use(async(req, res, next) => {
  // console.log(`req port : ${req.socket.localPort}`)
  if (checkIsRoute(req.path)) {
    // console.log(`req path : ${req.path}`)
    // await readNonce()
    await generateNonce()
    // await sedNonceStaticHTML(req.path)
    // await sedNonceJS()
    _currentRoute = req.path
  }
  res.locals.cspNonce = _nonce
  next()
})
app.use(async(req, res, next) => {
  if (checkIsRoute(req.path)) {
    // console.log('///////////////////////////////////////////////////////////////////')
    // console.log('express server accepts', accepts(req).languages())
    // const lang = req.acceptsLanguages(locales)
    // if (lang) {
    //   console.log(`express server : The first accepted of [${locales}] is: ${lang}`)
    // } else {
    //   console.log(`express server : None of [${locales}] is accepted`)
    // }
    // console.log('///////////////////////////////////////////////////////////////////')

    const _initLng = await initLang(req)
    res.locals._initLng = _initLng
    saveInitLang(_initLng)
    res.append('Flex-Lng-Header', _initLng)
    // res.locals._i18n = res.locals.i18n
    // const _initLng = res.locals.i18n.options.defaultLocale

    const { _parsedUrl } = req

    // console.log('///////////////////////////////////////////////////////////////////')
    // // console.log('express server [1]', req)
    // console.log('express server [1]', _parsedUrl)
    // // console.log('express server [1]', res)
    // console.log('///////////////////////////////////////////////////////////////////')

    const urlParams = new URLSearchParams(_parsedUrl.search)
    urlParams.set('lang', _initLng)
    const params = Object.fromEntries(urlParams)
    req.query = params
    req.search = new URLSearchParams(params).toString()
    // req.url = `${url}?${req.search}`

    if (req.path !== '/') {
      req.url = `${_parsedUrl.pathname}?${req.search}`
    } else {
      req.url = `/home?${req.search}`
    }
  }
  next()
})

// if (process.env.FLEX_MODE === 'production') {
  app.use((req, res, next) => {
    return getContentSecurityPolicy(req, res, next, _nonce)
  })
// }

const server = `${process.env.FLEX_PROTOCOL}` === 'http://'
  ? http.createServer(app)
  : https.createServer(optionsHTTPS(), app)

server.listen(port, hostname, 34, (err) => {
  if (err) throw err
  console.log('🚀 dev ', dev)
  console.log(`@flex-gateway-ssr/nextjs-telenko: ready - started server on ${hostname}:${port}, url: ${process.env.FLEX_GATEWAY_HOST}`)
})

server.on('request', (req, res) => {
  if (req.path === _currentRoute) {
    if (process.env.DEBUG === 'true') console.log(`NextJS route reloaded : ${_currentRoute}`)
    _currentRoute = ''
    // res.redirect(req.path)
  }
})

nextServer.prepare().then(async () => {
  app.use((req, res) => {
    const parsedUrl = parse(req.url, true)
    // if (checkIsRoute(req.path)) {
    //   console.log('///////////////////////////////////////////////////////////////////')
    //   console.log('express server [2]', req.url)
    //   console.log('express server [2]', parsedUrl)
    //   console.log('express server [2]', i18nextServerSide.t('gateway.components.options_panel.debug.label', { lng: 'fr' }))
    //   console.log('///////////////////////////////////////////////////////////////////')
    // }
    nextRequestHandler(req, res, parsedUrl)
  })

  // app.get('*', (req, res) => {
  //   return nextRequestHandler(req, res)
  // })

  // app.all('*', (req, res) => {
  //   return nextRequestHandler(req, res)
  // })
})
