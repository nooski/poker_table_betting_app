'use client'

import React from 'react'
import LogoSvg from '../../public/logo/filled/rectangle/logo_flexiness_2.svg'
import { LogoProps } from '../../additional'
import { getStores } from '@flexiness/domain-store'

const stores = getStores()

const Logo: React.FC<LogoProps> = () => {
  const { navigationState } = stores.UIStore
  return (
    <div
      className={`flex-gateway-logo ${navigationState}`}
      style={{ margin: '0 auto', width: '25vw', display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}
    >
      <LogoSvg id='flexiness_logo_1' />
    </div>
  )
}

export { Logo }
