import { getOnBoardEventsStore, getUIStore } from '@flexiness/domain-store'
const CreateOnBoardEvent = getOnBoardEventsStore()
const UIStore = getUIStore()

export const RootStore = {
  CreateOnBoardEvent,
  UIStore,
}
