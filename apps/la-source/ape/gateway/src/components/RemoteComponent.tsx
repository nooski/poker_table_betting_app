/* eslint-disable no-console */
/* eslint-disable  @typescript-eslint/no-explicit-any */
/* eslint-disable  @typescript-eslint/no-unsafe-call */
/* eslint-disable  @typescript-eslint/no-unsafe-return */

'use client'

import React, { Suspense } from 'react'
import { useDynamicScript } from '@flexiness/domain-utils'
import { Logo } from './logo'
import { loadComponent } from '@src/components/utils/mf-utils'

function RemoteComponent(props: { module: string; url: string; scope: string; nonce: string; children?: any }) {
  const { children } = props
  const { ready, failed } = useDynamicScript({
    url: props.module && props.url,
    nonce: props.nonce,
  })

  if (!props.module) {
    console.log('Not system specified')
    return <Logo />
  }

  if (!ready) {
    // console.log(`Loading dynamic script: ${props.url}`)
    return <Logo />
  }

  if (failed) {
    console.log(`Failed to load dynamic script: ${props.url}`)
    return <Logo />
  }

  if (!global) return null

  const Component = React.lazy(async () => {
    const m = await loadComponent(props.scope, props.module)
    return m
  })

  return (
    <Suspense fallback={<Logo />}>
      <Component>{children}</Component>
    </Suspense>
  )
}

export default RemoteComponent
