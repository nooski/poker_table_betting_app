/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */

import { FlexGlobalThis } from 'flexiness'
declare let global: FlexGlobalThis

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const loadComponent = async (scope: string, module: any) => {
  // const monorepoPackageJson = await import(`${process.env.FLEX_PROJ_ROOT}/package.json`, {
  //   assert: { type: "json" },
  // })
  // const { dependencies } = monorepoPackageJson.default
  // console.log("dependencies['react']", dependencies['react'])

  const container = global[scope]

  // @ts-ignore
  await container.init(
    // LOADING REACT
    Object.assign(
      {
        react: {
          [`${process.env.DEPS_MONOREPO_REACT}`]: {
            get: () => Promise.resolve(() => require('react')),
            loaded: true,
          },
          // '18.3.1': {
          //   get: () => Promise.resolve(() => require('react')),
          //   loaded: true,
          // },
        },
        mobx: {
          [`${process.env.DEPS_MONOREPO_MOBX}`]: {
            get: () => Promise.resolve(() => require('mobx')),
            loaded: true,
          },
          // '6.12.0': {
          //   get: () => Promise.resolve(() => require('mobx')),
          //   loaded: true,
          // },
        },
        'mobx-react-lite': {
          [`${process.env.DEPS_MONOREPO_MOBX_REACT_LITE}`]: {
            get: () => Promise.resolve(() => require('mobx-react-lite')),
            loaded: true,
          },
          // '4.0.5': {
          //   get: () => Promise.resolve(() => require('mobx-react-lite')),
          //   loaded: true,
          // },
        },
      },
      global.__webpack_require__ ? global.__webpack_require__.o : {},
    ),
  )

  const factory = await global[scope].get(module)
  const Module = factory()
  return Module
}
