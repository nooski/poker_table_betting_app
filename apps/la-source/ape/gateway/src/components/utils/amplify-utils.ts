import { AMPLIFY_AUTH_CONFIG_V2 } from '@flexiness/aws'
import { cookies } from 'next/headers'
import { getCurrentUser } from 'aws-amplify/auth/server'
import { createServerRunner } from '@aws-amplify/adapter-nextjs'
import { generateServerClientUsingCookies } from '@aws-amplify/adapter-nextjs/data'
// import { Schema } from '../../amplify/data/resource'

// globalThis.React = React;

export const cookieBasedClient = generateServerClientUsingCookies({
  config: AMPLIFY_AUTH_CONFIG_V2,
  cookies,
})

// export const cookieBasedClient = generateServerClientUsingCookies<Schema>({
//   config,
//   cookies,
//   authMode: 'userPool',
// })

export const { runWithAmplifyServerContext } = createServerRunner({
  config: AMPLIFY_AUTH_CONFIG_V2,
})

export const isAuthenticated = async () =>
  await runWithAmplifyServerContext({
    nextServerContext: { cookies },
    async operation(contextSpec) {
      try {
        const user = await getCurrentUser(contextSpec)
        return !!user
      } catch (error) {
        return false
      }
    },
  })
