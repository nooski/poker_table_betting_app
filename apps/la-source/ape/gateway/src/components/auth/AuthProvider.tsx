/* eslint-disable no-console */

'use client'

import React from 'react'
import { isServer } from '@flexiness/domain-utils'
import { Amplify, type ResourcesConfig } from 'aws-amplify'
import {
  // AMPLIFY_AUTH_CONFIG_V1,
  AMPLIFY_AUTH_CONFIG_V2,
  AMPLIFY_CLIENT,
  // getCustomCredentialsProvider,
} from '@flexiness/aws'
import '@aws-amplify/ui-react/styles.css'

const { AmplifyUIReact, AmplifyUtils } = AMPLIFY_CLIENT
const { Authenticator } = AmplifyUIReact

const _getConfig = () => {
  if (process.env.FLEX_MODE === 'development' && process.env.FLEX_GATEWAY_USE_AMPLIFY_SANDBOX === 'true') {
    void import('@/../amplifyconfiguration.json').then((devSandBoxConfig) => {
      const { parseAmplifyConfig } = AmplifyUtils
      console.log(devSandBoxConfig.default)
      return parseAmplifyConfig(devSandBoxConfig.default)
    })
  }
  const prodConfig = {
    ...AMPLIFY_AUTH_CONFIG_V2,
    ...{
      Auth: {
        Cognito: {
          ...AMPLIFY_AUTH_CONFIG_V2?.Auth?.Cognito,
          loginWith: {
            ...AMPLIFY_AUTH_CONFIG_V2?.Auth?.Cognito.loginWith,
            oauth: {
              ...AMPLIFY_AUTH_CONFIG_V2?.Auth?.Cognito.loginWith?.oauth,
              redirectSignIn: [`${process.env.FLEX_GATEWAY_HOST}/`],
              redirectSignOut: [`${process.env.FLEX_GATEWAY_HOST}/`],
            },
          },
        },
        // Supply the custom credentials provider to Amplify
        // credentialsProvider: await getCustomCredentialsProvider(),
      },
    },
    ...{
      API: {
        GraphQL: {
          ...AMPLIFY_AUTH_CONFIG_V2?.API?.GraphQL,
          defaultAuthMode: 'apiKey',
          apiKey: process.env.FLEX_AWS_APPSYNC_APIKEY ?? '',
        },
      },
    },
  } as ResourcesConfig
  return prodConfig
}
if (isServer && process.env.DEBUG === 'true') console.log(_getConfig())

Amplify.configure(_getConfig(), { ssr: true })

const Auth = ({ children }: { children: React.ReactNode }) => {
  return <Authenticator.Provider>{children}</Authenticator.Provider>
}

export default Auth
