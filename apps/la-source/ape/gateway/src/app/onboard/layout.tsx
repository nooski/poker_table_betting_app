'use server'

// https://github.com/module-federation/universe/issues/1183

// import * as path from 'path'
import React, { Suspense } from 'react'
import { headers } from 'next/headers'
import getConfig from 'next/config'
const { serverRuntimeConfig } = getConfig()
// import classNames from 'classnames'
// import { ChunkExtractor } from '@loadable/server'
import RemoteComponent from '../../components/RemoteComponent'
// const { default: RemoteComponent} = await import('../../components/RemoteComponent')
import { ErrorBoundary } from 'react-error-boundary'
import Error from './error'
// import { Logo } from '../../components/logo'

const host = serverRuntimeConfig.FLEX_POKER_CLIENT_HOST as string
// const host = serverRuntimeConfig.FLEX_POKER_CLIENT_PROXY_PATHNAME as string
const mf = serverRuntimeConfig.FLEX_POKER_CLIENT_NAME as string
const gitCommitSHA = serverRuntimeConfig.FLEX_BUILD_ID as string

// globalThis.React = React

// const stats = `${process.env.FLEX_PROJ_ROOT}/build/loadable-stats.json`
// const webExtractor = new ChunkExtractor({
//   statsFile: stats,
//   entrypoints: [`mainEntry_${process.env.FLEX_GATEWAY_NAME}_${process.env.FLEX_BUILD_ID}`],
// })

import { Title } from '../../components/FlexServerComponents'

// eslint-disable-next-line @typescript-eslint/require-await
const OnboardLayout = async ({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode
}) => {
  const _nonce = headers().get('x-nonce') || '---CSP-nonce---'
  function Loading() {
    return <h2>Loading...</h2>
  }
  return (
    <ErrorBoundary fallback={<Error />}>
      <Suspense fallback={<Loading />}>
        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Title>Hello, NextJS - APE - La Source 👋</Title>
        </div>
        <RemoteComponent url={`${host}/remoteEntry_${mf}_${gitCommitSHA}.js`} module='./App' scope={`${mf}`} nonce={_nonce} />
        {children}
      </Suspense>
    </ErrorBoundary>
  )
}

export default OnboardLayout
