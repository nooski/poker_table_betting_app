'use client'

// import { isServer } from '@flexiness/domain-utils'
import React, { Suspense } from 'react'
import classNames from 'classnames'
import { observer } from 'mobx-react-lite'
// import { observer, enableStaticRendering } from 'mobx-react-lite'
// enableStaticRendering(true)
import type { NextPage } from 'next'
import { PageAppProps } from '../../../additional'
import { ErrorBoundary } from 'react-error-boundary'
import Error from './error'
import { Logo } from '../../components/logo'
// import { ActiveOnBoardEvent } from '../../components/ActiveOnBoardEvent'

// import {
//   Title,
//   Box,
// } from '@flex-design-system/react-ts/client-sync-styled-named'
// import * as styles from '@flex-design-system/framework/named'

import { Title, Box } from '@flex-design-system/react-ts/client-sync-styled-default'
import styles from 'flex-design-system-framework/main/all.module.scss'

const Onboard: NextPage<PageAppProps> = observer(() => {
  const [isClient, setIsClient] = React.useState(false)
  React.useEffect(() => {
    setIsClient(true)
  }, [])
  function Loading() {
    return <h2>Loading...</h2>
  }
  return (
    <>
      {/* <ConfigureAmplifyClientSide /> */}
      <ErrorBoundary fallback={<Error />}>
        <Suspense fallback={<Loading />}>
          <Box skeleton={!isClient} className={classNames(styles.isFlex, styles.isAlignItemsCenter, 'm-2')}>
            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
              <Title>🚀 Contenu dynamique - Rejoindre événement</Title>
            </div>
            <Logo />
            {/* <ActiveOnBoardEvent /> */}
          </Box>
        </Suspense>
      </ErrorBoundary>
    </>
  )
})

export default Onboard
