'use client'

import React from 'react'
import type { NextPage } from 'next'
import { PageAppProps } from '../../additional'

// import {
//   Title,
// } from '@flex-design-system/react-ts/client-sync-styled'

const Home: NextPage<PageAppProps> = () => {
  return (
    // <Title>Hello, NextJS - APE - La Source - Home page!</Title>
    <h1>Hello, NextJS - APE - La Source - Home page!</h1>
  )
}

export default Home
