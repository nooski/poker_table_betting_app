import { useContext } from 'react'
import { StoreContext } from '@src/components/stores/MobxProvider'

export const useStores = () => {
  return useContext(StoreContext)
}
