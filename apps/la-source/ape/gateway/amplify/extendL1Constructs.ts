// import { Backend, DefineBackendProps, BackendBase } from '@aws-amplify/backend'
import { UserPool, UserPoolClient } from 'aws-cdk-lib/aws-cognito'

// https://docs.amplify.aws/gen2/build-a-backend/add-aws-services/overriding-resources/
// https://docs.aws.amazon.com/cdk/api/v2/python/aws_cdk.aws_cognito/CfnUserPool.html

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const extendWithL1Resources = (backend: any) => {
  // extract L1 CfnUserPool resources
  const { cfnUserPool, cfnUserPoolClient, cfnIdentityPool } = backend.auth.resources.cfnResources
  // use CDK's `addPropertyOverride` to modify properties directly

  // https://docs.aws.amazon.com/cdk/api/v2/docs/aws-cdk-lib.aws_cognito.CfnUserPool.html
  // https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-cognito-userpool.html#aws-resource-cognito-userpool-properties

  cfnUserPool.addPropertyOverride(
    "UserPoolName",
    `v3onboard_userpool_nextjs_${process.env.FLEX_MODE}`
  )
  cfnUserPool.addPropertyOverride(
    "Policies",
    {
      PasswordPolicy: {
        MinimumLength: 8,
        RequireLowercase: true,
        RequireNumbers: true,
        RequireSymbols: true,
        RequireUppercase: true,
        TemporaryPasswordValidityDays: 7,
      },
    },
  )
  cfnUserPool.addPropertyOverride(
    "UsernameAttributes",
    ["email", "phone_number"],
  )
  cfnUserPool.addPropertyOverride(
    "AutoVerifiedAttributes",
    ["email", "phone_number"],
  )
  cfnUserPool.addPropertyOverride(
    "AliasAttributes",
    ["email", "phone_number"],
  )
  cfnUserPool.addPropertyOverride(
    "MfaConfiguration",
    "OFF",
  )
  cfnUserPool.addPropertyOverride(
    "DeletionProtection",
    "INACTIVE",
  )
  cfnUserPool.addPropertyOverride(
    "Schema",
    [
      { Name: "given_name", Required: true },
      { Name: "family_name", Required: true },
      { Name: "email", Required: true },
    ],
  )
  cfnUserPool.addPropertyOverride(
    "UsernameConfiguration",
    { CaseSensitive: false },
  )
  cfnUserPool.addPropertyOverride(
    "AdminCreateUserConfig",
    { AllowAdminCreateUserOnly: false },
  )
  cfnUserPool.addPropertyOverride(
    "EmailConfiguration",
    { EmailSendingAccount: "COGNITO_DEFAULT" },
  )
  cfnUserPool.addPropertyOverride(
    "UserPoolAddOns",
    { AdvancedSecurityMode: "OFF" },
  )
  // cfnUserPool.addPropertyOverride(
  //   "EmailVerificationMessage",
  //   "🚀 Votre code de vérification : {####}",
  // )
  // cfnUserPool.addPropertyOverride(
  //   "EmailVerificationSubject",
  //   "Votre code de vérification à l'application APE | La Source",
  // )
  // cfnUserPool.addPropertyOverride(
  //   "SmsVerificationMessage",
  //   "🚀 Votre code de vérification : {####}",
  // )
  // cfnUserPool.addPropertyOverride(
  //   "SmsAuthenticationMessage",
  //   "🚀 Votre code de vérification : {####}",
  // )
  cfnUserPool.addPropertyOverride(
    "UserPoolTags",
    { "Name": "La Source | APE | UserPool | OnBoard | v4 | NextJS" },
  )

  // L2 AWS CDK constructs
  const userPool = backend.auth.resources.userPool as UserPool
  // userPool.applyRemovalPolicy(RemovalPolicy.RETAIN_ON_UPDATE_OR_DELETE)

  // https://docs.aws.amazon.com/cdk/api/v2/docs/aws-cdk-lib.aws_cognito.CfnUserPoolClient.html
  // https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-cognito-userpoolclient.html

  // //////////////////////////////////////////////////////////////////////
  // Required
  // The user pool ID for the user pool where you want to create a user pool client.
  cfnUserPoolClient.addPropertyOverride(
    "UserPoolId",
    userPool.userPoolId
  )
  // //////////////////////////////////////////////////////////////////////
  // Optional
  // The name of the user pool client. The name must be unique within the user pool.
  cfnUserPoolClient.addPropertyOverride(
    "ClientName",
    `v3onboard_userpoolclient_nextjs_${process.env.FLEX_MODE}`
  )
  cfnUserPoolClient.addPropertyOverride(
    "GenerateSecret",
    false
  )
  cfnUserPoolClient.addPropertyOverride(
    "ExplicitAuthFlows",
    [
      "ALLOW_REFRESH_TOKEN_AUTH",
      "ALLOW_CUSTOM_AUTH",
      "ALLOW_USER_SRP_AUTH",
    ]
  )
  cfnUserPoolClient.addPropertyOverride(
    "ReadAttributes",
    ["email", "phone_number"]
  )
  cfnUserPoolClient.addPropertyOverride(
    "WriteAttributes",
    ["email", "phone_number"]
  )
  cfnUserPoolClient.addPropertyOverride(
    "AllowedOAuthFlowsUserPoolClient",
    true
  )
  cfnUserPoolClient.addPropertyOverride(
    "AllowedOAuthFlows",
    ["code"]
  )
  cfnUserPoolClient.addPropertyOverride(
    "AllowedOAuthScopes",
    ["phone", "email", "openid", "profile", "aws.cognito.signin.user.admin"]
  )
  cfnUserPoolClient.addPropertyOverride(
    "RefreshTokenValidity",
    30
  )
  cfnUserPoolClient.addPropertyOverride(
    "AccessTokenValidity",
    { "AccessToken": "60", "IdToken": "60", "RefreshToken": "30" }
  )
  cfnUserPoolClient.addPropertyOverride(
    "TokenValidityUnits",
    { "AccessToken": "minutes", "IdToken": "minutes", "RefreshToken": "days" }
  )
  // cfnUserPoolClient.addPropertyOverride(
  //   "CallbackURLs",
  //   ['http://localhost:4009/','https://after-school.flexiness.com/', 'https://local.flexiness.com/', 'http://localhost:3000/', 'http://localhost:3001/']
  // )
  // cfnUserPoolClient.addPropertyOverride(
  //   "LogoutURLs",
  //   ['http://localhost:4009/','https://after-school.flexiness.com/', 'https://local.flexiness.com/', 'http://localhost:3000/', 'http://localhost:3001/'],]
  // )
  cfnUserPoolClient.addPropertyOverride(
    "SupportedIdentityProviders",
    ["COGNITO", "Google"]
  )

  // L2 AWS CDK constructs
  const userPoolClient = backend.auth.resources.userPoolClient as UserPoolClient

  // https://docs.aws.amazon.com/cdk/api/v2/docs/aws-cdk-lib.aws_cognito.CfnIdentityPool.html
  // https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-cognito-identitypool.html

  // //////////////////////////////////////////////////////////////////////
  // Required
  cfnIdentityPool.addPropertyOverride(
    "AllowUnauthenticatedIdentities",
    false
  )
  // //////////////////////////////////////////////////////////////////////
  // Optional
  cfnIdentityPool.addPropertyOverride(
    "AllowClassicFlow",
    true
  )
  cfnIdentityPool.addPropertyOverride(
    "IdentityPoolName",
    `v3onboard_identitypool_nextjs_${process.env.FLEX_MODE}`
  )
}

export { extendWithL1Resources }
