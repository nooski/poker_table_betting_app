import { defineAuth, secret } from '@aws-amplify/backend'

/**
 * Define and configure your auth resource
 * When used alongside data, it is automatically configured as an auth provider for data
 * @see https://docs.amplify.aws/gen2/build-a-backend/auth
 */
export const auth: any = defineAuth({
  loginWith: {
    email: {
      verificationEmailStyle: 'CODE',
      verificationEmailSubject: "Votre code de vérification à l'application APE | La Source",
      verificationEmailBody: (createCode: () => string) => `🚀 Votre code de vérification : ${createCode()}`,
      userInvitation: {
        emailSubject: "Votre mot de passe temporaire pour l'application APE | La Source.",
        emailBody: (username: () => string, code: () => string) => `🚀 Votre nom d'utilisateur est ${username()} et votre mot de passe temporaire est ${code()}.`,
        smsMessage: (username: () => string, code: () => string) => `🚀 Votre nom d'utilisateur est ${username()} et votre mot de passe temporaire est ${code()}.`,
      },
    },
    phone: {
      verificationMessage: (createCode: () => string) => `🚀 Votre code de vérification : ${createCode()}`,
    },
    externalProviders: {
      google: {
        clientId: secret('FLEX_GOOGLE_APP_CLIENT_ID'),
        clientSecret: secret('FLEX_GOOGLE_APP_CLIENT_SECRET'),
        // attributeMapping: {
        //   email: 'email',
        //   family_name: 'family_name',
        //   given_name: 'given_name',
        //   username: 'sub',
        // }
      },
      scopes: ['PHONE', 'EMAIL', 'PROFILE', 'OPENID', 'COGNITO_ADMIN'],
      // callbackUrls: ['http://localhost:4009/','https://after-school.flexiness.com/', 'https://local.flexiness.com/', 'http://localhost:3000/', 'http://localhost:3001/'],
      callbackUrls: ['http://localhost:3000/'],
      // logoutUrls: ['http://localhost:4009/','https://after-school.flexiness.com/', 'https://local.flexiness.com/', 'http://localhost:3000/', 'http://localhost:3001/'],
      logoutUrls: ['http://localhost:3000/'],
      /**
       * NOTE: If you need to update this in the future, you must first unset it, then deploy the change to remove the domain
       * from the UserPool. After the domain has been removed, you can then provide a new value, and perform another deployment.
       */
      // @ts-expect-error
      domainPrefix: 'onboardevent-v3-ape-la-source',
    },
  },
  /**
   * enable multifactor authentication
   * @see https://docs.amplify.aws/gen2/build-a-backend/auth/manage-mfa
   */
  // multifactor: {
  //   mode: 'OPTIONAL',
  //   sms: {
  //     smsMessage: (code) => `Your verification code is ${code}`,
  //   },
  // },
  accountRecovery: 'PHONE_WITHOUT_MFA_AND_EMAIL',
  // userAttributes: {
  //   /** request additional attributes for your app's users */
  //   // profilePicture: {
  //   //   mutable: true,
  //   //   required: false,
  //   // },
  // },
})

// // import { secret } from '@aws-amplify/backend'
// import { App, Stack, SecretValue } from 'aws-cdk-lib'
// import { AmplifyAuth } from '@aws-amplify/auth-construct'

// const app = new App();
// const stack = new Stack(app, 'AuthStack');

// export const auth = new AmplifyAuth(stack, 'Auth', {
//   loginWith: {
//     email: {
//       verificationEmailStyle: 'CODE',
//       verificationEmailSubject: "Votre code de vérification à l'application APE | La Source",
//       verificationEmailBody: (createCode: () => string) => `🚀 Votre code de vérification : ${createCode()}`,
//       userInvitation: {
//         emailSubject: "Votre mot de passe temporaire pour l'application APE | La Source.",
//         emailBody: (username: () => string, code: () => string) => `🚀 Votre nom d'utilisateur est ${username()} et votre mot de passe temporaire est ${code()}.`,
//         smsMessage: (username: () => string, code: () => string) => `🚀 Votre nom d'utilisateur est ${username()} et votre mot de passe temporaire est ${code()}.`,
//       },
//     },
//     phone: {
//       verificationMessage: (createCode: () => string) => `🚀 Votre code de vérification : ${createCode()}`,
//     },
//     externalProviders: {
//       google: {
//         // clientId: 'googleClientId',
//         // // see https://docs.aws.amazon.com/cdk/api/v2/docs/aws-cdk-lib.SecretValue.html
//         // clientSecret: SecretValue.unsafePlainText('googleClientSecret'),
//         clientId: secret('FLEX_GOOGLE_APP_CLIENT_ID') as unknown as string,
//         clientSecret: SecretValue.unsafePlainText(secret('FLEX_GOOGLE_APP_CLIENT_SECRET') as unknown as string),
//       },
//       scopes: ['PHONE', 'EMAIL', 'PROFILE', 'OPENID', 'COGNITO_ADMIN'],
//       // callbackUrls: ['http://localhost:4009/','https://after-school.flexiness.com/', 'https://local.flexiness.com/', 'http://localhost:3000/', 'http://localhost:3001/'],
//       callbackUrls: ['http://localhost:3000/'],
//       // logoutUrls: ['http://localhost:4009/','https://after-school.flexiness.com/', 'https://local.flexiness.com/', 'http://localhost:3000/', 'http://localhost:3001/'],
//       logoutUrls: ['http://localhost:3000/'],
//       /**
//        * NOTE: If you need to update this in the future, you must first unset it, then deploy the change to remove the domain
//        * from the UserPool. After the domain has been removed, you can then provide a new value, and perform another deployment.
//        */
//       domainPrefix: 'onboardevent-v3-ape-la-source',
//     },
//   },
//   accountRecovery: 'PHONE_WITHOUT_MFA_AND_EMAIL',
// })
