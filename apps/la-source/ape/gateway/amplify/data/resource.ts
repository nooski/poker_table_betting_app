import { a, defineData, type ClientSchema } from '@aws-amplify/backend'
// import { type ClientSchema } from '@aws-amplify/data-schema'
// // import * as _schema from 'on-board-event-api'

// import _schema from '@flexiness/aws/schema'
// import _schema from 'flex-aws-schema'

// import * as models from '@flexiness/aws/models'

// import {
//   Attendees,
//   Times,
//   OnBoardEvent,
//   Post,
//   Comment,
//   User,
//   Tag,
//   OnBoardEventTags,
//   OnBoardEventUsers,
//   EActivity,
//   EDayOfWeek,
//   EEventType,
//   ELifeCycle,
//   EPostStatus,
//   ESchool,
//   ESchoolLevel,
//   EStatus,
//   EWhichWay,
//   EAttendeeRole,
//   TAddress,
//   TCalendarDateTime,
//   TPoint,
//   TPointList,
//   TAttendee,
//   TTime
// } from '@flexiness/aws/models'
// } from 'flex-aws-models'

// import {
//   OnBoardEvent,
//   // EActivity,
//   // EEventType,
//   // EWhichWay,
//   // ELifeCycle,
//   // User,
//   // ModelAttendeesConnection,
//   // ModelTimesConnection,
//   // ModelOnBoardEventTagsConnection,
//   // ModelOnBoardEventUsersConnection,
// } from 'on-board-event-api'

export function getValues<T extends Record<string, any>>(obj: T) {
  return Object.values(obj) as [(typeof obj)[keyof T]]
}

// import graphQLSchema from '../../../../../../amplify/backend/api/v3onBoardEvent/schema.graphql'
// import _schema from '../../../../../../packages/flex/config/aws/src/cloud/on-board/models/index.js'

// /* == STEP 1 ===============================================================
// The section below creates a Todo database table with a "content" field. Try
// adding a new "isDone" field as a boolean. The authorization rules below
// specify that owners, authenticated via your Auth resource can "create",
// "read", "update", and "delete" their own records. Public users,
// authenticated via an API key, can only "read" records.
// =========================================================================*/

// const schema = a.schema({
//   Todo: a
//     .model({
//       content: a.string(),
//     })
//     .authorization(allow => [allow.publicApiKey()])
//     // .authorization([a.allow.owner(), a.allow.public().to(['read'])]),
// });

// const schema = a.schema({
//   // Attendees: a
//   //   .model({
//   //     ...Attendees,
//   //   })
//   //   .authorization(allow => [allow.publicApiKey()]),
//   // Times: a
//   //   .model({
//   //     ...Times,
//   //   })
//   //   .authorization(allow => [allow.publicApiKey()]),
//   OnBoardEvent: a
//     .model({
//       ...OnBoardEvent,
//     })
//     .authorization(allow => [allow.publicApiKey()]),
//   // Post: a
//   //   .model({
//   //     ...Post,
//   //   })
//   //   .authorization(allow => [allow.publicApiKey()]),
//   // Comment: a
//   //   .model({
//   //     ...Comment,
//   //   })
//   //   .authorization(allow => [allow.publicApiKey()]),
//   // User: a
//   //   .model({
//   //     ...User,
//   //   })
//   //   .authorization(allow => [allow.publicApiKey()]),
//   // Tag: a
//   //   .model({
//   //     ...Tag,
//   //   })
//   //   .authorization(allow => [allow.publicApiKey()]),
//   // OnBoardEventTags: a
//   //   .model({
//   //     ...OnBoardEventTags,
//   //   })
//   //   .authorization(allow => [allow.publicApiKey()]),
//   // OnBoardEventUsers: a
//   //   .model({
//   //     ...OnBoardEventUsers,
//   //   })
//   //   .authorization(allow => [allow.publicApiKey()]),
//   // EActivity: a
//   //   .enum(getValues(EActivity)),
//   // EDayOfWeek: a
//   //   .enum(getValues(EDayOfWeek)),
//   // EEventType: a
//   //   .enum(getValues(EEventType)),
//   // ELifeCycle: a
//   //   .enum(getValues(ELifeCycle)),
//   // EPostStatus: a
//   //   .enum(getValues(EPostStatus)),
//   // ESchool: a
//   //   .enum(getValues(ESchool)),
//   // ESchoolLevel: a
//   //   .enum(getValues(ESchoolLevel)),
//   // EStatus: a
//   //   .enum(getValues(EStatus)),
//   // EWhichWay: a
//   //   .enum(getValues(EWhichWay)),
//   // EAttendeeRole: a
//   //   .enum(getValues(EAttendeeRole)),

//   // ...TAddress,
//   // ...TCalendarDateTime,
//   // ...TPoint,
//   // ...TPointList,
//   // ...TAttendee,
//   // ...TTime
// })

// export type Schema = ClientSchema<typeof schema>
// export const data = defineData({
//   schema,
//   authorizationModes: {
//     defaultAuthorizationMode: 'apiKey',
//     // API Key is used for a.allow.public() rules
//     apiKeyAuthorizationMode: {
//       expiresInDays: 30,
//     },
//   },
// })

// import { App, Stack } from 'aws-cdk-lib'
// import { UserPool } from 'aws-cdk-lib/aws-cognito'
// import { AmplifyData, AmplifyDataDefinition } from '@aws-amplify/data-construct'

// const app = new App();
// const stack = new Stack(app, 'TodoStack');

// export const data = new AmplifyData(stack, 'TodoApp', {
//   definition: AmplifyDataDefinition.fromString(/* GraphQL */ `
//     type Todo @model @auth(rules: [{ allow: owner }]) {
//       description: String!
//       completed: Boolean
//     }
//   `),
//   authorizationModes: {
//     userPoolConfig: {
//       userPool: UserPool.fromUserPoolId(stack, 'ImportedUserPool', '<YOUR_USER_POOL_ID>'),
//     },
//   },
// });

// Create a graphql schema using the schema located at '../../../../../../packages/flex/config/aws/src/cloud/on-board/models/schema.js'

// /* == STEP 2 ===============================================================
// Go to your frontend source code. From your client-side code, generate a
// Data client to make CRUDL requests to your table. (THIS SNIPPET WILL ONLY
// WORK IN THE FRONTEND CODE FILE.)

// Using JavaScript or Next.js React Server Components, Middleware, Server
// Actions or Pages Router? Review how to generate Data clients for those use
// cases: https://docs.amplify.aws/gen2/build-a-backend/data/connect-to-API/
// =========================================================================*/

// /*
// "use client"
// import { generateClient } from "aws-amplify/data";
// import { type Schema } from "@src/amplify/data/resource";

// const client = generateClient<Schema>() // use this Data client for CRUDL requests
// */

// /* == STEP 3 ===============================================================
// Fetch records from the database and use them in your frontend component.
// (THIS SNIPPET WILL ONLY WORK IN THE FRONTEND CODE FILE.)
// =========================================================================*/

// /* For example, in a React component, you can use this snippet in your
//   function's RETURN statement */
// // const { data: todos } = client.models.Todo.list()

// // return <ul>{todos.map(todo => <li key={todo.id}>{todo.content}</li>)}</ul>

export {}
