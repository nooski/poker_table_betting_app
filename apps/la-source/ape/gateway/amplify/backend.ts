import { defineBackend } from '@aws-amplify/backend'
import { auth } from './auth/resource'
// import { data } from './data/resource'
// import { extendWithL1Resources } from './extendL1Constructs'
// import { UserPool, UserPoolClient } from 'aws-cdk-lib/aws-cognito'

// https://constructs.dev/packages/@aws-amplify/data-construct/v/1.8.2-acdk-upgrade-2-129.0?lang=typescript
// import * as path from 'path'
// import * as cdk from 'aws-cdk-lib'
// import { App, Stack } from 'aws-cdk-lib'
// import { UserPool } from 'aws-cdk-lib/aws-cognito'
// import { AmplifyData, AmplifyDataDefinition } from '@aws-amplify/data-construct'
// import { AMPLIFY_AUTH_CONFIG_V2 } from '@flexiness/aws'

// const app = new App()
// const stack = new Stack(app, 'MultiFileStack')
// new AmplifyData(stack, 'MultiFileDefinition', {
//   definition: AmplifyDataDefinition.fromFiles(path.join(__dirname, '../../../../../../amplify/backend/api/v3onBoardEvent/schema.graphql')),
//   authorizationModes: {
//     defaultAuthorizationMode: 'API_KEY',
//     apiKeyConfig: {
//       description: 'Api Key for public access',
//       expires: cdk.Duration.days(30),
//     },
//     userPoolConfig: {
//       userPool: UserPool.fromUserPoolId(stack, 'ImportedUserPool', AMPLIFY_AUTH_CONFIG_V2?.Auth?.Cognito?.userPoolId!),
//     },
//   },
// })
// app.synth

const backend = defineBackend({
  auth,
  // data,
})

// // L2 AWS CDK constructs
// const userPool = backend.auth.resources.userPool as UserPool
// UserPool.fromUserPoolId(userPool, 'ImportedUserPool', process.env.FLEX_AWS_COGNITO_USER_POOL_ID!)

// const userPoolClient = backend.auth.resources.userPoolClient as UserPoolClient
// UserPoolClient.fromUserPoolClientId(userPoolClient, 'ImportedUserPoolClient', process.env.FLEX_AWS_COGNITO_USER_POOL_APP_CLIENT_ID!)

// extendWithL1Resources(backend)

// https://docs.amplify.aws/react/build-a-backend/add-aws-services/custom-resources/

// const customResourceStack = backend.createStack('MyCustomResources')
