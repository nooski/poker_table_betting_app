// https://dev.to/cress/when-you-need-https-on-local-environment-local-ssl-proxy-is-the-best-solution-24n6
// https://www.npmjs.com/package/local-ssl-proxy

// https://github.com/vercel/next.js/discussions/10935
// __dirname is not defined in ES module scope
import * as path from 'path'
import { fileURLToPath, parse } from 'url'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

// require.resolve for ES modules
import { createRequire } from 'module'
const require = createRequire(import.meta.url)

// const tracer = require('dd-trace').init()

const chalk = require('chalk')
const fs = require('fs')
const proxy = require('http-proxy')
const { optionsHTTPS } = require('@flexiness/certs')

// local-ssl-proxy certs
const localhost_cert = `${process.env.HOME}/certs/localhost/mkcert/localhost.pem`
const localhost_key = `${process.env.HOME}/certs/localhost/mkcert/localhost-key.pem`

proxy
	.createServer({
		xfwd: true,
		ws: true,
		target: {
			host: process.env.FLEX_GATEWAY_HOSTNAME,
			port: process.env.FLEX_GATEWAY_PORT,
		},
		ssl: {
			key: process.env.FLEX_DOMAIN_NAME === 'localhost' ? fs.readFileSync(localhost_key, 'utf8') : optionsHTTPS().key,
			cert: process.env.FLEX_DOMAIN_NAME === 'localhost' ? fs.readFileSync(localhost_cert, 'utf8') : optionsHTTPS().cert,
		},
	})
	.on('proxyRes', function (proxyRes, req, res) {
		console.log('RAW Response from the target', JSON.stringify(proxyRes.headers, true, 2))
		const { url } = req
		const { protocol, host } = absoluteUrl(req)
		if (protocol === 'https:') {
			res.writeHead(301, {
				Location: `http://${host}${url}`
			});
			res.end()
			return {}
		}
	})
	.on('error', function (e) {
		console.error(chalk.red(`Request failed to proxy: ${chalk.bold(e.code)}`))
	})
	.listen(process.env.FLEX_PROXY_PORT, process.env.FLEX_GATEWAY_HOSTNAME, 34, (err) => {
		if (err) throw err
		console.log('🚀 local ssl dev server')
	})
