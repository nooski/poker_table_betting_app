// __dirname is not defined in ES module scope
import * as path from 'path'
import { fileURLToPath, parse } from 'url'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

// require.resolve for ES modules
import { createRequire } from 'module'
const require = createRequire(import.meta.url)

// const tracer = require('dd-trace').init()

// https://github.com/vercel/next.js/discussions/10935
// const chalk = require('chalk')
// // const fs = require('fs')
// const proxy = require('http-proxy')
// const { optionsHTTPS } = require('@flexiness/certs')

// // local-ssl-proxy certs
// const localhost_cert = `${process.env.FLEX_PROJ_ROOT}/node_modules/local-ssl-proxy/resources/localhost.pem`
// const localhost_key = `${process.env.FLEX_PROJ_ROOT}/node_modules/local-ssl-proxy/resources/localhost-key.pem`

// proxy
// 	.createServer({
// 		xfwd: true,
// 		ws: true,
// 		target: {
// 			host: process.env.FLEX_GATEWAY_HOSTNAME,
// 			port: process.env.FLEX_GATEWAY_PORT,
// 		},
// 		ssl: {
// 			key: process.env.FLEX_DOMAIN_NAME === 'localhost' ? fs.readFileSync(localhost_key, 'utf8') : optionsHTTPS().key,
// 			cert: process.env.FLEX_DOMAIN_NAME === 'localhost' ? fs.readFileSync(localhost_cert, 'utf8') : optionsHTTPS().cert,
// 		},
// 	})
// 	.on('error', function (e) {
// 		console.error(chalk.red(`Request failed to proxy: ${chalk.bold(e.code)}`))
// 	})
// 	.listen(process.env.FLEX_PROXY_PORT, process.env.FLEX_GATEWAY_HOSTNAME, 34, (err) => {
// 		if (err) throw err
// 		console.log('🚀 local ssl dev server')
// 	})

const { createServer } = require('http')
const next = require('next')
const absoluteUrl = require('next-absolute-url').default

// when using middleware `hostname` and `port` must be provided below
const app = next({ dev: true, hostname: `${process.env.FLEX_GATEWAY_HOSTNAME}`, port: `${process.env.FLEX_GATEWAY_PORT}` })
const handle = app.getRequestHandler()

app.prepare().then(() => createServer((req, res) => {
	const { url } = req;
	const { protocol, host } = absoluteUrl(req);

	if (protocol === 'https:') {
		res.writeHead(301, {
			Location: `http://${host}${url}`
		});
		res.end()
		return {}
	}

	const parsedUrl = parse(url, true)
	return handle(req, res, parsedUrl)
}).listen(`${process.env.FLEX_GATEWAY_PORT}`, (err) => {
	if (err) throw err
	console.log('🚀 > local dev server is running!')
}))
