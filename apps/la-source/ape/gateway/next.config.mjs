// https://stackoverflow.com/questions/65604469/how-to-combine-several-plugins-inside-next-config-js-file
// https://nextjs.org/docs/app/building-your-application/configuring/content-security-policy

// __dirname is not defined in ES module scope
import * as path from 'path'
import { fileURLToPath } from 'url'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

// require.resolve for ES modules
import { createRequire } from 'module'
const require = createRequire(import.meta.url)

import subprocess from 'node:child_process'
import { promisify } from 'node:util'
const execPromise = promisify(subprocess.exec)

// import { isServer } from '@flexiness/domain-utils'
// import { setSecurityHeaders } from './setSecurityHeaders.mjs'

import * as dotenv from 'dotenv'
import * as dotenvExpand from 'dotenv-expand'
let _envProcess = {}
dotenv.config({
  processEnv: _envProcess,
  path: path.resolve(__dirname, `../../../../env/public/.env.${process.env['NODE_ENV']}`),
  override: true
})

// const dotenv = await import('dotenv').then((module) => module.default)
// const dotenvExpand = await import('dotenv-expand').then((module) => module.default)
// let _envProcess = {}
// dotenv.config({
//   processEnv: _envProcess,
//   path: `${process.env.FLEX_PROJ_ROOT}/env/public/.env.${process.env['NODE_ENV']}`,
//   override: true
// })

dotenvExpand.expand(_envProcess)

delete _envProcess['NODE_ENV']
delete _envProcess['NODE_OPTIONS']

// console.log('process.env', process.env)
// console.log('_envProcess', _envProcess)

const mfClientName = process.env.FLEX_POKER_CLIENT_NAME
const mfClientHost = process.env.FLEX_POKER_CLIENT_HOST
const mfClientBuildSys = process.env.FLEX_POKER_CLIENT_BUILD_SYS
const mfClientTarget = process.env.FLEX_POKER_CLIENT_TARGET

const psl = await import('psl')
const parsedDomain = psl.parse(`${process.env.FLEX_DOMAIN_NAME}`).domain ?? process.env.FLEX_DOMAIN_NAME

// const envExpanded = () => {
//   const envExpanded = dotenv.config({
//     path: path.resolve(__dirname, `../../../../env/public/.env.${process.env['NODE_ENV']}`),
//     override: true
//   })
//   if (envExpanded.error) {
//     reject(new Error(envExpanded.error))
//   }
//   delete envExpanded['NODE_ENV']
//   delete envExpanded['NODE_OPTIONS']
//   console.log(dotenvExpand.expand(envExpanded.parsed))
//   return dotenvExpand.expand(envExpanded.parsed)
// }

import {
  PHASE_PRODUCTION_BUILD,
  PHASE_PRODUCTION_SERVER,
  PHASE_DEVELOPMENT_SERVER
} from 'next/constants.js'

// import findWorkspaceRoot from 'find-yarn-workspace-root'
// const workspacePath = findWorkspaceRoot(__dirname)
// const rootLocation = path.relative(__dirname, workspacePath)
const rootLocation = process.env.FLEX_PROJ_ROOT
const mode = process.env.NODE_ENV
const prod = mode === 'production'

// log.info(mode)

// https://miyauchi.dev/posts/import-assertions-json-modules/
// const pkgMonorepo = await import(`${rootLocation}/package.json`, { assert: { type: 'json' } }).then((module) => module.default)
// const pkg = await import(`./package.json`, { assert: { type: 'json' } }).then((module) => module.default)
// const depsMonorepo = pkgMonorepo.dependencies
// const deps = pkg.dependencies

// eslint-disable-next-line @typescript-eslint/no-var-requires
const depsMonorepo = require(`${rootLocation}/package.json`).dependencies
// eslint-disable-next-line @typescript-eslint/no-var-requires
const deps = require('./package.json').dependencies

const cssLoader = target => require(`@flexiness/webpack/cssLoaderOnlyMiniExtract.cjs`)(target)
const modularSass = (target, _gitCommitSHA) => prod
  ? require(`@flexiness/webpack/modularSassOnlyMiniExtract.cjs`)(target, _gitCommitSHA)
  : require(`@flexiness/webpack/modularSassOnlyStyleLoaderDev.cjs`)(target, _gitCommitSHA)

let _gitCommitSHA = ''
async function getGitCommitSHA() {
  if ('GIT_COMMIT_SHA' in process.env) {
    return process.env.GIT_COMMIT_SHA
  } else {
    const result = await execPromise(`git rev-parse --short HEAD`)
    const { stdout } = result
    if (!stdout) return result
    _gitCommitSHA = stdout.trim()
    // console.log(`Git Commit SHA :`, _gitCommitSHA)
    return _gitCommitSHA
  }
}
_gitCommitSHA = await getGitCommitSHA()

import webpack from 'webpack'
const { container } = webpack
// const { ModuleFederationPlugin } = container

// import { ModuleFederationPlugin } from '@module-federation/enhanced/webpack'
const { ModuleFederationPlugin } = require('@module-federation/enhanced/webpack')

// App Directory is not supported by nextjs-mf. Use only pages directory
// import { NextFederationPlugin } from '@module-federation/nextjs-mf'

// const { ModuleFederationPlugin } = require('@module-federation/enhanced')
// import { ModuleFederationPlugin } from '@module-federation/enhanced'
// import { default as ModuleFederationPlugin } from '@module-federation/enhanced'
// const { ModuleFederationPlugin } = await import('@module-federation/enhanced')

import HtmlWebpackPlugin from 'html-webpack-plugin'
import Dotenv from 'dotenv-webpack'
// import prettier from 'prettier'
import NodePolyfillPlugin from 'node-polyfill-webpack-plugin'
import { TsconfigPathsPlugin } from 'tsconfig-paths-webpack-plugin'
// import LoadableWebpackPlugin from '@loadable/webpack-plugin'
// import { WebpackManifestPlugin } from 'webpack-manifest-plugin'
import { merge as webpackMerge } from 'webpack-merge'

// const parseEnvProcess = async (phase)  => {
//   if (phase === PHASE_DEVELOPMENT_SERVER) {
//     console.log('project : ', __dirname)
//     console.log('rootLocation : ', rootLocation)
//     console.log('host : ', process.env.FLEX_GATEWAY_HOST)
//     console.log('port : ', process.env.FLEX_GATEWAY_PORT)
//     console.log(`env  : FLEX_GATEWAY_MODULE_CSS=${process.env.FLEX_GATEWAY_MODULE_CSS}`)
//     console.log(`env  : BUILD_RUNNING=${process.env.BUILD_RUNNING}`)
//     console.log(_envProcess)
//   }
//   return _envProcess
// }

let _nonce = ''
const seedeRefNonce = async ()  => {
  _nonce = '---CSP_NONCE---'
}

let phaseDevCounter = 0

// import { ChunkExtractor } from '@loadable/server'
// const clientStats = path.resolve(
//   __dirname,
//   '../',
//   'on-board/client/build/',
//   `${mfClientBuildSys}/${mfClientTarget}/`
//   'loadable-stats.json',
// )
// const clientExtractor = new ChunkExtractor({
//   statsFile: clientStats,
//   entrypoints: [`mainEntry_${mfClientName}_${_gitCommitSHA}`],
// })
// console.log(clientExtractor['stats']['assetsByChunkName'])

const mainConfig = (async (phase) => {
  seedeRefNonce()
  /** @type {import('next').NextConfig} */
  const nextConfig = {
    reactStrictMode: true,
    experimental: {
      serverComponentsExternalPackages: [
        // `${mfClientBuildSys}`,
        'webpack',
      ],
    },
    // async rewrites() {
    //   return [
    //     {
    //       source: "/:path*",
    //       destination: "http://localhost:4009/:path*",
    //     },
    //   ];
    // },
    async redirects() {
      return [
        // Basic redirect
        {
          source: '/',
          destination: '/onboard',
          permanent: true,
        },
      ]
    },
    // async headers() {
    //   return [
    //     {
    //       // Apply these headers to all routes in your application.
    //       source: '/(.*)',
    //       // source: '/:path*',
    //       headers: setSecurityHeaders('permissive'), // These are response headers only
    //     },
    //   ]
    // },

    distDir: 'build',
    output: 'standalone',
    productionBrowserSourceMaps: true,
    swcMinify: true,
    env: {
      // ...await parseEnvProcess(phase),
      FLEX_MODE: process.env.FLEX_MODE,
      FLEX_PARSED_DOMAIN: parsedDomain,
      FLEX_CSP_NONCE: _nonce,
      FLEX_BUILD_ID: _gitCommitSHA,
      region: process.env.AWS_DEFAULT_REGION,
      AWS_DEFAULT_REGION: process.env.AWS_DEFAULT_REGION,
      FLEX_AWS_COGNITO_IDENTITY_POOL: process.env.FLEX_AWS_COGNITO_IDENTITY_POOL,
      FLEX_PROJ_ROOT: process.env.FLEX_PROJ_ROOT,
      FLEX_GATEWAY_NAME: process.env.FLEX_GATEWAY_NAME,
      FLEX_GATEWAY_HOST: process.env.FLEX_GATEWAY_HOST,
      FLEX_GATEWAY_PORT: process.env.FLEX_GATEWAY_PORT,
      FLEX_CONTENT_HOST: process.env.FLEX_CONTENT_HOST,
      FLEX_GATEWAY_USE_AMPLIFY_SANDBOX: process.env.FLEX_GATEWAY_USE_AMPLIFY_SANDBOX,
      DEPS_MONOREPO_REACT: depsMonorepo['react'],
      DEPS_MONOREPO_MOBX: depsMonorepo['mobx'],
      DEPS_MONOREPO_MOBX_REACT_LITE: depsMonorepo['mobx-react-lite']
    },
    serverRuntimeConfig: {
      FLEX_MODE: process.env.FLEX_MODE,
      PROJECT_ROOT: __dirname,
      FLEX_CSP_NONCE: _nonce,
      FLEX_BUILD_ID: _gitCommitSHA,
      region: process.env.AWS_DEFAULT_REGION,
      AWS_DEFAULT_REGION: process.env.AWS_DEFAULT_REGION,
      FLEX_AWS_COGNITO_IDENTITY_POOL: process.env.FLEX_AWS_COGNITO_IDENTITY_POOL,
      FLEX_PROJ_ROOT: process.env.FLEX_PROJ_ROOT,
      FLEX_GATEWAY_NAME: process.env.FLEX_GATEWAY_NAME,
      FLEX_POKER_CLIENT_HOST: mfClientHost,
      FLEX_POKER_CLIENT_NAME: mfClientName,
      FLEX_POKER_CLIENT_PROXY_PATHNAME: process.env.FLEX_POKER_CLIENT_PROXY_PATHNAME,
      // scriptAsset1: clientExtractor['stats']['assetsByChunkName'][`mainEntry_${mfClientName}_${_gitCommitSHA}`][0].toString(),
      // scriptAsset2: clientExtractor['stats']['assetsByChunkName'][`${mfClientName}`][0].toString(),
      // scriptAsset3: clientExtractor['stats']['assetsByChunkName']['flex-framework-styles'][0].toString()
    },
    typescript: {
      // !! WARN !!
      // Dangerously allow production builds to successfully complete even if
      // your project has type errors.
      // !! WARN !!
      ignoreBuildErrors: false,
      tsconfigPath: './tsconfig.build.json'
    },
    eslint: {
      // Warning: This allows production builds to successfully complete even if
      // your project has ESLint errors.
      ignoreDuringBuilds: false,
      dirs: ['src'],
    },
    transpilePackages: [
      '@flex-design-system/framework',
      '@flex-design-system/react-ts',
      '@flexiness/aws',
      'amplify-gen2',
      // '@flexiness/certs',
      '@flexiness/domain-store',
      '@flexiness/domain-utils',
      'on-board-event-api',
      'on-board-event-client',
      'on-board-event-common',
      // '@flexiness/domain-lib-mobx-react-router',
      '@flexiness/domain-tailwind',
      // '@flexiness/languages'
    ],
    pageExtensions: ['mdx', 'md', 'jsx', 'js', 'tsx', 'ts'],
    // generateBuildId: async () => {
    //   if (!isServer) return
    //   return _gitCommitSHA
    // },
    // sassOptions: {
    // },

    webpack: (config, options) => {
      const { isServer } = options

      if (phase === PHASE_DEVELOPMENT_SERVER && phaseDevCounter === 0) {
        console.log('\n\r')
        console.log(' ⚙️ project : ', __dirname)
        console.log(' ⚙️ rootLocation : ', rootLocation)
        console.log(' ⚙️ host : ', process.env.FLEX_GATEWAY_HOST)
        console.log(' ⚙️ port : ', process.env.FLEX_GATEWAY_PORT)
        console.log(` ⚙️ env  :  FLEX_GATEWAY_MODULE_CSS=${process.env.FLEX_GATEWAY_MODULE_CSS}`)
        console.log(` ⚙️ env  :  BUILD_RUNNING=${process.env.BUILD_RUNNING}`)
        console.log('\n\r')
        phaseDevCounter++
      }

      const mfConf = {
        name: `${process.env.FLEX_GATEWAY_NAME}`,
        // filename: isServer
        //   ? `server/chunks/remoteEntry_${process.env.FLEX_GATEWAY_NAME}_${_gitCommitSHA}.js`
        //   : `static/chunks/remoteEntry_${process.env.FLEX_GATEWAY_NAME}_${_gitCommitSHA}.js`,
        remotes: {
          [`${mfClientName}`]: isServer
            ? null
            : `${mfClientName}@${mfClientHost}/${mfClientBuildSys}/${mfClientTarget}/remoteEntry_${mfClientName}_${_gitCommitSHA}.js`,
        },

        shared: [
          {
            ...deps,
            ...depsMonorepo,
            react: {
              requiredVersion: depsMonorepo['react'],
              singleton: true,
              eager: true,
            },
            'react-dom': {
              requiredVersion: depsMonorepo['react-dom'],
              singleton: true,
              eager: true,
            },
            mobx: {
              singleton: true,
              requiredVersion: depsMonorepo['mobx'],
              eager: true,
            },
            'mobx-react-lite': {
              singleton: true,
              requiredVersion: depsMonorepo['mobx-react-lite'],
              eager: true,
            },
            'helmet-csp': {
              singleton: true,
              requiredVersion: deps['helmet-csp'],
              eager: true,
            },
            'psl': {
              singleton: true,
              requiredVersion: deps['psl'],
              eager: true,
            },
            'react-error-boundary': {
              singleton: true,
              requiredVersion: deps['react-error-boundary'],
              eager: true,
            },
            '@flexiness/aws': {
              import: '@flexiness/aws',
              requiredVersion: require('@flexiness/aws/package.json').version,
              shareKey: '@flexiness/aws', // under this name the shared module will be placed in the share scope
              shareScope: 'default', // share scope with this name will be used
              singleton: true, // only a single version of the shared module is allowed
            },
            'on-board-event-api': {
              import: 'on-board-event-api',
              requiredVersion: '0.1.0',
              shareKey: 'on-board-event-api', // under this name the shared module will be placed in the share scope
              shareScope: 'default', // share scope with this name will be used
              singleton: true, // only a single version of the shared module is allowed
            },
            'on-board-event-common': {
              import: 'on-board-event-common',
              requiredVersion: '0.1.0',
              shareKey: 'on-board-event-common', // under this name the shared module will be placed in the share scope
              shareScope: 'default', // share scope with this name will be used
              singleton: true, // only a single version of the shared module is allowed
            },
            '@flexiness/domain-store': {
              import: '@flexiness/domain-store',
              requiredVersion: require('@flexiness/domain-store/package.json').version,
              shareKey: '@flexiness/domain-store', // under this name the shared module will be placed in the share scope
              shareScope: 'default', // share scope with this name will be used
              singleton: true, // only a single version of the shared module is allowed
              eager: true,
            },
            '@flexiness/domain-utils': {
              import: '@flexiness/domain-utils',
              requiredVersion: require('@flexiness/domain-utils/package.json').version,
              shareKey: '@flexiness/domain-utils', // under this name the shared module will be placed in the share scope
              shareScope: 'default', // share scope with this name will be used
              singleton: true, // only a single version of the shared module is allowed
              eager: true,
            },
            '@flex-design-system/framework': {
              import: '@flex-design-system/framework',
              requiredVersion: require('@flex-design-system/framework/package.json').version,
              shareKey: '@flex-design-system/framework', // under this name the shared module will be placed in the share scope
              shareScope: 'default', // share scope with this name will be used
              singleton: true, // only a single version of the shared module is allowed
              eager: true,
            },
            '@flex-design-system/react-ts': {
              import: '@flex-design-system/react-ts',
              requiredVersion: require('@flex-design-system/react-ts/package.json').version,
              shareKey: '@flex-design-system/react-ts', // under this name the shared module will be placed in the share scope
              shareScope: 'default', // share scope with this name will be used
              singleton: true, // only a single version of the shared module is allowed
              eager: true,
            },
          }
        ],
      }

      // https://github.com/module-federation/universe/issues/1937
      // if (isServer) {
      //   config.optimization.usedExports = false
      // }

      // if (!isServer) {
      //   config.plugins.push(
      //     new ModuleFederationPlugin({
      //       ...mfConf
      //     })
      //     // new NextFederationPlugin({
      //     //   ...mfConf
      //     // })
      //   )
      // }

      return webpackMerge({
      // return {
          ...config,
          mode: mode,
          ...(process.env.FLEX_MODE === 'development'
            ? {
              // devtool: 'eval-source-map',
              optimization: {
                minimize: false
              },
            } : {
              // devtool: false,
              optimization: {
                minimize: true
              },
            }
          ),
          watch: process.env.BUILD_RUNNING === 'undefined',
          watchOptions: {
            aggregateTimeout: 300,
            poll: 1000,
            ignored: /node_modules/,
            stdin: true,
          },
          target: !isServer ? 'browserslist:last 1 chrome version' : 'async-node20',
          // output: {
          //   path: path.resolve(__dirname, 'build', `${isServer ? 'server' : 'static'}`),
          //   publicPath: `${mfClientHost}/`,
          //   // publicPath: `${mfClientHost}/${target}/`,
          //   // publicPath: `${process.env.FLEX_GATEWAY_HOST}/${isServer ? 'server' : 'static'}/`,
          //   // publicPath: 'auto',
          //   // crossOriginLoading: 'anonymous',
          //   // clean: true,
          //   // filename: '[name].[contenthash].js',
          //   // chunkFilename: '[name].[contenthash].js',
          // },
          module: {
            ...config.module,
            rules: [
              ...config.module.rules,
              // SWC does not currently support TypeScript project references
              // https://github.com/swc-project/swc/discussions/2156
              // {
              //   test: /\.ts(x)?$/,
              //   exclude: /node_modules/,
              //   use: {
              //     loader: require.resolve('swc-loader'),
              //     options: {
              //       sourceMap: !prod,
              //       // sourceMap: false,
              //       jsc: {
              //         parser: {
              //           syntax: 'typescript',
              //           tsx: true,
              //         },
              //         transform: {
              //           react: {
              //             runtime: 'automatic',
              //             // development: !_prod,
              //             // refresh: !_prod,

              //             // pragma: 'React.createElement',
              //             // pragmaFrag: 'React.Fragment',
              //             // throwIfNamespace: true,
              //             // development: false,
              //             // useBuiltins: false,
              //           },
              //         },
              //       },
              //       env: {
              //         targets: 'Chrome >= 48', // browser compatibility
              //       },
              //     },
              //   }
              // },
              {
                test: /\.ts(x)?$/,
                loader: require.resolve('ts-loader'),
                exclude: /node_modules/,
                options: {
                  configFile: path.resolve(__dirname, 'tsconfig.build.json'),
                  projectReferences: true,
                  transpileOnly: true,
                }
              },
              // {
              //   test: /\.js$/,
              //   enforce: "pre",
              //   use: ["source-map-loader"],
              // },
              {
                test: /\.svg$/,
                use: ['@svgr/webpack'],
              },
              {
                test: /\.csv$/,
                // loader: require.resolve('csv-loader'),
                loader: 'csv-loader',
                options: {
                  dynamicTyping: true,
                  header: true,
                  skipEmptyLines: true
                }
              }
            ],
          },
          resolve: {
            ...config.resolve,
            extensions: [
              ...config.resolve.extensions,
              '.tsx',
              '.ts',
              '.js',
              '.jsx',
              '.d.ts',
              '.json',
              '.yaml',
              '.gql',
              '.graphql',
              '.svg',
              '.otf',
              '.ttf',
              '.css',
              '.scss'
            ],
            fallback: {
              ...config.resolve.fallback,
              // https://github.com/vercel/next.js/issues/7755
              ...(!isServer && {
                fs: false,
                path: false
              }),
              buffer: require.resolve('buffer')
            },
            plugins: [
              ...config.resolve.plugins,
              new TsconfigPathsPlugin({
                configFile: path.resolve(__dirname, 'tsconfig.build.json')
              })
            ],
          },
          plugins: [
            ...config.plugins,
            // new ForkTsCheckerWebpackPlugin(),
            // new ForkTsCheckerNotifierWebpackPlugin({ excludeWarnings: true }),

            new HtmlWebpackPlugin({
              inject: false,
              template: `./public/index.html`,
              // https://github.com/module-federation/module-federation-examples/issues/102
              publicPath: '/',
              filename: `index.ejs`,
              // https://stackoverflow.com/a/76229357/10159170
              // templateContent: ({ htmlWebpackPlugin }) => {
              //   const { html } = htmlWebpackPlugin.options;
              //   return prettier.format(html, { parser: 'html' });
              // },
            }),

            // https://github.com/mrsteele/dotenv-webpack/issues/70#issuecomment-942441074
            // new Dotenv({
            //   path: `${process.env.FLEX_PROJ_ROOT}/env/public/.env.${process.env.FLEX_MODE}`,
            //   systemvars: true,
            // }),

            new webpack.DefinePlugin({
              // // @ts-expect-error
              // __GIT_BRANCH__: childProcess.execSync('git rev-parse --abbrev-ref HEAD'),
              // // @ts-expect-error
              // __GIT_COMMIT__: childProcess.execSync('git rev-parse HEAD'),

              // ensure the NODE_ENV targets production, making react optimized for production
              // with lesser checks and assertions
              // as per https://reactjs.org/docs/optimizing-performance.html#webpack
              // 'process.env.NODE_ENV': JSON.stringify('production'),
              // 'process.env': JSON.stringify(process.env)

              // https://github.com/mrsteele/dotenv-webpack/issues/377
              ..._envProcess
            }),

            // new LoadableWebpackPlugin(),
            new NodePolyfillPlugin(),
            // new WebpackManifestPlugin({}),

            // Work around for Buffer is undefined:
            // https://github.com/webpack/changelog-v5/issues/10
            new webpack.ProvidePlugin({
              process: 'process/browser',
              Buffer: ['buffer', 'Buffer'],
            }),
          ],
          experiments: {
            css: true,
            topLevelAwait: true,
            // outputModule: true,
            layers: true
          },
          infrastructureLogging: {
            level: 'none',
            // colors: true,
            // level: 'verbose',
            // debug: [/PackFileCache/]
          },
        },

        // // Css Loaders
        // cssLoader({ target: isServer ? 'node' : 'web' }),
        // // Modular Sass loaders
        modularSass({ target: isServer ? 'node' : 'web', _gitCommitSHA }),
      )

    }
  }
  return nextConfig
})

export default mainConfig
