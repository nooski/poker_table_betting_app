/* eslint-disable prefer-destructuring */
/* eslint-disable no-useless-escape */
/* eslint-disable max-len */
/* eslint-disable no-console */
// __dirname is not defined in ES module scope
import * as path from 'path'
import subprocess from 'node:child_process'
import { promisify } from 'node:util'
const execPromise = promisify(subprocess.exec)
import { PathLike, promises as fs } from 'fs'
// import escapeStringRegexp from 'escape-string-regexp'
// import pc from "picocolors"
import { makeAutoObservable, observable, action, autorun, set, get, toJS } from 'mobx'
import * as dotenv from 'dotenv'
import * as dotenvExpand from 'dotenv-expand'

const _OSslashCPU = 'linux/amd64'

const getEnvHCL = dotenv.config({ path: path.resolve(`${process.env.FLEX_PROJ_ROOT}/pipeline-ci/docker/v2/env/ci/${_OSslashCPU}/.env`), override: true })
if (getEnvHCL.error) {
  throw getEnvHCL.error
}
const envHCL = dotenvExpand.expand(getEnvHCL).parsed
console.log('########################################################################')
console.log('⚡️... Loading Local Env Variables ...⚡️')
console.log('########################################################################')
console.log(`env from ${process.env.FLEX_CI_DIR}`, envHCL)
console.log('FLEX_MODE', process.env.FLEX_MODE)
console.log('FLEX_PROJ_ROOT', process.env.FLEX_PROJ_ROOT)
console.log('FLEX_IMAGE_REPOSITORY -> ORIGINAL', process.env.FLEX_IMAGE_REPOSITORY)

type JSONValue = string | number | boolean | { [x: string]: JSONValue } | Array<JSONValue>

// https://stackoverflow.com/a/67662539
interface Project {
  projDirAbs: string
  projDirRel: string
  projName: string
  contextName: string
  imageName: string
  jsonHCL: JSONValue | null
}

type ProjectsMap = Map<string, Project>

class Projects {
  constructor(
    public all: ProjectsMap,
    public yarnWorkspacesLoopCompleted: boolean,
    public parsingLoopItem: boolean,
  ) {
    makeAutoObservable(this, {
      all: observable,
      yarnWorkspacesLoopCompleted: observable,
      parsingLoopItem: observable,
      increment: action,
      byKey: action,
      finished: action,
      parsing: action,
    })
    this.all = all
    this.yarnWorkspacesLoopCompleted = yarnWorkspacesLoopCompleted
    this.parsingLoopItem = parsingLoopItem
  }

  get isFinished() {
    if (this.parsingLoopItem) return false
    return this.yarnWorkspacesLoopCompleted

    // return this.yarnWorkspacesLoopCompleted && !this.parsingLoopItem
  }

  get allSize() {
    return this.all.size
  }

  byKey = (key: string) => {
    // console.log(key)
    return get(this.all, key) as Project
  }

  increment = (key: string, value: Project) => {
    // console.log(value)
    set(this.all, key, value)
    // this.parsingLoopItem = false
  }

  finished = (bool: boolean) => {
    this.yarnWorkspacesLoopCompleted = bool
  }

  parsing = (bool: boolean) => {
    this.parsingLoopItem = bool
  }
}

const projects = new Projects(new Map(), false, false)

const workerData = {
  // scope: 'yarn workspaces foreach --all run flex-bake',
  scope: 'pnpm -r exec flex-bake',
}

let _rootDir: string | undefined
let _branch: string
// let _workDirAbs: string
// let _workDirRel: string
let _projName: string
let _contextName: string
let _imageName: string
let _jsonHcl: string
let _targetsArray: string[]
// const _flexOutputLoc:string = `${process.env.FLEX_OUTPUT_LOC}`
const _flexOutputLoc: string = `docker`

function setRoot() {
  _rootDir = process.env.FLEX_PROJ_ROOT
  return _rootDir
}

const accessFile = async (path: PathLike) => {
  try {
    await fs.access(path)
    return true
  } catch (err) {
    // console.log(err)
    return false
  }
}

async function cleanGeneratedFolder() {
  // const result = await execPromise(`
  //   rm -rf ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_BAKE_DIR}/generated && \
  //   rm -rf ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKERFILES_DIR}/generated && \
  //   rm -rf ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_INCLUDES_DIR}/generated && \
  //   mkdir -p ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_BAKE_DIR}/generated/ && \
  //   mkdir -p ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKERFILES_DIR}/generated/ && \
  //   mkdir -p ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_INCLUDES_DIR}/generated/
  // `)
  const result = await execPromise(`
    rm -rf ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_BAKE_DIR}/generated.json && \
    rm -rf ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_BAKE_DIR}/generated && \
    rm -rf ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_INCLUDES_DIR}/generated && \
    mkdir -p ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_BAKE_DIR}/generated/ && \
    mkdir -p ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_INCLUDES_DIR}/generated/
  `)
  const { stdout } = result
  if (!stdout) return result
  return stdout
}

async function getGitBranch() {
  const result = await execPromise(`git rev-parse --abbrev-ref HEAD`)
  const { stdout } = result
  if (!stdout) return result
  _branch = stdout.trim()

  const last = new RegExp(`([^\/]+$)`)
  process.env.FLEX_IMAGE_REPOSITORY = String(process.env.FLEX_IMAGE_REPOSITORY).replace(last, `${_branch}`)
  console.log('FLEX_IMAGE_REPOSITORY -> UPDATED', process.env.FLEX_IMAGE_REPOSITORY)

  return _branch
}

async function editEnvHCL() {
  const result = await execPromise(`
    sed -i -E 's~(FLEX_IMAGE_REPOSITORY=\")(.+)(\")~FLEX_IMAGE_REPOSITORY="${process.env.FLEX_IMAGE_REPOSITORY}"~' \
    ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKER_DIR}/env/ci/${_OSslashCPU}/.env.hcl && \
    sed -i -E 's~(FLEX_BRANCH=\")(.+)(\")~FLEX_BRANCH="${_branch}"~' \
    ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKER_DIR}/env/ci/${_OSslashCPU}/.env.hcl
  `)
  const { stdout } = result
  if (!stdout) return result
  return stdout
}

async function editEnv() {
  const result = await execPromise(`
    sed -i -E 's~(FLEX_IMAGE_REPOSITORY=)(.+)~FLEX_IMAGE_REPOSITORY=${process.env.FLEX_IMAGE_REPOSITORY}~' \
    ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKER_DIR}/env/ci/${_OSslashCPU}/.env && \
    sed -i -E 's~(FLEX_BRANCH=)(.+)~FLEX_BRANCH=${_branch}~' \
    ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKER_DIR}/env/ci/${_OSslashCPU}/.env
  `)
  const { stdout } = result
  if (!stdout) return result
  return stdout
}

async function getProjectName(projDirAbs: string) {
  const result = await execPromise(`jq '.name' ${`${projDirAbs}/package.json`}`)
  const { stdout } = result
  _projName = stdout.trim().replace(/['"]+/g, '') || ''
  _contextName = _projName.replace(/^\@/, '').replace(/\//, '-').replace(/_/g, '-') || ''
  _imageName = _contextName.replace(/-/g, '_') || ''
  // console.log(_projName)
  // console.log(_contextName)
  // console.log(_imageName)
  projects.increment(`${_projName}`, {
    ...projects.byKey(`${_projName}`),
    projName: `${_projName}`,
    contextName: `${_contextName}`,
    imageName: `${_imageName}`,
  } as Project)
  // console.log(toJS(projects.byKey(`${_projName}`)))
  return { projName: _projName, contextName: _contextName, imageName: _imageName }
}

async function makeJsonHCL(newProj: Project) {
  // https://unix.stackexchange.com/q/676634
  // https://stackoverflow.com/a/5753719

  const tagsArray = [`${process.env.FLEX_IMAGE_REPOSITORY}:${newProj.imageName}`]
  const cacheFromArray = [`type=${_flexOutputLoc},ref=${process.env.FLEX_IMAGE_REPOSITORY}:${newProj.imageName}`]
  const outputArray = [`type=${_flexOutputLoc}`]
  const result = await execPromise(`
    contextsJson=$(jq -n \
      --arg base-env "target:base_env" \
      --arg install-deps "target:install_deps" \
      --arg build-libs "target:build_libs" \
      '$ARGS.named'
    )
    argsJson=$(jq -n \
      --arg BUILDKIT_INLINE_CACHE "1" \
      --arg CI "${process.env.FLEX_CI}" \
      --arg FLEX_SERVICE "${process.env.FLEX_SERVICE}" \
      --arg FLEX_APP "${process.env.FLEX_APP}" \
      --arg FLEX_DOMAIN_NAME "${process.env.FLEX_DOMAIN_NAME}" \
      --arg FLEX_BASE_DOMAIN "${process.env.FLEX_BASE_DOMAIN}" \
      --arg FLEX_IMAGE_REPOSITORY "${process.env.FLEX_IMAGE_REPOSITORY}" \
      --arg FLEX_HOME "${process.env.FLEX_HOME}" \
      --arg FLEX_MODE "${process.env.FLEX_MODE}" \
      --arg FLEX_BRANCH "${_branch}" \
      --arg FLEX_PROTOCOL "${process.env.FLEX_PROTOCOL}" \
      --arg FLEX_USER "${process.env.FLEX_USER}" \
      --arg FLEX_PROJ_DIR_GENERATED "${newProj.projDirRel}" \
      --arg FLEX_DOCKER_PLATFORM "${_OSslashCPU}" \
      --arg FLEX_CI_ARCH_TAG "${process.env.FLEX_CI_ARCH_TAG}" \
      --arg FLEX_OS "${process.env.FLEX_OS}" \
      --arg FLEX_CPU "${process.env.FLEX_CPU}" \
      '$ARGS.named'
    )
    argsJsonOnBoardEventParent=$(jq -n \
      --arg FLEX_AWS_PROJECT_REGION "${process.env.FLEX_AWS_PROJECT_REGION}" \
      --arg FLEX_AWS_APPSYNC_GRAPHQL_ENDPOINT "${process.env.FLEX_AWS_APPSYNC_GRAPHQL_ENDPOINT}" \
      --arg FLEX_AWS_APPSYNC_REGION "${process.env.FLEX_AWS_APPSYNC_REGION}" \
      --arg FLEX_AWS_APPSYNC_AUTHENTICATIONTYPE "${process.env.FLEX_AWS_APPSYNC_AUTHENTICATIONTYPE}" \
      --arg FLEX_AWS_APPSYNC_APIKEY "${process.env.FLEX_AWS_APPSYNC_APIKEY}" \
      '$ARGS.named'
    )
    tagsArray=$(echo '[]' | jq '. + ${JSON.stringify(tagsArray)}')
    cacheFromArray=$(echo '[]' | jq '. + ${JSON.stringify(cacheFromArray)}')
    outputArray=$(echo '[]' | jq '. + ${JSON.stringify(outputArray)}')
    innerJson=$(jq -n \
      --arg context "." \
      --argjson contexts "$contextsJson" \
      --argjson args "$argsJson" \
      --argjson tags "$tagsArray" \
      --argjson cache-from "$cacheFromArray" \
      --argjson output "$outputArray" \
      --arg pull "true" \
      --arg dockerfile "${process.env.FLEX_DOCKERFILES_DIR}/generated/${newProj.imageName}.dockerfile" \
      '$ARGS.named'
    )
    final=$(jq -n \
      --argjson ${newProj.imageName} "$innerJson" \
      '$ARGS.named'
    )
    echo "$final" > "tmp" && mv "tmp" ${process.env.FLEX_DOCKER_DIR}/bake/generated/${newProj.imageName}.json
    echo "$final"
  `)
  const { stdout, stderr } = result
  if (!stderr) {
    _jsonHcl = stdout
    projects.increment(`${newProj.projName}`, {
      ...projects.byKey(`${newProj.projName}`),
      jsonHCL: _jsonHcl,
    })
    // console.log(toJS(projects.byKey(`${newProj.projName}`)))
  }
}

async function makeGitlabInclude(newProj: Project) {
  const result = await execPromise(`cat \
    ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_INCLUDES_DIR}/template_generated.yml > \
    ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_INCLUDES_DIR}/generated/${newProj.imageName}.yml
  `)
  const { stdout, stderr } = result
  if (!stderr) {
    return Promise.resolve(stdout)
  }
  return Promise.reject(stderr)
}

async function makeProjDockerfileTxt(newProj: Project) {
  const file = await accessFile(`${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKERFILES_DIR}/generated/${newProj.imageName}.dockerfile`)
  if (!file) {
    const result = await execPromise(`cat \
      ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKERFILES_DIR}/template_generated.txt > \
      ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKERFILES_DIR}/generated/${newProj.imageName}.txt
    `)
    const { stdout, stderr } = result
    if (!stderr) {
      return Promise.resolve(stdout)
    }
    return Promise.reject(stderr)
  }
  return Promise.resolve()
}

async function editGitlabInclude(newProj: Project) {
  const result = await execPromise(`envsub \
    --env TARGET_NAME=${newProj.imageName} \
    --env JOB_NAME=${newProj.imageName.toUpperCase()} \
    --env PROJ_DIR=${newProj.projDirRel} \
    --syntax dollar-both \
    ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_INCLUDES_DIR}/template_generated.yml \
    ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_INCLUDES_DIR}/generated/${newProj.imageName}.yml
  `)
  const { stdout, stderr } = result
  if (!stderr) {
    return Promise.resolve(stdout)
  }
  return Promise.reject(stderr)
}

async function editProjDockerfileTxt(newProj: Project) {
  const file = await accessFile(`${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKERFILES_DIR}/generated/${newProj.imageName}.dockerfile`)
  if (!file) {
    const result = await execPromise(`envsub \
      --env CONTEXT_NAME=${newProj.contextName} \
      --env PROJ_NAME=${newProj.projName} \
      --env PROJ_DIR=${newProj.projDirRel} \
      --env TARGET_NAME=${newProj.imageName} \
      --syntax dollar-both \
      ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKERFILES_DIR}/generated/${newProj.imageName}.txt \
      ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKERFILES_DIR}/generated/${newProj.imageName}.dockerfile
    `)
    const { stdout, stderr } = result
    if (!stderr) {
      return Promise.resolve(stdout)
    }
    return Promise.reject(stderr)
  }
  return Promise.resolve()
}

async function removeProjDockerfileTxt(newProj: Project) {
  const result = await execPromise(`rm -rf \
    ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_DOCKERFILES_DIR}/generated/${newProj.imageName}.txt
  `)
  const { stdout, stderr } = result
  if (!stderr) {
    return Promise.resolve(stdout)
  }
  return Promise.reject(stderr)
}

async function exportJsonHCL() {
  console.log('########################################################################')
  console.log('⚡️... Exporting Generated HCL Definitions ...⚡️')
  console.log('########################################################################')
  const result = await execPromise(`
    // jq -s 'reduce.[] as $item ({}; . * $item)' ${process.env.FLEX_DOCKER_DIR}/bake/generated/*.json | \
    // jq '{"target": .}' \
    // > "tmp" && mv "tmp" ${process.env.FLEX_DOCKER_DIR}/bake/generated.json

    targetJson=$(jq -s 'reduce.[] as $item ({}; . * $item)' ${process.env.FLEX_DOCKER_DIR}/bake/generated/*.json)
    groupDefaultTargetsArray=$(echo '[]' | jq '. + ${JSON.stringify(_targetsArray)}')
    defaultJson=$(jq -n \
      --argjson targets "$groupDefaultTargetsArray" \
      '$ARGS.named'
    )
    groupJson=$(jq -n \
      --argjson default "$defaultJson" \
      '$ARGS.named'
    )
    final=$(jq -n \
      --argjson group "$groupJson" \
      --argjson target "$targetJson" \
      '$ARGS.named'
    )
    echo "$final" > "tmp" && mv "tmp" ${process.env.FLEX_DOCKER_DIR}/bake/generated.json
    echo "$final"
  `)
  const { stdout } = result
  if (!stdout) return result
  return stdout
}

async function ansibleUpdateBuildHCL() {
  console.log('########################################################################')
  console.log('⚡️... Updating Build HCL Definitions ...⚡️')
  console.log('########################################################################')
  const result = await execPromise(`
    ansible-playbook \
    --extra-vars="base_path=${process.env.FLEX_PROJ_ROOT}" \
    ${process.env.FLEX_PROJ_ROOT}/${process.env.FLEX_CI_DIR}/ansible/build-bake-generated.yml
  `)
  const { stdout } = result
  if (!stdout) return result
  return stdout
}

async function testBake() {
  console.log('########################################################################')
  console.log('⚡️... Merging Bake Definitions ...⚡️')
  console.log('########################################################################')
  const result = await execPromise(`
    docker buildx bake \
    -f ${process.env.FLEX_DOCKER_DIR}/bake/build.hcl \
    -f ${process.env.FLEX_DOCKER_DIR}/bake/generated.json \
    -f ${process.env.FLEX_DOCKER_DIR}/env/ci/${_OSslashCPU}/.env.hcl \
    --print
  `)
  const { stdout } = result
  if (!stdout) return result
  return stdout
}

function getProjects(workerData: { scope: string }) {
  let pFinished = false
  const pArray: string[] = []
  projects.parsing(true)
  const p = subprocess.exec(`${workerData.scope}`)
  if (p.stdout) {
    p.stdout.pipe(process.stdout)
    p.stdout.on('data', (data: Buffer) => {
      const lines = data.toString().split('\n')
      const regexProjDir = new RegExp(`^((.+)\/([^\/]+))$`, 'm')
      const regexDone = new RegExp(`^(Done)(.*)$`, 'm')
      const resultProjDir = lines[0].match(regexProjDir)
      const resultDone = lines[0].match(regexDone)
      if (resultProjDir) {
        pArray.push(resultProjDir[0])
      }
      if (resultDone && resultDone[1] === 'Done') {
        pFinished = true
      }
    })
  }
  if (p.stderr) {
    p.stderr.on('data', (data) => {
      console.error(`${workerData.scope} | stderr: ${data}`)
    })
  }
  p.on('close', (code) => {
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    pArray.forEach(async (projDirAbs: string, index: number) => {
      await getProjectName(projDirAbs)
        .then(async (data) => {
          const newProj: Project = {
            projDirAbs: `${projDirAbs}`,
            projDirRel: `${path.relative(`${process.env.FLEX_PROJ_ROOT}`, projDirAbs)}`,
            projName: `${data.projName}`,
            contextName: `${data.contextName}`,
            imageName: `${data.imageName}`,
            jsonHCL: null,
          }
          projects.increment(`${data.projName}`, newProj)
          // console.log(toJS(projects.byKey(`${newProj.projName}`)))
          await makeJsonHCL(newProj)
          await makeGitlabInclude(newProj)
          await makeProjDockerfileTxt(newProj)
          await editGitlabInclude(newProj)
          await editProjDockerfileTxt(newProj)
          await removeProjDockerfileTxt(newProj)
        })
        .then(() => {
          if (projects.allSize === index + 1) {
            console.log('########################################################################')
            console.log('⚡️... Done Parsing Workspace Projects ...⚡️')
            console.log('########################################################################')
            projects.parsing(false)
          }
        })
    })
    if (pFinished) {
      console.log(`${workerData.scope} process exited with code ${code}`)
      projects.finished(true)
    }
  })
}

console.log('########################################################################')
console.log('⚡️... Getting things ready ...⚡️')
console.log('########################################################################')

setRoot()

const resultClean = await cleanGeneratedFolder()
console.log(`Result clean :`, resultClean)

const branch = await getGitBranch()
console.log(`Git branch :`, branch)

const resultEditEnvHCL = await editEnvHCL()
console.log(`Result edit env hcl :`, resultEditEnvHCL)

const resultEditEnv = await editEnv()
console.log(`Result edit env :`, resultEditEnv)

getProjects(workerData)

autorun(async () => {
  if (projects.isFinished) {
    _targetsArray = []
    toJS(projects.all).forEach((item: Project) => {
      console.log(`Result item.imageName :`, `${item.imageName}`)
      return _targetsArray.push(`${item.imageName}`)
    })

    const resultExport = await exportJsonHCL()
    console.log(`Result JSON exported :`, resultExport)

    const resultAnsibleUpdateBuildHCL = await ansibleUpdateBuildHCL()
    console.log(`Result ansibleUpdateBuildHCL :`, resultAnsibleUpdateBuildHCL)

    const resultTestBake = await testBake()
    console.log(`Result JSON exported :`, resultTestBake)

    console.log('########################################################################')
    console.log('⚡️... All done but wait !! ...⚡️')
    console.log(
      "\
    There is a discrepancy between 'docker buildx bake' and 'docker buildx build' commands'\n\
    It sucks, but dockerfiles cannot receive 'build-args' and the 'build-context' is lost\n\
    when 'docker buildx build' command is invoked using docker compose in gitlab-ci ...'\n\
    Unfortuatley we cannot use 'docker buildx bake' syntax in these particular cases because we\n\
    really need the 'add-host' functionality provided by docker compose. For example the nextjs \n\
    gateway ssr build process requires mfe remote projects to run at compile time. These must run\n\
    on the production domain name not just localhost. To cut to the chase, you have to manually\n\
    configure 3 files as a monkey patch, when you are switching git branches:\n\
      - /pipeline-ci/docker/v2/dockerfiles/5b_build_ssr.dockerfile\n\
      - /pipeline-ci/docker/v2/dockerfiles/6b_test_e2e.dockerfile\n\
      - /pipeline-ci/docker/v2/dockerfiles/7b_final.dockerfile\n\
    You gotta manaully specify the registry url for the --from tags. Just passing the env variable\n\
    $FLEX_IMAGE_REPOSITORY won't work... Weird but voilà everthings works\n\
    \n\
    ¯\\_(ツ)_/¯",
    )
    console.log('########################################################################')
  } else {
    console.log('########################################################################')
    console.log('⚡️... Parsing Workspace Projects ...⚡️')
    console.log('########################################################################')
  }
})
