/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */
exports.handler = async (event, context) => {
  if (event.triggerSource === "CustomMessage_SignUp") {
    const emailMessage = `
    <div style="text-align: center;">
    <span style="font-weight: bold;">Merci de finaliser votre inscription</span>
    </div>
    <br/>
    <div style="text-align: center;">
      🚀 Votre code de vérification : ${event.request.codeParameter}
    </div>`
    const smsMessage = `Merci de finaliser votre inscription. 🚀 Votre code de vérification : ${event.request.codeParameter}`;
    event.response.emailMessage = emailMessage;
    event.response.smsMessage = smsMessage;
    event.response.emailSubject = "Bienvenue sur l'application APE | la source";
  }

  return event;
};
